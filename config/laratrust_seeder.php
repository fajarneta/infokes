<?php

return [
    'list_role' => ['superadministrator','pemda','dinkes','pegawai','pengguna'],
    'role_structure' => [
        'superadministrator' => [
                 'card-admin'=>'r',
            'acl-menu' => 'r',
                'users' => 'r',
                'permissions' => 'r',
                'menus' => 'r',
            'master-menu' => 'r',

                'jabatan' => 'r',
                'unit-kerja' => 'r',
             
                'golongan-darah' => 'r',
                'agama' => 'r',
                'jenis-kelamin' => 'r',
                'profesi' => 'r',
                'status-pernikahan' => 'r',
                'status-pegawai' => 'r',
                'kategori-kasus' => 'r',
                'sub-kategori-kasus' => 'r',
                'wilayah-menu' => 'r',
                    'provinsi' => 'r',
                    'kabupaten' => 'r',
                    'kecamatan' => 'r',
                    'kelurahan' => 'r',
                    'map' => 'r',
       
                'roles' => 'r',
                'config-ids' => 'r',
                'perusahaan-menu'=>'r',

                'penduduk-menu' => 'r',
               
                    'penduduk' => 'r',
                'pegawai-menu' => 'r',
                    'pegawai' => 'r',
                    'tambah-pegawai' => 'r',
                'rekam-kasus-menu' => 'r',
                        'rekam-kasus' => 'r',
                'laporan-menu' => 'r', 
                        'laporan-rekam-kasus' => 'r',

        ],

        'pemda' => [
            'penduduk' => 'r',
           'laporan-menu' => 'r', 
                   'laporan-rekam-kasus' => 'r',

   ],
   'dinkes' => [
    'master-menu' => 'r',

                'jabatan' => 'r',
                'unit-kerja' => 'r',
             
                'golongan-darah' => 'r',
                'agama' => 'r',
                'jenis-kelamin' => 'r',
                'profesi' => 'r',
                'status-pernikahan' => 'r',
                'status-pegawai' => 'r',
                'kategori-kasus' => 'r',
                'sub-kategori-kasus' => 'r',
                'wilayah-menu' => 'r',
                    'provinsi' => 'r',
                    'kabupaten' => 'r',
                    'kecamatan' => 'r',
                    'kelurahan' => 'r',
                    'map' => 'r',
       
                'roles' => 'r',
                'config-ids' => 'r',
                'perusahaan-menu'=>'r',

                'penduduk-menu' => 'r',
               
                    'penduduk' => 'r',
                'pegawai-menu' => 'r',
                    'pegawai' => 'r',
                    'tambah-pegawai' => 'r',
                'rekam-kasus-menu' => 'r',
                        'rekam-kasus' => 'r',
                'laporan-menu' => 'r', 
                        'laporan-rekam-kasus' => 'r',
],
   'pegawai' => [
    'penduduk-menu' => 'r',
               
    'penduduk' => 'r',
    'rekam-kasus-menu' => 'r',
    'rekam-kasus' => 'r',

],

       
    ],
    'permissions_map' => [
        'r' => 'read'
    ]
   
];
