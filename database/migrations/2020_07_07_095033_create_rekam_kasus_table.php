<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRekamKasusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rekam_kasus', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('id_penduduk')->nullable();
            $table->unsignedInteger('id_sub_kategori_kasus')->nullable();
            $table->datetime('tanggal')->nullable();
            $table->text('riwayat_perjalanan')->nullable();
            $table->text('diagnosa')->nullable();
            $table->text('hasil')->nullable();
            $table->text('keterangan')->nullable();
            $table->unsignedInteger('user_input')->nullable();
            $table->unsignedInteger('user_update')->nullable();
            $table->timestamps();
            $table->foreign('id_penduduk')->references('id')->on('penduduk')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_sub_kategori_kasus')->references('id')->on('sub_kategori_kasus')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('user_input')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('user_update')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rekam_kasus');
    }
}
