<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePegawaiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pegawai', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nip',100)->nullable();
            $table->unsignedInteger('id_penduduk')->nullable();
            $table->unsignedInteger('id_jabatan')->nullable();
            $table->unsignedInteger('id_unit_kerja')->nullable();
            $table->unsignedInteger('id_status_pegawai')->nullable();
            $table->string('flag_aktif',1)->default('Y')->nullable();
            $table->date('tgl_bergabung')->nullable();
            $table->timestamps();

        
            $table->foreign('id_penduduk')->references('id')->on('penduduk')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_jabatan')->references('id')->on('jabatan')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_unit_kerja')->references('id')->on('unit_kerja')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_status_pegawai')->references('id')->on('status_pegawai')->onUpdate('cascade')->onDelete('cascade');
       
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pegawai');
    }
}
