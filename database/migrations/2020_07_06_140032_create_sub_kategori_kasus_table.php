<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubKategoriKasusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sub_kategori_kasus', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('id_kategori_kasus')->nullable();
            $table->string('sub_kategori_kasus',100)->nullable();

            $table->foreign('id_kategori_kasus')->references('id')->on('kategori_kasus')->onUpdate('cascade')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sub_kategori_kasus');
    }
}
