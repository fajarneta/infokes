<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePendudukTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('penduduk', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama_lengkap',100)->nullable();
            $table->string('nik',20)->nullable();
            $table->string('alamat_ktp',100)->nullable();
            $table->unsignedInteger('id_kelurahan_ktp')->nullable();
            $table->string('alamat_domisili',100)->nullable();
            $table->unsignedInteger('id_kelurahan_domisili')->nullable();
            $table->string('tempat_lahir',100)->nullable();
            $table->date('tanggal_lahir')->nullable();
            $table->Integer('usia')->nullable();
            $table->string('no_hp',15)->nullable();
            $table->string('foto',100)->nullable();
            $table->unsignedInteger('id_golongan_darah')->nullable();
            $table->unsignedInteger('id_jenis_kelamin')->nullable();
            $table->unsignedInteger('id_agama')->nullable();
            $table->unsignedInteger('id_status_pernikahan')->nullable();
            $table->unsignedInteger('id_profesi')->nullable();
        
            $table->timestamps();

            $table->foreign('id_kelurahan_ktp')->references('id')->on('kelurahan')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_kelurahan_domisili')->references('id')->on('kelurahan')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_golongan_darah')->references('id')->on('golongan_darah')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_agama')->references('id')->on('agama')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_jenis_kelamin')->references('id')->on('jenis_kelamin')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_status_pernikahan')->references('id')->on('status_pernikahan')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_profesi')->references('id')->on('profesi')->onUpdate('cascade')->onDelete('cascade');
         
       
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('penduduk');
    }
}
