<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\Menu;
use App\Permission;
class MenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $this->command->info('Delete semua tabel menu');
      Model::unguard();
      Menu::truncate();
      $this->menuAcl();
      $this->menuMaster();
      $this->menuPenduduk();
      $this->menuPegawai();
      $this->menuRekamKasus();
      $this->menuLaporan();
  

      // $this->command->info('Menu Seeder Complete');

      // diluar redmine, ditaruh paling bawah
      // $this->menuRecruitment();
    }

    private function menuAcl(){
      $this->command->info('Menu ACL Seeder');
      $permission = Permission::firstOrNew(array(
        'name'=>'read-acl-menu'
      ));
      $permission->display_name = 'Read ACL Menus';
      $permission->save();
      $menu = Menu::firstOrNew(array(
        'name'=>'ACL',
        'permission_id'=>$permission->id,
        'ordinal'=>1,
        'parent_status'=>'Y'
      ));
      $menu->icon = 'md-settings';
      $menu->save();

          //create SUBMENU master
         $permission = Permission::firstOrNew(array(
           'name'=>'read-users',
         ));
         $permission->display_name = 'Read Users';
         $permission->save();

         $submenu = Menu::firstOrNew(array(
           'name'=>'Users',
           'parent_id'=>$menu->id,
           'permission_id'=>$permission->id,
           'ordinal'=>2,
           'parent_status'=>'N',
           'url'=>'user',
           )
         );
         $submenu->save();

         $permission = Permission::firstOrNew(array(
           'name'=>'read-permissions',
         ));
         $permission->display_name = 'Read Permissions';
         $permission->save();

         $submenu = Menu::firstOrNew(array(
           'name'=>'Permissions',
           'parent_id'=>$menu->id,
           'permission_id'=>$permission->id,
           'ordinal'=>2,
           'parent_status'=>'N',
           'url'=>'permission',
           )
         );
         $submenu->save();

         $permission = Permission::firstOrNew(array(
           'name'=>'read-menus',
         ));
         $permission->display_name = 'Read Menus';
         $permission->save();

         $submenu = Menu::firstOrNew(array(
           'name'=>'Menus',
           'parent_id'=>$menu->id,
           'permission_id'=>$permission->id,
           'ordinal'=>2,
           'parent_status'=>'N',
           'url'=>'menu',
           )
         );
         $submenu->save();



        $permission = Permission::firstOrNew(array(
          'name' => 'read-roles',
));
$permission->display_name = 'Read Roles';
$permission->save();

$submenu = Menu::firstOrNew(array(
          'name' => 'Roles',
          'parent_id' => $menu->id,
          'permission_id' => $permission->id,
          'ordinal' => 2,
          'parent_status' => 'N',
          'url' => 'role',
              )
);
$submenu->save();
    }

    private function menuMaster(){
      $this->command->info('Menu Master Seeder');
      $permission = Permission::firstOrNew(array(
        'name'=>'read-master-menu'
      ));
      $permission->display_name = 'Read Master Menus';
      $permission->save();
      $menu = Menu::firstOrNew(array(
        'name'=>'Master Data',
        'permission_id'=>$permission->id,
        'ordinal'=>1,
        'parent_status'=>'Y'
      ));
      $menu->icon = 'md-folder';
      $menu->save();

          //create SUBMENU master
        // new
       
          $permission = Permission::firstOrNew(array(
            'name'=>'read-jabatan',
          ));
          $permission->display_name = 'Read Jabatan';
          $permission->save();

          $submenu = Menu::firstOrNew(array(
            'name'=>'Jabatan',
            'parent_id'=>$menu->id,
            'permission_id'=>$permission->id,
            'ordinal'=>2,
            'parent_status'=>'N',
            'url'=>'jabatan',
            )
          );
          $submenu->save();


          $permission = Permission::firstOrNew(array(
            'name'=>'read-unit-kerja',
          ));
          $permission->display_name = 'Read Unit Kerja';
          $permission->save();

          $submenu = Menu::firstOrNew(array(
            'name'=>'Unit Kerja',
            'parent_id'=>$menu->id,
            'permission_id'=>$permission->id,
            'ordinal'=>2,
            'parent_status'=>'N',
            'url'=>'unit-kerja',
            )
          );
          $submenu->save();
          $permission = Permission::firstOrNew(array(
            'name'=>'read-status-pernikahan',
          ));
          $permission->display_name = 'Read Status Pernikahan';
          $permission->save();

          $submenu = Menu::firstOrNew(array(
            'name'=>'Status Pernikahan',
            'parent_id'=>$menu->id,
            'permission_id'=>$permission->id,
            'ordinal'=>2,
            'parent_status'=>'N',
            'url'=>'status-pernikahan',
            )
          );
          $submenu->save();

          $permission = Permission::firstOrNew(array(
            'name'=>'read-golongan-darah',
          ));
          $permission->display_name = 'Read Golongan Darah';
          $permission->save();

          $submenu = Menu::firstOrNew(array(
            'name'=>'Golongan Darah',
            'parent_id'=>$menu->id,
            'permission_id'=>$permission->id,
            'ordinal'=>2,
            'parent_status'=>'N',
            'url'=>'golongan-darah',
            )
          );
          $submenu->save();

          $permission = Permission::firstOrNew(array(
            'name'=>'read-profesi',
          ));
          $permission->display_name = 'Read Profesi';
          $permission->save();

          $submenu = Menu::firstOrNew(array(
            'name'=>'Profesi',
            'parent_id'=>$menu->id,
            'permission_id'=>$permission->id,
            'ordinal'=>2,
            'parent_status'=>'N',
            'url'=>'profesi',
            )
          );
          $submenu->save();

          $permission = Permission::firstOrNew(array(
            'name'=>'read-status-pegawai',
          ));
          $permission->display_name = 'Read Status Pegawai';
          $permission->save();

          $submenu = Menu::firstOrNew(array(
            'name'=>'Status Pegawai',
            'parent_id'=>$menu->id,
            'permission_id'=>$permission->id,
            'ordinal'=>2,
            'parent_status'=>'N',
            'url'=>'status-pegawai',
            )
          );
          $submenu->save();

          $permission = Permission::firstOrNew(array(
            'name'=>'read-kategori-kasus',
          ));
          $permission->display_name = 'Read Kategori Kasus';
          $permission->save();

          $submenu = Menu::firstOrNew(array(
            'name'=>'Kategori Kasus',
            'parent_id'=>$menu->id,
            'permission_id'=>$permission->id,
            'ordinal'=>2,
            'parent_status'=>'N',
            'url'=>'kategori-kasus',
            )
          );
          $submenu->save();
          
          $permission = Permission::firstOrNew(array(
            'name'=>'read-jenis-kelamin',
          ));
          $permission->display_name = 'Read Jenis Kelamin';
          $permission->save();

          $submenu = Menu::firstOrNew(array(
            'name'=>'Jenis Kelamin',
            'parent_id'=>$menu->id,
            'permission_id'=>$permission->id,
            'ordinal'=>2,
            'parent_status'=>'N',
            'url'=>'jenis-kelamin',
            )
          );
          $submenu->save();


          $permission = Permission::firstOrNew(array(
            'name'=>'read-agama',
          ));
          $permission->display_name = 'Read Agama';
          $permission->save();

          $submenu = Menu::firstOrNew(array(
            'name'=>'Agama',
            'parent_id'=>$menu->id,
            'permission_id'=>$permission->id,
            'ordinal'=>2,
            'parent_status'=>'N',
            'url'=>'agama',
            )
          );
          $submenu->save();


          $permission = Permission::firstOrNew(array(
            'name'=>'read-status-pernikahan',
          ));
          $permission->display_name = 'Read Status Pernikahan';
          $permission->save();

          $submenu = Menu::firstOrNew(array(
            'name'=>'Status Pernikahan',
            'parent_id'=>$menu->id,
            'permission_id'=>$permission->id,
            'ordinal'=>2,
            'parent_status'=>'N',
            'url'=>'status-pernikahan',
            )
          );
          $submenu->save();

          $permission = Permission::firstOrNew(array(
            'name'=>'read-wilayah-menu',
          ));
             $permission->display_name = 'Read Menu Wilayah';
             $permission->save();

             $submenu = Menu::firstOrNew(
               array(
              'name'=>'Wilayah',
              'parent_id'=>$menu->id,
              'permission_id'=>$permission->id,
              'ordinal'=>2,
              'parent_status'=>'Y',
            )
          );
             $submenu->save();
             $permission = Permission::firstOrNew(array(
              'name'=>'read-provinsi',
            ));
             $permission->display_name = 'Read Provinsi';
             $permission->save();

             $subsubmenu = Menu::firstOrNew(
               array(
              'name'=>'Provinsi',
              'parent_id'=>$submenu->id,
              'permission_id'=>$permission->id,
              'ordinal'=>3,
              'parent_status'=>'N',
              'url'=>'provinsi',
            )
          );
             $subsubmenu->save();

             $permission = Permission::firstOrNew(array(
            'name'=>'read-kabupaten',
          ));
             $permission->display_name = 'Read Kabupaten';
             $permission->save();

             $subsubmenu = Menu::firstOrNew(
               array(
              'name'=>'Kabupaten',
              'parent_id'=>$submenu->id,
              'permission_id'=>$permission->id,
              'ordinal'=>3,
              'parent_status'=>'N',
              'url'=>'kabupaten',
            )
          );
             $subsubmenu->save();

             $permission = Permission::firstOrNew(array(
            'name'=>'read-kecamatan',
          ));
             $permission->display_name = 'Read Kecamatan';
             $permission->save();

             $subsubmenu = Menu::firstOrNew(
               array(
              'name'=>'Kecamatan',
              'parent_id'=>$submenu->id,
              'permission_id'=>$permission->id,
              'ordinal'=>3,
              'parent_status'=>'N',
              'url'=>'kecamatan',
            )
          );
             $subsubmenu->save();

             $permission = Permission::firstOrNew(array(
            'name'=>'read-kelurahan',
          ));
             $permission->display_name = 'Read Kelurahan';
             $permission->save();

             $subsubmenu = Menu::firstOrNew(
               array(
              'name'=>'Kelurahan',
              'parent_id'=>$submenu->id,
              'permission_id'=>$permission->id,
              'ordinal'=>3,
              'parent_status'=>'N',
              'url'=>'kelurahan',
            )
          );
          $subsubmenu->save();


        }
        private function menuPenduduk(){
          $this->command->info('Menu Penduduk Seeder');
          $permission = Permission::firstOrNew(array(
            'name'=>'read-penduduk-menu'
          ));
          $permission->display_name = 'Read Penduduk Menus';
          $permission->save();
          $menu = Menu::firstOrNew(array(
            'name'=>'Penduduk',
            'permission_id'=>$permission->id,
            'ordinal'=>1,
            'parent_status'=>'Y'
          ));
          $menu->icon = 'md-folder';
          $menu->save();
    
           
              $permission = Permission::firstOrNew(array(
                'name'=>'read-penduduk',
              ));
              $permission->display_name = 'Read penduduk';
              $permission->save();
    
              $submenu = Menu::firstOrNew(array(
                'name'=>'Data Penduduk',
                'parent_id'=>$menu->id,
                'permission_id'=>$permission->id,
                'ordinal'=>2,
                'parent_status'=>'N',
                'url'=>'penduduk',
                )
              );
              $submenu->save();
    
    
    
            }
        private function menuPegawai(){
          $this->command->info('Menu Pegawai Seeder');
          $permission = Permission::firstOrNew(array(
            'name'=>'read-pegawai-menu'
          ));
          $permission->display_name = 'Read Pegawai Menus';
          $permission->save();
          $menu = Menu::firstOrNew(array(
            'name'=>'Pegawai',
            'permission_id'=>$permission->id,
            'ordinal'=>1,
            'parent_status'=>'Y'
          ));
          $menu->icon = 'md-folder';
          $menu->save();
    
           
              $permission = Permission::firstOrNew(array(
                'name'=>'read-pegawai',
              ));
              $permission->display_name = 'Read pegawai';
              $permission->save();
    
              $submenu = Menu::firstOrNew(array(
                'name'=>'Data Pegawai',
                'parent_id'=>$menu->id,
                'permission_id'=>$permission->id,
                'ordinal'=>2,
                'parent_status'=>'N',
                'url'=>'pegawai',
                )
              );
              $submenu->save();
    
    
    
            }

            private function menuRekamKasus(){
              $this->command->info('Menu Rekam Kasus Seeder');
              $permission = Permission::firstOrNew(array(
                'name'=>'read-rekam-kasus-menu'
              ));
              $permission->display_name = 'Read Rekam Kasus Menus';
              $permission->save();
              $menu = Menu::firstOrNew(array(
                'name'=>'Rekam Kasus',
                'permission_id'=>$permission->id,
                'ordinal'=>1,
                'parent_status'=>'Y'
              ));
              $menu->icon = 'md-folder';
              $menu->save();
        
               
                  $permission = Permission::firstOrNew(array(
                    'name'=>'read-rekam-kasus',
                  ));
                  $permission->display_name = 'Read Rekam Kasus';
                  $permission->save();
        
                  $submenu = Menu::firstOrNew(array(
                    'name'=>'Data Kasus',
                    'parent_id'=>$menu->id,
                    'permission_id'=>$permission->id,
                    'ordinal'=>2,
                    'parent_status'=>'N',
                    'url'=>'rekam-kasus',
                    )
                  );
                  $submenu->save();
        
        
        
                }

                private function menuLaporan(){
                  $this->command->info('Menu Laporan Seeder');
                  $permission = Permission::firstOrNew(array(
                    'name'=>'read-laporan-menu'
                  ));
                  $permission->display_name = 'Read Laporan Menus';
                  $permission->save();
                  $menu = Menu::firstOrNew(array(
                    'name'=>'Laporan',
                    'permission_id'=>$permission->id,
                    'ordinal'=>1,
                    'parent_status'=>'Y'
                  ));
                  $menu->icon = 'md-folder';
                  $menu->save();
            
                   
                      $permission = Permission::firstOrNew(array(
                        'name'=>'read-laporan-rekam-kasus',
                      ));
                      $permission->display_name = 'Read Laporan Rekam Kasus';
                      $permission->save();
            
                      $submenu = Menu::firstOrNew(array(
                        'name'=>'Laporan Rekam Kasus',
                        'parent_id'=>$menu->id,
                        'permission_id'=>$permission->id,
                        'ordinal'=>2,
                        'parent_status'=>'N',
                        'url'=>'laporan-rekam-kasus',
                        )
                      );
                      $submenu->save();
            
            
            
                    }

}
