<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \Artisan::call('config:cache');
        $this->call(LaratrustSeeder::class);
        $this->call(MenuSeeder::class);
        $this->call(ConfigIdsSeeder::class);
        $this->call(MasterDataSeeder::class);
        $this->call(ContentSeeder::class);

    }
}
