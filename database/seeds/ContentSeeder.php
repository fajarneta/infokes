<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\Provinsi;
use App\Models\Kabupaten;
use App\Models\Kecamatan;
use App\Models\Kelurahan;
use App\Models\Map;

use App\Models\User;
use App\Permission;

class ContentSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
      $this->importWilayahWonogiriJson();

    }

  
    private function importWilayahWonogiriJson(){
      $this->command->info("Hapus MAP");
      DB::table('map')->delete();
      $fileName = 'data/wilayah-wonogiri.json';
      $this->command->info("Seeding Map");
      $json              = file_get_contents('data/wilayah-wonogiri.json');
      $json = preg_replace('/\[\n *(\d*.\d*)\,\n *([-+]\d*\.\d*)\,\n *0\n *]/', '["\1","\2",0]', $json);
      $data = json_decode($json, true);
      $data              = $data['features'];
      
      $keys              = 0;
      $migrasi_kelurahan = [];
      $bar=$this->command->getOutput()->createProgressBar(count($data));
      foreach ($data as $keyz => $valuez) {
        $no             = 1;
        $kode           = $data[$keyz]['geometry']['coordinates'][0][0];
        $no_prov        = $data[$keyz]['properties']['PROVNO'];
        $no_kab         = $data[$keyz]['properties']['KABKOTNO'];
        $no_kec         = $data[$keyz]['properties']['KECNO'];
        $no_des         = $data[$keyz]['properties']['DESANO'];
        
        $provinsi       = $data[$keyz]['properties']['PROVINSI'];
        $kabkot         = $data[$keyz]['properties']['KABKOT'];
        $kecamatan      = $data[$keyz]['properties']['KECAMATAN'];
        $desa           = $data[$keyz]['properties']['DESA'];
        
        
        // $no_kec         = substr($no_kec,0,2);
        $nodesa         = $no_prov.'.'.$no_kab.'.'.$no_kec.'.'.$no_des;
        
        
        if(isset($provinsi)){
          $dataProvinsi = Provinsi::firstOrNew(array(
            
            'provinsi'=>ucwords(strtolower($provinsi)),
            'kode'=>$no_prov,
          ));
          $dataProvinsi->flag_aktif='Y';
          $dataProvinsi->save();

          if(isset($kabkot)){
            $dataKabupaten = Kabupaten::firstOrNew(array(
              
              'kabupaten'=>ucwords(strtolower($kabkot)),
              'kode'=>$no_kab,
            ));
            $dataKabupaten->flag_aktif='Y';
            $dataKabupaten->id_provinsi=$dataProvinsi->id;
            $dataKabupaten->save();

            if(isset($kecamatan)){
              $dataKecamatan = Kecamatan::firstOrNew(array(
                
                'kecamatan'=>ucwords(strtolower($kecamatan)),
                'kode'=>$no_kec,
              ));
              $dataKecamatan->flag_aktif='Y';
              $dataKecamatan->id_kabupaten=$dataKabupaten->id;
              $dataKecamatan->save();

              if(isset($desa)){
                $dataKelurahan = Kelurahan::firstOrNew(array(
                  
                  'kelurahan'=>ucwords(strtolower($desa)),
                  'kode'=>$nodesa,
                ));
                $dataKelurahan->flag_aktif='Y';
                $dataKelurahan->id_kecamatan=$dataKecamatan->id;
                $dataKelurahan->save();
              }
            }
          }
        }
        $bar->advance();
     
        foreach ($kode as $key => $value) {
            # code...
            $value['urutan']    = $no++;
            $value['latitude']  = $value[1];
            $value['longitude'] = $value[0];
            $value['nodesa']    = $nodesa;
            $value['provinsi']  = $nodesa;

            if ($value['latitude'] != 0 && $value['longitude'] !=0) {
                $data_insert = [
                    'desano'    => $value['nodesa'],
                    'provinsi'  => $provinsi,
                    'kabkot'    => $kabkot,
                    'kecamatan' => $kecamatan,
                    'desa'      => $desa,
                    'urutan'    => $value['urutan'],
                    'longitude' => $value['longitude'],
                    'latitude'  => $value['latitude'],

                ];

            
                $migrasi_kelurahan = Map::firstOrNew(array(
                  
                  'longitude' => $value['longitude'],
                  'latitude'  => $value['latitude'],
                  'id_kelurahan'  => $dataKelurahan->id,
                ));
                $migrasi_kelurahan->save();
            }
        }
    }
   
 
    $bar->finish();
      echo "\n\n";
    }

}
