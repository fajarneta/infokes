<?php

use Illuminate\Database\Seeder;
use App\Models\ConfigId;
class ConfigIdsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->configId();
    }
    private function configId()
    {
       $this->command->info("Hapus Config IDS");
       DB::table('config_ids')->delete();

       $this->command->info("Simpan Config IDS");
       $data=array(
   
        [
          'table_source'=>'roles',
          'config_name'=>'ROLE_SUPERADMINISTRATOR',
          'config_value'=>1,
          'description'=>'Role untuk Superadministrator',
        ],
        [
          'table_source'=>'roles',
          'config_name'=>'ROLE_ADMINISTRATOR',
          'config_value'=>2,
          'description'=>'Role untuk Administrator',
        ],
        [
          'table_source'=>'roles',
          'config_name'=>'ROLE_MANAJER',
          'config_value'=>3,
          'description'=>'Role untuk Manajer',
        ],
        [
          'table_source'=>'roles',
          'config_name'=>'ROLE_PEGAWAI',
          'config_value'=>4,
          'description'=>'Role untuk Pegawai',
        ],
        [
          'table_source'=>'roles',
          'config_name'=>'ROLE_PENGGUNA',
          'config_value'=>5,
          'description'=>'Role untuk Pengguna',
        ],
        [
          'table_source'=>'kategori_kasus',
          'config_name'=>'KATEGORI_KASUS_POSITIF',
          'config_value'=>1,
          'description'=>'Kategori Kasus Positif',
        ],
        [
          'table_source'=>'kategori_kasus',
          'config_name'=>'KATEGORI_KASUS_PDP',
          'config_value'=>2,
          'description'=>'Kategori Kasus Pdp',
        ],  [
          'table_source'=>'kategori_kasus',
          'config_name'=>'KATEGORI_KASUS_ODP',
          'config_value'=>3,
          'description'=>'Kategori Kasus Odp',
        ],
     
       );

       if(DB::table('config_ids')->get()->count() == 0){
          $bar=$this->command->getOutput()->createProgressBar(count($data));
          foreach($data as $a){
              ConfigId::create($a);
              $bar->advance();
          }
          $bar->finish();
      }
   }
}
