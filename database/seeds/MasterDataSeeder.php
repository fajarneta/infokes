<?php

use Illuminate\Database\Seeder;

use App\Models\Agama;
use App\Models\StatusPernikahan;
use App\Models\GolonganDarah;
use App\Models\JenisKelamin;
use App\Models\Profesi;
use App\Models\Provinsi;
use App\Models\Kabupaten;
use App\Models\Kecamatan;
use App\Models\Kelurahan;
use App\Models\Perusahaan;
use App\Models\KategoriKasus;
use App\Models\SubKategoriKasus;
class MasterDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->MasterAgama();
        $this->MasterStatusPernikahan();
        $this->MasterGolonganDarah();
        $this->MasterJenisKelamin();
        $this->MasterProfesi();
        // $this->importProvinsi();
        // $this->importKabupaten();
        // $this->importKecamatan();
        // $this->importKelurahan();
        $this->masterPerusahaan();
        $this->masterKategoriKasus();
        $this->masterSubKategoriKasus();
     
        
    }

    private function MasterAgama()
    {
        $this->command->info("Hapus Agama");
        DB::table('agama')->delete();
        $fileName = 'data/master/agama/agama.xlsx';
        $this->command->info("Seeding Agama");
        \Excel::load($fileName,function($reader){
            // $reader->dump();
          $reader->each(function($row){
            $bar = $this->command->getOutput()->createProgressBar($row->count());
            $row->each(function($agama) use ($bar){
              if(isset($agama['id'])){
                          // die("hasil = ".$row->count());
                $data = Agama::firstOrNew(array(
                  'id'=>$agama['id']
                ));
                
                $data->agama=$agama['agama'];
                $data->save();
              }
              $bar->advance();
            });
            $bar->finish();
  
          });
        });
        echo "\n\n";
    }

    private function MasterStatusPernikahan()
    {
        $this->command->info("Hapus Status Pernikahan");
        DB::table('status_pernikahan')->delete();
        $fileName = 'data/master/status_pernikahan/status_pernikahan.xlsx';
        $this->command->info("Seeding Status Pernikahan");
        \Excel::load($fileName,function($reader){
            // $reader->dump();
          $reader->each(function($row){
            $bar = $this->command->getOutput()->createProgressBar($row->count());
            $row->each(function($sp) use ($bar){
              if(isset($sp['id'])){
                          // die("hasil = ".$row->count());
                $data = StatusPernikahan::firstOrNew(array(
                  'id'=>$sp['id']
                ));
                
                $data->status_pernikahan=$sp['status_pernikahan'];
                $data->save();
              }
              $bar->advance();
            });
            $bar->finish();
  
          });
        });
        echo "\n\n";
    }
       private function MasterGolonganDarah()
      {
        $this->command->info("Hapus Golongan Darah");
        DB::table('golongan_darah')->delete();
        $fileName = 'data/master/golongan_darah/golongan_darah.xlsx';
        $this->command->info("Seeding Golongan Darah");
        // $reader->dump();
        \Excel::load($fileName,function($reader){
          $reader->each(function($row){
            $bar = $this->command->getOutput()->createProgressBar($row->count());
            $row->each(function($gd) use ($bar){
              if(isset($gd['id'])){
                          // die("hasil = ".$row->count());
                $data = GolonganDarah::firstOrNew(array(
                  'id'=>$gd['id']
                ));
                
                $data->golongan_darah=$gd['golongan_darah'];
                $data->save();
              }
              $bar->advance();
            });
            $bar->finish();

          });
        });
        echo "\n\n";
      }

       private function MasterJenisKelamin()
      {
        $this->command->info("Hapus Jenis Kelamin");
        DB::table('jenis_kelamin')->delete();
        $fileName = 'data/master/jenis_kelamin/jenis_kelamin.xlsx';
        $this->command->info("Seeding Jenis Kelamin");
        // $reader->dump();
        \Excel::load($fileName,function($reader){
          $reader->each(function($row){
            $bar = $this->command->getOutput()->createProgressBar($row->count());
            $row->each(function($gd) use ($bar){
              if(isset($gd['id'])){
                          // die("hasil = ".$row->count());
                $data = JenisKelamin::firstOrNew(array(
                  'id'=>$gd['id']
                ));
                
                $data->jenis_kelamin=$gd['jenis_kelamin'];
                $data->save();
              }
              $bar->advance();
            });
            $bar->finish();

          });
        });
        echo "\n\n";
      }

      private function MasterProfesi()
      {
        $this->command->info("Hapus Profesi");
        DB::table('profesi')->delete();
        $fileName = 'data/master/profesi/profesi.xlsx';
        $this->command->info("Seeding Profesi");
        // $reader->dump();
        \Excel::load($fileName,function($reader){
          $reader->each(function($row){
            $bar = $this->command->getOutput()->createProgressBar($row->count());
            $row->each(function($gd) use ($bar){
              if(isset($gd['id'])){
                          // die("hasil = ".$row->count());
                $data = Profesi::firstOrNew(array(
                  'id'=>$gd['id']
                ));
                
                $data->profesi=$gd['profesi'];
                $data->save();
              }
              $bar->advance();
            });
            $bar->finish();

          });
        });
        echo "\n\n";
      }


    private function importProvinsi(){
      $this->command->info("Hapus Provinsi");
      DB::table('provinsi')->delete();
      $fileName = 'data/master/wilayah/provinsi.xls';
      $this->command->info("Seeding Provinsi");
      \Excel::load($fileName,function($reader){
        // $reader->dump();
        $reader->each(function($row){
          $bar = $this->command->getOutput()->createProgressBar($row->count());
          // die("hasil = ".$row->count());

          $row->each(function($provinsi) use ($bar){
            // echo ($provinsi['kode']."\n");


            if(isset($provinsi['id'])){
              $data = Provinsi::firstOrNew(array(
                'id'=>$provinsi['id'],
                'kode'=>$provinsi['kd_provinsi']
              ));
              $data->provinsi=$provinsi['provinsi'];
              $data->flag_aktif='Y';
              $data->save();
            }
            $bar->advance();
          });
          $bar->finish();

        });
      });
      echo "\n\n";
    }

    private function importKabupaten(){
      $this->command->info("Hapus Kabupaten");
      DB::table('kabupaten')->delete();
      $fileName = 'data/master/wilayah/kabupaten.xls';
      $this->command->info("Seeding Kabupaten");
      \Excel::load($fileName,function($reader){
        // $reader->dump();
        $reader->each(function($row){
          $bar = $this->command->getOutput()->createProgressBar($row->count());
          $row->each(function($kabupaten) use ($bar){
            // echo ($kabupaten['kode']."\n");
            if(isset($kabupaten['id'])){

              $data = Kabupaten::firstOrNew(array(
                'kode'=>$kabupaten['kd_kabupaten'],
              'id'=>$kabupaten['id']

              ));
              $data->id_provinsi=$kabupaten['id_provinsi'];
              $data->kabupaten=$kabupaten['kabupaten'];
              $data->flag_aktif='Y';
              $data->save();

            }
            $bar->advance();
          });
          $bar->finish();
        });
      });
      echo "\n\n";
    }

    private function importKecamatan(){
      $this->command->info("Hapus Kecamatan");
      DB::table('kecamatan')->delete();
      $fileName = 'data/master/wilayah/kecamatan.xls';
      $this->command->info("Seeding Kecamatan");
      \Excel::load($fileName,function($reader){
        $reader->each(function($row){
          $bar = $this->command->getOutput()->createProgressBar($row->count());
          $row->each(function($kecamatan) use ($bar){
            if(isset($kecamatan['id'])){

              $data = Kecamatan::firstOrNew(array(
                'kode'=>$kecamatan['kd_kecamatan'],
              'id'=>$kecamatan['id']

              ));
              $data->id_kabupaten=$kecamatan['id_kabupaten'];
              $data->kecamatan=$kecamatan['kecamatan'];
              $data->flag_aktif='Y';
              $data->save();

            }
            $bar->advance();
          });
          $bar->finish();
        });
      });
      echo "\n\n";
    }

    private function importKelurahan(){
      $this->command->info("Hapus Kelurahan");
      DB::table('kelurahan')->delete();
      $fileName = 'data/master/wilayah/kelurahan.xlsx';
      $this->command->info("Seeding kelurahan");
      \Excel::load($fileName,function($reader){
        $reader->each(function($row){
          $bar = $this->command->getOutput()->createProgressBar($row->count());
          $row->each(function($kelurahan) use ($bar){
            if(isset($kelurahan['id'])){

              $data = Kelurahan::firstOrNew(array(
                'kode'=>$kelurahan['kd_kelurahan'],
              'id'=>$kelurahan['id']

              ));
              $data->id_kecamatan=$kelurahan['id_kecamatan'];
              $data->kelurahan=$kelurahan['kelurahan'];
              $data->flag_aktif='Y';
              $data->kodepos=$kelurahan['kode_pos'];
              $data->save();

            }
            $bar->advance();
          });
          $bar->finish();
        });
      });
      echo "\n\n";
    }


    private function masterPerusahaan()
    {
       $this->command->info("Hapus Data Perusahaan");
       DB::table('perusahaan')->delete();

       $this->command->info("Simpan Data Perusahaan");
       $data=array(
        [
            'nama_perusahaan'=>'Infokes - Pemerintah Daerah Kkabupaten Wonogiri',
            'alamat'=>'Jl. Kabupaten No.4-6,  Wonogiri 57612',
            'website'=>'infokes.wonogirikab.go.id',
            'email'=>'infocorona@wonogirikab.go.id.com',
            'logo'=>null,
            'no_telp'=>'(0273) 321002',
            'fax'=>'322318',
       
          ],
    
       );

       if(DB::table('perusahaan')->get()->count() == 0){
          $bar=$this->command->getOutput()->createProgressBar(count($data));
          foreach($data as $a){
              Perusahaan::create($a);
              $bar->advance();
          }
          $bar->finish();
      }
   }

   private function masterKategoriKasus()
    {
       $this->command->info("Hapus Data Kategori Kasus");
       DB::table('kategori_kasus')->delete();

       $this->command->info("Simpan Data Kategori Kasus");
       $data=array(
        [
            'kategori_kasus'=>'Positif Covid-19',
            'keterangan'=>'Daftar Kasus Positif Covid-19',

          ],
          [
            'kategori_kasus'=>'Pasien Dalam Pengawasan (PDP)',
            'keterangan'=>'Daftar Pasien Dalam Pengawasan',

          ],
          [
            'kategori_kasus'=>'Orang Dalam Pemantauan (ODP)',
            'keterangan'=>'Daftar Orang Dalam Pemantauan',

          ],
    
       );

       if(DB::table('kategori_kasus')->get()->count() == 0){
          $bar=$this->command->getOutput()->createProgressBar(count($data));
          foreach($data as $a){
              KategoriKasus::create($a);
              $bar->advance();
          }
          $bar->finish();
      }
   }

   private function masterSubKategoriKasus()
    {
       $this->command->info("Hapus Data Sub Kategori Kasus");
       DB::table('sub_kategori_kasus')->delete();

       $this->command->info("Simpan Data Sub Kategori Kasus");
       $data=array(
        [
            'sub_kategori_kasus'=>'Dirawat',
            'id_kategori_kasus'=>1,

          ],
          [
            'sub_kategori_kasus'=>'Sembuh',
            'id_kategori_kasus'=>1,

          ],
          [
            'sub_kategori_kasus'=>'Meninggal',
            'id_kategori_kasus'=>1,

          ],
          [
            'sub_kategori_kasus'=>'Dirawat',
            'id_kategori_kasus'=>2,

          ],
          [
            'sub_kategori_kasus'=>'Sembuh Negatif Covid-10',
            'id_kategori_kasus'=>2,

          ],
          [
            'sub_kategori_kasus'=>'Meninggal',
            'id_kategori_kasus'=>2,

          ],
          [
            'sub_kategori_kasus'=>'Dalam Pemantauan',
            'id_kategori_kasus'=>3,

          ],
          [
            'sub_kategori_kasus'=>'Selesai Pemantauan',
            'id_kategori_kasus'=>3,

          ],
          [
            'sub_kategori_kasus'=>'Meninggal',
            'id_kategori_kasus'=>3,

          ],
          
    
       );

       if(DB::table('sub_kategori_kasus')->get()->count() == 0){
          $bar=$this->command->getOutput()->createProgressBar(count($data));
          foreach($data as $a){
              SubKategoriKasus::create($a);
              $bar->advance();
          }
          $bar->finish();
      }
   }

}
