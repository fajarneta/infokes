<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'WebsiteController@index')->name('home');
Route::get('/home', 'WebsiteController@index')->name('home');
Route::get('/peta-persebaran', 'WebsiteController@petaPersebaran');
Route::get('/info-corona', 'WebsiteController@infoCorona');
Route::get('/user/verify/{token}','Auth\RegisterController@verifyUser');
Route::get('register/create-resend/{user}',['as' => 'register.create_resend', 'uses' => 'Auth\RegisterController@createResend']);
Route::post('register/resend',['as' => 'register.resend', 'uses' => 'Auth\RegisterController@resend']);

Route::get('/send', 'SendMessageController@index')->name('send');
Route::get('/sendEmailNotifikasi', 'SendMessageController@sendEmailNotifikasi');
Route::post('/postMessage', 'SendMessageController@sendMessage')->name('postMessage');

Route::get('/admin', function () {
    // return view('welcome');
    if (Auth::check()) {
    
        return redirect('beranda');
    } else {
        

        return redirect('login');
    }
});

Auth::routes();


Route::get('/beranda', 'HomeController@index')->name('beranda');
Route::match(['get','post'],'home/get-notif', 'HomeController@getNotif');
Route::match(['get','post'],'home/update-notif', 'HomeController@updateNotif');
Route::match(['get','post'],'home/delete-notif', 'HomeController@deleteNotif');

Route::get('user/load-data','UserController@loadData');
Route::get('user/json','UserController@json');
Route::get('user/activate/{id}','UserController@activate');
Route::get('user/update/{id}','UserController@update');
Route::get('user/deactivate/{id}','UserController@deactivate');
Route::get('user/{id}/reset','UserController@reset');
Route::resource('user','UserController');
Route::delete('user/{id}/restore','UserController@restore');

Route::get('autocomplete/{method}','AutocompleteController@search');

Route::post('role/createpermissionrole', ['as' => 'role.createpermissionrole', 'uses' => 'RoleController@createpermissionrole']);

Route::get('role/load-data', 'RoleController@loadData');
Route::get('permission-role/get/{id}/menu', 'RoleController@hakmenus');
Route::get('role/permission-role/get/{id}/menu', 'RoleController@hakmenus');
Route::resource('role', 'RoleController');
Route::delete('role/{id}/restore', 'RoleController@restore');

Route::get('permission-role/load-data', 'PermissionRoleController@loadData');
Route::resource('permission-role', 'PermissionRoleController');
Route::delete('permission-role/{id}/restore', 'PermissionRoleController@restore');

Route::get('menu/load-data','MenuController@loadData');
Route::resource('menu','MenuController');
Route::delete('menu/{id}/restore','MenuController@restore');

Route::get('permission/load-data','PermissionController@loadData');
Route::resource('permission','PermissionController');
Route::delete('permission/{id}/restore','PermissionController@restore');

Route::get('status-pegawai/load-data','StatusPegawaiController@loadData');
Route::resource('status-pegawai','StatusPegawaiController');
Route::delete('status-pegawai/{id}/restore','StatusPegawaiController@restore');

Route::get('golongan/load-data','GolonganController@loadData');
Route::resource('golongan','GolonganController');
Route::delete('golongan/{id}/restore','GolonganController@restore');

Route::get('jabatan/load-data','JabatanController@loadData');
Route::resource('jabatan','JabatanController');
Route::delete('jabatan/{id}/restore','JabatanController@restore');

Route::get('unit-kerja/load-data','UnitKerjaController@loadData');
Route::resource('unit-kerja','UnitKerjaController');
Route::delete('unit-kerja/{id}/restore','UnitKerjaController@restore');

Route::get('golongan-darah/load-data','GolonganDarahController@loadData');
Route::resource('golongan-darah','GolonganDarahController');
Route::delete('golongan-darah/{id}/restore','GolonganDarahController@restore');

Route::get('agama/load-data','AgamaController@loadData');
Route::resource('agama','AgamaController');
Route::delete('agama/{id}/restore','AgamaController@restore');


Route::get('status-pernikahan/load-data','StatusPernikahanController@loadData');
Route::resource('status-pernikahan','StatusPernikahanController');
Route::delete('status-pernikahan/{id}/restore','StatusPernikahanController@restore');

Route::get('provinsi/load-data','ProvinsiController@loadData');
Route::resource('provinsi','ProvinsiController');
Route::delete('provinsi/{id}/restore','ProvinsiController@restore');

Route::get('kabupaten/load-data','KabupatenController@loadData');
Route::resource('kabupaten','KabupatenController');
Route::delete('kabupaten/{id}/restore','KabupatenController@restore');

Route::get('kecamatan/load-data','KecamatanController@loadData');
Route::resource('kecamatan','KecamatanController');
Route::delete('kecamatan/{id}/restore','KecamatanController@restore');

Route::get('kelurahan/load-data','KelurahanController@loadData');
Route::resource('kelurahan','KelurahanController');
Route::delete('kelurahan/{id}/restore','KelurahanController@restore');

Route::get('pegawai/load-data','PegawaiController@loadData');
Route::get('tambah-pegawai','PegawaiController@create');
Route::resource('pegawai','PegawaiController');
Route::delete('pegawai/{id}/restore','PegawaiController@restore');

Route::get('/tambah-penduduk','PendudukController@createPenduduk');
Route::get('autocomplete/{method}','AutocompleteController@search');
Route::get('dropdown/{method}','DropDownController@index');

Route::get('/profile','PendudukController@loadProfile');
Route::get('penduduk/load-agama','PendudukController@loadAgama');
Route::get('penduduk/load-golongan-darah','PendudukController@loadGolonganDarah');
Route::get('penduduk/load-status','PendudukController@loadStatus');
Route::get('penduduk/load-pekerjaan','PendudukController@loadPekerjaan');
Route::get('penduduk/load-jk','PendudukController@loadJenisKelamin');
Route::get('penduduk/load-provinsi','PendudukController@loadProvinsi');
Route::get('penduduk/get-kabupaten', 'PendudukController@getKabupaten');
Route::get('penduduk/get-kecamatan', 'PendudukController@getKecamatan');
Route::get('penduduk/get-kelurahan', 'PendudukController@getKelurahan');
Route::match(['get', 'post'],'penduduk/upload-foto','PendudukController@uploadFoto');
Route::match(['get', 'post'],'penduduk/simpan-pelamar','PendudukController@simpan');
Route::match(['get', 'post'],'penduduk/simpan-penduduk','PendudukController@simpanData');
Route::get('penduduk/delete-foto','PendudukController@deleteFoto');
Route::get('penduduk/load-data','PendudukController@loadData');
Route::resource('penduduk','PendudukController');
Route::delete('penduduk/{id}/restore','PendudukController@restore');



Route::get('perusahaan/load-data','PerusahaanController@loadData');
Route::resource('perusahaan','PerusahaanController');
Route::delete('perusahaan/{id}/restore','PerusahaanController@restore');


Route::get('kategori-news/load-data','KategoriNewsController@loadData');
Route::resource('kategori-news','KategoriNewsController');
Route::delete('kategori-news/{id}/restore','KategoriNewsController@restore');

Route::get('news/load-data','NewsController@loadData');
Route::resource('news','NewsController');
Route::delete('news/{id}/restore','NewsController@restore');

Route::get('jabatan/load-data','JabatanController@loadData');
Route::resource('jabatan','JabatanController');
Route::delete('jabatan/{id}/restore','JabatanController@restore');

Route::get('golongan-darah/load-data','GolonganDarahController@loadData');
Route::resource('golongan-darah','GolonganDarahController');
Route::delete('golongan-darah/{id}/restore','GolonganDarahController@restore');

Route::get('unit-kerja/load-data','UnitKerjaController@loadData');
Route::resource('unit-kerja','UnitKerjaController');
Route::delete('unit-kerja/{id}/restore','UnitKerjaController@restore');

Route::get('jenis-kelamin/load-data','JenisKelaminController@loadData');
Route::resource('jenis-kelamin','JenisKelaminController');
Route::delete('jenis-kelamin/{id}/restore','JenisKelaminController@restore');

Route::get('status-pernikahan/load-data','StatusPernikahanController@loadData');
Route::resource('status-pernikahan','StatusPernikahanController');
Route::delete('status-pernikahan/{id}/restore','StatusPernikahanController@restore');

Route::get('/pengaturan', 'ConfigIdController@settings')->name('settings');
Route::get('/delete-logo', 'ConfigIdController@deleteLogo');
Route::get('config-id/load-data','ConfigIdController@loadData');
Route::resource('config-id','ConfigIdController');
Route::delete('config-id/{id}/restore','ConfigIdController@restore');

Route::get('profesi/load-data','ProfesiController@loadData');
Route::resource('profesi','ProfesiController');
Route::delete('profesi/{id}/restore','ProfesiController@restore');

Route::get('status-pegawai/load-data','StatusPegawaiController@loadData');
Route::resource('status-pegawai','StatusPegawaiController');
Route::delete('status-pegawai/{id}/restore','StatusPegawaiController@restore');

Route::get('lihat-pegawai/{id}','PegawaiController@lihatPegawai');
Route::get('edit-pegawai/{id}','PegawaiController@editPegawai');
Route::get('tambah-pegawai','PegawaiController@tambahPegawai');
Route::get('pegawai/load-data','PegawaiController@loadData');
Route::resource('pegawai','PegawaiController');
Route::delete('pegawai/{id}/restore','PegawaiController@restore');

Route::get('kategori-kasus/load-data','KategoriKasusController@loadData');
Route::resource('kategori-kasus','KategoriKasusController');
Route::delete('kategori-kasus/{id}/restore','KategoriKasusController@restore');

Route::get('edit-rekam-kasus/{id}','RekamKasusController@editData');
Route::get('tambah-rekam-kasus','RekamKasusController@tambahData');
Route::match(['get', 'post'],'rekam-kasus/simpan-kasus','RekamKasusController@simpanData');

Route::get('rekam-kasus/load-data','RekamKasusController@loadData');
Route::resource('rekam-kasus','RekamKasusController');
Route::delete('rekam-kasus/{id}/restore','RekamKasusController@restore');

Route::get('bp/load-data','BpController@loadData');
Route::resource('bp','BpController');
Route::delete('bp/{id}/restore','BpController@restore');

Route::get('petum/load-data','PetumController@loadData');
Route::resource('petum','PetumController');
Route::delete('petum/{id}/restore','PetumController@restore');

Route::get('map/load-data','MapController@loadData');
Route::resource('map','MapController');
Route::delete('map/{id}/restore','MapController@restore');

Route::get('sub-kategori-kasus/load-data','SubKategoriKasusController@loadData');
Route::resource('sub-kategori-kasus','SubKategoriKasusController');
Route::delete('sub-kategori-kasus/{id}/restore','SubKategoriKasusController@restore');

Route::get('laporan-rekam-kasus','LaporanController@indexLaporanRekamKasus');
Route::get('laporan-rekam-kasus/load-data','LaporanController@loadDataLaporanRekamKasus');
Route::get('laporan-rekam-kasus/cetak/{date_from}/{date_to}/{kategori}','LaporanController@cetakLaporanRekamKasus');
