<p align="center">Infokes - Informasi Kesetahan Covid19</p>


## Tentang Aplikasi

Aplikasi atau sistem ini bertujuan sebagai informasi terkait kesehatan dan persebaran virus covid19 yang sedang melanda negeri ini.

## Requirement

1. Minimum php version = 7.2
2. Database Mysql
3. Have composer
4. Built in laravel version => 5.7 

## Cara Instalasi

1. Git clone url
2. git checkout dev (pindah branch)
3. composer install
4. composer dump-autoload
5. cp .env.example .env
6. php artisan config:cache
7. php artisan migrate
8. php artisan db:seed --class=LaratrustSeeder
9. php artisan db:seed --class=MenuSeeder
10. php artisan db:seed --class=ContentSeeder
