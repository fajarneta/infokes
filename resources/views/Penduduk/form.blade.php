<div class="modal-dialog modal-simple">

		{{ Form::model($penduduk,array('route' => array((!$penduduk->exists) ? 'penduduk.store':'penduduk.update',$penduduk->pk()),
	        'class'=>'modal-content','id'=>'penduduk-form','method'=>(!$penduduk->exists) ? 'POST' : 'PUT')) }}

		<div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
      </button>
      <h4 class="modal-title" id="formModalLabel">{{ ($penduduk->exists?'Edit':'Tambah').' Penduduk' }}</h4>
    </div>
    <div class="modal-body">
																				        {!! App\Console\Commands\Generator\Form::input('nama_lengkap','text')->model($penduduk)->show() !!}
																        {!! App\Console\Commands\Generator\Form::input('nik','text')->model($penduduk)->show() !!}
																        {!! App\Console\Commands\Generator\Form::input('alamat_ktp','text')->model($penduduk)->show() !!}
																        {!! App\Console\Commands\Generator\Form::input('id_kelurahan_ktp','hidden')->model($penduduk)->showHidden() !!}
{!! App\Console\Commands\Generator\Form::autocomplete('kelurahan_ktp',array('value'=>$penduduk->exists?(isset($penduduk->kelurahan_ktp)?$penduduk->kelurahan_ktp->kelurahan_ktp:null):null))->model(null)->show() !!}
																        {!! App\Console\Commands\Generator\Form::input('alamat_domisili','text')->model($penduduk)->show() !!}
																        {!! App\Console\Commands\Generator\Form::input('id_kelurahan_domisili','hidden')->model($penduduk)->showHidden() !!}
{!! App\Console\Commands\Generator\Form::autocomplete('kelurahan_domisili',array('value'=>$penduduk->exists?(isset($penduduk->kelurahan_domisili)?$penduduk->kelurahan_domisili->kelurahan_domisili:null):null))->model(null)->show() !!}
																        {!! App\Console\Commands\Generator\Form::input('tempat_lahir','text')->model($penduduk)->show() !!}
																        {!! App\Console\Commands\Generator\Form::input('tanggal_lahir','date')->model($penduduk)->show() !!}
																        {!! App\Console\Commands\Generator\Form::input('usia','text')->model($penduduk)->show() !!}
																        {!! App\Console\Commands\Generator\Form::input('no_hp','text')->model($penduduk)->show() !!}
																        {!! App\Console\Commands\Generator\Form::input('foto','text')->model($penduduk)->show() !!}
																        {!! App\Console\Commands\Generator\Form::input('id_golongan_darah','hidden')->model($penduduk)->showHidden() !!}
{!! App\Console\Commands\Generator\Form::autocomplete('golongan_darah',array('value'=>$penduduk->exists?(isset($penduduk->golongan_darah)?$penduduk->golongan_darah->golongan_darah:null):null))->model(null)->show() !!}
																        {!! App\Console\Commands\Generator\Form::input('id_jenis_kelamin','hidden')->model($penduduk)->showHidden() !!}
{!! App\Console\Commands\Generator\Form::autocomplete('jenis_kelamin',array('value'=>$penduduk->exists?(isset($penduduk->jenis_kelamin)?$penduduk->jenis_kelamin->jenis_kelamin:null):null))->model(null)->show() !!}
																        {!! App\Console\Commands\Generator\Form::input('id_agama','hidden')->model($penduduk)->showHidden() !!}
{!! App\Console\Commands\Generator\Form::autocomplete('agama',array('value'=>$penduduk->exists?(isset($penduduk->agama)?$penduduk->agama->agama:null):null))->model(null)->show() !!}
																        {!! App\Console\Commands\Generator\Form::input('id_status_pernikahan','hidden')->model($penduduk)->showHidden() !!}
{!! App\Console\Commands\Generator\Form::autocomplete('status_pernikahan',array('value'=>$penduduk->exists?(isset($penduduk->status_pernikahan)?$penduduk->status_pernikahan->status_pernikahan:null):null))->model(null)->show() !!}
																        {!! App\Console\Commands\Generator\Form::input('id_profesi','hidden')->model($penduduk)->showHidden() !!}
{!! App\Console\Commands\Generator\Form::autocomplete('profesi',array('value'=>$penduduk->exists?(isset($penduduk->profesi)?$penduduk->profesi->profesi:null):null))->model(null)->show() !!}
																												<div class="col-md-12 float-right">
					<div class="text-right">
						<button class="btn btn-primary" id="simpan">Simpan</button>
					</div>
				</div>
		</div>

	    {{ Form::close() }}
</div>


<script type="text/javascript">
$(document).ready(function(){
	$('#penduduk-form').formValidation({
	  framework: "bootstrap4",
	  button: {
	    selector: "#simpan",
	    disabled: "disabled"
	  },
	  icon: null,
	  fields: {
	
},
err: {
	clazz: 'invalid-feedback'
},
control: {
	// The CSS class for valid control
	valid: 'is-valid',

	// The CSS class for invalid control
	invalid: 'is-invalid'
},
row: {
	invalid: 'has-danger'
}
});
	
					var kelurahan_ktpEngine = new Bloodhound({
							datumTokenizer: function(d) { return d.tokens; },
							queryTokenizer: Bloodhound.tokenizers.whitespace,
							cache: false,
							remote: {
								url: '{{ url("autocomplete/kelurahan_ktp") }}?q=%QUERY',
								wildcard: "%QUERY"
							}
						});

						$("#kelurahan_ktp").typeahead({
									hint: true,
									highlight: true,
									minLength: 1
							},
							{
									source: kelurahan_ktpEngine.ttAdapter(),
									name: "kelurahan_ktp",
									displayKey: "kelurahan_ktp",
									templates: {
										suggestion: function(data){
											return Handlebars.compile([
																"<div class=\"tt-dataset\">",
																		"<div>@{{kelurahan_ktp}}</div>",
																"</div>",
														].join(""))(data);
										},
											empty: [
													"<div>kelurahan_ktp tidak ditemukan</div>"
											]
									}
							}).bind("typeahead:selected", function(obj, datum, name) {
								$("#id_kelurahan_ktp").val(datum.id);
							}).bind("typeahead:change", function(obj, datum, name) {

							});
					
					var kelurahan_domisiliEngine = new Bloodhound({
							datumTokenizer: function(d) { return d.tokens; },
							queryTokenizer: Bloodhound.tokenizers.whitespace,
							cache: false,
							remote: {
								url: '{{ url("autocomplete/kelurahan_domisili") }}?q=%QUERY',
								wildcard: "%QUERY"
							}
						});

						$("#kelurahan_domisili").typeahead({
									hint: true,
									highlight: true,
									minLength: 1
							},
							{
									source: kelurahan_domisiliEngine.ttAdapter(),
									name: "kelurahan_domisili",
									displayKey: "kelurahan_domisili",
									templates: {
										suggestion: function(data){
											return Handlebars.compile([
																"<div class=\"tt-dataset\">",
																		"<div>@{{kelurahan_domisili}}</div>",
																"</div>",
														].join(""))(data);
										},
											empty: [
													"<div>kelurahan_domisili tidak ditemukan</div>"
											]
									}
							}).bind("typeahead:selected", function(obj, datum, name) {
								$("#id_kelurahan_domisili").val(datum.id);
							}).bind("typeahead:change", function(obj, datum, name) {

							});
					
					var golongan_darahEngine = new Bloodhound({
							datumTokenizer: function(d) { return d.tokens; },
							queryTokenizer: Bloodhound.tokenizers.whitespace,
							cache: false,
							remote: {
								url: '{{ url("autocomplete/golongan_darah") }}?q=%QUERY',
								wildcard: "%QUERY"
							}
						});

						$("#golongan_darah").typeahead({
									hint: true,
									highlight: true,
									minLength: 1
							},
							{
									source: golongan_darahEngine.ttAdapter(),
									name: "golongan_darah",
									displayKey: "golongan_darah",
									templates: {
										suggestion: function(data){
											return Handlebars.compile([
																"<div class=\"tt-dataset\">",
																		"<div>@{{golongan_darah}}</div>",
																"</div>",
														].join(""))(data);
										},
											empty: [
													"<div>golongan_darah tidak ditemukan</div>"
											]
									}
							}).bind("typeahead:selected", function(obj, datum, name) {
								$("#id_golongan_darah").val(datum.id);
							}).bind("typeahead:change", function(obj, datum, name) {

							});
					
					var jenis_kelaminEngine = new Bloodhound({
							datumTokenizer: function(d) { return d.tokens; },
							queryTokenizer: Bloodhound.tokenizers.whitespace,
							cache: false,
							remote: {
								url: '{{ url("autocomplete/jenis_kelamin") }}?q=%QUERY',
								wildcard: "%QUERY"
							}
						});

						$("#jenis_kelamin").typeahead({
									hint: true,
									highlight: true,
									minLength: 1
							},
							{
									source: jenis_kelaminEngine.ttAdapter(),
									name: "jenis_kelamin",
									displayKey: "jenis_kelamin",
									templates: {
										suggestion: function(data){
											return Handlebars.compile([
																"<div class=\"tt-dataset\">",
																		"<div>@{{jenis_kelamin}}</div>",
																"</div>",
														].join(""))(data);
										},
											empty: [
													"<div>jenis_kelamin tidak ditemukan</div>"
											]
									}
							}).bind("typeahead:selected", function(obj, datum, name) {
								$("#id_jenis_kelamin").val(datum.id);
							}).bind("typeahead:change", function(obj, datum, name) {

							});
					
					var agamaEngine = new Bloodhound({
							datumTokenizer: function(d) { return d.tokens; },
							queryTokenizer: Bloodhound.tokenizers.whitespace,
							cache: false,
							remote: {
								url: '{{ url("autocomplete/agama") }}?q=%QUERY',
								wildcard: "%QUERY"
							}
						});

						$("#agama").typeahead({
									hint: true,
									highlight: true,
									minLength: 1
							},
							{
									source: agamaEngine.ttAdapter(),
									name: "agama",
									displayKey: "agama",
									templates: {
										suggestion: function(data){
											return Handlebars.compile([
																"<div class=\"tt-dataset\">",
																		"<div>@{{agama}}</div>",
																"</div>",
														].join(""))(data);
										},
											empty: [
													"<div>agama tidak ditemukan</div>"
											]
									}
							}).bind("typeahead:selected", function(obj, datum, name) {
								$("#id_agama").val(datum.id);
							}).bind("typeahead:change", function(obj, datum, name) {

							});
					
					var status_pernikahanEngine = new Bloodhound({
							datumTokenizer: function(d) { return d.tokens; },
							queryTokenizer: Bloodhound.tokenizers.whitespace,
							cache: false,
							remote: {
								url: '{{ url("autocomplete/status_pernikahan") }}?q=%QUERY',
								wildcard: "%QUERY"
							}
						});

						$("#status_pernikahan").typeahead({
									hint: true,
									highlight: true,
									minLength: 1
							},
							{
									source: status_pernikahanEngine.ttAdapter(),
									name: "status_pernikahan",
									displayKey: "status_pernikahan",
									templates: {
										suggestion: function(data){
											return Handlebars.compile([
																"<div class=\"tt-dataset\">",
																		"<div>@{{status_pernikahan}}</div>",
																"</div>",
														].join(""))(data);
										},
											empty: [
													"<div>status_pernikahan tidak ditemukan</div>"
											]
									}
							}).bind("typeahead:selected", function(obj, datum, name) {
								$("#id_status_pernikahan").val(datum.id);
							}).bind("typeahead:change", function(obj, datum, name) {

							});
					
					var profesiEngine = new Bloodhound({
							datumTokenizer: function(d) { return d.tokens; },
							queryTokenizer: Bloodhound.tokenizers.whitespace,
							cache: false,
							remote: {
								url: '{{ url("autocomplete/profesi") }}?q=%QUERY',
								wildcard: "%QUERY"
							}
						});

						$("#profesi").typeahead({
									hint: true,
									highlight: true,
									minLength: 1
							},
							{
									source: profesiEngine.ttAdapter(),
									name: "profesi",
									displayKey: "profesi",
									templates: {
										suggestion: function(data){
											return Handlebars.compile([
																"<div class=\"tt-dataset\">",
																		"<div>@{{profesi}}</div>",
																"</div>",
														].join(""))(data);
										},
											empty: [
													"<div>profesi tidak ditemukan</div>"
											]
									}
							}).bind("typeahead:selected", function(obj, datum, name) {
								$("#id_profesi").val(datum.id);
							}).bind("typeahead:change", function(obj, datum, name) {

							});
					

});
</script>
