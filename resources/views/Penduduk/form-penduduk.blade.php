@extends('layouts.app')

@section('content')

<style>
	.kv-avatar .krajee-default.file-preview-frame,.kv-avatar .krajee-default.file-preview-frame:hover {
		margin: 0;
		padding: 0;
		border: none;
		box-shadow: none;
		text-align: center;
	}
	.kv-avatar {
		display: inline-block;
	}
	.kv-avatar .file-input {
		display: table-cell;
		width: 213px;
	}
	.kv-reqd {
		color: red;
		font-family: monospace;
		font-weight: normal;
	}
	.kv-file-remove {
		display:none;
	}
	.ajax-loader{
		position:fixed;
		top:0px;
		right:0px;
		width:100%;
		height:auto;
		background-color:#A9A9A9;
		background-repeat:no-repeat;
		background-position:center;
		z-index:10000000;
		opacity: 0.4;
		filter: alpha(opacity=40); /* For IE8 and earlier */
	}

	#progress_upload{
		position:fixed;
		top:0px;
		right:0px;
		width:100%;
		height:auto;
		background-color:#757575;
		background-repeat:no-repeat;
		background-position:center;
		z-index:10000000;
		opacity: 0.4;
		filter: alpha(opacity=40); /* For IE8 and earlier */
	}
</style>

<div class="page-content">
	<div class="panel">
{{-- 		<div class="page-header">
			<h4>Profile Pelamar</h4>
		</div> --}}
		<div class="panel-body container-fluid">
			<div id="kv-avatar-errors-2" class="center-block" style="width:800px;display:none"></div>
			<form method="post" id="form" action="#" enctype="multipart/form-data">
				{{ csrf_field() }}
				<div class="col-lg-12">
					<div class="panel-heading">
						<span><h4><b>Data Penduduk</b></h4></span>
					</div>
					<hr/>
					{{-- <input id="select_file" name="select_file" class="hidden" type="file" /> --}}

			

					<div class="row col-lg-12 informasi" style="margin-bottom: 1rem">
						<span class="col-form-label col-sm-2 offset-md-0"><h5><b>Data Pribadi</b></h5></span>
					</div>

					<div class="row col-lg-12" style="margin-bottom: 0.8rem">
						<label for="nama" style="font-size:9pt" class="col-form-label col-sm-2 offset-md-0">Nama Lengkap</label>
						<div class="col-lg-8">
							<input name="nama" placeholder="" id="nama" class="form-control form-control-sm" value="{{isset($penduduk) && !empty($penduduk->nama_lengkap)?$penduduk->nama_lengkap:''}}" type="text">
							<span id="help-block"></span>
						</div>
					</div>
					<div class="row col-lg-12" style="margin-bottom: 0.8rem">
						<label for="nama" style="font-size:9pt" class="col-form-label col-sm-2 offset-md-0">NIK</label>
						<div class="col-lg-8">
							<input name="nik" placeholder="" id="nik" class="form-control form-control-sm" value="{{isset($penduduk) && !empty($penduduk->nik)?$penduduk->nik:''}}" type="text">
							<span id="help-block"></span>
						</div>
					</div>
			
					<div class="row col-lg-12" style="margin-bottom: 2rem">
						<label for="nama" style="font-size:9pt" class="col-form-label col-sm-2 offset-md-0">Tempat Lahir</label>
						<div class="col-lg-3">
							<input name="tempat_lahir" placeholder="" id="tempat_lahir" class="form-control form-control-sm" value="{{isset($penduduk) && !empty($penduduk->tempat_lahir)?$penduduk->tempat_lahir:''}}" type="text">
							<span id="help-block"></span>
						</div>
						<label for="nama" style="font-size:9pt" class="col-form-label col-sm-2 offset-md-0">Tanggal Lahir</label>

						<div class="col-lg-3">
							<input name="tanggal_lahir"  placeholder="" type="datepicker" id="tanggal_lahir" class="form-control form-control-sm datepicker"    value="{{isset($penduduk) && !empty($penduduk->tanggal_lahir)?date('d-m-Y',strtotime($penduduk->tanggal_lahir)):''}}" type="text">

							<span id="help-block"></span>
						</div>
					</div>




<!-- tambahan -->
<?php
$tahun='';
$bulan='';
$hari='';
if(isset($penduduk) && !empty($penduduk->tanggal_lahir))
{
	$tahun=time_diff_string(date('Y-m-d',strtotime($penduduk->tanggal_lahir)),'now')['tahun'];
	$bulan=time_diff_string(date('Y-m-d',strtotime($penduduk->tanggal_lahir)),'now')['bulan'];
	$hari=time_diff_string(date('Y-m-d',strtotime($penduduk->tanggal_lahir)),'now')['hari'];
}

?>
				{{-- {{dd(time_diff_string(date('1995-09-17'),'now'))}}	 --}}
					<div class="row col-lg-12" style="margin-bottom: 2rem">
						<label for="usia_" style="font-size:9pt" class="col-form-label col-sm-2 offset-md-0">Usia</label>
						<div class="col-lg-1">
							<input name="usia" placeholder="" id="usia" class="form-control form-control-sm" value="{{isset($penduduk) && !empty($penduduk->tanggal_lahir)?$tahun:''}}" type="text" readonly="">
							<span id="help-block"></span>
						</div>
						<label for="usia_" style="font-size:9pt" class="col-form-label col-sm-1 offset-md-0">Tahun</label>
						<div class="col-lg-1">
							<input name="bulan" placeholder="" id="bulan" class="form-control form-control-sm" value="{{isset($penduduk) && !empty($penduduk->tanggal_lahir)?$bulan:''}}" type="text" readonly="">
							<span id="help-block"></span>
						</div>
						<label for="usia_" style="font-size:9pt" class="col-form-label col-sm-1 offset-md-0">Bulan</label>
						<div class="col-lg-1">
							<input name="hari" placeholder="" id="hari" class="form-control form-control-sm" value="{{isset($penduduk) && !empty($penduduk->tanggal_lahir)?$hari:''}}" type="text" readonly="">
							<span id="help-block"></span>
						</div>
						<label for="usia_" style="font-size:9pt" class="col-form-label col-sm-1 offset-md-0">Hari</label>
					</div>
					<div class="row col-lg-12" style="margin-bottom: 0.8rem;">
						 <label for="jenis_kelamin" style="font-size:9pt" class="col-form-label col-sm-2 offset-md-0">Jenis Kelamin</label>
						 <div class="col-lg-5">
						 	<select class="form-control form-control-sm select2" id="jenis_kelamin" name="jenis_kelamin" data-plugin="select2">
						 		<optgroup label="Jenis Kelamin">
						 		</optgroup>
						 	</select>
						 	<span id="help-block"></span>
						 </div>
					</div>
					<div class="row col-lg-12" style="margin-bottom: 0.8rem">
						<label for="nama" style="font-size:9pt" class="col-form-label col-sm-2 offset-md-0">Golongan Darah</label>
						<div class="col-lg-5">
							 <select class="form-control form-control-sm select2" id="golongan_darah" name="golongan_darah" data-plugin="select2">
                        <optgroup label="Golongan Darah">
                        </optgroup>
                      </select>
							<span id="help-block"></span>
						</div>
					</div>

<!-- end tambahan -->





					<div class="row col-lg-12" style="margin-bottom: 0.8rem">
						<label for="nama" style="font-size:9pt" class="col-form-label col-sm-2 offset-md-0">Agama</label>
						<div class="col-lg-5">
							 <select class="form-control form-control-sm select2" id="agama" name="agama" data-plugin="select2">
                        <optgroup label="Agama">
                        </optgroup>
                      </select>
							<span id="help-block"></span>
						</div>
					</div>

					<div class="row col-lg-12" style="margin-bottom: 0.8rem">
						<label for="nama" style="font-size:9pt" class="col-form-label col-sm-2 offset-md-0">Status Pernikahan</label>
						<div class="col-lg-5">
							 <select class="form-control form-control-sm select2" id="status_pernikahan" name="status_pernikahan" data-plugin="select2">
                        <optgroup label="Status Pernikahan">
                        </optgroup>
                      </select>
							<span id="help-block"></span>
						</div>
					</div>

					<div class="row col-lg-12" style="margin-bottom: 2rem">
						<label for="nama" style="font-size:9pt" class="col-form-label col-sm-2 offset-md-0">Pekerjaan / Profesi</label>
						<div class="col-lg-5">
							<select class="form-control form-control-sm select2" id="pekerjaan" name="pekerjaan" data-plugin="select2">
								<optgroup label="pekerjaan">
								</optgroup>
							</select>
							<span id="help-block"></span>
						</div>
					</div>
					

					<div class="row col-lg-12 alamat_ktp_scroll" style="margin-bottom: 1rem">
						<span class="col-form-label col-sm-2 offset-md-0"><h5><b>Alamat KTP</b></h5></span>
					</div>

					<div class="row col-lg-12" style="margin-bottom: 0.8rem">
						<label for="nama" style="font-size:9pt" class="col-form-label col-sm-2 offset-md-0">Alamat KTP</label>
						<div class="col-lg-5">
							<input name="alamat_ktp" placeholder="" id="alamat_ktp" class="form-control form-control-sm" value="{{isset($penduduk) && !empty($penduduk->alamat_ktp)?$penduduk->alamat_ktp:''}}" type="text">
							<span id="help-block"></span>
						</div>
					</div>
					<div class="row col-lg-12" style="margin-bottom: 0.8rem">
						<label for="nama" style="font-size:9pt" class="col-form-label col-sm-2 offset-md-0">Provinsi</label>
						<div class="col-lg-5">
							 <select class="form-control form-control-sm select2" id="provinsi_ktp" name="provinsi_ktp" data-plugin="select2">
                        <optgroup label="Provinsi">
                        </optgroup>
                      </select>
							<span id="help-block"></span>
						</div>
					</div>

					<div class="row col-lg-12" style="margin-bottom: 0.8rem">
						<label for="nama" style="font-size:9pt" class="col-form-label col-sm-2 offset-md-0">Kabupaten</label>
						<div class="col-lg-5">
							 <select class="form-control form-control-sm select2" id="kabupaten_ktp" name="kabupaten_ktp" data-plugin="select2">
                        <optgroup label="Kabupaten">
                        </optgroup>
                      </select>
							<span id="help-block"></span>
						</div>
					</div>

					<div class="row col-lg-12" style="margin-bottom: 0.8rem">
						<label for="nama" style="font-size:9pt" class="col-form-label col-sm-2 offset-md-0">Kecamatan</label>
						<div class="col-lg-5">
							 <select class="form-control form-control-sm select2" id="kecamatan_ktp" name="kecamatan_ktp" data-plugin="select2">
                        <optgroup label="Kecamatan">
                        </optgroup>
                      </select>
							<span id="help-block"></span>
						</div>
					</div>

					<div class="row col-lg-12" style="margin-bottom: 2rem">
						<label for="nama" style="font-size:9pt" class="col-form-label col-sm-2 offset-md-0">Kelurahan</label>
						<div class="col-lg-5">
							 <select class="form-control form-control-sm select2" id="kelurahan_ktp" name="kelurahan_ktp" data-plugin="select2">
                        <optgroup label="Kelurahan">
                        </optgroup>
                      </select>
							<span id="help-block"></span>
						</div>
					</div>

					<div class="row col-lg-12 alamat_domisili_scroll" style="margin-bottom: 1rem">
						<span class="col-form-label col-sm-2 offset-md-0"><h5><b>Alamat Domisili</b></h5></span>
						<div class="col-lg-5 text-right">
							<div class="checkbox-custom checkbox-default">
								<input type="checkbox" class="check" name="checkbox_alamat_domisili" id="checkbox_alamat_domisili" value="Y">
								<label for="inputBasicRemember">sama dengan alamat KTP</label>
							</div>
							<span id="help-block"></span>
						</div>
					</div>

					<div class="row col-lg-12" style="margin-bottom: 0.8rem">
						<label for="nama" style="font-size:9pt" class="col-form-label col-sm-2 offset-md-0">Alamat Domisili</label>
						<div class="col-lg-5">
							<input name="alamat_domisili" placeholder="" id="alamat_domisili" class="form-control form-control-sm" value="{{isset($penduduk) && !empty($penduduk->alamat_domisili)?$penduduk->alamat_domisili:''}}" type="text">
							<span id="help-block"></span>
						</div>
					</div>
					<div class="row col-lg-12" style="margin-bottom: 0.8rem">
						<label for="nama" style="font-size:9pt" class="col-form-label col-sm-2 offset-md-0">Provinsi</label>
						<div class="col-lg-5">
							 <select class="form-control form-control-sm select2" id="provinsi_domisili" name="provinsi_domisili" data-plugin="select2">
                        <optgroup label="Provinsi">
                        </optgroup>
                      </select>
							<span id="help-block"></span>
						</div>
					</div>

					<div class="row col-lg-12" style="margin-bottom: 0.8rem">
						<label for="nama" style="font-size:9pt" class="col-form-label col-sm-2 offset-md-0">Kabupaten</label>
						<div class="col-lg-5">
							 <select class="form-control form-control-sm select2" id="kabupaten_domisili" name="kabupaten_domisili" data-plugin="select2">
                        <optgroup label="Kabupaten">
                        </optgroup>
                      </select>
							<span id="help-block"></span>
						</div>
					</div>

					<div class="row col-lg-12" style="margin-bottom: 0.8rem">
						<label for="nama" style="font-size:9pt" class="col-form-label col-sm-2 offset-md-0">Kecamatan</label>
						<div class="col-lg-5">
							 <select class="form-control form-control-sm select2" id="kecamatan_domisili" name="kecamatan_domisili" data-plugin="select2">
                        <optgroup label="Kecamatan">
                        </optgroup>
                      </select>
							<span id="help-block"></span>
						</div>
					</div>

					<div class="row col-lg-12" style="margin-bottom: 2rem">
						<label for="nama" style="font-size:9pt" class="col-form-label col-sm-2 offset-md-0">Kelurahan</label>
						<div class="col-lg-5">
							 <select class="form-control form-control-sm select2" id="kelurahan_domisili" name="kelurahan_domisili" data-plugin="select2">
                        <optgroup label="Kelurahan">
                        </optgroup>
                      </select>
							<span id="help-block"></span>
						</div>
					</div>

					{{-- <div class="row col-lg-12 kontak" style="margin-bottom: 1rem">
						<span class="col-form-label col-sm-2 offset-md-0"><h5><b>Kontak</b></h5></span>
					</div>

					<div class="row col-lg-12" style="margin-bottom: 2rem">
						<label for="nama" style="font-size:9pt" class="col-form-label col-sm-2 offset-md-0">No HP</label>
						<div class="col-lg-5">
							<input name="no_hp" placeholder="Contoh : +62xxxxxxxxxxx" id="no_hp" class="form-control form-control-sm" value="{{isset($penduduk) && !empty($penduduk->no_hp)?$penduduk->no_hp:''}}" type="text">
							<span id="help-block"></span>
						</div>
					</div> --}}


					{{-- <div class="row col-lg-12 kontak" style="margin-bottom: 1rem">
						<span class="col-form-label col-sm-2 offset-md-0"><h5><b>Data Pekerjaan</b></h5></span>
					</div>

					<div class="row col-lg-12" style="margin-bottom: 2rem">
						<label for="nama" style="font-size:9pt" class="col-form-label col-sm-2 offset-md-0">Posisi / Jabatan </label>
						<div class="col-lg-5">
							<input class="form-control form-control-sm" value="{{isset($penduduk) && !empty($penduduk->jabatan->jabatan)?$penduduk->jabatan->jabatan:'Volunter'}}" disabled type="text">
							<span id="help-block"></span>
						</div>
					</div>

					<div class="row col-lg-12" style="margin-bottom: 2rem">
						<label for="nama" style="font-size:9pt" class="col-form-label col-sm-2 offset-md-0">Lokasi / Unit Kerja </label>
						<div class="col-lg-5">
							<input class="form-control form-control-sm" value="{{isset($penduduk) && !empty($penduduk->unit_kerja->unit_kerja)?$penduduk->unit_kerja->unit_kerja:'Data Kosong / Belum ada pembagian terkait unit kerja'}}" disabled type="text">
							<span id="help-block"></span>
						</div>
					</div> --}}


					<input type="hidden" id="id_provinsi" value="{{isset($penduduk->kelurahan_ktp->kecamatan->kabupaten->provinsi)?$penduduk->kelurahan_ktp->kecamatan->kabupaten->provinsi->id:''}}">

					<input type="hidden" id="id_kabupaten" value="{{isset($penduduk->kelurahan_ktp->kecamatan->kabupaten)?$penduduk->kelurahan_ktp->kecamatan->kabupaten->id:''}}">

					<input type="hidden" id="id_kecamatan" value="{{isset($penduduk->kelurahan_ktp->kecamatan)?$penduduk->kelurahan_ktp->kecamatan->id:''}}">

					<input type="hidden" id="id_kelurahan" value="{{isset($penduduk->id_kelurahan_ktp)?$penduduk->id_kelurahan_ktp:''}}">

					<input type="hidden" id="id_provinsi1" value="{{isset($penduduk->kelurahan_domisili->kecamatan->kabupaten->provinsi)?$penduduk->kelurahan_domisili->kecamatan->kabupaten->provinsi->id:''}}">

					<input type="hidden" id="id_kabupaten1" value="{{isset($penduduk->kelurahan_domisili->kecamatan->kabupaten)?$penduduk->kelurahan_domisili->kecamatan->kabupaten->id:''}}">

					<input type="hidden" id="id_kecamatan1" value="{{isset($penduduk->kelurahan_domisili->kecamatan)?$penduduk->kelurahan_domisili->kecamatan->id:''}}">

					<input type="hidden" id="id_kelurahan1" value="{{isset($penduduk->id_kelurahan_domisili)?$penduduk->id_kelurahan_domisili:''}}">

					<hr/>
					<div class="text-right">
						<button class="btn btn-primary btn-sm" id="simpan">Simpan</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection

@include('penduduk.form-penduduk-js')