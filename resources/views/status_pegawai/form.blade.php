<div class="modal-dialog modal-simple">

		{{ Form::model($statusPegawai,array('route' => array((!$statusPegawai->exists) ? 'status-pegawai.store':'status-pegawai.update',$statusPegawai->pk()),
	        'class'=>'modal-content','id'=>'status-pegawai-form','method'=>(!$statusPegawai->exists) ? 'POST' : 'PUT')) }}

		<div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
      </button>
      <h4 class="modal-title" id="formModalLabel">{{ ($statusPegawai->exists?'Edit':'Tambah').' Status Pegawai' }}</h4>
    </div>
    <div class="modal-body">
																				        {!! App\Console\Commands\Generator\Form::input('status_pegawai','text')->model($statusPegawai)->show() !!}
																												<div class="col-md-12 float-right">
					<div class="text-right">
						<button class="btn btn-primary" id="simpan">Simpan</button>
					</div>
				</div>
		</div>

	    {{ Form::close() }}
</div>


<script type="text/javascript">
$(document).ready(function(){
	$('#status-pegawai-form').formValidation({
	  framework: "bootstrap4",
	  button: {
	    selector: "#simpan",
	    disabled: "disabled"
	  },
	  icon: null,
	  fields: {
	status_pegawai : { validators: {
				        notEmpty: {
				          message: 'Kolom status_pegawai tidak boleh kosong'
							}
						}
					}
},
err: {
	clazz: 'invalid-feedback'
},
control: {
	// The CSS class for valid control
	valid: 'is-valid',

	// The CSS class for invalid control
	invalid: 'is-invalid'
},
row: {
	invalid: 'has-danger'
}
});
	

});
</script>
