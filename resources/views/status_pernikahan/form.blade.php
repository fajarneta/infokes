<div class="modal-dialog modal-simple">

		{{ Form::model($statusPernikahan,array('route' => array((!$statusPernikahan->exists) ? 'status-pernikahan.store':'status-pernikahan.update',$statusPernikahan->pk()),
	        'class'=>'modal-content','id'=>'status-pernikahan-form','method'=>(!$statusPernikahan->exists) ? 'POST' : 'PUT')) }}

		<div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
      </button>
      <h4 class="modal-title" id="formModalLabel">{{ ($statusPernikahan->exists?'Edit':'Tambah').' Status Pernikahan' }}</h4>
    </div>
    <div class="modal-body">
																				        {!! App\Console\Commands\Generator\Form::input('status_pernikahan','text')->model($statusPernikahan)->show() !!}
																												<div class="col-md-12 float-right">
					<div class="text-right">
						<button class="btn btn-primary" id="simpan">Simpan</button>
					</div>
				</div>
		</div>

	    {{ Form::close() }}
</div>


<script type="text/javascript">
$(document).ready(function(){
	$('#status-pernikahan-form').formValidation({
	  framework: "bootstrap4",
	  button: {
	    selector: "#simpan",
	    disabled: "disabled"
	  },
	  icon: null,
	  fields: {
	status_pernikahan : { validators: {
				        notEmpty: {
				          message: 'Kolom status_pernikahan tidak boleh kosong'
							}
						}
					}
},
err: {
	clazz: 'invalid-feedback'
},
control: {
	// The CSS class for valid control
	valid: 'is-valid',

	// The CSS class for invalid control
	invalid: 'is-invalid'
},
row: {
	invalid: 'has-danger'
}
});
	

});
</script>
