<div class="modal-dialog modal-simple">

		{{ Form::model($news,array('route' => array((!$news->exists) ? 'news.store':'news.update',$news->pk()),
	        'class'=>'modal-content','id'=>'news-form','method'=>(!$news->exists) ? 'POST' : 'PUT')) }}

		<div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
      </button>
      <h4 class="modal-title" id="formModalLabel">{{ ($news->exists?'Edit':'Tambah').' News' }}</h4>
    </div>
    <div class="modal-body">
																				        {!! App\Console\Commands\Generator\Form::input('judul','text')->model($news)->show() !!}
																        {!! App\Console\Commands\Generator\Form::textarea( 'isi' )->model($news)->show() !!}
																        {!! App\Console\Commands\Generator\Form::input('gambar','text')->model($news)->show() !!}
																        {!! App\Console\Commands\Generator\Form::input('flag_aktif','text')->model($news)->show() !!}
																        {!! App\Console\Commands\Generator\Form::input('id_kategori_news','hidden')->model($news)->showHidden() !!}
{!! App\Console\Commands\Generator\Form::autocomplete('kategori_news',array('value'=>$news->exists?(isset($news->kategori_news)?$news->kategori_news->kategori_news:null):null))->model(null)->show() !!}
																        {!! App\Console\Commands\Generator\Form::input('user_input','text')->model($news)->show() !!}
																        {!! App\Console\Commands\Generator\Form::input('user_update','text')->model($news)->show() !!}
																												<div class="col-md-12 float-right">
					<div class="text-right">
						<button class="btn btn-primary" id="simpan">Simpan</button>
					</div>
				</div>
		</div>

	    {{ Form::close() }}
</div>


<script type="text/javascript">
$(document).ready(function(){
	$('#news-form').formValidation({
	  framework: "bootstrap4",
	  button: {
	    selector: "#simpan",
	    disabled: "disabled"
	  },
	  icon: null,
	  fields: {
	
},
err: {
	clazz: 'invalid-feedback'
},
control: {
	// The CSS class for valid control
	valid: 'is-valid',

	// The CSS class for invalid control
	invalid: 'is-invalid'
},
row: {
	invalid: 'has-danger'
}
});
	
					var kategori_newsEngine = new Bloodhound({
							datumTokenizer: function(d) { return d.tokens; },
							queryTokenizer: Bloodhound.tokenizers.whitespace,
							cache: false,
							remote: {
								url: '{{ url("autocomplete/kategori_news") }}?q=%QUERY',
								wildcard: "%QUERY"
							}
						});

						$("#kategori_news").typeahead({
									hint: true,
									highlight: true,
									minLength: 1
							},
							{
									source: kategori_newsEngine.ttAdapter(),
									name: "kategori_news",
									displayKey: "kategori_news",
									templates: {
										suggestion: function(data){
											return Handlebars.compile([
																"<div class=\"tt-dataset\">",
																		"<div>@{{kategori_news}}</div>",
																"</div>",
														].join(""))(data);
										},
											empty: [
													"<div>kategori_news tidak ditemukan</div>"
											]
									}
							}).bind("typeahead:selected", function(obj, datum, name) {
								$("#id_kategori_news").val(datum.id);
							}).bind("typeahead:change", function(obj, datum, name) {

							});
					

});
</script>
