@extends('layouts.app')

@section('content')
   <div class="page-header">
     <h1 class="page-title">Laporan Rekam Kasus</h1>
      @include('layouts.inc.breadcrumb')
   
   </div>
   <div class="page-content">
     <!-- Panel Table Tools -->
     <div class="panel">
       <header class="panel-heading">
         <div class="form-group col-md-12">
          
         </div>
       </header>
       <div class="panel-body">
     

      <div class="form-group row">
          <div class="col-sm-2" ><b>Tanggal</b></div>
  
          <div class="col-lg-2">
            <input name="date_from"  type="datepicker" id="date_from" class="form-control form-control-sm datepicker"  value="{{date('d-m-Y')}}" type="text">
          </div>
          <div class="col-lg-2">
            <input name="date_to"  type="datepicker" id="date_to" class="form-control form-control-sm datepicker" value="{{date('d-m-Y')}}" type="text">
         
        </div>
       
      </div>
       <div class="form-group row">
        <div class="col-md-2">
          <span><b>Kategori Kasus</b></span>
        </div>

        <div class="col-md-4">
          <select class="form-control select" id="kategori_kasus" name="" data-plugin="select2">
            <optgroup label="Kategori Kasus">
         
              @foreach($kategori_kasus as $a)
              <option value="{{$a->kategori_kasus}}">{{$a->kategori_kasus}}</option>
              @endforeach
            </optgroup>
          </select>
        </div>
      </div>



  <div class="row" style="margin-bottom: 0.2em">
  <div class="col-md-6 text-right">
      <button class="btn btn-primary" id="cari">Cari</button>&nbsp&nbsp
      <button class="btn btn-outline-success" id="reset">Reset</button>
  </div>

</div>  <br>
<br>
<br>
<br>
<h3 style="text-align:center;">Tabel Laporan Rekam Kasus</h3>
<hr>
         <table class="table table-hover dataTable table-striped w-full" id="rekam-kasus-table">
           <thead>
               <tr>
                 <th style="text-align:center;">No</th>
                                           <th style="text-align:center;">Tanggal</th>
                                           <th style="text-align:center;">Kategori Kasus</th>
                                           <th style="text-align:center;">Jumlah Kasus</th>
                                         
                                 
                             <th style="text-align:center;">Aksi</th>
               </tr>
           </thead>
         </table>

         {{-- <br>
         <hr>
         <div style="text-align:right;">
          <a class="btn btn-info btn-md" href="{{url('tambah-rekam-kasus')}}"  ><i class="md md-print"> </i> Cetak Laporan</a>
          </div> --}}
       </div>
       
     </div>
     <!-- End Panel Table Tools -->
    
 </div>
 
 <div class="modal fade" id="formModal" aria-hidden="true" aria-labelledby="formModalLabel" role="dialog" tabindex="-1">
 </div>

@endsection

@push('js')
<script type="text/javascript">
$(function() {

  $('.datepicker').datepicker({
    // placement: 'button',
    format: "dd-mm-yyyy",
    align: 'left',
    autoclose: true,
})
$('#cari').click(function(){
      // console.log('aaaa');
      rekamkasustable.ajax.reload( null, false );
    //  pembayaran1.ajax.reload( null, false );
  });
  $('.trash-ck').click(function(){
    if ($('.trash-ck').prop('checked')) {
      document.location = '{{ url("rekam-kasus?status=trash") }}';
    } else {
      document.location = '{{ url("rekam-kasus") }}';
    }
  });
   var rekamkasustable =  $('#rekam-kasus-table').DataTable({
      stateSave: true,
    processing : true,
    serverSide : true,
    info:false,
    searching:false,
    paginate:false,
    pageLength:20,
        ajax : {
                  url:"{{ url('laporan-rekam-kasus/load-data') }}",
                data: function (d) {
                  return $.extend( {}, d, {
              "date_from": $("#date_from").val(),
              "date_to": $("#date_to").val(),                 
              "kategori_kasus": $("#kategori_kasus").val(),                 
            } );
                }
      },
        columns: [
          { data: 'nomor', name: 'nomor',searchable:false,orderable:false },
                      { data: 'tanggal', name: 'tanggal' },
                      { data: 'kategori_kasus', name: 'kategori_kasus' },
                                      { data: 'jumlah', name: 'jumlah' },
                   
                    
                      { data: 'action', name: 'action', orderable: false, searchable: false },
      ],
      language: {
            lengthMenu : '{{ "Menampilkan _MENU_ data" }}',
            zeroRecords : '{{ "Data tidak ditemukan" }}' ,
            info : '{{ "_PAGE_ dari _PAGES_ halaman" }}',
            infoEmpty : '{{ "Data tidak ditemukan" }}',
            infoFiltered : '{{ "(Penyaringan dari _MAX_ data)" }}',
            loadingRecords : '{{ "Memuat data dari server" }}' ,
            processing :    '{{ "Memuat data data" }}',
            search :        '{{ "Pencarian:" }}',
            paginate : {
                first :     '{{ "<" }}' ,
                last :      '{{ ">" }}' ,
                next :      '{{ ">>" }}',
                previous :  '{{ "<<" }}'
            }
        },
        aoColumnDefs: [{
          bSortable: false,
          aTargets: [-1]
        }],
        iDisplayLength: 5,
        aLengthMenu: [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
        // sDom: '<"dt-panelmenu clearfix"Bfr>t<"dt-panelfooter clearfix"ip>',
        // buttons: ['copy', 'excel', 'csv', 'pdf', 'print'],
    });
});

$(document).on('click','#reset',function(event){
			event.preventDefault();
	
	
			$('#date_from').val('{{date('d-m-Y')}}');
			$('#date_to').val('{{date('d-m-Y')}}');
		
		})
</script>
@endpush
