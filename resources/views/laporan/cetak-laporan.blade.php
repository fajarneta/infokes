@extends('layouts.print-paper')
{{-- @section('title', $title='PEMESANAN NO.'.$pemesanan->no_pemesanan) --}}
@section('content')
{{--  <link rel="stylesheet" href="{{ asset('admin_remark_base/') }}/global/css/bootstrap.min.css">  --}}
<style>
    body {
        zoom: 150%;
    }
    #print-area {
        width: 150mm;
        padding: 10px;
        margin: 0 auto;
        box-shadow: 0 0 20px rgba(100,100,100,0.5);
        background: #fff;
    }
    table {
        /*font: 9px/1 'Century Gothic', sans-serif;*/
        font: 9pt 'Arial', sans-serif;
        width: 100%;
      
        margin: 0 0 5px 0;
     
      
        border-collapse: collapse;
    }
    th,
    td {
        padding: 3px 0 0 0;
        overflow: hidden;
        margin: 0;
    }
    .table-bordered tfoot tr:first-child td {
        border-top: solid 1px #000;
    }
    .table-bordered th {
        border-top: double 3px #000;
        border-bottom: solid 1px #000;
    }

    @media print {
        body {
            zoom: 100%;
        }
        #print-area {
            width: 90%;
            /*margin: 0;*/
            margin-top:50px;
            padding: 0;
            box-shadow: none;
        }
    }
</style>
</div>
    <?php
    $perusahaan=\App\Models\Perusahaan::orderby('id','desc')->first();
    ?>
	<div id="print-area">
        <div class="panel-body">
            <table style="text-align: left" style="font-family: sans-serif;   border: none;   padding: 0;   border-spacing: 0;">
                <tr valign="top">
                    {{-- <td width="10%">
                       @if(isset($perusahaan->logo))
                       @if(file_exists( public_path().'/images/logo_perusahaan/'.$perusahaan->logo ))
                       <img alt="" border=0 height=auto width=70 src="{{ asset('images/') }}/logo_perusahaan/{{$perusahaan->logo}}">
                       @else
                       <img alt="" border=0 height=70 width=70 src="{{ asset('images/') }}/no_picture.jpg">
                       @endif
                       @else
                       <img alt="" border=0 height=70 width=70 src="{{ asset('images/') }}/no_picture.jpg">
                       @endif
                   </td> --}}
                   <td width="2%"></td>
                   <td style="text-align: left">
                    <h2> <center><b>LAPORAN REKAM KASUS</b></center> </h2>
                    <center><span style="font-size: 9pt;font-weight: bold">{{isset($perusahaan->nama_perusahaan)?$perusahaan->nama_perusahaan:''}}<br/></span></center>
                        <center> <span style="font-size: 7pt">{{isset($perusahaan->alamat)?$perusahaan->alamat:''}}<br/></span></center>
                   </td>
                 
               </tr>
           </table>
           <hr>
           <br>
           <table style="font-family: sans-serif;font-size:12px;" border="1">
            <thead>
            <tr style="text-align: center;">
                <th>Tanggal</th>
                <th>Nama</th>
                <th>NIK</th>
                <th>Alamat KTP</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>
            @foreach($rekamkasus as $value)
            <tr>
                <td>{{\Carbon\Carbon::createfromformat('Y-m-d H:i:s',$value->tanggal)->format('d-m-Y')}}</td>
                <td>{{$value->nama_penduduk}}</td>
                <td>{{$value->nik}}</td>
                <td>{{$value->alamat_ktp}}</td>
                <td>{{$status}}</td>
            </tr>
            @endforeach
        </tbody>
        </table>
        </div>
	</div>
@endsection
