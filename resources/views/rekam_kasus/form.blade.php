<div class="modal-dialog modal-simple">

		{{ Form::model($rekamKasus,array('route' => array((!$rekamKasus->exists) ? 'rekam-kasus.store':'rekam-kasus.update',$rekamKasus->pk()),
	        'class'=>'modal-content','id'=>'rekam-kasus-form','method'=>(!$rekamKasus->exists) ? 'POST' : 'PUT')) }}

		<div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
      </button>
      <h4 class="modal-title" id="formModalLabel">{{ ($rekamKasus->exists?'Edit':'Tambah').' Rekam Kasus' }}</h4>
    </div>
    <div class="modal-body">
																				        {!! App\Console\Commands\Generator\Form::input('id_penduduk','hidden')->model($rekamKasus)->showHidden() !!}
{!! App\Console\Commands\Generator\Form::autocomplete('penduduk',array('value'=>$rekamKasus->exists?(isset($rekamKasus->penduduk)?$rekamKasus->penduduk->penduduk:null):null))->model(null)->show() !!}
																        {!! App\Console\Commands\Generator\Form::input('id_kategori_kasus','hidden')->model($rekamKasus)->showHidden() !!}
{!! App\Console\Commands\Generator\Form::autocomplete('kategori_kasus',array('value'=>$rekamKasus->exists?(isset($rekamKasus->kategori_kasus)?$rekamKasus->kategori_kasus->kategori_kasus:null):null))->model(null)->show() !!}
																        {!! App\Console\Commands\Generator\Form::input('tanggal','text')->model($rekamKasus)->show() !!}
																        {!! App\Console\Commands\Generator\Form::textarea( 'riwayat_perjalanan' )->model($rekamKasus)->show() !!}
																        {!! App\Console\Commands\Generator\Form::textarea( 'diagnosa' )->model($rekamKasus)->show() !!}
																        {!! App\Console\Commands\Generator\Form::textarea( 'hasil' )->model($rekamKasus)->show() !!}
																        {!! App\Console\Commands\Generator\Form::textarea( 'keterangan' )->model($rekamKasus)->show() !!}
																        {!! App\Console\Commands\Generator\Form::input('user_input','text')->model($rekamKasus)->show() !!}
																        {!! App\Console\Commands\Generator\Form::input('user_update','text')->model($rekamKasus)->show() !!}
																												<div class="col-md-12 float-right">
					<div class="text-right">
						<button class="btn btn-primary" id="simpan">Simpan</button>
					</div>
				</div>
		</div>

	    {{ Form::close() }}
</div>


<script type="text/javascript">
$(document).ready(function(){
	$('#rekam-kasus-form').formValidation({
	  framework: "bootstrap4",
	  button: {
	    selector: "#simpan",
	    disabled: "disabled"
	  },
	  icon: null,
	  fields: {
	
},
err: {
	clazz: 'invalid-feedback'
},
control: {
	// The CSS class for valid control
	valid: 'is-valid',

	// The CSS class for invalid control
	invalid: 'is-invalid'
},
row: {
	invalid: 'has-danger'
}
});
	
					var pendudukEngine = new Bloodhound({
							datumTokenizer: function(d) { return d.tokens; },
							queryTokenizer: Bloodhound.tokenizers.whitespace,
							cache: false,
							remote: {
								url: '{{ url("autocomplete/penduduk") }}?q=%QUERY',
								wildcard: "%QUERY"
							}
						});

						$("#penduduk").typeahead({
									hint: true,
									highlight: true,
									minLength: 1
							},
							{
									source: pendudukEngine.ttAdapter(),
									name: "penduduk",
									displayKey: "penduduk",
									templates: {
										suggestion: function(data){
											return Handlebars.compile([
																"<div class=\"tt-dataset\">",
																		"<div>@{{penduduk}}</div>",
																"</div>",
														].join(""))(data);
										},
											empty: [
													"<div>penduduk tidak ditemukan</div>"
											]
									}
							}).bind("typeahead:selected", function(obj, datum, name) {
								$("#id_penduduk").val(datum.id);
							}).bind("typeahead:change", function(obj, datum, name) {

							});
					
					var kategori_kasusEngine = new Bloodhound({
							datumTokenizer: function(d) { return d.tokens; },
							queryTokenizer: Bloodhound.tokenizers.whitespace,
							cache: false,
							remote: {
								url: '{{ url("autocomplete/kategori_kasus") }}?q=%QUERY',
								wildcard: "%QUERY"
							}
						});

						$("#kategori_kasus").typeahead({
									hint: true,
									highlight: true,
									minLength: 1
							},
							{
									source: kategori_kasusEngine.ttAdapter(),
									name: "kategori_kasus",
									displayKey: "kategori_kasus",
									templates: {
										suggestion: function(data){
											return Handlebars.compile([
																"<div class=\"tt-dataset\">",
																		"<div>@{{kategori_kasus}}</div>",
																"</div>",
														].join(""))(data);
										},
											empty: [
													"<div>kategori_kasus tidak ditemukan</div>"
											]
									}
							}).bind("typeahead:selected", function(obj, datum, name) {
								$("#id_kategori_kasus").val(datum.id);
							}).bind("typeahead:change", function(obj, datum, name) {

							});
					

});
</script>
