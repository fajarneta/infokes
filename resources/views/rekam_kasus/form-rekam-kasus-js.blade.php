@push('js')
<script type="text/javascript">

	var id_golongan_darah='{{isset($rekamKasus) && !empty($rekamKasus->penduduk->id_golongan_darah)?$rekamKasus->penduduk->id_golongan_darah:''}}';
	var id_agama='{{isset($rekamKasus) && !empty($rekamKasus->penduduk->id_agama)?$rekamKasus->penduduk->id_agama:''}}';
	var id_status='{{isset($rekamKasus) && !empty($rekamKasus->penduduk->id_status_pernikahan)?$rekamKasus->penduduk->id_status_pernikahan:''}}';
	var id_jk='{{isset($rekamKasus) && !empty($rekamKasus->penduduk->id_jenis_kelamin)?$rekamKasus->penduduk->id_jenis_kelamin:''}}';
	var id_pekerjaan='{{isset($rekamKasus) && !empty($rekamKasus->penduduk->id_jenis_pekerjaan)?$rekamKasus->penduduk->id_jenis_pekerjaan:''}}';
	var id_sub_kategori_kasus='{{isset($rekamKasus) && !empty($rekamKasus->id_sub_kategori_kasus)?$rekamKasus->id_sub_kategori_kasus:''}}';
	var id_kategori_kasus='{{isset($rekamKasus) && !empty($rekamKasus->sub_kategori_kasus->id_kategori_kasus)?$rekamKasus->sub_kategori_kasus->id_kategori_kasus:''}}';
	var valid;

	// preview();

	var bar = $('.bar');
	var percent = $('.percent');
	var status = $('#status');

	var btnCust = '<button type="button" class="btn btn-secondary" title="Add picture tags" ' +
	'onclick="alert(\'Call your custom code here.\')">' +
	'<i class="glyphicon glyphicon-tag"></i>' +
	'</button>';

	$("#avatar-2").fileinput({
		overwriteInitial: true,
		maxFileSize: 1500,
		showClose: false,
		showCaption: false,
		showBrowse: false,
		browseOnZoneClick: true,
		autoOrientImage:false,
		removeLabel: '',
		removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
		removeTitle: 'Cancel or reset changes',
		elErrorContainer: '#kv-avatar-errors-2',
		msgErrorClass: 'alert alert-block alert-danger',
		defaultPreviewContent: '<img src="{{ asset('images/') }}/no_photos.png" alt="Your Avatar"><h6 class="text-muted">Click to select</h6>',
		layoutTemplates: {main2: '{preview} ' + ' {remove} {browse}'},
		allowedFileExtensions: ["jpg", "png"],
	});


// start tgl
	 $('.datepicker').datepicker({
    // placement: 'button',
	format: "dd-mm-yyyy",
    align: 'left',
    autoclose: true,
	}).on('change', function (ev) {
			hitungUmur();
	});

	$('.datepicker2').datepicker({
    // placement: 'button',
	format: "dd-mm-yyyy",
    align: 'left',
    autoclose: true,
	}).on('change', function (ev) {
		
	});

	function hitungUmur(){
        //console.log(tgl_lahir);
        var tgl_lahir = $('#tanggal_lahir').val();
        var cek = Date.parse(tgl_lahir);
        if(cek){
            var tanggal= tgl_lahir;
        }else {
            var tanggal= $('#tanggal_lahir').val();
        }

        var now=new Date();
        var day =now.getUTCDate();
        var month =now.getUTCMonth()+1;
        var year =now.getYear()+1900;
        var elem = tanggal.split('-');
        var tahun = elem[2];
        var bulan = elem[1];
        var hari  = elem[0];
        if((tahun==0 && bulan==0 && tahun==0)|| tanggal==null || tanggal=='' || (tanggal == day+'/'+month+'/'+year) ){
            $('#usia').val(0);
            $('#bulan').val(0);
            $('#hari').val(0);

            return false;
        }

        tahun=year-tahun;
        bulan=month-bulan;
        hari=day-hari;

        var jumlahHari;
        var bulanTemp=(month==1)?12:month-1;
        if(bulanTemp==1 || bulanTemp==3 || bulanTemp==5 || bulanTemp==7 || bulanTemp==8 || bulanTemp==10 || bulanTemp==12){
            jumlahHari=31;
        }else if(bulanTemp==2){
            if(tahun % 4==0)
                jumlahHari=29;
            else
                jumlahHari=28;
        }else{
            jumlahHari=30;
        }
        // console.log(bulan);

        if(hari<0){
            hari+=jumlahHari;
            bulan--;
        }
        if(bulan<0 || (bulan==0 && tahun!=0)){
            bulan+=12;
            tahun--;
        }
        if(bulan == 12){
            bulan = 0;
            tahun += 1;
        }
        $('#usia').val(tahun);
        $('#bulan').val(bulan);
        $('#hari').val(hari);

    }
 // end tgl








	$(document).ready(function(){

		$('.pendidikan').select2({
			disabled: true,
			dropdownParent: $("#form"),
            width: '100%'
		});


		$('#edit_button').click(function(){
			$('.upload').fadeIn();
			$('.preview').fadeOut();
		})

		$('#hapus_button').click(function(){
			// $('.upload').fadeIn();
         	// $('.preview').fadeOut();
         	var con=confirm('Apakah anda yakin akan menghapus file ini?');
         	if(con==true)
         	{
         		$('.ajax-loader').fadeIn();
         		$('#loader').css('width','100%');
         		$.getJSON("{{url('penduduk/delete-foto')}}", function(result){
         			// $.each(result, function(i, data){
         				console.log(result);
         				if(result.status==true)
         				{
         					toastr.success(result.msg,'');
         					$('#avatar-2').fileinput('reset');
         					// $('#avatar-3').fileinput('getPreview');
         					$('.file-preview-image').attr('src', '');
         					$('.upload').fadeIn();
         					$('.preview').fadeOut();
         					$('.ajax-loader').fadeOut();
         					preview('');

         				}
         			// });
         		});
         	}
         	else
         	{
         		return false;
         	}

         })

		$('#avatar-2').change(function() {
		// console.log(this.files[0]);
		var formData = new FormData();
		formData.append('select_file', this.files[0]);
		formData.append('_token', '{{csrf_token()}}');
		console.log(formData);
		$('#progress_upload').fadeIn();
		$.ajax({
			url: '{{url('penduduk/upload-foto')}}',
			type: 'POST',
			data: formData,
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
         //Options to tell JQuery not to process data or worry about content-type
         cache: false,
         contentType: false,
         processData: false,
         xhr: function () {
         	var xhr = new window.XMLHttpRequest();
         	xhr.upload.addEventListener("progress",
         		uploadProgressHandler,
         		false
         		);
         	xhr.addEventListener("load", loadHandler, false);
         	xhr.addEventListener("error", errorHandler, false);
         	xhr.addEventListener("abort", abortHandler, false);

         	return xhr;
         },
         success:function(data){
         	console.log(data.fail);
         	if(data.fail==false)
         	{
         		toastr.success('File Berhasil Diupload','');
         		$('#avatar-2').fileinput('reset');
         	// $('#avatar-3').fileinput('getPreview');
         	$('.file-preview-image').attr('src', '{{ asset('images/') }}/profil/'+data.filename);
         	// $("#avatar-3").fileinput('refresh',
          //                {
          //                	overwriteInitial: true,
          //                    initialPreview:
          //                    [
          //                        '<img src="{{ asset('images/') }}/profil/'+data.filename+'" class="file-preview-image kv-preview-data">'
          //                    ],
          //                    showUpload:false
          //                }
          //                );

          $('.upload').fadeOut();
          $('.preview').fadeIn();
          preview(data.filename);
      }
      else
      {
      	toastr.error(data.errors,'');
      }

  },
  error:function (xhr, status, error){
  	alert(xhr.responseText);
  },
});
	});

		$('#form').find('#checkbox_alamat_domisili').change(function(){
			if($(this).is(':checked'))
			{
      // console.log('aaaa');
      $('#alamat_domisili').val($('#alamat_ktp').val());
      $('#provinsi_domisili').val($('#provinsi_ktp').val()).trigger('change');
      // $('#kabupaten_domisili').val($('#kabupaten_ktp').val());
      // $('#kecamatan_domisili').val($('#kecamatan_ktp').val());
      // $('#kelurahan_domisili').val($('#kelurahan_ktp').val());
      kabupaten1($('#provinsi_ktp').val());
      kecamatan1($('#kabupaten_ktp').val());
      kelurahan1($('#kecamatan_ktp').val());
  }
  else{
  	$('#alamat_domisili').val('');
  	$('#provinsi_domisili').val('').trigger('change');
  	domisili_reset();
  }
});


		$('#form').find('#simpan').on('click',function(e){
			e.preventDefault();
			validasi_simpan();
      console.log(valid);
			// console.log($("input[name='jenis_kelamin']:checked").val());
			if(valid==true)
			{
				// console.log('true');
				var formData = new FormData();
				formData.append('nama', $('#nama').val());
				formData.append('nik', $('#nik').val());
		
				formData.append('tempat_lahir', $('#tempat_lahir').val());
				formData.append('tanggal_lahir', $('#tanggal_lahir').val());
				formData.append('golongan_darah', $('#golongan_darah').val());
        formData.append('jenis_kelamin', $('#jenis_kelamin').val());
				// formData.append('jk',$("input[name='jenis_kelamin']:checked").val());
				formData.append('agama', $('#agama').val());
				formData.append('status_pernikahan', $('#status_pernikahan').val());
        formData.append('pekerjaan', $('#pekerjaan').val());
				formData.append('alamat_ktp', $('#alamat_ktp').val());
				formData.append('provinsi_ktp', $('#provinsi_ktp').val());
				formData.append('kabupaten_ktp', $('#kabupaten_ktp').val());
				formData.append('kecamatan_ktp', $('#kecamatan_ktp').val());
				formData.append('kelurahan_ktp', $('#kelurahan_ktp').val());
				formData.append('alamat_domisili', $('#alamat_domisili').val());
				formData.append('provinsi_domisili', $('#provinsi_domisili').val());
				formData.append('kabupaten_domisili', $('#kabupaten_domisili').val());
				formData.append('kecamatan_domisili', $('#kecamatan_domisili').val());
				formData.append('kelurahan_domisili', $('#kelurahan_domisili').val());
				formData.append('kategori_kasus', $('#kategori_kasus').val());
				formData.append('sub_kategori_kasus', $('#sub_kategori_kasus').val());
				formData.append('tanggal', $('#tanggal').val());
				formData.append('diagnosa', $('#diagnosa').val());
				formData.append('hasil', $('#hasil').val());
				formData.append('riwayat_perjalanan', $('#riwayat_perjalanan').val());
				formData.append('keterangan', $('#keterangan').val());
				formData.append('usia', $('#usia').val());
				formData.append('id_rekam_kasus', $('#id_rekam_kasus').val());
				// formData.append('no_hp', $('#no_hp').val());

				formData.append('_token', '{{csrf_token()}}');
			$('.ajax-loader').fadeIn();
			$.ajax({
				url: '{{url('rekam-kasus/simpan-kasus')}}',
				type: 'POST',
				data: formData,
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				cache: false,
				contentType: false,
				processData: false,
				xhr: function () {
					var xhr = new window.XMLHttpRequest();
					xhr.upload.addEventListener("progress",
						uploadProgressHandler,
						false
						);
					xhr.addEventListener("load", loadHandler, false);
					xhr.addEventListener("error", errorHandler, false);
					xhr.addEventListener("abort", abortHandler, false);

					return xhr;
				},
				success:function(data){
					if(data.status==true)
					{
            $('.ajax-loader').fadeOut();
						toastr.success(data.msg,'');
						setTimeout(function(){
							// location.reload();
							location.href="{{url('/rekam-kasus')}}";
						}, 1000);

					}
					else
					{
            $('.ajax-loader').fadeOut();
						toastr.warning(data.msg,'');
					}

				},
				error:function (xhr, status, error){
					alert(xhr.responseText);
				},
			});
		}
		else{
			toastr.error('Silahkan cek kembali isian data anda','');
			return false;
		}
	})

	})

	$(window).on('load',function(){


		$.ajax({
			url : "{{url('dropdown/golonganDarah')}}",
			type: "GET",
			dataType: "JSON",
			success: function(data){
          // console.log(data);
          if(data.length>0)
          {
          	$('#golongan_darah').empty();
          	$('#golongan_darah').append('<option value="">-Pilih-</option>');
          	$.each(data,function(key,value){
          		if(id_golongan_darah==value.id){
          			$('#golongan_darah').append('<option value="'+value.id+'" selected>'+value.golongan_darah+'</option>').trigger('change');
          		}
          		else
          		{
          			$('#golongan_darah').append('<option value="'+value.id+'">'+value.golongan_darah+'</option>')
          		}
          	});
          }
          else{
          	$('#golongan_darah').empty();
          	$('#golongan_darah').append('<option value=""selected>-Data is empty-</option>');
          }
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
      	alert('Error get data from ajax');
      }
  });

		$.ajax({
			url : "{{url('dropdown/agama')}}",
			type: "GET",
			dataType: "JSON",
			success: function(data){
          // console.log(data);
          if(data.length>0)
          {
          	$('#agama').empty();
          	$('#agama').append('<option value="">-Pilih-</option>');
          	$.each(data,function(key,value){
          		if(id_agama==value.id){
          			$('#agama').append('<option value="'+value.id+'" selected>'+value.agama+'</option>').trigger('change');
          		}
          		else
          		{
          			$('#agama').append('<option value="'+value.id+'">'+value.agama+'</option>')
          		}
          	});
          }
          else{
          	$('#agama').empty();
          	$('#agama').append('<option value=""selected>-Data is empty-</option>');
          }
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
      	alert('Error get data from ajax');
      }
  });

  $.ajax({
			url : "{{url('dropdown/kategoriKasus')}}",
			type: "GET",
			dataType: "JSON",
			success: function(data){
          // console.log(data);
          if(data.length>0)
          {
          	$('#kategori_kasus').empty();
          	$('#kategori_kasus').append('<option value="">-Pilih-</option>');
          	$.each(data,function(key,value){
          		if(id_kategori_kasus==value.id){
          			$('#kategori_kasus').append('<option value="'+value.id+'" selected>'+value.kategori_kasus+'</option>').trigger('change');
          		}
          		else
          		{
          			$('#kategori_kasus').append('<option value="'+value.id+'">'+value.kategori_kasus+'</option>')
          		}
          	});
          }
          else{
          	$('#kategori_kasus').empty();
          	$('#kategori_kasus').append('<option value=""selected>-Data is empty-</option>');
          }
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
      	alert('Error get data from ajax');
	  }

  });
  $('#kategori_kasus').change(function(){
      var id_kategori_kasus=$('#kategori_kasus').val();
      if(id_kategori_kasus!=='')
      {
        $('#kategori_kasus_a').text('');
        $('#simpan').removeClass('disabled');
        subkategorikasus(id_kategori_kasus)
      }
      if(id_kategori_kasus=='')
      {
        $('#id_kategori_kasus').empty();
        $('#id_kategori_kasus').append('<option value=""selected>-Data is empty-</option>');
 
      }
    });
		 $.ajax({
		 	url : "{{url('dropdown/statusPernikahan')}}",
		 	type: "GET",
		 	dataType: "JSON",
		 	success: function(data){
          // console.log(data);
          if(data.length>0)
          {
          	$('#status_pernikahan').empty();
          	$('#status_pernikahan').append('<option value="">-Pilih-</option>');
          	$.each(data,function(key,value){
          		if(id_status==value.id){
          			$('#status_pernikahan').append('<option value="'+value.id+'" selected>'+value.status_pernikahan+'</option>').trigger('change');
          		}
          		else
          		{
          			$('#status_pernikahan').append('<option value="'+value.id+'">'+value.status_pernikahan+'</option>')
          		}
          	});
          }
          else{
          	$('#status_pernikahan').empty();
          	$('#status_pernikahan').append('<option value=""selected>-Data is empty-</option>');
          }
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
      	alert('Error get data from ajax');
      }
  });

		  $.ajax({
		 	url : "{{url('dropdown/pekerjaan')}}",
		 	type: "GET",
		 	dataType: "JSON",
		 	success: function(data){
          // console.log(data);
          if(data.length>0)
          {
          	$('#pekerjaan').empty();
          	$('#pekerjaan').append('<option value="">-Pilih-</option>');
          	$.each(data,function(key,value){
          		if(id_pekerjaan==value.id){
          			$('#pekerjaan').append('<option value="'+value.id+'" selected>'+value.profesi+'</option>').trigger('change');
          		}
          		else
          		{
          			$('#pekerjaan').append('<option value="'+value.id+'">'+value.profesi+'</option>')
          		}
          	});
          }
          else{
          	$('#pekerjaan').empty();
          	$('#pekerjaan').append('<option value=""selected>-Data is empty-</option>');
          }
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
      	alert('Error get data from ajax');
      }
  });

        $.ajax({
      url : "{{url('dropdown/jenisKelamin')}}",
      type: "GET",
      dataType: "JSON",
      success: function(data){
          // console.log(data);
          if(data.length>0)
          {
            $('#jenis_kelamin').empty();
            $('#jenis_kelamin').append('<option value="">-Pilih-</option>');
            $.each(data,function(key,value){
              if(id_jk==value.id){
                $('#jenis_kelamin').append('<option value="'+value.id+'" selected>'+value.jenis_kelamin+'</option>').trigger('change');
              }
              else
              {
                $('#jenis_kelamin').append('<option value="'+value.id+'">'+value.jenis_kelamin+'</option>')
              }
            });
          }
          else{
            $('#jenis_kelamin').empty();
            $('#jenis_kelamin').append('<option value=""selected>-Data is empty-</option>');
          }
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        alert('Error get data from ajax');
      }
  });

		 $.ajax({
      url : "{{url('dropdown/provinsi')}}",
      type: "GET",
      dataType: "JSON",
      success: function(data){
          // console.log(data);
          if(data.length>0)
          {
            $('#provinsi_ktp').empty();
            $('#provinsi_ktp').append('<option value="">-Pilih-</option>');
            $.each(data,function(key,value){
              // console.log(value.id);
              if($('#id_provinsi').val()==value.id){
                $('#provinsi_ktp').append('<option value="'+value.id+'" selected>'+value.provinsi+'</option>').change();
              }
              else
              {
                $('#provinsi_ktp').append('<option value="'+value.id+'">'+value.provinsi+'</option>');
              }
            });
          }
          else{
            $('#provinsi_ktp').empty();
            $('#provinsi_ktp').append('<option value=""selected>-Data is empty-</option>');
          }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
          alert('Error get data from ajax');
        }
      });

		  $('#provinsi_ktp').change(function(){
      var id_prov=$('#provinsi_ktp').val();
      if(id_prov!=='')
      {
        $('#provinsi_ktp_a').text('');
        $('#simpan').removeClass('disabled');
        kabupaten(id_prov)
      }
      if(id_prov=='')
      {
        $('#kabupaten_ktp').empty();
        $('#kabupaten_ktp').append('<option value=""selected>-Data is empty-</option>');
        $('#kecamatan_ktp').empty();
        $('#kecamatan_ktp').append('<option value=""selected>-Data is empty-</option>');
        $('#kelurahan_ktp').empty();
        $('#kelurahan_ktp').append('<option value=""selected>-Data is empty-</option>');
      }
    });

	function subkategorikasus(id)
    {
      var id=id;
          // alert('cccp');
          $.ajax({
            url : "{{url('dropdown/SubKategoriKasus')}}",
            type: "GET",
            dataType: "JSON",
            data:{id:id},
            success: function(data){
              $.each(data,function(key,value){
                if(value.length>0)
                {
                  $('#sub_kategori_kasus').empty();
                  $('#sub_kategori_kasus').append('<option value="" selected>-Pilih-</option>');
                  value.forEach(function(data) {
                          // alert
                          if($('#id_sub_kategori_kasus').val()==data.id){
                              // alert('aaaa');
                              $('#sub_kategori_kasus').append('<option value="'+data.id+'" selected>'+data.sub_kategori_kasus+'</option>').trigger('change');
                            }
                            else{
                              $('#sub_kategori_kasus').append('<option value="'+data.id+'">'+data.sub_kategori_kasus+'</option>');
                            }

                          });
                }
                else
                {
                  $('#sub_kategori_kasus').empty();
                  $('#sub_kategori_kasus').append('<option value=""selected>-Data is empty-</option>');
                }

              });
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
              alert('Error get data from ajax');
            }
          });
        }
    function kabupaten(id)
    {
      var id=id;
          // alert('cccp');
          $.ajax({
            url : "{{url('dropdown/kabupaten')}}",
            type: "GET",
            dataType: "JSON",
            data:{id:id},
            success: function(data){
          // console.log(data.length);
              // console.log('aaaa');
              $.each(data,function(key,value){
                // console.log(value.length);
                if(value.length>0)
                {
                  $('#kabupaten_ktp').empty();
                  $('#kabupaten_ktp').append('<option value="" selected>-Pilih-</option>');
                  value.forEach(function(data) {
                          // alert
                          if($('#id_kabupaten').val()==data.id){
                              // alert('aaaa');
                              $('#kabupaten_ktp').append('<option value="'+data.id+'" selected>'+data.kabupaten+'</option>').trigger('change');
                            }
                            else{
                              $('#kabupaten_ktp').append('<option value="'+data.id+'">'+data.kabupaten+'</option>');
                            }

                          });
                }
                else
                {
                  $('#kabupaten_ktp').empty();
                  $('#kabupaten_ktp').append('<option value=""selected>-Data is empty-</option>');
                }

              });
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
              alert('Error get data from ajax');
            }
          });
        }

        $('#kabupaten_ktp').change(function(){
          var id_kab=$('#kabupaten_ktp').val();
          if(id_kab!=='')
          {
            $('#kabupaten_ktp_a').text('');
            $('#simpan').removeClass('disabled');
            kecamatan(id_kab)
          }
          if(id_kab=='')
          {
            $('#kecamatan_ktp').empty();
            $('#kecamatan_ktp').append('<option value=""selected>-Data is empty-</option>');
            $('#kelurahan_ktp').empty();
            $('#kelurahan_ktp').append('<option value=""selected>-Data is empty-</option>');
          }
        });

        function kecamatan(id)
        {
          var id=id;
          // alert('cccp');
          $.ajax({
            url : "{{url('dropdown/kecamatan')}}",
            type: "GET",
            dataType: "JSON",
            data:{id:id},
            success: function(data){
          // console.log(data.length);
              // console.log('aaaa');
              $.each(data,function(key,value){
                // console.log(value.length);
                if(value.length>0)
                {
                  $('#kecamatan_ktp').empty();
                  $('#kecamatan_ktp').append('<option value="" selected>-Pilih-</option>');
                  value.forEach(function(data) {
                          // alertc
                          // alert(data.id);
                          if($('#id_kecamatan').val()==data.id){
                              // alert('aaaa');
                              $('#kecamatan_ktp').append('<option value="'+data.id+'" selected>'+data.kecamatan+'</option>').trigger('change');
                            }
                            else{
                              $('#kecamatan_ktp').append('<option value="'+data.id+'">'+data.kecamatan+'</option>');
                            }

                          });
                }
                else
                {
                  $('#kecamatan_ktp').empty();
                  $('#kecamatan_ktp').append('<option value=""selected>-Data is empty-</option>');
                }

              });
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
              alert('Error get data from ajax');
            }
          });

        }

        $('#kecamatan_ktp').change(function(){
          var id_kec=$('#kecamatan_ktp').val();
          if(id_kec!=='')
          {
            $('#kecamatan_ktp_a').text('');
            $('#simpan').removeClass('disabled');
            kelurahan(id_kec)
          }
          if(id_kec=='')
          {
            $('#kelurahan_ktp').empty();
            $('#kelurahan_ktp').append('<option value=""selected>-Data is empty-</option>');
          }
        });

        function kelurahan(id)
        {
          var id=id;
          // alert('cccp');
          $.ajax({
            url : "{{url('dropdown/kelurahan')}}",
            type: "GET",
            dataType: "JSON",
            data:{id:id},
            success: function(data){
          // console.log(data.length);
              // console.log('aaaa');
              $.each(data,function(key,value){
                // console.log(value.length);
                if(value.length>0)
                {
                  $('#kelurahan_ktp').empty();
                  $('#kelurahan_ktp').append('<option value="" selected>-Pilih-</option>');
                  value.forEach(function(data) {
                          // alertc
                          // alert(data.id);
                          if($('#id_kelurahan').val()==data.id){
                              // alert('aaaa');
                              $('#kelurahan_ktp').append('<option value="'+data.id+'" selected>'+data.kelurahan+'</option>').trigger('change');
                            }
                            else{
                              $('#kelurahan_ktp').append('<option value="'+data.id+'">'+data.kelurahan+'</option>');
                            }

                          });
                }
                else
                {
                  $('#kelurahan_ktp').empty();
                  $('#kelurahan_ktp').append('<option value=""selected>-Data is empty-</option>');
                }

              });
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
              alert('Error get data from ajax');
            }
          });

        }

        $.ajax({
          url : "{{url('dropdown/provinsi')}}",
          type: "GET",
          dataType: "JSON",
          success: function(data){
          // console.log(data);
          if(data.length>0)
          {
            $('#provinsi_domisili').empty();
            $('#provinsi_domisili').append('<option value="">-Pilih-</option>');
            $.each(data,function(key,value){
              // console.log(value.id);
              if($('#id_provinsi1').val()==value.id){
                $('#provinsi_domisili').append('<option value="'+value.id+'" selected>'+value.provinsi+'</option>').change();
              }
              else
              {
                $('#provinsi_domisili').append('<option value="'+value.id+'">'+value.provinsi+'</option>');
              }
            });
          }
          else{
            $('#provinsi_domisili').empty();
            $('#provinsi_domisili').append('<option value=""selected>-Data is empty-</option>');
          }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
          alert('Error get data from ajax');
        }
      });

        $('#provinsi_domisili').change(function(){
          var id_prov=$('#provinsi_domisili').val();
          if(id_prov!=='')
          {
            $('#provinsi_domisili_a').text('');
            $('#simpan').removeClass('disabled');
            kabupaten1(id_prov)
          }
          if(id_prov=='')
          {
            $('#kabupaten_domisili').empty();
            $('#kabupaten_domisili').append('<option value=""selected>-Data is empty-</option>');
            $('#kecamatan_domisili').empty();
            $('#kecamatan_domisili').append('<option value=""selected>-Data is empty-</option>');
            $('#kelurahan_domisili').empty();
            $('#kelurahan_domisili').append('<option value=""selected>-Data is empty-</option>');
          }
        });



        $('#kabupaten_domisili').change(function(){
          var id_kab=$('#kabupaten_domisili').val();
          if(id_kab!=='')
          {
            $('#kabupaten_domisili_a').text('');
            $('#simpan').removeClass('disabled');
            kecamatan1(id_kab)
          }
          if(id_kab=='')
          {
            $('#kecamatan_domisili').empty();
            $('#kecamatan_domisili').append('<option value=""selected>-Data is empty-</option>');
            $('#kelurahan_domisili').empty();
            $('#kelurahan_domisili').append('<option value=""selected>-Data is empty-</option>');
          }
        });



        $('#kecamatan_domisili').change(function(){
          var id_kec=$('#kecamatan_domisili').val();
          if(id_kec!=='')
          {
            $('#kecamatan_domisili_a').text('');
            $('#simpan').removeClass('disabled');
            kelurahan1(id_kec)
          }
          if(id_kec=='')
          {
            $('#kelurahan_domisili').empty();
            $('#kelurahan_domisili').append('<option value=""selected>-Data is empty-</option>');
          }
        });


		var foto='';
		// console.log(foto);

		switch(foto)
		{
			case '':
			break;
			case null:
			break;
			default:
			$('.upload').fadeOut();
			$('.preview').fadeIn();
			preview(foto);
		}

		validasi();
	})


	function preview(file)
	{
		console.log(file);
		$("#avatar-3").fileinput({
			overwriteInitial: true,
			initialPreview: [
        // IMAGE DATA
        "{{ asset('images/') }}/profil/"+file+"",
        ],
    initialPreviewAsData: true, // identify if you are sending preview data only and not the raw markup
    initialPreviewFileType: 'image', // image is the default and can be overridden in config below
    // initialPreviewDownloadUrl: 'https://kartik-v.github.io/bootstrap-fileinput-samples/samples/{filename}', // includes the dynamic `filename` tag to be replaced for each config
    // initialPreviewConfig: [
    // {caption: "Desert.jpg", size: 827000, width: "120px", url: "/file-upload-batch/2", key: 1},
    // ],
    purifyHtml: true, // this by default purifies HTML data for preview
    uploadExtraData: {
        // img_key: "1000",
        // img_keywords: "happy, places"
    },
    showClose: false,
    showCaption: false,
    showBrowse: false,
    showRemove: false,
    browseOnZoneClick: false,
    // removeLabel: '',
    // removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
    // removeTitle: 'Cancel or reset changes',
    fileActionSettings: {
    	showDrag: false,
    	showZoom: true,
    	showUpload: false,
    },
    pluginOptions:{
    	showRemove : false,
    }
});
	}

	function uploadProgressHandler(event) {
		$("#loaded_n_total").html("Uploaded " + event.loaded + " bytes of " + event.total);
		var percent = (event.loaded / event.total) * 100;
		var progress = Math.round(percent);
		$("#percent").html(progress + "%");
		$(".progress-bar").css("width", progress + "%");
		$("#status").html(progress + "% uploaded... please wait");
	}

	function loadHandler(event) {
		$("#status").html('Upload Completed');
		setTimeout(function(){
			$('#progress_upload').fadeOut()
			$("#percent").html("0%");
			$(".progress-bar").css("width", "0%");
		}, 500);
	}

	function errorHandler(event) {
		$("#status").html("Upload Failed");
	}

	function abortHandler(event) {
		$("#status").html("Upload Aborted");
	}


	function kabupaten1(id)
	{
		var id=id;
          // alert('cccp');
          $.ajax({
          	url : "{{url('dropdown/kabupaten')}}",
          	type: "GET",
          	dataType: "JSON",
          	data:{id:id},
          	success: function(data){
          // console.log(data.length);
              // console.log('aaaa');
              $.each(data,function(key,value){
                // console.log(value.length);
                if(value.length>0)
                {
                	$('#kabupaten_domisili').empty();
                	$('#kabupaten_domisili').append('<option value="" selected>-Pilih-</option>');
                	value.forEach(function(data) {
                          // alert
                          if($('#id_kabupaten1').val()==data.id){
                              // alert('aaaa');
                              $('#kabupaten_domisili').append('<option value="'+data.id+'" selected>'+data.kabupaten+'</option>').trigger('change');
                          }
                          else if($('#checkbox_alamat_domisili').is(':checked'))
                          {
                          	if($('#kabupaten_ktp').val()==data.id){
                          		$('#kabupaten_domisili').append('<option value="'+data.id+'" selected>'+data.kabupaten+'</option>').trigger('change');
                          	}
                          }
                          else{
                          	$('#kabupaten_domisili').append('<option value="'+data.id+'">'+data.kabupaten+'</option>');
                          }

                      });
                }
                else
                {
                	$('#kabupaten_domisili').empty();
                	$('#kabupaten_domisili').append('<option value=""selected>-Data is empty-</option>');
                }

            });
          },
          error: function (jqXHR, textStatus, errorThrown)
          {
          	alert('Error get data from ajax');
          }
      });
      }

      function kecamatan1(id)
      {
      	var id=id;
          // alert('cccp');
          $.ajax({
          	url : "{{url('dropdown/kecamatan')}}",
          	type: "GET",
          	dataType: "JSON",
          	data:{id:id},
          	success: function(data){
          // console.log(data.length);
              // console.log('aaaa');
              $.each(data,function(key,value){
                // console.log(value.length);
                if(value.length>0)
                {
                	$('#kecamatan_domisili').empty();
                	$('#kecamatan_domisili').append('<option value="" selected>-Pilih-</option>');
                	value.forEach(function(data) {
                          // alertc
                          // alert(data.id);
                          if($('#id_kecamatan1').val()==data.id){
                              // alert('aaaa');
                              $('#kecamatan_domisili').append('<option value="'+data.id+'" selected>'+data.kecamatan+'</option>').trigger('change');
                          }
                          else if($('#checkbox_alamat_domisili').is(':checked'))
                          {
                          	if($('#kecamatan_ktp').val()==data.id){
                          		$('#kecamatan_domisili').append('<option value="'+data.id+'" selected>'+data.kecamatan+'</option>').trigger('change');
                          	}
                          }
                          else{
                          	$('#kecamatan_domisili').append('<option value="'+data.id+'">'+data.kecamatan+'</option>');
                          }

                      });
                }
                else
                {
                	$('#kecamatan_domisili').empty();
                	$('#kecamatan_domisili').append('<option value=""selected>-Data is empty-</option>');
                }

            });
          },
          error: function (jqXHR, textStatus, errorThrown)
          {
          	alert('Error get data from ajax');
          }
      });

      }

      function kelurahan1(id)
      {
      	var id=id;
      	console.log(id);
          // alert('cccp');
          $.ajax({
          	url : "{{url('dropdown/kelurahan')}}",
          	type: "GET",
          	dataType: "JSON",
          	data:{id:id},
          	success: function(data){
          // console.log(data.length);
              // console.log('aaaa');
              $.each(data,function(key,value){
                // console.log(value.length);
                if(value.length>0)
                {
                	$('#kelurahan_domisili').empty();
                	$('#kelurahan_domisili').append('<option value="" selected>-Pilih-</option>');
                	value.forEach(function(data) {
                          // alertc
                          // alert(data.id);
                          if($('#id_kelurahan1').val()==data.id){
                              // alert('aaaa');
                              $('#kelurahan_domisili').append('<option value="'+data.id+'" selected>'+data.kelurahan+'</option>').trigger('change');
                          }
                          else if($('#checkbox_alamat_domisili').is(':checked'))
                          {
                          	if($('#kelurahan_ktp').val()==data.id){
                          		$('#kelurahan_domisili').append('<option value="'+data.id+'" selected>'+data.kelurahan+'</option>').trigger('change');
                          	}
                          }
                          else{
                          	$('#kelurahan_domisili').append('<option value="'+data.id+'">'+data.kelurahan+'</option>');
                          }

                      });
                }
                else
                {
                	$('#kelurahan_domisili').empty();
                	$('#kelurahan_domisili').append('<option value=""selected>-Data is empty-</option>');
                }

            });
          },
          error: function (jqXHR, textStatus, errorThrown)
          {
          	alert('Error get data from ajax');
          }
      });

      }

      function domisili_reset()
      {
      	$('#provinsi_domisili').change(function(){
      		var id_prov=$('#provinsi_domisili').val();
      		if(id_prov=='')
      		{
      			$('#kabupaten_domisili').empty();
      			$('#kabupaten_domisili').append('<option value=""selected>-Data is empty-</option>');
      			$('#kecamatan_domisili').empty();
      			$('#kecamatan_domisili').append('<option value=""selected>-Data is empty-</option>');
      			$('#kelurahan_domisili').empty();
      			$('#kelurahan_domisili').append('<option value=""selected>-Data is empty-</option>');
      		}
      	});



      	$('#kabupaten_domisili').change(function(){
      		var id_kab=$('#kabupaten_domisili').val();
      		if(id_kab=='')
      		{
      			$('#kecamatan_domisili').empty();
      			$('#kecamatan_domisili').append('<option value=""selected>-Data is empty-</option>');
      			$('#kelurahan_domisili').empty();
      			$('#kelurahan_domisili').append('<option value=""selected>-Data is empty-</option>');
      		}
      	});



      	$('#kecamatan_domisili').change(function(){
      		var id_kec=$('#kecamatan_domisili').val();
      		if(id_kec=='')
      		{
      			$('#kelurahan_domisili').empty();
      			$('#kelurahan_domisili').append('<option value=""selected>-Data is empty-</option>');
      		}
      	});
      }

      function validasi()
      {
      	$("#form").find("#nama").keyup(function(){
      		if (!$.trim($(this).val())) {
      			$(this).next().fadeIn();
      			$(this).next().html("* Nama Wajib Diisi");
      			$(this).next().css({"color":"red",
      				"font-size":"11px",
      				"font-family":"sans-serif"});
      			$('#simpan').attr('disabled',true);
      			valid=false;
      		}else{
      			if($(this).val().length >100)
      			{
      				$(this).next().fadeIn();
      				$(this).next().html("* Maksimal 100 Karakter");
      				$(this).next().css({"color":"red",
      					"font-size":"11px",
      					"font-family":"sans-serif"});
      				$('#simpan').attr('disabled',true);
      				valid=false;
      			}
      			else
      			{
      				$(this).next().fadeOut();
      				$(this).css("border",'');
      				$('#simpan').attr('disabled',false);
      				valid=true;
      			}

      		}
      	});

		  $("#form").find("#no_bpjs").keyup(function(){
      		if (!$.trim($(this).val())) {
      			$(this).next().fadeIn();
      			$(this).next().html("* Nomor BPJS Wajib Diisi");
      			$(this).next().css({"color":"red",
      				"font-size":"11px",
      				"font-family":"sans-serif"});
      			$('#simpan').attr('disabled',true);
      			valid=false;
      		}else{
      			if($.isNumeric($(this).val()))
      			{
      				if($(this).val().length >=12 && $(this).val().length<=20)
      				{
      					$(this).next().fadeOut();
      					$(this).css("border",'');
      					$('#simpan').attr('disabled',false);
      					valid=true;
      				}
      				else
      				{
      					$(this).next().fadeIn();
      					$(this).next().html("* Minimal 12, Maksimal 20 Karakter");
      					$(this).next().css({"color":"red",
      						"font-size":"11px",
      						"font-family":"sans-serif"});
      					$('#simpan').attr('disabled',true);
      					valid=false;
      				}
      			}
      			else
      			{
      				$(this).next().fadeIn();
      				$(this).next().html("* Format Harus Angka");
      				$(this).next().css({"color":"red",
      					"font-size":"11px",
      					"font-family":"sans-serif"});
      				$('#simpan').attr('disabled',true);
      				valid=false;
      			}
      		}
      	});

		  $("#form").find("#no_kk").keyup(function(){
      		if (!$.trim($(this).val())) {
      			$(this).next().fadeIn();
      			$(this).next().html("* Nomor KK Wajib Diisi");
      			$(this).next().css({"color":"red",
      				"font-size":"11px",
      				"font-family":"sans-serif"});
      			$('#simpan').attr('disabled',true);
      			valid=false;
      		}else{
      			if($.isNumeric($(this).val()))
      			{
      				if($(this).val().length >=16 && $(this).val().length<=20)
      				{
      					$(this).next().fadeOut();
      					$(this).css("border",'');
      					$('#simpan').attr('disabled',false);
      					valid=true;
      				}
      				else
      				{
      					$(this).next().fadeIn();
      					$(this).next().html("* Minimal 16, Maksimal 20 Karakter");
      					$(this).next().css({"color":"red",
      						"font-size":"11px",
      						"font-family":"sans-serif"});
      					$('#simpan').attr('disabled',true);
      					valid=false;
      				}
      			}
      			else
      			{
      				$(this).next().fadeIn();
      				$(this).next().html("* Format Harus Angka");
      				$(this).next().css({"color":"red",
      					"font-size":"11px",
      					"font-family":"sans-serif"});
      				$('#simpan').attr('disabled',true);
      				valid=false;
      			}
      		}
      	});

      	$("#form").find("#nik").keyup(function(){
      		if (!$.trim($(this).val())) {
      			$(this).next().fadeIn();
      			$(this).next().html("* NIK Wajib Diisi");
      			$(this).next().css({"color":"red",
      				"font-size":"11px",
      				"font-family":"sans-serif"});
      			$('#simpan').attr('disabled',true);
      			valid=false;
      		}else{
      			if($.isNumeric($(this).val()))
      			{
      				if($(this).val().length >=16 && $(this).val().length<=20)
      				{
      					$(this).next().fadeOut();
      					$(this).css("border",'');
      					$('#simpan').attr('disabled',false);
      					valid=true;
      				}
      				else
      				{
      					$(this).next().fadeIn();
      					$(this).next().html("* Minimal 16, Maksimal 20 Karakter");
      					$(this).next().css({"color":"red",
      						"font-size":"11px",
      						"font-family":"sans-serif"});
      					$('#simpan').attr('disabled',true);
      					valid=false;
      				}
      			}
      			else
      			{
      				$(this).next().fadeIn();
      				$(this).next().html("* Format Harus Angka");
      				$(this).next().css({"color":"red",
      					"font-size":"11px",
      					"font-family":"sans-serif"});
      				$('#simpan').attr('disabled',true);
      				valid=false;
      			}
      		}
      	});

      	$("#form").find("#tempat_lahir").keyup(function(){
      		if (!$.trim($(this).val())) {
      			$(this).next().fadeIn();
      			$(this).next().html("* Tempat Lahir Wajib Diisi");
      			$(this).next().css({"color":"red",
      				"font-size":"11px",
      				"font-family":"sans-serif"});
      			$('#simpan').attr('disabled',true);
      			valid=false;
      		}else{
      			if($(this).val().length >100)
      			{
      				$(this).next().fadeIn();
      				$(this).next().html("* Maksimal 100 Karakter");
      				$(this).next().css({"color":"red",
      					"font-size":"11px",
      					"font-family":"sans-serif"});
      				$('#simpan').attr('disabled',true);
      				valid=false;
      			}
      			else
      			{
      				$(this).next().fadeOut();
      				$(this).css("border",'');
      				$('#simpan').attr('disabled',false);
      				valid=true;
      			}

      		}
      	});

      	$("#form").find("#tanggal_lahir").bind('keyup change',function(){
      		if (!$.trim($(this).val())) {
      			$(this).next().fadeIn();
      			$(this).next().html("* Tanggal Lahir Wajib Diisi");
      			$(this).next().css({"color":"red",
      				"font-size":"11px",
      				"font-family":"sans-serif"});
      			$('#simpan').attr('disabled',true);
      			valid=false;
      		}else{
      			if(isDate($(this).val())==true)
      			{
      				$(this).next().fadeOut();
      				$(this).css("border",'');
      				$('#simpan').attr('disabled',false);
      				valid=true;
      			}
      			else
      			{
      				$(this).next().fadeIn();
      				$(this).next().html("* Silahkan masukkan sesuai format dd-mm-YYYY");
      				$(this).next().css({"color":"red",
      					"font-size":"11px",
      					"font-family":"sans-serif"});
      				$('#simpan').attr('disabled',true);
      				valid=false;
      			}
      		}
      	});

      	$("#form").find("#alamat_ktp").keyup(function(){
      		if (!$.trim($(this).val())) {
      			$(this).next().fadeIn();
      			$(this).next().html("* Alamat KTP Wajib Diisi");
      			$(this).next().css({"color":"red",
      				"font-size":"11px",
      				"font-family":"sans-serif"});
      			$('#simpan').attr('disabled',true);
      			valid=false;
      		}else{
      			if($(this).val().length >100)
      			{
      				$(this).next().fadeIn();
      				$(this).next().html("* Maksimal 100 Karakter");
      				$(this).next().css({"color":"red",
      					"font-size":"11px",
      					"font-family":"sans-serif"});
      				$('#simpan').attr('disabled',true);
      				valid=false;
      			}
      			else
      			{
      				$(this).next().fadeOut();
      				$(this).css("border",'');
      				$('#simpan').attr('disabled',false);
      				valid=true;
      			}

      		}
      	});

      	$("#form").find("#provinsi_ktp").change(function(){
      		if (!$.trim($(this).val())) {
      			$(this).next().next().fadeIn();
      			$(this).next().next().html("* Provinsi KTP Wajib Diisi");
      			$(this).next().next().css({"color":"red",
      				"font-size":"11px",
      				"font-family":"sans-serif"});
      			$('#simpan').attr('disabled',true);
      			valid=false;
      		}else{
      			$(this).next().next().fadeOut();
      			$(this).css("border",'');
      			$('#simpan').attr('disabled',false);
      			valid=true;
      		}
      	});

      	$("#form").find("#kabupaten_ktp").change(function(){
      		if (!$.trim($(this).val())) {
      			$(this).next().next().fadeIn();
      			$(this).next().next().html("* Kabupaten KTP Wajib Diisi");
      			$(this).next().next().css({"color":"red",
      				"font-size":"11px",
      				"font-family":"sans-serif"});
      			$('#simpan').attr('disabled',true);
      			valid=false;
      		}else{
      			$(this).next().next().fadeOut();
      			$(this).css("border",'');
      			$('#simpan').attr('disabled',false);
      			valid=true;
      		}
      	});

      	$("#form").find("#kecamatan_ktp").change(function(){
      		if (!$.trim($(this).val())) {
      			$(this).next().next().fadeIn();
      			$(this).next().next().html("* Kecamatan KTP Wajib Diisi");
      			$(this).next().next().css({"color":"red",
      				"font-size":"11px",
      				"font-family":"sans-serif"});
      			$('#simpan').attr('disabled',true);
      			valid=false;
      		}else{
      			$(this).next().next().fadeOut();
      			$(this).css("border",'');
      			$('#simpan').attr('disabled',false);
      			valid=true;
      		}
      	});

      	$("#form").find("#kelurahan_ktp").change(function(){
      		if (!$.trim($(this).val())) {
      			$(this).next().next().fadeIn();
      			$(this).next().next().html("* Kelurahan KTP Wajib Diisi");
      			$(this).next().next().css({"color":"red",
      				"font-size":"11px",
      				"font-family":"sans-serif"});
      			$('#simpan').attr('disabled',true);
      			valid=false;
      		}else{
      			$(this).next().next().fadeOut();
      			$(this).css("border",'');
      			$('#simpan').attr('disabled',false);
      			valid=true;
      		}
      	});

      	$("#form").find("#alamat_domisili").bind('keyup change',function(){
      		if (!$.trim($(this).val())) {
      			$(this).next().fadeIn();
      			$(this).next().html("* Alamat Domisili Wajib Diisi");
      			$(this).next().css({"color":"red",
      				"font-size":"11px",
      				"font-family":"sans-serif"});
      			$('#simpan').attr('disabled',true);
      			valid=false;
      		}else{
      			if($(this).val().length >100)
      			{
      				$(this).next().fadeIn();
      				$(this).next().html("* Maksimal 100 Karakter");
      				$(this).next().css({"color":"red",
      					"font-size":"11px",
      					"font-family":"sans-serif"});
      				$('#simpan').attr('disabled',true);
      				valid=false;
      			}
      			else
      			{
      				$(this).next().fadeOut();
      				$(this).css("border",'');
      				$('#simpan').attr('disabled',false);
      				valid=true;
      			}

      		}
      	});

      	$("#form").find("#provinsi_domisili").change(function(){
      		if (!$.trim($(this).val())) {
      			$(this).next().next().fadeIn();
      			$(this).next().next().html("* Provinsi Domisili Wajib Diisi");
      			$(this).next().next().css({"color":"red",
      				"font-size":"11px",
      				"font-family":"sans-serif"});
      			$('#simpan').attr('disabled',true);
      			valid=false;
      		}else{
      			$(this).next().next().fadeOut();
      			$(this).css("border",'');
      			$('#simpan').attr('disabled',false);
      			valid=true;
      		}
      	});

      	$("#form").find("#kabupaten_domisili").change(function(){
      		if (!$.trim($(this).val())) {
      			$(this).next().next().fadeIn();
      			$(this).next().next().html("* Kabupaten Domisili Wajib Diisi");
      			$(this).next().next().css({"color":"red",
      				"font-size":"11px",
      				"font-family":"sans-serif"});
      			$('#simpan').attr('disabled',true);
      			valid=false;
      		}else{
      			$(this).next().next().fadeOut();
      			$(this).css("border",'');
      			$('#simpan').attr('disabled',false);
      			valid=true;
      		}
      	});

      	$("#form").find("#kecamatan_domisili").change(function(){
      		if (!$.trim($(this).val())) {
      			$(this).next().next().fadeIn();
      			$(this).next().next().html("* Kecamatan Domisili Wajib Diisi");
      			$(this).next().next().css({"color":"red",
      				"font-size":"11px",
      				"font-family":"sans-serif"});
      			$('#simpan').attr('disabled',true);
      			valid=false;
      		}else{
      			$(this).next().next().fadeOut();
      			$(this).css("border",'');
      			$('#simpan').attr('disabled',false);
      			valid=true;
      		}
      	});

      	$("#form").find("#kelurahan_domisili").change(function(){
      		if (!$.trim($(this).val())) {
      			$(this).next().next().fadeIn();
      			$(this).next().next().html("* Kelurahan Domisili Wajib Diisi");
      			$(this).next().next().css({"color":"red",
      				"font-size":"11px",
      				"font-family":"sans-serif"});
      			$('#simpan').attr('disabled',true);
      			valid=false;
      		}else{
      			$(this).next().next().fadeOut();
      			$(this).css("border",'');
      			$('#simpan').attr('disabled',false);
      			valid=true;
      		}
      	});

      	$("#form").find("#agama").change(function(){
      		if (!$.trim($(this).val())) {
      			$(this).next().next().fadeIn();
      			$(this).next().next().html("* Agama Wajib Diisi");
      			$(this).next().next().css({"color":"red",
      				"font-size":"11px",
      				"font-family":"sans-serif"});
      			$('#simpan').attr('disabled',true);
      			valid=false;
      		}else{
      			$(this).next().next().fadeOut();
      			$(this).css("border",'');
      			$('#simpan').attr('disabled',false);
      			valid=true;
      		}
      	});

      	$("#form").find("#status_pernikahan").change(function(){
      		if (!$.trim($(this).val())) {
      			$(this).next().next().fadeIn();
      			$(this).next().next().html("* Status Pernikahan Wajib Diisi");
      			$(this).next().next().css({"color":"red",
      				"font-size":"11px",
      				"font-family":"sans-serif"});
      			$('#simpan').attr('disabled',true);
      			valid=false;
      		}else{
      			$(this).next().next().fadeOut();
      			$(this).css("border",'');
      			$('#simpan').attr('disabled',false);
      			valid=true;
      		}
      	});

          $("#form").find("#golongan_darah").change(function(){
          if (!$.trim($(this).val())) {
            $(this).next().next().fadeIn();
            $(this).next().next().html("* Golongan Darah Wajib Diisi");
            $(this).next().next().css({"color":"red",
              "font-size":"11px",
              "font-family":"sans-serif"});
            $('#simpan').attr('disabled',true);
            valid=false;
          }else{
            $(this).next().next().fadeOut();
            $(this).css("border",'');
            $('#simpan').attr('disabled',false);
            valid=true;
          }
        });

        $("#form").find("#jenis_kelamin").change(function(){
          if (!$.trim($(this).val())) {
            $(this).next().next().fadeIn();
            $(this).next().next().html("* Jenis Kelamin Wajib Diisi");
            $(this).next().next().css({"color":"red",
              "font-size":"11px",
              "font-family":"sans-serif"});
            $('#simpan').attr('disabled',true);
            valid=false;
          }else{
            $(this).next().next().fadeOut();
            $(this).css("border",'');
            $('#simpan').attr('disabled',false);
            valid=true;
          }
        });

        $("#form").find("#pekerjaan").change(function(){
          if (!$.trim($(this).val())) {
            $(this).next().next().fadeIn();
            $(this).next().next().html("* Pekerjaan Wajib Diisi");
            $(this).next().next().css({"color":"red",
              "font-size":"11px",
              "font-family":"sans-serif"});
            $('#simpan').attr('disabled',true);
            valid=false;
          }else{
            $(this).next().next().fadeOut();
            $(this).css("border",'');
            $('#simpan').attr('disabled',false);
            valid=true;
          }
        });

      	$("#form").find("#email").keyup(function(){
      		if (!$.trim($(this).val())) {
      			$(this).next().fadeIn();
      			$(this).next().html("* Email Wajib Diisi");
      			$(this).next().css({"color":"red",
      				"font-size":"11px",
      				"font-family":"sans-serif"});
      			$('#simpan').attr('disabled',true);
      			valid=false;
      		}else{
      			if(!isEmail($(this).val()))
      			{
      				$(this).next().fadeIn();
      				$(this).next().html("* Format email example@mbi.biz.id");
      				$(this).next().css({"color":"red",
      					"font-size":"11px",
      					"font-family":"sans-serif"});
      				$('#simpan').attr('disabled',true);
      				valid=false;
      			}
      			else{
      				$(this).next().fadeOut();
      				$(this).css("border",'');
      				$('#simpan').attr('disabled',false);
      				valid=true;
      			}

      		}
      	});

      	$("#form").find("#no_hp").keyup(function(){
      		if (!$.trim($(this).val())) {
      			$(this).next().fadeIn();
      			$(this).next().html("* No HP Wajib Diisi");
      			$(this).next().css({"color":"red",
      				"font-size":"11px",
      				"font-family":"sans-serif"});
      			$('#simpan').attr('disabled',true);
      			valid=false;
      		}else{
      			if($.isNumeric($(this).val()))
      			{
      				if($(this).val().length >=10 && $(this).val().length<=16)
      				{
      					$(this).next().fadeOut();
      					$(this).css("border",'');
      					$('#simpan').attr('disabled',false);
      					valid=true;
      				}
      				else
      				{
      					$(this).next().fadeIn();
      					$(this).next().html("* Minimal 10, Maksimal 16 Karakter");
      					$(this).next().css({"color":"red",
      						"font-size":"11px",
      						"font-family":"sans-serif"});
      					$('#simpan').attr('disabled',true);
      					valid=false;
      				}
      			}
      			else
      			{
      				$(this).next().fadeIn();
      				$(this).next().html("* Format harus angka, contoh 0811xxxxxxxx");
      				$(this).next().css({"color":"red",
      					"font-size":"11px",
      					"font-family":"sans-serif"});
      				$('#simpan').attr('disabled',true);
      				valid=false;
      			}
      		}
      	});

      	$("#form").find("#hobi").keyup(function(){
      		if (!$.trim($(this).val())) {
      			$(this).next().fadeIn();
      			$(this).next().html("* Hobi Wajib Diisi");
      			$(this).next().css({"color":"red",
      				"font-size":"11px",
      				"font-family":"sans-serif"});
      			$('#simpan').attr('disabled',true);
      			valid=false;
      		}else{
      			if($(this).val().length >100)
      			{
      				$(this).next().fadeIn();
      				$(this).next().html("* Maksimal 100 Karakter");
      				$(this).next().css({"color":"red",
      					"font-size":"11px",
      					"font-family":"sans-serif"});
      				$('#simpan').attr('disabled',true);
      				valid=false;
      			}
      			else
      			{
      				$(this).next().fadeOut();
      				$(this).css("border",'');
      				$('#simpan').attr('disabled',false);
      				valid=true;
      			}

      		}
      	});


      	$("#form").find("#keahlian").keyup(function(){
      		if (!$.trim($(this).val())) {
      			$(this).next().fadeIn();
      			$(this).next().html("* Keahlian Wajib Diisi");
      			$(this).next().css({"color":"red",
      				"font-size":"11px",
      				"font-family":"sans-serif"});
      			$('#simpan').attr('disabled',true);
      			valid=false;
      		}else{
      			if($(this).val().length >100)
      			{
      				$(this).next().fadeIn();
      				$(this).next().html("* Maksimal 100 Karakter");
      				$(this).next().css({"color":"red",
      					"font-size":"11px",
      					"font-family":"sans-serif"});
      				$('#simpan').attr('disabled',true);
      				valid=false;
      			}
      			else
      			{
      				$(this).next().fadeOut();
      				$(this).css("border",'');
      				$('#simpan').attr('disabled',false);
      				valid=true;
      			}

      		}
      	});


      	$("#form").find("#sifat_positif").keyup(function(){
      		if (!$.trim($(this).val())) {
      			$(this).next().fadeIn();
      			$(this).next().html("* Sifat Positif Wajib Diisi");
      			$(this).next().css({"color":"red",
      				"font-size":"11px",
      				"font-family":"sans-serif"});
      			$('#simpan').attr('disabled',true);
      			valid=false;
      		}else{
      			if($(this).val().length >100)
      			{
      				$(this).next().fadeIn();
      				$(this).next().html("* Maksimal 100 Karakter");
      				$(this).next().css({"color":"red",
      					"font-size":"11px",
      					"font-family":"sans-serif"});
      				$('#simpan').attr('disabled',true);
      				valid=false;
      			}
      			else
      			{
      				$(this).next().fadeOut();
      				$(this).css("border",'');
      				$('#simpan').attr('disabled',false);
      				valid=true;
      			}

      		}
      	});


      	$("#form").find("#sifat_negatif").keyup(function(){
      		if (!$.trim($(this).val())) {
      			$(this).next().fadeIn();
      			$(this).next().html("* Sifat Negatif Wajib Diisi");
      			$(this).next().css({"color":"red",
      				"font-size":"11px",
      				"font-family":"sans-serif"});
      			$('#simpan').attr('disabled',true);
      			valid=false;
      		}else{
      			if($(this).val().length >100)
      			{
      				$(this).next().fadeIn();
      				$(this).next().html("* Maksimal 100 Karakter");
      				$(this).next().css({"color":"red",
      					"font-size":"11px",
      					"font-family":"sans-serif"});
      				$('#simpan').attr('disabled',true);
      				valid=false;
      			}
      			else
      			{
      				$(this).next().fadeOut();
      				$(this).css("border",'');
      				$('#simpan').attr('disabled',false);
      				valid=true;
      			}

      		}
      	});

      	$("#form").find("#nama_sekolah_sd").keyup(function(){
      		if (!$.trim($(this).val())) {
      			$(this).next().fadeIn();
      			$(this).next().html("* Nama Instansi Wajib Diisi");
      			$(this).next().css({"color":"red",
      				"font-size":"11px",
      				"font-family":"sans-serif"});
      			$('#simpan').attr('disabled',true);
      			valid=false;
      		}else{

      			$(this).next().fadeOut();
      			$(this).css("border",'');
      			$('#simpan').attr('disabled',false);
      			valid=true;
      		}
      	});

      	$("#form").find("#nama_sekolah_smp").keyup(function(){
      		if (!$.trim($(this).val())) {
      			$(this).next().fadeIn();
      			$(this).next().html("* Nama Instansi Wajib Diisi");
      			$(this).next().css({"color":"red",
      				"font-size":"11px",
      				"font-family":"sans-serif"});
      			$('#simpan').attr('disabled',true);
      			valid=false;
      		}else{

      			$(this).next().fadeOut();
      			$(this).css("border",'');
      			$('#simpan').attr('disabled',false);
      			valid=true;
      		}
      	});

      	$("#form").find("#nama_sekolah_sma").keyup(function(){
      		if (!$.trim($(this).val())) {
      			$(this).next().fadeIn();
      			$(this).next().html("* Nama Instansi Wajib Diisi");
      			$(this).next().css({"color":"red",
      				"font-size":"11px",
      				"font-family":"sans-serif"});
      			$('#simpan').attr('disabled',true);
      			valid=false;
      		}else{

      			$(this).next().fadeOut();
      			$(this).css("border",'');
      			$('#simpan').attr('disabled',false);
      			valid=true;
      		}
      	});

      	$("#form").find("#jurusan_sma").keyup(function(){
      		if (!$.trim($(this).val())) {
      			$(this).next().fadeIn();
      			$(this).next().html("* Jurusan Wajib Diisi");
      			$(this).next().css({"color":"red",
      				"font-size":"11px",
      				"font-family":"sans-serif"});
      			$('#simpan').attr('disabled',true);
      			valid=false;
      		}else{

      			$(this).next().fadeOut();
      			$(this).css("border",'');
      			$('#simpan').attr('disabled',false);
      			valid=true;
      		}
      	});

      	$("#form").find("#kota_sd").keyup(function(){
      		if (!$.trim($(this).val())) {
      			$(this).next().fadeIn();
      			$(this).next().html("* Kota Wajib Diisi");
      			$(this).next().css({"color":"red",
      				"font-size":"11px",
      				"font-family":"sans-serif"});
      			$('#simpan').attr('disabled',true);
      			valid=false;
      		}else{

      			$(this).next().fadeOut();
      			$(this).css("border",'');
      			$('#simpan').attr('disabled',false);
      			valid=true;
      		}
      	});

      	$("#form").find("#kota_smp").keyup(function(){
      		if (!$.trim($(this).val())) {
      			$(this).next().fadeIn();
      			$(this).next().html("* Kota Wajib Diisi");
      			$(this).next().css({"color":"red",
      				"font-size":"11px",
      				"font-family":"sans-serif"});
      			$('#simpan').attr('disabled',true);
      			valid=false;
      		}else{

      			$(this).next().fadeOut();
      			$(this).css("border",'');
      			$('#simpan').attr('disabled',false);
      			valid=true;
      		}
      	});

      	$("#form").find("#kota_sma").keyup(function(){
      		if (!$.trim($(this).val())) {
      			$(this).next().fadeIn();
      			$(this).next().html("* Kota Wajib Diisi");
      			$(this).next().css({"color":"red",
      				"font-size":"11px",
      				"font-family":"sans-serif"});
      			$('#simpan').attr('disabled',true);
      			valid=false;
      		}else{

      			$(this).next().fadeOut();
      			$(this).css("border",'');
      			$('#simpan').attr('disabled',false);
      			valid=true;
      		}
      	});

      	$("#form").find("#tahun_lulus_sd").keyup(function(){
      		if (!$.trim($(this).val())) {
      			$(this).next().fadeIn();
      			$(this).next().html("* Tahun Lulus Wajib Diisi");
      			$(this).next().css({"color":"red",
      				"font-size":"11px",
      				"font-family":"sans-serif"});
      			$('#simpan').attr('disabled',true);
      			valid=false;
      		}else{
      			if($.isNumeric($(this).val()))
      			{
      				if($(this).val().length >=4 && $(this).val().length<=4)
      				{
      					$(this).next().fadeOut();
      					$(this).css("border",'');
      					$('#simpan').attr('disabled',false);
      					valid=true;
      				}
      				else{
      					$(this).next().fadeIn();
      					$(this).next().html("* Tahun Lulus Maksimal 4 Karakter");
      					$(this).next().css({"color":"red",
      						"font-size":"11px",
      						"font-family":"sans-serif"});
      					$('#simpan').attr('disabled',true);
      					valid=false;
      				}
      			}
      			else{
      				$(this).next().fadeIn();
      				$(this).next().html("* Format Tahun Lulus Harus Angka, contoh 2002");
      				$(this).next().css({"color":"red",
      					"font-size":"11px",
      					"font-family":"sans-serif"});
      				$('#simpan').attr('disabled',true);
      				valid=false;
      			}
      		}
      	});

      	$("#form").find("#tahun_lulus_smp").keyup(function(){
      		if (!$.trim($(this).val())) {
      			$(this).next().fadeIn();
      			$(this).next().html("* Tahun Lulus Wajib Diisi");
      			$(this).next().css({"color":"red",
      				"font-size":"11px",
      				"font-family":"sans-serif"});
      			$('#simpan').attr('disabled',true);
      			valid=false;
      		}else{
      			if($.isNumeric($(this).val()))
      			{
      				if($(this).val().length >=4 && $(this).val().length<=4)
      				{
      					$(this).next().fadeOut();
      					$(this).css("border",'');
      					$('#simpan').attr('disabled',false);
      					valid=true;
      				}
      				else{
      					$(this).next().fadeIn();
      					$(this).next().html("* Tahun Lulus Maksimal 4 Karakter");
      					$(this).next().css({"color":"red",
      						"font-size":"11px",
      						"font-family":"sans-serif"});
      					$('#simpan').attr('disabled',true);
      					valid=false;
      				}
      			}
      			else{
      				$(this).next().fadeIn();
      				$(this).next().html("* Format Tahun Lulus Harus Angka, contoh 2002");
      				$(this).next().css({"color":"red",
      					"font-size":"11px",
      					"font-family":"sans-serif"});
      				$('#simpan').attr('disabled',true);
      				valid=false;
      			}
      		}
      	});

      	$("#form").find("#tahun_lulus_sma").keyup(function(){
      		if (!$.trim($(this).val())) {
      			$(this).next().fadeIn();
      			$(this).next().html("* Tahun Lulus Wajib Diisi");
      			$(this).next().css({"color":"red",
      				"font-size":"11px",
      				"font-family":"sans-serif"});
      			$('#simpan').attr('disabled',true);
      			valid=false;
      		}else{
      			if($.isNumeric($(this).val()))
      			{
      				if($(this).val().length >=4 && $(this).val().length<=4)
      				{
      					$(this).next().fadeOut();
      					$(this).css("border",'');
      					$('#simpan').attr('disabled',false);
      					valid=true;
      				}
      				else{
      					$(this).next().fadeIn();
      					$(this).next().html("* Tahun Lulus Maksimal 4 Karakter");
      					$(this).next().css({"color":"red",
      						"font-size":"11px",
      						"font-family":"sans-serif"});
      					$('#simpan').attr('disabled',true);
      					valid=false;
      				}
      			}
      			else{
      				$(this).next().fadeIn();
      				$(this).next().html("* Format Tahun Lulus Harus Angka, contoh 2002");
      				$(this).next().css({"color":"red",
      					"font-size":"11px",
      					"font-family":"sans-serif"});
      				$('#simpan').attr('disabled',true);
      				valid=false;
      			}
      		}
      	});

      	$("#form").find("#fail_sd").change(function(){
      		if (!$.trim($(this).val())) {
      			$(this).next().fadeIn();
      			$(this).next().html("* File Wajib Diisi");
      			$(this).next().css({"color":"red",
      				"font-size":"11px",
      				"font-family":"sans-serif"});
      			$('#simpan').attr('disabled',true);
      			valid=false;
      		}else{
      			$(this).next().fadeOut();
      			$(this).css("border",'');
      			$('#simpan').attr('disabled',false);
      			valid=true;
      		}
      	});

      	$("#form").find("#fail_smp").change(function(){
      		if (!$.trim($(this).val())) {
      			$(this).next().fadeIn();
      			$(this).next().html("* File Wajib Diisi");
      			$(this).next().css({"color":"red",
      				"font-size":"11px",
      				"font-family":"sans-serif"});
      			$('#simpan').attr('disabled',true);
      			valid=false;
      		}else{
      			$(this).next().fadeOut();
      			$(this).css("border",'');
      			$('#simpan').attr('disabled',false);
      			valid=true;
      		}
      	});

      	$("#form").find("#fail_sma").change(function(){
      		if (!$.trim($(this).val())) {
      			$(this).next().fadeIn();
      			$(this).next().html("* File Wajib Diisi");
      			$(this).next().css({"color":"red",
      				"font-size":"11px",
      				"font-family":"sans-serif"});
      			$('#simpan').attr('disabled',true);
      			valid=false;
      		}else{
      			$(this).next().fadeOut();
      			$(this).css("border",'');
      			$('#simpan').attr('disabled',false);
      			valid=true;
      		}
      	});

      	$("#form").find("#ktp_file").change(function(){
      		if (!$.trim($(this).val())) {
      			$(this).next().fadeIn();
      			$(this).next().html("* KTP File Wajib Diisi");
      			$(this).next().css({"color":"red",
      				"font-size":"11px",
      				"font-family":"sans-serif"});
      			$('#simpan').attr('disabled',true);
      			valid=false;
      		}else{
      			$(this).next().fadeOut();
      			$(this).css("border",'');
      			$('#simpan').attr('disabled',false);
      			valid=true;
      		}
      	});

      	$("#form").find("#kk_file").change(function(){
      		if (!$.trim($(this).val())) {
      			$(this).next().fadeIn();
      			$(this).next().html("* KK File Wajib Diisi");
      			$(this).next().css({"color":"red",
      				"font-size":"11px",
      				"font-family":"sans-serif"});
      			$('#simpan').attr('disabled',true);
      			valid=false;
      		}else{
      			$(this).next().fadeOut();
      			$(this).css("border",'');
      			$('#simpan').attr('disabled',false);
      			valid=true;
      		}
      	});

      	$("#form").find("#cv_file").change(function(){
      		if (!$.trim($(this).val())) {
      			$(this).next().fadeIn();
      			$(this).next().html("* CV File Wajib Diisi");
      			$(this).next().css({"color":"red",
      				"font-size":"11px",
      				"font-family":"sans-serif"});
      			$('#simpan').attr('disabled',true);
      			valid=false;
      		}else{
      			$(this).next().fadeOut();
      			$(this).css("border",'');
      			$('#simpan').attr('disabled',false);
      			valid=true;
      		}
      	});

      }

      function isEmail(email) {
      	var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
      	return regex.test(email);
      }

      function validasi_simpan()
      {
        var valid_nama=false;
        var valid_nik=false;
        var valid_no_kk=false;
        var valid_no_bpjs=false;
        var valid_tempat_lahir=false;
        var valid_tgl_lahir=false;
        var valid_agama=false;
        var valid_sp=false;
        var valid_pekerjaan=false;
        var valid_jenis_kelamin=false;
        var valid_alamat_ktp=false;
        var valid_provinsi_ktp=false;
        var valid_kabupaten_ktp=false;
        var valid_kecamatan_ktp=false;
        var valid_kelurahan_ktp=false;

         var valid_alamat_domisili=false;
        var valid_provinsi_domisili=false;
        var valid_kabupaten_domisili=false;
        var valid_kecamatan_domisili=false;
        var valid_kelurahan_domisili=false;

        var valid_nohp=false;

      	if($('#nama').val()=='')
      	{
      		$('#nama').next().fadeIn();
      		$('#nama').next().html("* Nama Wajib Diisi");
      		$('#nama').next().css({"color":"red",
      			"font-size":"11px",
      			"font-family":"sans-serif"});
      		$('#simpan').attr('disabled',true);
      		$('html, body').stop(true, true).animate({
              scrollTop: ($('.informasi').offset().top)
            },500);
      		valid_nama=false;
      	}
      	else
      	{
      		$('#nama').next().fadeOut();
      		$('#nama').css("border",'');
      		$('#simpan').attr('disabled',false);
      		valid_nama=true;
      	}

      	if($('#nik').val()=='')
      	{
      		$('#nik').next().fadeIn();
      		$('#nik').next().html("* NIK Wajib Diisi");
      		$('#nik').next().css({"color":"red",
      			"font-size":"11px",
      			"font-family":"sans-serif"});
      		$('#simpan').attr('disabled',true);
      		$('html, body').stop(true, true).animate({
              scrollTop: ($('.informasi').offset().top)
            },500);
      		valid_nik=false;
      	}
      	else
      	{
      		$('#nik').next().fadeOut();
      		$('#nik').css("border",'');
      		$('#simpan').attr('disabled',false);
      		valid_nik=true;
      	}

		  if($('#no_kk').val()=='')
      	{
      		$('#no_kk').next().fadeIn();
      		$('#no_kk').next().html("* Nomor KK Wajib Diisi");
      		$('#no_kk').next().css({"color":"red",
      			"font-size":"11px",
      			"font-family":"sans-serif"});
      		$('#simpan').attr('disabled',true);
      		$('html, body').stop(true, true).animate({
              scrollTop: ($('.informasi').offset().top)
            },500);
      		valid_no_kk=false;
      	}
      	else
      	{
      		$('#no_kk').next().fadeOut();
      		$('#no_kk').css("border",'');
      		$('#simpan').attr('disabled',false);
      		valid_no_kk=true;
      	}


		  if($('#no_bpjs').val()=='')
      	{
      		$('#no_bpjs').next().fadeIn();
      		$('#no_bpjs').next().html("* Nomor BPJS Wajib Diisi");
      		$('#no_bpjs').next().css({"color":"red",
      			"font-size":"11px",
      			"font-family":"sans-serif"});
      		$('#simpan').attr('disabled',true);
      		$('html, body').stop(true, true).animate({
              scrollTop: ($('.informasi').offset().top)
            },500);
      		valid_no_bpjs=false;
      	}
      	else
      	{
      		$('#no_bpjs').next().fadeOut();
      		$('#no_bpjs').css("border",'');
      		$('#simpan').attr('disabled',false);
      		valid_no_bpjs=true;
      	}



      	if($('#tempat_lahir').val()=='')
      	{
      		$('#tempat_lahir').next().fadeIn();
      		$('#tempat_lahir').next().html("* Tempat Lahir Wajib Diisi");
      		$('#tempat_lahir').next().css({"color":"red",
      			"font-size":"11px",
      			"font-family":"sans-serif"});
      		$('#simpan').attr('disabled',true);
      		$('html, body').stop(true, true).animate({
              scrollTop: ($('.informasi').offset().top)
            },500);
      		valid_tempat_lahir=false;
      	}
      	else
      	{
      		$('#tempat_lahir').next().fadeOut();
      		$('#tempat_lahir').css("border",'');
      		$('#simpan').attr('disabled',false);
      		valid_tempat_lahir=true;
      	}

      	if($('#tanggal_lahir').val()=='')
      	{
      		$('#tanggal_lahir').next().fadeIn();
      		$('#tanggal_lahir').next().html("* Tanggal Lahir Wajib Diisi");
      		$('#tanggal_lahir').next().css({"color":"red",
      			"font-size":"11px",
      			"font-family":"sans-serif"});
      		$('#simpan').attr('disabled',true);
      		$('html, body').stop(true, true).animate({
              scrollTop: ($('.informasi').offset().top)
            },500);
      		valid_tgl_lahir=false;
      	}
      	else
      	{
      		$('#tanggal_lahir').next().fadeOut();
      		$('#tanggal_lahir').css("border",'');
      		$('#simpan').attr('disabled',false);
      		valid_tgl_lahir=true;
      	}

      	if($('#agama').val()=='')
      	{
      		$('#agama').next().next().fadeIn();
      		$('#agama').next().next().html("* Agama Wajib Diisi");
      		$('#agama').next().next().css({"color":"red",
      			"font-size":"11px",
      			"font-family":"sans-serif"});
      		$('#simpan').attr('disabled',true);
      		$('html, body').stop(true, true).animate({
              scrollTop: ($('.informasi').offset().top)
            },500);
      		valid_agama=false;
      	}
      	else
      	{
      		$('#agama').next().next().fadeOut();
      		// $('#agama').css("border",'');
      		$('#simpan').attr('disabled',false);
      		valid_agama=true;
      	}

      	if($('#status_pernikahan').val()=='')
      	{
      		$('#status_pernikahan').next().next().fadeIn();
      		$('#status_pernikahan').next().next().html("* Status Pernikahan Wajib Diisi");
      		$('#status_pernikahan').next().next().css({"color":"red",
      			"font-size":"11px",
      			"font-family":"sans-serif"});
      		$('#simpan').attr('disabled',true);
      		$('html, body').stop(true, true).animate({
              scrollTop: ($('.informasi').offset().top)
            },500);
      		valid_sp=false;
      	}
      	else
      	{
      		$('#status_pernikahan').next().next().fadeOut();
      		// $('#status_pernikahan').css("border",'');
      		$('#simpan').attr('disabled',false);
      		valid_sp=true;
      	}

        if($('#golongan_darah').val()=='')
        {
          $('#golongan_darah').next().next().fadeIn();
          $('#golongan_darah').next().next().html("* Golongan Darah Wajib Diisi");
          $('#golongan_darah').next().next().css({"color":"red",
            "font-size":"11px",
            "font-family":"sans-serif"});
          $('#simpan').attr('disabled',true);
          $('html, body').stop(true, true).animate({
              scrollTop: ($('.informasi').offset().top)
            },500);
          valid_gd=false;
        }
        else
        {
          $('#golongan_darah').next().next().fadeOut();
          // $('#status_pernikahan').css("border",'');
          $('#simpan').attr('disabled',false);
          valid_gd=true;
        }

        if($('#jenis_kelamin').val()=='')
        {
          $('#jenis_kelamin').next().next().fadeIn();
          $('#jenis_kelamin').next().next().html("* Jenis Kelamin Wajib Diisi");
          $('#jenis_kelamin').next().next().css({"color":"red",
            "font-size":"11px",
            "font-family":"sans-serif"});
          $('#simpan').attr('disabled',true);
          $('html, body').stop(true, true).animate({
              scrollTop: ($('.informasi').offset().top)
            },500);
          valid_jenis_kelamin=false;
        }
        else
        {
          $('#jenis_kelamin').next().next().fadeOut();
          // $('#status_pernikahan').css("border",'');
          $('#simpan').attr('disabled',false);
          valid_jenis_kelamin=true;
        }

        if($('#pekerjaan').val()=='')
        {
          $('#pekerjaan').next().next().fadeIn();
          $('#pekerjaan').next().next().html("* Pekerjaan Wajib Diisi");
          $('#pekerjaan').next().next().css({"color":"red",
            "font-size":"11px",
            "font-family":"sans-serif"});
          $('#simpan').attr('disabled',true);
          $('html, body').stop(true, true).animate({
              scrollTop: ($('.informasi').offset().top)
            },500);
          valid_pekerjaan=false;
        }
        else
        {
          $('#pekerjaan').next().next().fadeOut();
          // $('#status_pernikahan').css("border",'');
          $('#simpan').attr('disabled',false);
          valid_pekerjaan=true;
        }

      	if($('#alamat_ktp').val()=='')
      	{
      		$('#alamat_ktp').next().fadeIn();
      		$('#alamat_ktp').next().html("* Alamat KTP Wajib Diisi");
      		$('#alamat_ktp').next().css({"color":"red",
      			"font-size":"11px",
      			"font-family":"sans-serif"});
      		$('#simpan').attr('disabled',true);
      		$('html, body').stop(true, true).animate({
      			scrollTop: ($('.alamat_ktp_scroll').offset().top)
      		},500);
      		valid_alamat_ktp=false;
      	}
      	else
      	{
      		$('#alamat_ktp').next().fadeOut();
      		$('#alamat_ktp').css("border",'');
      		$('#simpan').attr('disabled',false);
      		valid_alamat_ktp=true;
      	}
      	if($('#provinsi_ktp').val()=='')
      	{
      		$('#provinsi_ktp').next().next().fadeIn();
      		$('#provinsi_ktp').next().next().html("* Provinsi KTP Wajib Diisi");
      		$('#provinsi_ktp').next().next().css({"color":"red",
      			"font-size":"11px",
      			"font-family":"sans-serif"});
      		$('#simpan').attr('disabled',true);
      		$('html, body').stop(true, true).animate({
      			scrollTop: ($('.alamat_ktp_scroll').offset().top)
      		},500);
      		valid_provinsi_ktp=false;
      	}
      	else
      	{
      		$('#provinsi_ktp').next().next().fadeOut();
      		// $('#provinsi_ktp').css("border",'');
      		$('#simpan').attr('disabled',false);
      		valid_provinsi_ktp=true;
      	}
      	if($('#kabupaten_ktp').val()=='')
      	{
      		$('#kabupaten_ktp').next().next().fadeIn();
      		$('#kabupaten_ktp').next().next().html("* Kabupaten KTP Wajib Diisi");
      		$('#kabupaten_ktp').next().next().css({"color":"red",
      			"font-size":"11px",
      			"font-family":"sans-serif"});
      		$('#simpan').attr('disabled',true);
      		$('html, body').stop(true, true).animate({
      			scrollTop: ($('.alamat_ktp_scroll').offset().top)
      		},500);
      		valid_kabupaten_ktp=false;
      	}
      	else
      	{
      		$('#kabupaten_ktp').next().next().fadeOut();
      		// $('#kabupaten_ktp').css("border",'');
      		$('#simpan').attr('disabled',false);
      		valid_kabupaten_ktp=true;
      	}
      	if($('#kecamatan_ktp').val()=='')
      	{
      		$('#kecamatan_ktp').next().next().fadeIn();
      		$('#kecamatan_ktp').next().next().html("* Kecamatan KTP Wajib Diisi");
      		$('#kecamatan_ktp').next().next().css({"color":"red",
      			"font-size":"11px",
      			"font-family":"sans-serif"});
      		$('#simpan').attr('disabled',true);
      		$('html, body').stop(true, true).animate({
      			scrollTop: ($('.alamat_ktp_scroll').offset().top)
      		},500);
      		valid_kecamatan_ktp=false;
      	}
      	else
      	{
      		$('#kecamatan_ktp').next().next().fadeOut();
      		// $('#kecamatan_ktp').css("border",'');
      		$('#simpan').attr('disabled',false);
      		valid_kecamatan_ktp=true;
      	}
      	if($('#kelurahan_ktp').val()=='')
      	{
      		$('#kelurahan_ktp').next().next().fadeIn();
      		$('#kelurahan_ktp').next().next().html("* Kelurahan KTP Wajib Diisi");
      		$('#kelurahan_ktp').next().next().css({"color":"red",
      			"font-size":"11px",
      			"font-family":"sans-serif"});
      		$('#simpan').attr('disabled',true);
      		$('html, body').stop(true, true).animate({
      			scrollTop: ($('.alamat_ktp_scroll').offset().top)
      		},500);
      		valid_kelurahan_ktp=false;
      	}
      	else
      	{
      		$('#kelurahan_ktp').next().next().fadeOut();
      		// $('#kecamatan_ktp').css("border",'');
      		$('#simpan').attr('disabled',false);
      		valid_kelurahan_ktp=true;
      	}

      	if($('#alamat_domisili').val()=='')
      	{
      		$('#alamat_domisili').next().fadeIn();
      		$('#alamat_domisili').next().html("* Alamat Domisili Wajib Diisi");
      		$('#alamat_domisili').next().css({"color":"red",
      			"font-size":"11px",
      			"font-family":"sans-serif"});
      		$('#simpan').attr('disabled',true);
      		$('html, body').stop(true, true).animate({
      			scrollTop: ($('.alamat_domisili_scroll').offset().top)
      		},500);
      		valid_alamat_domisili=false;
      	}
      	else
      	{
      		$('#alamat_domisili').next().fadeOut();
      		$('#alamat_domisili').css("border",'');
      		$('#simpan').attr('disabled',false);
      		valid_alamat_domisili=true;
      	}
      	if($('#provinsi_domisili').val()=='')
      	{
      		$('#provinsi_domisili').next().next().fadeIn();
      		$('#provinsi_domisili').next().next().html("* Provinsi Domisili Wajib Diisi");
      		$('#provinsi_domisili').next().next().css({"color":"red",
      			"font-size":"11px",
      			"font-family":"sans-serif"});
      		$('#simpan').attr('disabled',true);
      		$('html, body').stop(true, true).animate({
      			scrollTop: ($('.alamat_domisili_scroll').offset().top)
      		},500);
      		valid_provinsi_domisili=false;
      	}
      	else
      	{
      		$('#provinsi_domisili').next().next().fadeOut();
      		// $('#provinsi_ktp').css("border",'');
      		$('#simpan').attr('disabled',false);
      		valid_provinsi_domisili=true;
      	}
      	if($('#kabupaten_domisili').val()=='')
      	{
      		$('#kabupaten_domisili').next().next().fadeIn();
      		$('#kabupaten_domisili').next().next().html("* Kabupaten Domisili Wajib Diisi");
      		$('#kabupaten_domisili').next().next().css({"color":"red",
      			"font-size":"11px",
      			"font-family":"sans-serif"});
      		$('#simpan').attr('disabled',true);
      		$('html, body').stop(true, true).animate({
      			scrollTop: ($('.alamat_domisili_scroll').offset().top)
      		},500);
      		valid_kabupaten_domisili=false;
      	}
      	else
      	{
      		$('#kabupaten_domisili').next().next().fadeOut();
      		// $('#kabupaten_ktp').css("border",'');
      		$('#simpan').attr('disabled',false);
      		valid_kabupaten_domisili=true;
      	}
      	if($('#kecamatan_domisili').val()=='')
      	{
      		$('#kecamatan_domisili').next().next().fadeIn();
      		$('#kecamatan_domisili').next().next().html("* Kecamatan Domisili Wajib Diisi");
      		$('#kecamatan_domisili').next().next().css({"color":"red",
      			"font-size":"11px",
      			"font-family":"sans-serif"});
      		$('#simpan').attr('disabled',true);
      		$('html, body').stop(true, true).animate({
      			scrollTop: ($('.alamat_domisili_scroll').offset().top)
      		},500);
      		valid_kecamatan_domisili=false;
      	}
      	else
      	{
      		$('#kecamatan_domisili').next().next().fadeOut();
      		// $('#kecamatan_ktp').css("border",'');
      		$('#simpan').attr('disabled',false);
      		valid_kecamatan_domisili=true;
      	}
      	if($('#kelurahan_domisili').val()=='')
      	{
      		$('#kelurahan_domisili').next().next().fadeIn();
      		$('#kelurahan_domisili').next().next().html("* Kelurahan Domisili Wajib Diisi");
      		$('#kelurahan_domisili').next().next().css({"color":"red",
      			"font-size":"11px",
      			"font-family":"sans-serif"});
      		$('#simpan').attr('disabled',true);
      		$('html, body').stop(true, true).animate({
      			scrollTop: ($('.alamat_domisili_scroll').offset().top)
      		},500);
      		valid_kelurahan_domisili=false;
      	}
      	else
      	{
      		$('#kelurahan_domisili').next().next().fadeOut();
      		// $('#kecamatan_ktp').css("border",'');
      		$('#simpan').attr('disabled',false);
      		valid_kelurahan_domisili=true;
      	}

      	if($('#no_hp').val()=='')
      	{
      		$('#no_hp').next().fadeIn();
      		$('#no_hp').next().html("* No HP Wajib Diisi");
      		$('#no_hp').next().css({"color":"red",
      			"font-size":"11px",
      			"font-family":"sans-serif"});
      		$('#simpan').attr('disabled',true);
      		$('html, body').stop(true, true).animate({
      			scrollTop: ($('.kontak').offset().top)
      		},500);
      		valid_nohp=false;
      	}
      	else
      	{
      		$('#no_hp').next().fadeOut();
      		$('#no_hp').css("border",'');
      		$('#simpan').attr('disabled',false);
      		valid_nohp=true;
      	}

        if(valid_nama==true && valid_nik==true && valid_no_kk==true && valid_no_bpjs==true && valid_tempat_lahir==true && valid_jenis_kelamin==true && valid_pekerjaan==true && valid_tgl_lahir==true && valid_agama==true && valid_sp == true  && valid_alamat_ktp == true && valid_provinsi_ktp==true && valid_kabupaten_ktp==true && valid_kecamatan_ktp==true && valid_kelurahan_ktp==true && valid_alamat_domisili==true && valid_provinsi_domisili==true && valid_kabupaten_domisili==true && valid_kecamatan_domisili==true && valid_kelurahan_domisili==true && valid_nohp==true)
        {
          valid=true;
        }
        else
        {
          valid=false;
        }
      }

      function isDate(str) {
      	var parms = str.split(/[\.\-\/]/);
      	var yyyy = parseInt(parms[2],10);
      	var mm   = parseInt(parms[1],10);
      	var dd   = parseInt(parms[0],10);
      	var date = new Date(yyyy,mm-1,dd,0,0,0,0);
      	return mm === (date.getMonth()+1) && dd === date.getDate() && yyyy === date.getFullYear();
      }




// function tanggal_lahir()
//     {
//       $('#tanggal_lahir').change(function() {
//         status = 'awal';
//         hitung_umur(status);
//       })

//     }


    // function hitung_umur(){
    //   var tanggal_ini = new Date();
    //   var tgl_akhir = $('#tanggal_lahir').val();

    //   var date_awal = parseInt(tanggal_ini.slice(0,2));
    //   var month_awal = parseInt(tanggal_ini.slice(3,5));
    //   var year_awal = parseInt(tanggal_ini.slice(6,9));

    //   var date_akhir = parseInt(tgl_akhir.slice(0,2));
    //   var month_akhir = parseInt(tgl_akhir.slice(3,5));
    //   var year_akhir = parseInt(tgl_akhir.slice(6,9));

    //   var total_hari = (date_akhir-date_awal)+1;
    //   var total_bulan = (month_akhir-month_awal);
    //   var total_tahun = (year_awal-year_akhir);


    //   $('#usia').val(total_tahun);
    // }






</script>

@endpush
