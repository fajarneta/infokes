<div class="modal-dialog modal-simple">

		{{ Form::model($bp,array('route' => array((!$bp->exists) ? 'bp.store':'bp.update',$bp->pk()),
	        'class'=>'modal-content','id'=>'bp-form','method'=>(!$bp->exists) ? 'POST' : 'PUT')) }}

		<div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
      </button>
      <h4 class="modal-title" id="formModalLabel">{{ ($bp->exists?'Edit':'Tambah').' Bps' }}</h4>
    </div>
    <div class="modal-body">
																				        {!! App\Console\Commands\Generator\Form::input('id_kelurahan','hidden')->model($bp)->showHidden() !!}
{!! App\Console\Commands\Generator\Form::autocomplete('kelurahan',array('value'=>$bp->exists?(isset($bp->kelurahan)?$bp->kelurahan->kelurahan:null):null))->model(null)->show() !!}
																        {!! App\Console\Commands\Generator\Form::input('longitude','text')->model($bp)->show() !!}
																        {!! App\Console\Commands\Generator\Form::input('latitude','text')->model($bp)->show() !!}
																												<div class="col-md-12 float-right">
					<div class="text-right">
						<button class="btn btn-primary" id="simpan">Simpan</button>
					</div>
				</div>
		</div>

	    {{ Form::close() }}
</div>


<script type="text/javascript">
$(document).ready(function(){
	$('#bp-form').formValidation({
	  framework: "bootstrap4",
	  button: {
	    selector: "#simpan",
	    disabled: "disabled"
	  },
	  icon: null,
	  fields: {
	
},
err: {
	clazz: 'invalid-feedback'
},
control: {
	// The CSS class for valid control
	valid: 'is-valid',

	// The CSS class for invalid control
	invalid: 'is-invalid'
},
row: {
	invalid: 'has-danger'
}
});
	
					var kelurahanEngine = new Bloodhound({
							datumTokenizer: function(d) { return d.tokens; },
							queryTokenizer: Bloodhound.tokenizers.whitespace,
							cache: false,
							remote: {
								url: '{{ url("autocomplete/kelurahan") }}?q=%QUERY',
								wildcard: "%QUERY"
							}
						});

						$("#kelurahan").typeahead({
									hint: true,
									highlight: true,
									minLength: 1
							},
							{
									source: kelurahanEngine.ttAdapter(),
									name: "kelurahan",
									displayKey: "kelurahan",
									templates: {
										suggestion: function(data){
											return Handlebars.compile([
																"<div class=\"tt-dataset\">",
																		"<div>@{{kelurahan}}</div>",
																"</div>",
														].join(""))(data);
										},
											empty: [
													"<div>kelurahan tidak ditemukan</div>"
											]
									}
							}).bind("typeahead:selected", function(obj, datum, name) {
								$("#id_kelurahan").val(datum.id);
							}).bind("typeahead:change", function(obj, datum, name) {

							});
					

});
</script>
