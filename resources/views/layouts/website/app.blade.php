<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
        
        
    <link rel="shortcut icon" href="{{ asset('admin_remark_base/') }}/website/assets/images/favicon.ico" type="image/x-icon">
    <link rel="icon" href="{{ asset('admin_remark_base/') }}/website/assets/images/favicon.ico" type="image/x-icon">

    <title>Infokes - Informasi Kesehatan</title>

    <!-- Bootstrap -->
    <link href="{{ asset('admin_remark_base/') }}/website/assets/css/bootstrap.min.css" rel="stylesheet">
        
    <link href="{{ asset('admin_remark_base/') }}/website/assets/css/font-awesome.min.css" rel="stylesheet">
    <link href="{{ asset('admin_remark_base/') }}/website/assets/css/magnific-popup.css" rel="stylesheet">
    <link href="{{ asset('admin_remark_base/') }}/website/assets/css/jquery-ui.css" rel="stylesheet">


    <link href="{{ asset('admin_remark_base/') }}/website/assets/css/animate.css" rel="stylesheet">
    <link href="{{ asset('admin_remark_base/') }}/website/assets/css/owl.carousel.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('admin_remark_base/') }}/global/vendor/toastr/toastr.css">
    <link rel="stylesheet" href="{{ asset('admin_remark_base/') }}/assets/examples/css/advanced/toastr.css">

    <!-- Main css -->
    <link href="{{ asset('admin_remark_base/') }}/website/assets/css/main.css" rel="stylesheet">



</head>
<body>
    <!-- Preloader -->
	<div class="preloader">
		<div class="lds-default"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
    </div><!-- /Preloader -->

    <!--Header Area-->
    <header class="header-area fixed-header white-bg"">
        <nav class="navbar navbar-expand-lg main-menu">
            <div class="container">

                <a class="navbar-brand" href="{{url('/')}}"><img src="{{ asset('admin_remark_base/') }}/website/assets/images/infokes.png" class="d-inline-block align-top" alt="" ></a>
                {{-- <h1>Logo Disini</h1> --}}
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="menu-toggle"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav m-auto">
                        <li class="nav-item"><a class="nav-link" href="{{url('/home')}}">Beranda</a></li>
                        <li class="nav-item"><a class="nav-link" href="{{url('/home')}}#covid">Covid19</a></li>
                        <li class="nav-item"><a class="nav-link" href="{{url('/home')}}#pencegahan">Pencegahan</a></li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-expanded="false">Informasi</a>
                            <ul class="dropdown-menu">
                                <li><a class="dropdown-item" href="{{url('/home')}}#covid">Apa Itu Covid19 ?</a></li>
                                <li><a class="dropdown-item" href="{{url('/home')}}#gejala">Gejala Penyakit</a></li>
                                <li><a class="dropdown-item" href="{{url('/home')}}#pencegahan">Langkah Pencegahan</a></li>
                              
                              
                            </ul>
                        </li>
                     
                    </ul>
                    <div class="header-btn justify-content-end">
                        <a href="{{url('/info-corona')}}" class="bttn-small btn-fill">Info Corona</a>
                    </div>
                </div>
            </div>
        </nav>
    </header><!--/Header Area-->
    @yield('content')
    <!--Footer Area-->
    <footer class="footer-area section-padding black-bg-2">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-10 col-lg-10 col-md-12 col-sm-12">
                    <div class="footer-logo centered mb-4">
                        <img src="{{ asset('admin_remark_base/') }}/website/assets/images/infokes.png" width="50%" alt="">
                   {{-- <h1 style="color:white;">Logo Disini </h1> --}}
                    </div>
                    <div class="footer-social">
                        <ul>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-globe"></i></a></li>
                            <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                            <li><a href="#"><i class="fa fa-youtube"></i></a></li>
                        </ul>
                    </div>
                    <div class="footer-nav">
                        <ul>
                            <li><a href="{{url('/home')}}#covid">Apa itu Covid19 ? </a></li>
                            <li><a href="{{url('/home')}}#gejala">Gejala Penyakit</a></li>
                            <li><a href="{{url('/home')}}#pencegahan">Langkah Pencegahan</a></li>
                            <li><a href="{{url('/peta-persebaran')}}">Peta Persebaran Penyakit</a></li>
                          
                            <li><a href="#">Berita Terbaru</a></li>
                            <li><a href="#">Contact</a></li>
                        </ul>
                    </div>
                    <div class="copyright">
                        <p>Copyright &copy; {{date('Y')}} All Rights Reserved</p>
                    </div>
                </div>
            </div>
        </div>
    </footer><!--/Footer Area-->



    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="{{ asset('admin_remark_base/') }}/website/assets/js/jquery-3.2.1.min.js"></script>
    <script src="{{ asset('admin_remark_base/') }}/website/assets/js/jquery-migrate.js"></script>
    <script src="{{ asset('admin_remark_base/') }}/website/assets/js/jquery-ui.js"></script>

    <script src="{{ asset('admin_remark_base/') }}/website/assets/js/popper.js"></script>
    <script src="{{ asset('admin_remark_base/') }}/website/assets/js/bootstrap.min.js"></script>
    <script src="{{ asset('admin_remark_base/') }}/website/assets/js/owl.carousel.min.js"></script>

    <script src="{{ asset('admin_remark_base/') }}/website/assets/js/magnific-popup.min.js"></script>
    <script src="{{ asset('admin_remark_base/') }}/website/assets/js/imagesloaded.pkgd.min.js"></script>
    <script src="{{ asset('admin_remark_base/') }}/website/assets/js/isotope.pkgd.min.js"></script>
    
    <script src="{{ asset('admin_remark_base/') }}/website/assets/js/waypoints.min.js"></script>
    <script src="{{ asset('admin_remark_base/') }}/website/assets/js/jquery.counterup.min.js"></script>
    <script src="{{ asset('admin_remark_base/') }}/website/assets/js/wow.min.js"></script>
    <script src="{{ asset('admin_remark_base/') }}/website/assets/js/scrollUp.min.js"></script>
    <script src="{{ asset('admin_remark_base/') }}/global/vendor/toastr/toastr.js"></script>

    <script src="{{ asset('admin_remark_base/') }}/website/assets/js/script.js"></script>
    <script src="{{ Asset('custom/dialog.js')}}" type="text/javascript"></script>
    <script>

        @if ($errors->any())
          @foreach ($errors->all() as $error)
            notification("{!! $error !!}","error");
          @endforeach
        @endif
        @if(Session::get('messageType'))
          notification("{!! Session::get('message') !!}","{!! Session::get('messageType') !!}");
          <?php
          Session::forget('messageType');
          Session::forget('message');
          ?>
        @endif
    
      
      </script>
    @yield('js')
    @stack('js')
</body>

</html>