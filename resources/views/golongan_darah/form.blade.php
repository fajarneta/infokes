<div class="modal-dialog modal-simple">

		{{ Form::model($golonganDarah,array('route' => array((!$golonganDarah->exists) ? 'golongan-darah.store':'golongan-darah.update',$golonganDarah->pk()),
	        'class'=>'modal-content','id'=>'golongan-darah-form','method'=>(!$golonganDarah->exists) ? 'POST' : 'PUT')) }}

		<div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
      </button>
      <h4 class="modal-title" id="formModalLabel">{{ ($golonganDarah->exists?'Edit':'Tambah').' Golongan Darah' }}</h4>
    </div>
    <div class="modal-body">
																				        {!! App\Console\Commands\Generator\Form::input('golongan_darah','text')->model($golonganDarah)->show() !!}
																												<div class="col-md-12 float-right">
					<div class="text-right">
						<button class="btn btn-primary" id="simpan">Simpan</button>
					</div>
				</div>
		</div>

	    {{ Form::close() }}
</div>


<script type="text/javascript">
$(document).ready(function(){
	$('#golongan-darah-form').formValidation({
	  framework: "bootstrap4",
	  button: {
	    selector: "#simpan",
	    disabled: "disabled"
	  },
	  icon: null,
	  fields: {
	golongan_darah : { validators: {
				        notEmpty: {
				          message: 'Kolom golongan_darah tidak boleh kosong'
							}
						}
					}
},
err: {
	clazz: 'invalid-feedback'
},
control: {
	// The CSS class for valid control
	valid: 'is-valid',

	// The CSS class for invalid control
	invalid: 'is-invalid'
},
row: {
	invalid: 'has-danger'
}
});
	

});
</script>
