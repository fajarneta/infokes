@extends('layouts.app')

@section('content')
<div class="page-content">
	<div class="panel">
		<div class="panel-body container-fluid">
			{{ Form::model(null,array('route' => 'config-id.store',
			'class'=>'','id'=>'form','method'=>'POST','files'=>true)) }} 
			  <input type="text" style="display: none" id="address" value="">
			<div class="row row-lg">
				<div class="col-lg-12">
					<!-- Example Tabs Left -->
					<div class="example-wrap">
						<div class="nav-tabs-vertical" data-plugin="tabs">
							<ul class="nav nav-tabs mr-25" role="tablist">
								@if(\Auth::user()->can('read-config-ids'))
								<li class="nav-item" role="presentation">
									<a class="nav-link" data-toggle="tab" href="#config"
									aria-controls="config" role="tab">Config IDS</a>
								</li>
								@endif
								@if(\Auth::user()->can('read-perusahaan-menu'))
								<li class="nav-item" role="presentation">
									<a class="nav-link" data-toggle="tab" href="#perusahaan"
									aria-controls="perusahaan" role="tab">Perusahaan</a>
								</li>
								@endif
						{{-- 		<li class="nav-item" role="presentation">
									<a class="nav-link" data-toggle="tab" href="#exampleTabsLeftThree"
									aria-controls="exampleTabsLeftThree" role="tab">Css</a>
								</li>
								<li class="nav-item" role="presentation">
									<a class="nav-link" data-toggle="tab" href="#exampleTabsLeftFour"
									aria-controls="exampleTabsLeftFour" role="tab">Javascript</a>
								</li> --}}
							</ul>
							<div class="tab-content py-15">
								<div class="tab-pane" id="config" role="tabpanel">
									@include('config_id.config')
								</div>
								<div class="tab-pane" id="perusahaan" role="tabpanel">
									@include('config_id.perusahaan')
								</div>
								{{-- <div class="tab-pane" id="exampleTabsLeftThree" role="tabpanel">
									Chrysippe rebus institutionem utrisque dixisset manus quippiam ignorare defatigatio
									doctiores, essent doctus ipsam tamquam complexiones corporisque,
									ars umbram sentiri venandi. Ipsam. Reprehenderit tantum
									debent sicine assumenda comprobavit, assumenda primos fuerit
									atomos amicorum inducitur quaedam miserum, potitur numquid
									effluere haeret ipsos consuetudine, munere putet fugiendis
									orationis quantumcumque. Perferendis attento saluti liberatione
									contra, constituam efficeret quaeso accusamus quieti petat
									rem nisi amicum.
								</div>
								<div class="tab-pane" id="exampleTabsLeftFour" role="tabpanel">
									Laudabilis. At artes audiebamus firmam discordiae potione albam ferantur, epicureum
									loquerer videretur formidinum utrisque simulent postremo,
									praesidia variari fecerit ferantur. Hominibus doctissimos
									multi, ferentur, certissimam medicorum bonum iucundius
									depravare facile. Degendae istius perfunctio quisquis ordinem
									ornatum, praeda atomi degendae animus. Mens eximiae placuit
									terrore, sollicitant efficeret audeam tantalo, vulgo laudantium
									evertitur spe meminerunt timentis populo, senserit inprobitas
									facilius referri consiliisque.
								</div> --}}
							</div>
						</div>
					</div>
					<!-- End Example Tabs Left -->
				</div>
			</div>
			<?php if(Auth::user()->roles[0]->name=='superadministrator')
			{
				$start='#config';
			}
			elseif(Auth::user()->roles[0]->name=='admin')
			{
				$start='#perusahaan';
			}
			else
			{
				$start='#pengguna';
			}

			?>
			<div class="text-right">
				<button class="btn btn-primary" id="simpan" style="display: none">Simpan</button>
			</div>
			{{ Form::close() }}
		</div>
	</div>
</div>
@endsection
@push('js')
<script type="text/javascript">
	var nama='{{$start}}';
	$(document).ready(function(){
		// let nama=$('.nav-tabs-vertical li a.nav-link.active').attr('href');
       $('#address').val(nama);
       $('[href="'+nama+'"]').click();

       if(nama=='#perusahaan')
       {
       	$('#simpan').fadeIn();
       	.
       }

        $('.nav-tabs-vertical li a.nav-link').click(function(){
            var a=$(this).attr('href');
            $('#address').val(a);
            if(a=='#config')
            {
            	$('#simpan').fadeOut();
            }
            else
            {
            	$('#simpan').fadeIn();

            	$("#logo").change(function () {
		      if(fileExtValidate(this)) { // file extension validation function
		         if(fileSizeValidate(this)) { // file size validation function
		         }   
				     }    
				 });

            $('#form').formValidation({
			framework: "bootstrap4",
			button: {
				selector: "#simpan",
				disabled: "disabled"
			},
			icon: null,
			fields: {
				nama_perusahaan : { validators: {
				        notEmpty: {
				          message: 'Kolom Nama Perusahaan tidak boleh kosong'
							},
						stringLength: {
                        	min: 1,
                        	max: 255,
                        	message: 'Kolom Nama Perusahaan minimal 1 dan maksimal 255 karakter',
                    		},
						}
					},

					alamat : { validators: {
				        notEmpty: {
				          message: 'Kolom Alamat tidak boleh kosong'
							},
						stringLength: {
                        	min: 1,
                        	max: 255,
                        	message: 'Kolom Alamat minimal 1 dan maksimal 255 karakter',
                    		},
						}
					},

					website1 : { validators: {
				        notEmpty: {
				          message: 'Kolom Website tidak boleh kosong'
							},
						stringLength: {
                        	min: 1,
                        	max: 255,
                        	message: 'Kolom Website minimal 1 dan maksimal 255 karakter',
                    		},
						}
					},

					email : { validators: {
				        notEmpty: {
				          message: 'Kolom Email tidak boleh kosong'
							},
						stringLength: {
                        	min: 1,
                        	max: 255,
                        	message: 'Kolom Email minimal 1 dan maksimal 255 karakter',
                    		},
						}
					},

					email1 : { validators: {
				        notEmpty: {
				          message: 'Kolom Prefiks tidak boleh kosong'
							},
						stringLength: {
                        	min: 1,
                        	max: 255,
                        	message: 'Kolom Prefiks minimal 1 dan maksimal 255 karakter',
                    		},
						}
					},
			},
			err: {
				clazz: 'invalid-feedback'
			},
			control: {
	// The CSS class for valid control
	valid: 'is-valid',

	// The CSS class for invalid control
	invalid: 'is-invalid'
},
row: {
	invalid: 'has-danger'
}
});
            }
         })
	})


function hapus_logo()
{
	var con=confirm('Apakah anda yakin akan menghapus file ini?');
	if(con==true)
	{
		$.getJSON("{{url('/delete-logo')}}", function(result){
         			// $.each(result, function(i, data){
         				console.log(result);
         				if(result.status==true)
         				{
         					toastr.success(result.msg,'');
         					location.reload();
							return false;
         				}
         			// });
         		});	
	}
	else
	{
		return false;
	}
}

var validExt = ".jpg, .png, .jpeg";
function fileExtValidate(fdata) {
   var filePath = fdata.value;
   var getFileExt = filePath.substring(filePath.lastIndexOf('.') + 1).toLowerCase();
   var pos = validExt.indexOf(getFileExt);
   if(pos < 0) {
      toastr.warning('Silahkan upload file ekstensi .png, .jpeg, dan .jpg','')
      // alert("This file is not allowed, please upload valid file.");
      fdata.value='';
      return false;
  } else {
      return true;
  }
}
//function for validate file size 
var maxSize = '1024';
function fileSizeValidate(fdata) 
{
   if (fdata.files && fdata.files[0]) 
   {
      var fsize = fdata.files[0].size/1024;
      if(fsize > maxSize) 
      {
         toastr.warning('Ukuran file maksimum melebihi 1024 KB. Ukuran file saat ini sebesar: '+fsize+' KB');
         fdata.value='';
         return false;
     } 
     else 
     {
        return true;
    }
  }
}
</script>
@endpush