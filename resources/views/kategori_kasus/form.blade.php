<div class="modal-dialog modal-simple">

		{{ Form::model($kategoriKasus,array('route' => array((!$kategoriKasus->exists) ? 'kategori-kasus.store':'kategori-kasus.update',$kategoriKasus->pk()),
	        'class'=>'modal-content','id'=>'kategori-kasus-form','method'=>(!$kategoriKasus->exists) ? 'POST' : 'PUT')) }}

		<div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
      </button>
      <h4 class="modal-title" id="formModalLabel">{{ ($kategoriKasus->exists?'Edit':'Tambah').' Kategori Kasus' }}</h4>
    </div>
    <div class="modal-body">
																				        {!! App\Console\Commands\Generator\Form::input('kategori_kasus','text')->model($kategoriKasus)->show() !!}
																        {!! App\Console\Commands\Generator\Form::textarea( 'keterangan' )->model($kategoriKasus)->show() !!}
																												<div class="col-md-12 float-right">
					<div class="text-right">
						<button class="btn btn-primary" id="simpan">Simpan</button>
					</div>
				</div>
		</div>

	    {{ Form::close() }}
</div>


<script type="text/javascript">
$(document).ready(function(){
	$('#kategori-kasus-form').formValidation({
	  framework: "bootstrap4",
	  button: {
	    selector: "#simpan",
	    disabled: "disabled"
	  },
	  icon: null,
	  fields: {
	kategori_kasus : { validators: {
				        notEmpty: {
				          message: 'Kolom kategori_kasus tidak boleh kosong'
							}
						}
					}
},
err: {
	clazz: 'invalid-feedback'
},
control: {
	// The CSS class for valid control
	valid: 'is-valid',

	// The CSS class for invalid control
	invalid: 'is-invalid'
},
row: {
	invalid: 'has-danger'
}
});
	

});
</script>
