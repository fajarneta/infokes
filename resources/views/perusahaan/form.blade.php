<div class="modal-dialog modal-simple">

		{{ Form::model($perusahaan,array('route' => array((!$perusahaan->exists) ? 'perusahaan.store':'perusahaan.update',$perusahaan->pk()),
	        'class'=>'modal-content','id'=>'perusahaan-form','method'=>(!$perusahaan->exists) ? 'POST' : 'PUT')) }}

		<div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
      </button>
      <h4 class="modal-title" id="formModalLabel">{{ ($perusahaan->exists?'Edit':'Tambah').' Perusahaan' }}</h4>
    </div>
    <div class="modal-body">
																				        {!! App\Console\Commands\Generator\Form::input('nama_perusahaan','text')->model($perusahaan)->show() !!}
																        {!! App\Console\Commands\Generator\Form::textarea( 'alamat' )->model($perusahaan)->show() !!}
																        {!! App\Console\Commands\Generator\Form::input('website','text')->model($perusahaan)->show() !!}
																        {!! App\Console\Commands\Generator\Form::input('email','text')->model($perusahaan)->show() !!}
																        {!! App\Console\Commands\Generator\Form::textarea( 'logo' )->model($perusahaan)->show() !!}
																												<div class="col-md-12 float-right">
					<div class="text-right">
						<button class="btn btn-primary" id="simpan">Simpan</button>
					</div>
				</div>
		</div>

	    {{ Form::close() }}
</div>


<script type="text/javascript">
$(document).ready(function(){
	$('#perusahaan-form').formValidation({
	  framework: "bootstrap4",
	  button: {
	    selector: "#simpan",
	    disabled: "disabled"
	  },
	  icon: null,
	  fields: {
	nama_perusahaan : { validators: {
				        notEmpty: {
				          message: 'Kolom nama_perusahaan tidak boleh kosong'
							}
						}
					}
},
err: {
	clazz: 'invalid-feedback'
},
control: {
	// The CSS class for valid control
	valid: 'is-valid',

	// The CSS class for invalid control
	invalid: 'is-invalid'
},
row: {
	invalid: 'has-danger'
}
});
	

});
</script>
