<div class="modal-dialog modal-simple">

		{{ Form::model($pegawai,array('route' => array((!$pegawai->exists) ? 'pegawai.store':'pegawai.update',$pegawai->pk()),
	        'class'=>'modal-content','id'=>'pegawai-form','method'=>(!$pegawai->exists) ? 'POST' : 'PUT')) }}

		<div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
      </button>
      <h4 class="modal-title" id="formModalLabel">{{ ($pegawai->exists?'Edit':'Tambah').' Pegawai' }}</h4>
    </div>
    <div class="modal-body">
																				        {!! App\Console\Commands\Generator\Form::input('nip','text')->model($pegawai)->show() !!}
																        {!! App\Console\Commands\Generator\Form::input('id_penduduk','hidden')->model($pegawai)->showHidden() !!}
{!! App\Console\Commands\Generator\Form::autocomplete('penduduk',array('value'=>$pegawai->exists?(isset($pegawai->penduduk)?$pegawai->penduduk->penduduk:null):null))->model(null)->show() !!}
																        {!! App\Console\Commands\Generator\Form::input('id_jabatan','hidden')->model($pegawai)->showHidden() !!}
{!! App\Console\Commands\Generator\Form::autocomplete('jabatan',array('value'=>$pegawai->exists?(isset($pegawai->jabatan)?$pegawai->jabatan->jabatan:null):null))->model(null)->show() !!}
																        {!! App\Console\Commands\Generator\Form::input('id_unit_kerja','hidden')->model($pegawai)->showHidden() !!}
{!! App\Console\Commands\Generator\Form::autocomplete('unit_kerja',array('value'=>$pegawai->exists?(isset($pegawai->unit_kerja)?$pegawai->unit_kerja->unit_kerja:null):null))->model(null)->show() !!}
																        {!! App\Console\Commands\Generator\Form::input('id_status_pegawai','hidden')->model($pegawai)->showHidden() !!}
{!! App\Console\Commands\Generator\Form::autocomplete('status_pegawai',array('value'=>$pegawai->exists?(isset($pegawai->status_pegawai)?$pegawai->status_pegawai->status_pegawai:null):null))->model(null)->show() !!}
																        {!! App\Console\Commands\Generator\Form::input('flag_aktif','text')->model($pegawai)->show() !!}
																        {!! App\Console\Commands\Generator\Form::input('tgl_bergabung','date')->model($pegawai)->show() !!}
																												<div class="col-md-12 float-right">
					<div class="text-right">
						<button class="btn btn-primary" id="simpan">Simpan</button>
					</div>
				</div>
		</div>

	    {{ Form::close() }}
</div>


<script type="text/javascript">
$(document).ready(function(){
	$('#pegawai-form').formValidation({
	  framework: "bootstrap4",
	  button: {
	    selector: "#simpan",
	    disabled: "disabled"
	  },
	  icon: null,
	  fields: {
	
},
err: {
	clazz: 'invalid-feedback'
},
control: {
	// The CSS class for valid control
	valid: 'is-valid',

	// The CSS class for invalid control
	invalid: 'is-invalid'
},
row: {
	invalid: 'has-danger'
}
});
	
					var pendudukEngine = new Bloodhound({
							datumTokenizer: function(d) { return d.tokens; },
							queryTokenizer: Bloodhound.tokenizers.whitespace,
							cache: false,
							remote: {
								url: '{{ url("autocomplete/penduduk") }}?q=%QUERY',
								wildcard: "%QUERY"
							}
						});

						$("#penduduk").typeahead({
									hint: true,
									highlight: true,
									minLength: 1
							},
							{
									source: pendudukEngine.ttAdapter(),
									name: "penduduk",
									displayKey: "penduduk",
									templates: {
										suggestion: function(data){
											return Handlebars.compile([
																"<div class=\"tt-dataset\">",
																		"<div>@{{penduduk}}</div>",
																"</div>",
														].join(""))(data);
										},
											empty: [
													"<div>penduduk tidak ditemukan</div>"
											]
									}
							}).bind("typeahead:selected", function(obj, datum, name) {
								$("#id_penduduk").val(datum.id);
							}).bind("typeahead:change", function(obj, datum, name) {

							});
					
					var jabatanEngine = new Bloodhound({
							datumTokenizer: function(d) { return d.tokens; },
							queryTokenizer: Bloodhound.tokenizers.whitespace,
							cache: false,
							remote: {
								url: '{{ url("autocomplete/jabatan") }}?q=%QUERY',
								wildcard: "%QUERY"
							}
						});

						$("#jabatan").typeahead({
									hint: true,
									highlight: true,
									minLength: 1
							},
							{
									source: jabatanEngine.ttAdapter(),
									name: "jabatan",
									displayKey: "jabatan",
									templates: {
										suggestion: function(data){
											return Handlebars.compile([
																"<div class=\"tt-dataset\">",
																		"<div>@{{jabatan}}</div>",
																"</div>",
														].join(""))(data);
										},
											empty: [
													"<div>jabatan tidak ditemukan</div>"
											]
									}
							}).bind("typeahead:selected", function(obj, datum, name) {
								$("#id_jabatan").val(datum.id);
							}).bind("typeahead:change", function(obj, datum, name) {

							});
					
					var unit_kerjaEngine = new Bloodhound({
							datumTokenizer: function(d) { return d.tokens; },
							queryTokenizer: Bloodhound.tokenizers.whitespace,
							cache: false,
							remote: {
								url: '{{ url("autocomplete/unit_kerja") }}?q=%QUERY',
								wildcard: "%QUERY"
							}
						});

						$("#unit_kerja").typeahead({
									hint: true,
									highlight: true,
									minLength: 1
							},
							{
									source: unit_kerjaEngine.ttAdapter(),
									name: "unit_kerja",
									displayKey: "unit_kerja",
									templates: {
										suggestion: function(data){
											return Handlebars.compile([
																"<div class=\"tt-dataset\">",
																		"<div>@{{unit_kerja}}</div>",
																"</div>",
														].join(""))(data);
										},
											empty: [
													"<div>unit_kerja tidak ditemukan</div>"
											]
									}
							}).bind("typeahead:selected", function(obj, datum, name) {
								$("#id_unit_kerja").val(datum.id);
							}).bind("typeahead:change", function(obj, datum, name) {

							});
					
					var status_pegawaiEngine = new Bloodhound({
							datumTokenizer: function(d) { return d.tokens; },
							queryTokenizer: Bloodhound.tokenizers.whitespace,
							cache: false,
							remote: {
								url: '{{ url("autocomplete/status_pegawai") }}?q=%QUERY',
								wildcard: "%QUERY"
							}
						});

						$("#status_pegawai").typeahead({
									hint: true,
									highlight: true,
									minLength: 1
							},
							{
									source: status_pegawaiEngine.ttAdapter(),
									name: "status_pegawai",
									displayKey: "status_pegawai",
									templates: {
										suggestion: function(data){
											return Handlebars.compile([
																"<div class=\"tt-dataset\">",
																		"<div>@{{status_pegawai}}</div>",
																"</div>",
														].join(""))(data);
										},
											empty: [
													"<div>status_pegawai tidak ditemukan</div>"
											]
									}
							}).bind("typeahead:selected", function(obj, datum, name) {
								$("#id_status_pegawai").val(datum.id);
							}).bind("typeahead:change", function(obj, datum, name) {

							});
					

});
</script>
