@extends('layouts.app')
@section('content')
<style>
  a.btn-outline-primary{
    border-color: #4caf50 ;
    background-color:white ;
    color:#4caf50;
  }
  a.btn-outline-primary:hover{
    border-color: #4caf50 ;
    background-color: #4caf50 ;
    color:white;
  }
  a.btn-outline-primary:active{
    border-color: #4caf50 ;
    background-color: #4caf50 ;
    color:#4caf50;
  }
  a.not{
    color: red;
  }
  label{
    font-size:9pt;
    font-family: sans-serif;
    font-weight: normal;
  }


</style>

<div class="page-header informasi" >
  <h1 class="page-title">Data Pegawai</h1>
  {{-- @include('layouts.inc.breadcrumb') --}}


    {{-- @error('nama_jurusan')
    <div class="invalid-feedback">
       {{$message}}
    </div>
    @enderror --}}

  <div class="page-header-actions">
      {{-- <a class="btn btn-block btn-primary data-modal" id="data-modal" href="#" onclick="show_modal('{{ route('pemesanan.create') }}')"
      >Tambah</a> --}}
    </div>
  </div>
  <div class="page-content">
    <!-- Panel Table Tools -->
    <div class="panel">
      <header class="panel-heading">
        <div class="form-group col-md-12">
    <div class="panel-body container-fluid">
          {{-- <div class="modal-dialog modal-simple"> --}}

            {{ Form::model($pegawai,array('route' => array((!$pegawai->exists) ? 'pegawai.store':'pegawai.store',(isset($id_pegawai)&&!empty($id_pegawai))?$id_pegawai:''),
            'class'=>'','id'=>'pegawai-form','method'=>(!$pegawai->exists) ? 'POST' : 'POST')) }}
            <input type="hidden" value="{{(isset($id_pegawai)?$id_pegawai:'')}}" class="form-control" id="id_pegawai_check" name="id_pegawai_check">
  
    <div class="col-lg-12 " >
             
                    <div class="panel-heading data-pribadi">
                      <span><h4>
                        <b>DATA PRIBADI PEGAWAI</b>
                      </h4></span>
                     
                    </div>
                    <hr/>
            
                  <label for="nama" style="color:red;"><i> Isian Dengan Tanda * Wajib Diisi </i></a></label>
                <div class="row col-lg-12" style="margin-bottom: 1.429rem;">
                  <label for="nama" class="col-form-label col-sm-2 offset-md-0">Nama Lengkap <a style="color:red;">*</a></label>
                  <div class="col-lg-4 form-group">
                    <input name="nama_lengkap" placeholder="NAMA LENGKAP" id="nama" class="form-control form-control-sm" value="{{($pegawai->exists?!empty($pegawai->penduduk->nama_lengkap)?$pegawai->penduduk->nama_lengkap:'':'')}}" type="text">
                    <span id="nama_a"></span>
                  </div>
                  <label for="alamat" class="col-form-label col-sm-2 offset-md-0">Alamat Ktp</label>
                  <div class="col-lg-4 form-group">
                    <textarea class="form-control form-control-sm" placeholder="ALAMAT" name="alamat_ktp" id="alamat_ktp" rows="2">{{($pegawai->exists?!empty($pegawai->penduduk->alamat_ktp)?$pegawai->penduduk->alamat_ktp:'':'')}}</textarea>
                    <span id="alamat_ktp_a"></span>
                  </div>
                </div>
                <div class="row col-lg-12" style="margin-bottom: 1.429rem;">
                  <label for="nama" class="col-form-label col-sm-2 offset-md-0">NIP / Nomor Induk Pegawai <a style="color:red;">*</a></label>
                  <div class="col-lg-4 form-group">
                    <input name="nip" placeholder="NIP" id="nip" class="form-control form-control-sm" tyoe="text" value="{{($pegawai->exists?!empty($pegawai->nip)?$pegawai->nip:'':'')}}">
                    <span id="nip_a"></span>
                  </div>
                  <label for="alamat" class="col-form-label col-sm-2 offset-md-0">Provinsi Ktp</label>
                  <div class="col-lg-4 form-group">
                    <div class="input-search clearfix">
                  
                       <input type="hidden" class="form-control" value="{{(isset($provinsi)?!empty($provinsi->id_provinsi)?$provinsi->id_provinsi:'':'')}}" id="id_provinsi" name="id_provinsi">
                      <select class="form-control" id="provinsi_ktp" name="provinsi_ktp" data-plugin="select2">
                        <optgroup label="Provinsi">
                        </optgroup>
                      </select>
                      <span class="help-block" id="provinsi_ktp_a"></span>
                      </div>
                    </div>
                  </div>
                <div class="row col-lg-12" style="margin-bottom: 1.429rem;">
                  <label for="nama" class="col-form-label col-sm-2 offset-md-0">NIK / Nomor Induk Kependudukan</label>
                  <div class="col-lg-4 form-group">
                    <input name="nik" placeholder="NIK" id="nik" class="form-control form-control-sm" value="{{($pegawai->exists?!empty($pegawai->penduduk->nik)?$pegawai->penduduk->nik:'':'')}}" type="text">
                    <span id="nik_a"></span>
                  </div>
                  <label for="alamat" class="col-form-label col-sm-2 offset-md-0">Kabupaten Ktp</label>
                  <div class="col-lg-4 form-group">
                    <div class="input-search clearfix">
                  
                        <input type="hidden" value="{{(isset($provinsi)?!empty($provinsi->id_kabupaten)?$provinsi->id_kabupaten:'':'')}}" class="form-control" id="id_kabupaten" name="id_kabupaten">
                        <select class="form-control" id="kabupaten_ktp" name="kabupaten_ktp" data-plugin="select2">
                          <optgroup label="Kabupaten">
                          </optgroup>
                        </select>
                    </div>
                    {{-- {!! App\Console\Commands\Generator\Form::input('id_kabupaten_ktp','hidden')->model($pegawai)->showHidden() !!} --}}
                    <span id="kabupaten_ktp_a"></span>
                  </div>
                </div>
                <div class="row col-lg-12" style="margin-bottom: 1.429rem;">
                  <label for="jenis_kelamin" class="col-form-label col-sm-2 offset-md-0">Jenis Kelamin</label>
                    <div class="col-lg-4 form-group">
         
                  <?php $jenis_kelamin=\App\Models\JenisKelamin::all()?>
                  <select class="form-control" name="id_jenis_kelamin" id="id_jenis_kelamin" data-plugin="select2">
                      <option value="">-Silahkan Pilih-</option>
                      @if(isset($jenis_kelamin) && !$jenis_kelamin->isEmpty())
                      @foreach($jenis_kelamin as $row)
                      <option value="{{ $row->id }}" {{ isset($pegawai->penduduk->id_jenis_kelamin)?($row->id==$pegawai->penduduk->id_jenis_kelamin)?'selected':'':'' }}>{{ $row->jenis_kelamin }}</option>
                      @endforeach
                      @endif
                  </select>
                  <span id="jenis_kelamin_a"></span>
                </div>
                <label for="alamat" class="col-form-label col-sm-2 offset-md-0">Kecamatan Ktp</label>
                <div class="col-lg-4 form-group">
                  <div class="input-search clearfix">
                
                      <input type="hidden" class="form-control"  value="{{(isset($provinsi)?!empty($provinsi->id_kecamatan)?$provinsi->id_kecamatan:'':'')}}" id="id_kecamatan" name="id_kecamatan">
                      <select class="form-control" id="kecamatan_ktp" name="kecamatan_ktp" data-plugin="select2">
                          <optgroup label="kecamatan">
                          </optgroup>
                      </select>
                  </div>
                  {{-- {!! App\Console\Commands\Generator\Form::input('id_kecamatan_ktp','hidden')->model($pegawai)->showHidden() !!} --}}
                  <span id="kecamatan_ktp_a"></span>
                </div>
              </div>
              <div class="row col-lg-12" style="margin-bottom: 1.429rem;">
                <label for="nama" class="col-form-label col-sm-2 offset-md-0">Tempat / Tanggal Lahir</label>
                <div class="col-lg-2 form-group">
                  <input name="tempat_lahir" placeholder="TEMPAT LAHIR" id="tempat_lahir" class="form-control form-control-sm" value="{{($pegawai->exists?!empty($pegawai->penduduk->tempat_lahir)?$pegawai->penduduk->tempat_lahir:'':'')}}" type="text">
                  <span id="tempat_lahir_a"></span>
                </div>
                <div class="col-lg-2 form-group">
                  <input name="tanggal_lahir" placeholder="TANGGAL" id="tanggal_lahir" class="form-control form-control-sm datepicker" value="{{($pegawai->exists?!empty($pegawai->penduduk->tanggal_lahir)? date("d-m-Y", strtotime($pegawai->penduduk->tanggal_lahir)):'':'')}}" type="datepicker">
                  <span id="tanggal_lahir_a"></span>
                </div>
                <label for="alamat" class="col-form-label col-sm-2 offset-md-0">Kelurahan Ktp</label>
                <div class="col-lg-4 form-group">
                  <div class="input-search clearfix">
          
                  <input type="hidden" id="id_kelurahan_ktp" value="{{isset($pegawai->penduduk->id_kelurahan_ktp)?$pegawai->penduduk->id_kelurahan_ktp:''}}" name="id_kelurahan_ktp">
                  <select class="form-control" id="kelurahan_ktp" name="kelurahan_ktp" data-plugin="select2" value="{{(isset($provinsi)?!empty($provinsi->kelurahan)?$provinsi->kelurahan:'':'')}}">
                          <optgroup label="Kelurahan">
                          </optgroup>
                  </select>
                   <span id="kelurahan_ktp_a"></span>
                </div>
              </div>
            </div>
              <div class="row col-lg-12" style="margin-bottom: 1.429rem;">
                <label for="nama" class="col-form-label col-sm-2 offset-md-0">Agama</label>
                <div class="col-lg-4 form-group">
                   <?php $agama=Illuminate\Support\Facades\DB::table('agama')->get();
              ?>
              <select class="form-control" name="id_agama" id="id_agama" data-plugin="select2">
                  <option value="">-Silahkan Pilih-</option>
                  @foreach($agama as $row)
                  <option value="{{ $row->id }}" {{ isset($pegawai->penduduk->id_agama)?($row->id==$pegawai->penduduk->id_agama)?'selected':'':'' }}>{{ $row->agama }}</option>
                  @endforeach
                </select>
               
                  <span id="agama_a"></span>
                </div>
                <label for="nama" class="col-form-label col-sm-2 offset-md-0">E-Mail <a style="color:red;">*</a></label>
                <div class="col-lg-3 form-group">
                  <input name="email" placeholder="E-MAIL" id="email" class="form-control form-control-sm" value="{{($pegawai->exists?!empty($user->email)?$user->email:'':'')}}" type="text">
                  <span id="email_a"></span>
                </div>
                <div class="col-md-1 float-left">
                  <div class="text-left">
                    <span class="help-block" id="message1"></span>
                  </div>
                </div>
              </div>
              <div class="row col-lg-12" style="margin-bottom: 1.429rem;">
                <label for="nama" class="col-form-label col-sm-2 offset-md-0">Status Pernikahan</label>
                <div class="col-lg-4 form-group">
                  <?php $pernikahan=Illuminate\Support\Facades\DB::table('status_pernikahan')->get();
              ?>
               <select class="form-control" name="id_status_pernikahan" id="id_status_pernikahan" data-plugin="select2">
                  <option value="">-Silahkan Pilih-</option>
                  @foreach($pernikahan as $row)
                  <option value="{{ $row->id }}" {{ isset($pegawai->penduduk->id_status_pernikahan)?($row->id==$pegawai->penduduk->id_status_pernikahan)?'selected':'':'' }}>{{ $row->status_pernikahan }}</option>
                  @endforeach
                </select>
               
                  <span id="status_pernikahan_a"></span>
                </div>
                <label for="username" class="col-form-label col-sm-2 offset-md-0">Username <a style="color:red;">*</a></label>
                <div class="col-lg-3 form-group">
                  <input placeholder="USERNAME" name="username" id="username" class="form-control form-control-sm" value="{{(isset($user)?!empty($user->username)?$user->username:'':'')}}" type="text">
                  <span id="username_a"></span>
                </div>
                 <div class="col-md-1 float-left">
                  <div class="text-left">
                    <span class="help-block" id="message"></span>
                  </div>
                </div>
               
        
              </div>
             
              <div class="row col-lg-12" style="margin-bottom: 1.429rem;">
                <label for="username" class="col-form-label col-sm-2 offset-md-0">Golongan Darah</label>
                <div class="col-lg-4 form-group">
                 <select class="form-control" id="id_golongan_darah" name="id_golongan_darah" data-plugin="select2">
                  <optgroup label="Golongan Darah">
                    <option value="">-Pilih-</option>
                    @foreach($golongan_darah as $a)
                    @if(isset($pegawai->id_golongan_darah) && !empty($pegawai->id_golongan_darah))
                    <option value="{{$a->id}}" {{($pegawai->id_golongan_darah==$a->id)?'selected':''}}>{{$a->golongan_darah}}</option>
                    @else
                    <option value="{{$a->id}}">{{$a->golongan_darah}}</option>
                    @endif
                    @endforeach
                  </optgroup>
                </select>
                  <span id="id_golongan_darah_a"></span>
                </div>

                <label for="nama" class="col-form-label col-sm-2 offset-md-0">Nomor Ponsel</label>
              
                <div class="col-lg-4 form-group">
                  <input name="no_hp" placeholder="NOMOR PONSEL" id="no_hp" class="form-control form-control-sm" value="{{isset($pegawai) && !empty($pegawai->penduduk->no_hp)?$pegawai->penduduk->no_hp:''}}" type="text">
                  <span id="no_hp_a"></span>
                </div>
              </div>

              
                          
              <br/>
              <div class="panel-heading data-domisili">
                <span><h4><b>DATA DOMISILI PEGAWAI</b></h4></span>
              </div>
              <hr/>
              <div class="row col-lg-12" style="margin-bottom: 1.429rem;">
                <label for="nama" class="col-form-label col-sm-2 offset-md-0">Alamat Domisili</label>
                <div class="col-lg-7 form-group">
                  <textarea placeholder="ALAMAT DOMISILI" class="form-control form-control-sm" name="alamat_domisili" id="alamat_domisili" rows="2">{{($pegawai->exists?!empty($pegawai->penduduk->alamat_domisili)?$pegawai->penduduk->alamat_domisili:'':'')}}</textarea>
                  <span id="alamat_domisili_a"></span>
                </div>
              </div>
              <div class="row col-lg-12" style="margin-bottom: 1.429rem;">
                <label for="nama" class="col-form-label col-sm-2 offset-md-0">Provinsi Domisili</label>
                <div class="col-lg-4 form-group">
                  <div class="input-search clearfix">

                      <input type="hidden" class="form-control" id="id_provinsi_domisili" value="{{(isset($provinsi1)?!empty($provinsi1->id_provinsi)?$provinsi1->id_provinsi:'':'')}}" name="id_provinsi_domisili">
                      <select class="form-control" id="provinsi_domisili" name="provinsi_domisili" data-plugin="select2">
                        <optgroup label="Provinsi">
                        </optgroup>
                      </select>
                      <span class="help-block" id="provinsi_domisili_a"></span>
                  </div>
                </div>
              </div>
              <div class="row col-lg-12" style="margin-bottom: 1.429rem;">
                <label for="nama" class="col-form-label col-sm-2 offset-md-0">Kabupaten Domisili</label>
                <div class="col-lg-4 form-group">
                  <div class="input-search clearfix">
               
                    <input type="hidden" class="form-control" value="{{(isset($provinsi1)?!empty($provinsi1->id_kabupaten)?$provinsi1->id_kabupaten:'':'')}}" id="id_kabupaten_domisili" name="id_kabupaten_domisili">
                    <select class="form-control" id="kabupaten_domisili" name="kabupaten_domisili" data-plugin="select2">
                        <optgroup label="Kabupaten">
                        </optgroup>
                    </select>
                    <span class="help-block" id="kabupaten_domisili_a"></span>
                  </div>
                  {{-- {!! App\Console\Commands\Generator\Form::input('id_kabupaten_domisili','hidden')->model($pegawai)->showHidden() !!} --}}
                </div>
              </div>
              <div class="row col-lg-12" style="margin-bottom: 1.429rem;">
                <label for="nama" class="col-form-label col-sm-2 offset-md-0">Kecamatan Domisili</label>
                <div class="col-lg-4 form-group">
                  <div class="input-search clearfix">
                
                    <input type="hidden" class="form-control" value="{{(isset($provinsi1)?!empty($provinsi1->id_kecamatan)?$provinsi1->id_kecamatan:'':'')}}" id="id_kecamatan_domisili" name="id_kecamatan_domisili">
                    <select class="form-control" id="kecamatan_domisili" name="kecamatan_domisili" data-plugin="select2">
                        <optgroup label="Kecamatan">
                        </optgroup>
                    </select>
                    <span class="help-block" id="kecamatan_domisili_a"></span>
                  </div>
                  <span id="kecamatan_domisili_a"></span>

                </div>
              </div>
              <div class="row col-lg-12" style="margin-bottom: 1.429rem;">
                <label for="nama" class="col-form-label col-sm-2 offset-md-0">Kelurahan Domisili</label>
                <div class="col-lg-4 form-group">
                  <div class="input-search clearfix">
 
                    <input type="hidden" id="id_kelurahan_domisili" value="{{isset($pegawai->id_kelurahan_domisili)?$pegawai->id_kelurahan_domisili:''}}" name="id_kelurahan_domisili">
                    <select class="form-control" id="kelurahan_domisili" name="kelurahan_domisili" data-plugin="select2">
                        <optgroup label="Kelurahan">
                        </optgroup>
                    </select>
                    <span class="help-block" id="kelurahan_domisili_a"></span>
                  </div>                
                </div>
              </div>
              <br/>
              <div class="panel-heading data-pegawai">
                <span><h4><b>STATUS PEGAWAI</b></h4></span>
              </div>
              <hr/>
 
        
              <div class="row col-lg-12" style="margin-bottom: 1.429rem;">
                <label for="nama" class="col-form-label col-sm-2 offset-md-0">Jabatan </label>
                <div class="col-lg-4 form-group">
             
                  <?php $jabatan=Illuminate\Support\Facades\DB::table('jabatan')->get();?>
                  <select class="form-control" name="jabatan" id="jabatan" data-plugin="select2">
                      <option value="">-Silahkan Pilih-</option>
                      @foreach($jabatan as $row)
                      <option value="{{ $row->id }}" {{ isset($pegawai->id_jabatan)?($row->id==$pegawai->id_jabatan)?'selected':'':'' }}>{{ $row->jabatan }}</option>
                      @endforeach
                  </select>
                  <span id="id_jabatan_a"></span>
                </div>
                <label for="nama" class="col-form-label col-sm-2 offset-md-0">Unit Kerja </label>
                <div class="col-lg-4 form-group">

                  <?php $unit_kerja=Illuminate\Support\Facades\DB::table('unit_kerja')->get();?>
                  <select class="form-control" name="unit_kerja" id="unit_kerja" data-plugin="select2">
                      <option value="">-Silahkan Pilih-</option>
                      @foreach($unit_kerja as $row)
                      <option value="{{ $row->id }}" {{ isset($pegawai->id_unit_kerja)?($row->id==$pegawai->id_unit_kerja)?'selected':'':'' }}>{{ $row->unit_kerja }}</option>
                      @endforeach
                  </select>
               
                  <span id="help-block"></span>                </div>
              </div>
          
              <div class="row col-lg-12" style="margin-bottom: 1.429rem;">
                <label for="nama" class="col-form-label col-sm-2 offset-md-0">Tanggal Bergabung</label>
                <div class="col-lg-4 form-group">
                  <input name="tgl_bergabung" placeholder="TANGGAL BERGABUNG" id="tgl_bergabung" class="form-control form-control-sm datepicker" value="{{($pegawai->exists?!empty($pegawai->tgl_bergabung)?date('d-m-Y',strtotime($pegawai->tgl_bergabung)):'':date('d-m-Y'))}}" type="datepicker">
                  <span id="tgl_bergabung_a"></span>
                </div>
                  <label for="nama" class="col-form-label col-sm-2 offset-md-0">Status Pegawai</label>
                <div class="col-lg-4 form-group">

                  <?php $status_pegawai=Illuminate\Support\Facades\DB::table('status_pegawai')->get();?>
                  <select class="form-control" name="status_pegawai" id="status_pegawai" data-plugin="select2">
                      <option value="">-Silahkan Pilih-</option>
                      @foreach($status_pegawai as $row)
                      <option value="{{ $row->id }}" {{ isset($pegawai->id_status_pegawai)?($row->id==$pegawai->id_status_pegawai)?'selected':'':'' }}>{{ $row->status }}</option>
                      @endforeach
                  </select>
               
                  
                  <span id="id_status_pegawai_a"></span>
                </div>
                
          </div>
            <div class="row col-lg-12" style="margin-bottom: 1.429rem;">
       
                <label for="nama" class="col-form-label col-sm-2 offset-md-0">Pegawai Masih AKtif</label>
                <div class="col-lg-4">
                  <div class="form-group form-inline">
                    <div class="">
                      <div class="checkbox-custom checkbox-default">
                       <input type="checkbox" class="check" name="flag_aktif" id="flag_aktif" value="Y" {{($pegawai->exists?!empty($pegawai->flag_aktif)?$pegawai->flag_aktif=='Y'?'checked':'':'':'checked')}}>
                       <label for="inputBasicRemember"></label>
                     </div>
                   </div>&nbsp;&nbsp;&nbsp;
                   <span style="padding-top:9px">Ya</span>
                   &nbsp&nbsp&nbsp&nbsp
                   <div class="">
                    <div class="checkbox-custom checkbox-default">
                      <input type="checkbox" class="check" name="flag_aktif" id="flag_aktif" value="N" {{($pegawai->exists?!empty($pegawai->flag_aktif)?$pegawai->flag_aktif=='N'?'checked':'':'':'')}}>
                      <label for="inputBasicRemember"></label>
                    </div>
                  </div>&nbsp;&nbsp;&nbsp;
                  <span style="padding-top:9px">Tidak</span>

                  <span id="flag_aktif_a"></span>
                </div>
              </div>
            
          </div>
            <div class="row col-lg-12" style="margin-bottom: 1.429rem;">
       
                  
              </div>
      
            <br/>
            <br/>
            <br/>
            <div class="col-md-12 float-right">
              <div class="text-right">
                <button type="submit" class="btn btn-primary" id="simpan">Simpan</button>
                <a class="btn btn-outline-primary " href="{{ url('pegawai') }}" id="batal">Kembali</a>
              </div>
            </div>
          </div>
          {{ Form::close() }}
        </div>
      </div>
    </div>
  </div>
  @endsection
  @include('pegawai.form-create-js') 
  @push('js')
  <script type="text/javascript">
      $(document).ready(function(){
          $.ajax({
            url : "{{url('penduduk/load-provinsi')}}",
            type: "GET",
            dataType: "JSON",
            success: function(data){
                // console.log(data);
                if(data.length>0)
                {
                  $('#provinsi_ktp').empty();
                  $('#provinsi_ktp').append('<option value="">-Pilih-</option>');
                  $.each(data,function(key,value){
                    // console.log(value.id);        
                    if($('#id_provinsi').val()==value.id){
                      $('#provinsi_ktp').append('<option value="'+value.id+'" selected>'+value.provinsi+'</option>').change();
                    }
                    else
                    {
                      $('#provinsi_ktp').append('<option value="'+value.id+'">'+value.provinsi+'</option>');
                    }
                  });
                }
                else{
                  $('#provinsi_ktp').empty();
                  $('#provinsi_ktp').append('<option value=""selected>-Data is empty-</option>');
                }
              },
              error: function (jqXHR, textStatus, errorThrown)
              {
                alert('Error get data from ajax provinsi');
              }
            });

          $('#provinsi_ktp').change(function(){
            var id_prov=$('#provinsi_ktp').val();
            if(id_prov!=='')
            {
              $('#provinsi_ktp_a').text('');
              $('#simpan').removeClass('disabled');
              kabupaten(id_prov)
            }
            if(id_prov=='')
            {
              $('#kabupaten_ktp').empty();
              $('#kabupaten_ktp').append('<option value=""selected>-Data is empty-</option>');
              $('#kecamatan_ktp').empty();
              $('#kecamatan_ktp').append('<option value=""selected>-Data is empty-</option>');
              $('#kelurahan_ktp').empty();
              $('#kelurahan_ktp').append('<option value=""selected>-Data is empty-</option>');
            }
          });
  // ini untuk  alamat Domisili
           $.ajax({
          url : "{{url('penduduk/load-provinsi')}}",
          type: "GET",
          dataType: "JSON",
          success: function(data){
          // console.log(data);
          if(data.length>0)
          {
            $('#provinsi_domisili').empty();
            $('#provinsi_domisili').append('<option value="">-Pilih-</option>');
            $.each(data,function(key,value){
              // console.log(value.id);        
              if($('#id_provinsi_domisili').val()==value.id){
                $('#provinsi_domisili').append('<option value="'+value.id+'" selected>'+value.provinsi+'</option>').change();
              }
              else
              {
                $('#provinsi_domisili').append('<option value="'+value.id+'">'+value.provinsi+'</option>');
              }
            });
          }
          else{
            $('#provinsi_domisili').empty();
            $('#provinsi_domisili').append('<option value=""selected>-Data is empty-</option>');
          }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
          alert('Error get data from ajax domisili provinsi');
        }
      });
        $('#provinsi_domisili').change(function(){
          var id_prov=$('#provinsi_domisili').val();
          if(id_prov!=='')
          {
            $('#provinsi_domisili_a').text('');
            $('#simpan').removeClass('disabled');
            kabupaten1(id_prov)
          }
          if(id_prov=='')
          {
            $('#kabupaten_domisili').empty();
            $('#kabupaten_domisili').append('<option value=""selected>-Data is empty-</option>');
            $('#kecamatan_domisili').empty();
            $('#kecamatan_domisili').append('<option value=""selected>-Data is empty-</option>');
            $('#kelurahan_domisili').empty();
            $('#kelurahan_domisili').append('<option value=""selected>-Data is empty-</option>');
          }
        });
        $('#kabupaten_domisili').change(function(){
          var id_kab=$('#kabupaten_domisili').val();
          if(id_kab!=='')
          {
            $('#kabupaten_domisili_a').text('');
            $('#simpan').removeClass('disabled');
            kecamatan1(id_kab)
          }
          if(id_kab=='')
          {
            $('#kecamatan_domisili').empty();
            $('#kecamatan_domisili').append('<option value=""selected>-Data is empty-</option>');
            $('#kelurahan_domisili').empty();
            $('#kelurahan_domisili').append('<option value=""selected>-Data is empty-</option>');
          }
        });
        $('#kecamatan_domisili').change(function(){
          var id_kec=$('#kecamatan_domisili').val();
          if(id_kec!=='')
          {
            $('#kecamatan_domisili_a').text('');
            $('#simpan').removeClass('disabled');
            kelurahan1(id_kec)
          }
          if(id_kec=='')
          {
            $('#kelurahan_domisili').empty();
            $('#kelurahan_domisili').append('<option value=""selected>-Data is empty-</option>');
          }
        });
        // end untuk alamat domisili
      });//tutup load atas
// alamat sesuai ktp
    function kabupaten(id)
    {
      var id=id;
          // alert('cccp');
          $.ajax({
            url : "{{url('penduduk/get-kabupaten')}}",
            type: "GET",
            dataType: "JSON",
            data:{id:id},
            success: function(data){
          // console.log(data.length); 
              // console.log('aaaa');
              $.each(data,function(key,value){
                // console.log(value.length);
                if(value.length>0)
                {
                  $('#kabupaten_ktp').empty();
                  $('#kabupaten_ktp').append('<option value="" selected>-Pilih-</option>');
                  value.forEach(function(data) {
                          // alert
                          if($('#id_kabupaten').val()==data.id){
                              // alert('aaaa');
                              $('#kabupaten_ktp').append('<option value="'+data.id+'" selected>'+data.kabupaten+'</option>').trigger('change');
                            }
                            else{
                              $('#kabupaten_ktp').append('<option value="'+data.id+'">'+data.kabupaten+'</option>');
                            }

                          });
                }
                else
                {
                  $('#kabupaten_ktp').empty();
                  $('#kabupaten_ktp').append('<option value=""selected>-Data is empty-</option>');
                }

              });
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
              alert('Error get data from ajax kabupaten');
            }
          });
        }
        $('#kabupaten_ktp').change(function(){
          var id_kab=$('#kabupaten_ktp').val();
          if(id_kab!=='')
          {
            $('#kabupaten_ktp_a').text('');
            $('#simpan').removeClass('disabled');
            kecamatan(id_kab)
          }
          if(id_kab=='')
          {
            $('#kecamatan_ktp').empty();
            $('#kecamatan_ktp').append('<option value=""selected>-Data is empty-</option>');
            $('#kelurahan_ktp').empty();
            $('#kelurahan_ktp').append('<option value=""selected>-Data is empty-</option>');
          }
    });
    function kecamatan(id)
    {
      var id=id;
      // alert('cccp');
      $.ajax({
        url : "{{url('penduduk/get-kecamatan')}}",
        type: "GET",
        dataType: "JSON",
        data:{id:id},
        success: function(data){
      // console.log(data.length); 
          // console.log('aaaa');
          $.each(data,function(key,value){
            // console.log(value.length);
            if(value.length>0)
            {
              $('#kecamatan_ktp').empty();
              $('#kecamatan_ktp').append('<option value="" selected>-Pilih-</option>');
              value.forEach(function(data) {
                      // alertc
                      // alert(data.id);
                      if($('#id_kecamatan').val()==data.id){
                          // alert('aaaa');
                          $('#kecamatan_ktp').append('<option value="'+data.id+'" selected>'+data.kecamatan+'</option>').trigger('change');
                        }
                        else{
                          $('#kecamatan_ktp').append('<option value="'+data.id+'">'+data.kecamatan+'</option>');
                        }

                      });
            }
            else
            {
              $('#kecamatan_ktp').empty();
              $('#kecamatan_ktp').append('<option value=""selected>-Data is empty-</option>');
            }

          });
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
          alert('Error get data from ajax kecamatan');
        }
      });

    }

    $('#kecamatan_ktp').change(function(){
      var id_kec=$('#kecamatan_ktp').val();
      if(id_kec!=='')
      {
        $('#kecamatan_ktp_a').text('');
        $('#simpan').removeClass('disabled');
        kelurahan(id_kec)
      }
      if(id_kec=='')
      {
        $('#kelurahan_ktp').empty();
        $('#kelurahan_ktp').append('<option value=""selected>-Data is empty-</option>');
      }
    });
    $('#kelurahan_ktp').change(function(){
      $('#kelurahan_ktp_a').text('');
      $('#simpan').removeClass('disabled');
        // kelurahan(id_kec)
    });
    $('#kelurahan_domisili').change(function(){
      $('#kelurahan_domisili_a').text('');
      $('#simpan').removeClass('disabled');
        // kelurahan(id_kec)
    });
    function kelurahan(id)
    {
      var id=id;
      // alert('cccp');
      $.ajax({
        url : "{{url('penduduk/get-kelurahan')}}",
        type: "GET",
        dataType: "JSON",
        data:{id:id},
        success: function(data){
      // console.log(data.length); 
          // console.log('aaaa');
          $.each(data,function(key,value){
            // console.log(value.length);
            if(value.length>0)
            {
              $('#kelurahan_ktp').empty();
              $('#kelurahan_ktp').append('<option value="" selected>-Pilih-</option>');
              value.forEach(function(data) {
                      // alertc
                      // alert(data.id);
                      if($('#id_kelurahan_ktp').val()==data.id){
                          // alert('aaaa');
                          $('#kelurahan_ktp').append('<option value="'+data.id+'" selected>'+data.kelurahan+'</option>').trigger('change');
                        }
                        else{
                          $('#kelurahan_ktp').append('<option value="'+data.id+'">'+data.kelurahan+'</option>');
                        }

                      });
            }
            else
            {
              // $('#kelurahan_ktp').empty();
              // $('#kelurahan_ktp').append('<option value=""selected>-Data is empty-</option>');
            }

          });
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
          alert('Error get data from ajax kelurahan');
        }
      });
    }
// end alamat sesuai ktp
// alamat domisili
function kabupaten1(id)
{
  var id=id;
          // alert('cccp');
          $.ajax({
            url : "{{url('penduduk/get-kabupaten')}}",
            type: "GET",
            dataType: "JSON",
            data:{id:id},
            success: function(data){
          // console.log(data.length); 
              // console.log('aaaa');
              $.each(data,function(key,value){
                // console.log(value.length);
                if(value.length>0)
                {
                  $('#kabupaten_domisili').empty();
                  $('#kabupaten_domisili').append('<option value="" selected>-Pilih-</option>');
                  value.forEach(function(data) {
                          // alert
                          if($('#id_kabupaten_domisili').val()==data.id){
                              // alert('aaaa');
                              $('#kabupaten_domisili').append('<option value="'+data.id+'" selected>'+data.kabupaten+'</option>').trigger('change');
                            }
                            else if($('#checkbox_alamat_domisili').is(':checked'))
                            {
                              if($('#kabupaten_ktp').val()==data.id){
                              $('#kabupaten_domisili').append('<option value="'+data.id+'" selected>'+data.kabupaten+'</option>').trigger('change');
                            }
                            }
                            else{
                              $('#kabupaten_domisili').append('<option value="'+data.id+'">'+data.kabupaten+'</option>');
                            }

                          });
                }
                else
                {
                  $('#kabupaten_domisili').empty();
                  $('#kabupaten_domisili').append('<option value=""selected>-Data is empty-</option>');
                }

              });
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
              alert('Error get data from ajax');
            }
          });
        }

        function kecamatan1(id)
        {
          var id=id;
          // alert('cccp');
          $.ajax({
            url : "{{url('penduduk/get-kecamatan')}}",
            type: "GET",
            dataType: "JSON",
            data:{id:id},
            success: function(data){
          // console.log(data.length); 
              // console.log('aaaa');
              $.each(data,function(key,value){
                // console.log(value.length);
                if(value.length>0)
                {
                  $('#kecamatan_domisili').empty();
                  $('#kecamatan_domisili').append('<option value="" selected>-Pilih-</option>');
                  value.forEach(function(data) {
                          // alertc
                          // alert(data.id);
                          if($('#id_kecamatan_domisili').val()==data.id){
                              // alert('aaaa');
                              $('#kecamatan_domisili').append('<option value="'+data.id+'" selected>'+data.kecamatan+'</option>').trigger('change');
                            }
                            else if($('#checkbox_alamat_domisili').is(':checked'))
                            {
                              if($('#kecamatan_ktp').val()==data.id){
                              $('#kecamatan_domisili').append('<option value="'+data.id+'" selected>'+data.kecamatan+'</option>').trigger('change');
                            }
                            }
                            else{
                              $('#kecamatan_domisili').append('<option value="'+data.id+'">'+data.kecamatan+'</option>');
                            }

                          });
                }
                else
                {
                  $('#kecamatan_domisili').empty();
                  $('#kecamatan_domisili').append('<option value=""selected>-Data is empty-</option>');
                }

              });
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
              alert('Error get data from ajax');
            }
          });

        }

        function kelurahan1(id)
        {
          var id=id;
          $.ajax({
            url : "{{url('penduduk/get-kelurahan')}}",
            type: "GET",
            dataType: "JSON",
            data:{id:id},
            success: function(data){
          // console.log(data.length); 
              // console.log('aaaa');
              $.each(data,function(key,value){
                // console.log(value.length);
                if(value.length>0)
                {
                  $('#kelurahan_domisili').empty();
                  $('#kelurahan_domisili').append('<option value="" selected>-Pilih-</option>');
                  value.forEach(function(data) {
                          // alertc
                          // alert(data.id);
                          if($('#id_kelurahan_domisili').val()==data.id){
                              // alert('aaaa');
                              $('#kelurahan_domisili').append('<option value="'+data.id+'" selected>'+data.kelurahan+'</option>').trigger('change');
                            }
                             else if($('#checkbox_alamat_domisili').is(':checked'))
                            {
                              if($('#kelurahan_ktp').val()==data.id){
                              $('#kelurahan_domisili').append('<option value="'+data.id+'" selected>'+data.kelurahan+'</option>').trigger('change');
                            }
                            }
                            else{
                              $('#kelurahan_domisili').append('<option value="'+data.id+'">'+data.kelurahan+'</option>');
                            }

                          });
                }
                else
                {
                  $('#kelurahan_domisili').empty();
                  $('#kelurahan_domisili').append('<option value=""selected>-Data is empty-</option>');
                }

              });
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
              alert('Error get data from ajax');
            }
          });

        }
// end_alamat domisili       
</script>
@endpush