@extends('layouts.website.app')

@section('content')
    <section class="hero-section dark-overlay" style="background: url('{{ asset('admin_remark_base/') }}/website/assets/images/banners/3.jpg') no-repeat center / cover;">
        <div class="hero-area-1 owl-carousel">
            <div class="single-hero">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-7 col-lg-7 col-md-12 col-sm-12">
                            <div class="hero-sub">
                                <div class="hero-left">
                                    <h4>Covid 19</h4>
                                    <h1>Bagaimana mencegah COVID19 dan tetap aman?</h1>
                                    <p>Mari kita simak langkah - langkah terkait pencegahan terhadap virus corona atau covid19 yang sudah menjadi pandemi bangsa ini.</p>
                                    <a href="{{url('/home')}}#pencegahan" class="bttn-mid btn-fill">Langkah Pencegahan</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-5 col-lg-5 col-md-12 col-sm-12">
                            <div class="hero-sub">
                                <div class="hero-left">
                                    <img src="{{ asset('admin_remark_base/') }}/website/assets/images/banners/5.jpg" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="single-hero">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-7 col-lg-7 col-md-12 col-sm-12">
                            <div class="hero-sub">
                                <div class="hero-left">
                                    <h4>Ikuti Petunjuk Medis</h4>
                                    <h1>Ketahuilah bagaimana Anda akan melindungi diri sendiri dan tetap aman</h1>
                                    <p>Mari Kita simak gejala - gejala dari virus covid19 agar anda dapat melindungi diri dan melakukan pencegahan sejak dini.</p>
                                    <a href="{{url('/home')}}#gejala" class="bttn-mid btn-fill">Kenali Gejala Covid19</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-5 col-lg-5 col-md-12 col-sm-12">
                            <div class="hero-sub">
                                <div class="hero-left">
                                    <img src="{{ asset('admin_remark_base/') }}/website/assets/images/banners/4.jpg" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!--/Hero Area-->
    
    <!--About Section-->
    <section class="about-area gray-bg" id="covid">
        <div class="about-content mid-bg-gray">
            <div class="about-content-inner-2">
                <div class="section-title mb-10">
                    <h4>Tentang Penyakit</h4>
                    <h2>Apa itu Coronavirus?</h2>
                </div>
                <p>Virus Corona atau severe acute respiratory syndrome coronavirus 2 (SARS-CoV-2) adalah virus yang menyerang sistem pernapasan. Penyakit karena infeksi virus ini disebut COVID-19. Virus Corona bisa menyebabkan gangguan pada sistem pernapasan, pneumonia akut, sampai kematian.</p>
                <ul>
                    <li>Risiko transmisi</li>
                    <li>Tidak Menunjukkan Gejala Selama Perawatan</li>
                    <li>Persebaran sangat cepat</li>
                    <li>Masa Inkubasi adalah selama 14 Hari</li>
                    <li>Penyakti yang menyerang alat pernapasan</li>
                 
                </ul>
            </div>
        </div>
        <div class="about-left" style="background: url('{{ asset('admin_remark_base/') }}/website/assets/images/about.jpg') no-repeat center / cover;">
            <div class="left-img-wrap">
                <a href="https://www.youtube.com/watch?v=dDD5N0tWouQ" class="video-popup"><i class="flaticon-play-button"></i></a>
            </div>
        </div>
    </section><!--/About Section-->

    <!--Prevention Area-->
    <section class="section-padding-3">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-8 centered">
                    <div class="section-title">
                        <h4>Perawatan Dasar</h4>
                        <h2>Pemeriksaan Dasar</h2>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                    <div class="single-prevention">
                        <img src="{{ asset('admin_remark_base/') }}/website/assets/images/icons/1.png" alt="">
                        <h3>Kenali Tips dari Dokter</h3>
                        <p>Pelajari tips tips perawatan diri yang sudah di berikan oleh dokter</p>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                    <div class="single-prevention">
                        <img src="{{ asset('admin_remark_base/') }}/website/assets/images/icons/2.png" alt="">
                        <h3>Lakukan Perawatan Diri</h3>
                        <p>Mulai lakukan perawatan diri secara mandiri </p>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                    <div class="single-prevention">
                        <img src="{{ asset('admin_remark_base/') }}/website/assets/images/icons/3.png" alt="">
                        <h3>Periksa Diri ke Medis</h3>
                        <p>Lakukan pemeriksaan rutin baik ketika kondisi sehat maupun sedang sakit</p>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                    <div class="single-prevention">
                        <img src="{{ asset('admin_remark_base/') }}/website/assets/images/icons/4.png" alt="">
                        <h3>Uji Pengecekan Lab</h3>
                        <p>Lakukan pemeriksaan lab secara berkala</p>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                    <div class="single-prevention">
                        <img src="{{ asset('admin_remark_base/') }}/website/assets/images/icons/5.png" alt="">
                        <h3>Pengecekan Pernapasan</h3>
                        <p>Lakukan pengecekan terhadap kondisi pernapasan anda</p>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                    <div class="single-prevention">
                        <img src="{{ asset('admin_remark_base/') }}/website/assets/images/icons/6.png" alt="">
                        <h3>Konsumsi Obat / Vitamin</h3>
                        <p>Perbanyak konsumsi multivitamin untuk pencegahan</p>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                    <div class="single-prevention">
                        <img src="{{ asset('admin_remark_base/') }}/website/assets/images/icons/7.png" alt="">
                        <h3>Pelajari Tips Hidup Sehat</h3>
                        <p>Kenali dan pelajari tips hidup sehat dan bersih dari berbagai sumber</p>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                    <div class="single-prevention">
                        <img src="{{ asset('admin_remark_base/') }}/website/assets/images/icons/8.png" alt="">
                        <h3>Perbanyak Istirahat</h3>
                        <p>Kurangi aktivitas berlebih ketika badan kurang fit dan perbanyak istirahat</p>
                    </div>
                </div>
            </div>
        </div>
    </section><!--/Prevention Area-->

    <!--Symtoms Area-->
    <section class="section-padding gray-bg" id="gejala">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-6">
                    <img src="{{ asset('admin_remark_base/') }}/website/assets/images/about-2.jpg" alt="">
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6">
                    <div class="section-title">
                        <h4>Gejala Penyakit</h4>
                        <h2>Gejala Coronavirus</h2>
                    </div>
                    <div class="about-list">
                        <ul>
                            <li><i class="flaticon-right-arrow-1"></i>Demam Tinggi <span>- Mengalami Gejala Demam Tinggi</span></li>
                            <li><i class="flaticon-right-arrow-1"></i>Sakit Kepala <span>- Mengalami Gejala Sakit Kepala</span></li>
                            <li><i class="flaticon-right-arrow-1"></i>Batuk <span>- Mengalami Gejala Batuk Kering</span></li>
                            <li><i class="flaticon-right-arrow-1"></i>Hidung Berair <span>- Mengalami Gejala Hidung Berair</span></li>
                            <li><i class="flaticon-right-arrow-1"></i>Sakit tenggorokan <span>- Mengalami Gejala Sakit Tenggorokan</span></li>
                            <li><i class="flaticon-right-arrow-1"></i>Sesak Napas <span>- Mengalami Gejala Sesak pada Pernapasan</span></li>
                            <li><i class="flaticon-right-arrow-1"></i>Merasa Kelelahan <span>- Mengalami Gejala Sering Merasa Kelelahan</span></li>
                            <li><i class="flaticon-right-arrow-1"></i>Sering Mengalami Diare <span>- Mengalami Gejala Sering Mengalami Diare</span></li>
                            <li><i class="flaticon-right-arrow-1"></i>Bersin - Bersin <span>- Mengalami Gejala Sering Bersin - bersin</span></li>
                            
                            
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section><!--/Symtoms Area-->


    <!-- How it works Area -->
    <section class="growth-stat section-padding" id="pencegahan">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-6 col-lg-6 centered wow fadeInUp" data-wow-delay="0.3s">
                    <div class="section-title">
                        <h4>Lakukan Perlinduran Diri</h4>
                        <h2>Pencegahan Mendasar</h2>
                    </div>
                </div>
            </div>
            <div class="row mb-30">
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay="0.4s">
                    <img src="{{ asset('admin_remark_base/') }}/website/assets/images/products/feature-1.jpg" alt="">
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay="0.4s">
                    <div class="growth-content">
                        <div class="content-grow">
                            <h3>Gunakan Masker</h3>
                            <p>Ada dua tipe masker yang bisa Anda digunakan untuk mencegah penularan virus Corona, yaitu masker bedah dan masker N95. </p>

                            <p> Masker bedah atau surgical mask merupakan masker sekali pakai yang umum digunakan. Masker ini mudah ditemukan, harganya terjangkau, dan nyaman dipakai, sehingga banyak orang yang menggunakan masker ini saat beraktivitas sehari-hari.</p>
                             
                                
                                <p>Sedangkan masker N95 adalah jenis masker yang dirancang khusus untuk menyaring partikel berbahaya di udara. Jenis masker inilah yang sebenarnya lebih direkomendasikan untuk mencegah infeksi virus Corona. Meski demikian, masker ini kurang nyaman untuk dikenakan sehari-hari dan harganya pun relatif mahal.
                                </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mb-30">
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 order-2 order-xl-1 order-lg-1 order-md-1 order-sm-2 wow fadeInUp" data-wow-delay="0.4s">
                    <div class="growth-content">
                        <div class="content-grow">
                            <h3>Social Distancing</h3>
                            <p>Mengacu Pedoman Penanganan Cepat Medis dan Kesehatan Masyarakat COVID-19 di Indonesia, social distancing atau pembatasan sosial adalah pembatasan kegiatan tertentu penduduk dalam suatu wilayah.</p>

                                <p>Pembatasan sosial dilakukan oleh semua orang di wilayah yang diduga terinfeksi penyakit.</p>
                                
                                    <p> Hal itu disebabkan, virus Corona sangat mudah menular. Cara penularan utama penyakit ini adalah melalui tetesan kecil (droplet) yang dikeluarkan pada saat seseorang batuk atau bersin.</p>
                           
                                            <p>Pembatasan sosial berskala besar bertujuan untuk mencegah meluasnya penyebaran penyakit di wilayah tertentu.</p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 order-1 order-xl-2 order-lg-2 order-md-2 order-sm-1 wow fadeInUp" data-wow-delay="0.4s">
                    <img src="{{ asset('admin_remark_base/') }}/website/assets/images/products/feature-2.jpg" alt="">
                </div>
            </div>
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay="0.4s">
                    <img src="{{ asset('admin_remark_base/') }}/website/assets/images/products/cuci-tangan.jpg" alt="">
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay="0.4s">
                    <div class="growth-content">
                        <div class="content-grow">
                            <h3>Mencuci Tangan Dengan Benar</h3>
                            <p>Mencuci tangan dengan benar adalah cara paling sederhana namun efektif untuk mencegah penyebaran virus 2019-nCoV. Cucilah tangan dengan air mengalir dan sabun, setidaknya selama 20 detik. Pastikan seluruh bagian tangan tercuci hingga bersih, termasuk punggung tangan, pergelangan tangan, sela-sela jari, dan kuku. Setelah itu, keringkan tangan menggunakan tisu, handuk bersih, atau mesin pengering tangan.</p>
                            <p>Gunakan produk hand sanitizer dengan kandungan alkohol minimal 60% agar lebih efektif membasmi kuman.</p>
                                
                                    <p>Cucilah tangan secara teratur, terutama sebelum dan setelah makan, setelah menggunakan toilet, setelah menyentuh hewan, membuang sampah, serta setelah batuk atau bersin. </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- /How it works Area -->

    <!--services Area-->
    <section class="services-area section-padding-2 gray-bg">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-6 centered">
                   <div class="section-title">
                       <h4>Penting</h4>
                       <h2>Pencegahan Coronavirus</h2>
                   </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6">
                    <div class="single-service-3">
                        <div class="service-icon">
                            <img src="{{ asset('admin_remark_base/') }}/website/assets/images/icons/9.png" alt="">
                        </div>
                        <div class="service-content">
                            <h4>Sering Mencuci Tangan</h4>
                            <p>Mencuci tangan sangat penting untuk membunuh kuman dan bakteri yang ada di tangan dengan sabun atau sanitizer.</p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6">
                    <div class="single-service-3">
                        <div class="service-icon">
                            <img src="{{ asset('admin_remark_base/') }}/website/assets/images/icons/10.png" alt="">
                        </div>
                        <div class="service-content">
                            <h4>Melakukan social distancing</h4>
                            <p>Melakukan pembatasan sosial sementara untuk mengurangi persebaran virus covid19 yang saat ini menjadi pandemik.</p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6">
                    <div class="single-service-3">
                        <div class="service-icon">
                            <img src="{{ asset('admin_remark_base/') }}/website/assets/images/icons/11.png" alt="">
                        </div>
                        <div class="service-content">
                            <h4>Waspada menyentuh wajah</h4>
                            <p>Pastikan tangan anda selalu bersih ketika menyentuk wajah terutama bagian hidung.</p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6">
                    <div class="single-service-3">
                        <div class="service-icon">
                            <img src="{{ asset('admin_remark_base/') }}/website/assets/images/icons/12.png" alt="">
                        </div>
                        <div class="service-content">
                            <h4>Menggunakan Masker</h4>
                            <p>Selalu gunakan masker ketika berada di luar rumah baik kondisi sehat maupun tidak sehat.</p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6">
                    <div class="single-service-3">
                        <div class="service-icon">
                            <img src="{{ asset('admin_remark_base/') }}/website/assets/images/icons/13.png" alt="">
                        </div>
                        <div class="service-content">
                            <h4>Pemeriksaan Medis Secara Rutin</h4>
                            <p>Lakukan Pemeriksaan rutin ketika badan tidak sedang fit maupun kondisi sehat.</p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6">
                    <div class="single-service-3">
                        <div class="service-icon">
                            <img src="{{ asset('admin_remark_base/') }}/website/assets/images/icons/14.png" alt="">
                        </div>
                        <div class="service-content">
                            <h4>Selalu ketahui informasi terbaru</h4>
                            <p>Selalu pantau informasi terbaru tentang virus covid19 ini.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!--/services Area-->


    
  
    <!-- Cta -->
    <section class="cta section-padding dark-overlay" style="background: url('{{ asset('admin_remark_base/') }}/website/assets/images/banners/2.jpg') no-repeat fixed;">
        <div class="container">
            <div class="row">
                <div class="col centered cl-white">
                    <div class="section-title mb-20">
                        <h2>Anda Memiliki Pertanyaan Seputar Covid19 ? </h2>
                    </div>
                    <a href="#" class="bttn-mid btn-emt">Kirim Pesan Pada Kami</a>
                </div>
            </div>
        </div>
    </section><!-- /Cta -->



    {{-- <!--Blog Area-->
    <section class="blog-area section-padding-2 gray-bg">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-8 centered wow fadeInUp" data-wow-delay="0.3s">
                    <div class="section-title">
                        <h4>Informasi dan Berita</h4>
                        <h2>Simak Berita Terbaru</h2>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 wow fadeInUp" data-wow-delay="0.4s">
                    <div class="single-blog">
                        <div class="single-blog-img">
                            <img src="{{ asset('admin_remark_base/') }}/website/assets/images/blog/1.jpg" alt="">
                            <a href="#"><i class="fa fa-link"></i></a>
                        </div>
                        <div class="single-blog-content">
                            <div class="blog-meta">
                                <span><a href="#"><i class="flaticon-calendar"></i>Tgl posting</a></span>
                            
                            </div>
                            <h3><a href="#">Judul Berita</a></h3>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 wow fadeInUp" data-wow-delay="0.4s">
                    <div class="single-blog">
                        <div class="single-blog-img">
                            <img src="{{ asset('admin_remark_base/') }}/website/assets/images/blog/1.jpg" alt="">
                            <a href="#"><i class="fa fa-link"></i></a>
                        </div>
                        <div class="single-blog-content">
                            <div class="blog-meta">
                                <span><a href="#"><i class="flaticon-calendar"></i>Tgl posting</a></span>
                            
                            </div>
                            <h3><a href="#">Judul Berita</a></h3>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 wow fadeInUp" data-wow-delay="0.4s">
                    <div class="single-blog">
                        <div class="single-blog-img">
                            <img src="{{ asset('admin_remark_base/') }}/website/assets/images/blog/1.jpg" alt="">
                            <a href="#"><i class="fa fa-link"></i></a>
                        </div>
                        <div class="single-blog-content">
                            <div class="blog-meta">
                                <span><a href="#"><i class="flaticon-calendar"></i>Tgl posting</a></span>
                            
                            </div>
                            <h3><a href="#">Judul Berita</a></h3>
                        </div>
                    </div>
                </div>
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                    <ul class="styled-pagination centered">
                        <li class="previous"><a href="#"><span class="fa fa-angle-left"></span></a></li>
                        <li><a href="#" class="active">1</a></li>
                        <li><a href="#">2</a></li>
                     
                        <li><a href="#"><span class="fa fa-angle-right"></span></a></li>
                    </ul>
                </div>     
                      
            </div>
        </div>
    </section><!--/Blog Area--> --}}
    
    {{-- <!-- Newslatter -->
    <section class="section-padding dark-overlay" style="background: url('{{ asset('admin_remark_base/') }}/website/assets/images/banners/1.jpg') no-repeat fixed;">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-8 centered cl-white">
                    <div class="section-title mb-20">
                        <h1>Chatbot Covid19</h1><br>
                        <h2>Dapatkan Informasi Terbaru terkait Covid19</h2>
                    </div>
                    <div class="newslatter">
                        <form action="{{url('/sendEmailNotifikasi')}}">
                            <input type="email" name="email" placeholder="Ketik Email Anda untuk registrasi" required>
                            <button type="submit"><i class="fa fa-angle-right"></i></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- /Newslatter --> --}}

    <!--FAQ Area-->
    <section class="section-padding-2">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12">
                    <div class="section-title centered">
                        <h4>Pertanyaan dan Informasi</h4>
                        <h2>Beberapa Pertanyaan Seputar Covid19</h2>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                    <div class="faq-contents mb-30">
                        <ul class="accordion">
                            <li>
                                <a href="#">Apakah COVID-19 sama seperti SARS?</a>
                           
                                    <p>
                                        COVID-19 disebabkan oleh SARS-COV2 yang termasuk keluarga besar coronavirus yang sama dengan penyebab SARS pada 2003, hanya berbeda jenis virusnya.<br>Gejalanya mirip dengan SARS, namun angka kematian SARS (9,6 persen) lebih tinggi dibanding COVID-19 (kurang dari 5 persen), walau jumlah kasus COVID-19 jauh lebih banyak dibanding SARS. COVID-19 juga memiliki penyebaran yang lebih luas dan cepat ke beberapa negara dibanding SARS.</p>
                            </li>
                            <li>
                                <a href="#">Apakah COVID-19 dapat ditularkan dari orang yang tidak bergejala?</a>
                                <p>Cara penularan utama penyakit ini adalah melalui tetesan kecil (droplet) yang dikeluarkan pada saat seseorang batuk atau bersin. Saat ini WHO menilai risiko penularan dari seseorang yang tidak bergejala COVID-19 sama sekali, sangat kecil kemungkinannya.</p>

                                    <p>Namun, banyak orang yang teridentifikasi COVID-19 hanya mengalami gejala ringan seperti batuk ringan atau tidak mengeluh sakit, yang mungkin terjadi pada tahap awal penyakit. Sampai saat ini, para ahli masih terus melakukan penyelidikan untuk menentukan periode penularan atau masa inkubasi COVID-19.</p>
                                    
                                        <p>Tetap pantau sumber informasi yang akurat dan resmi mengenai perkembangan penyakit ini.</p>
                            </li>
                            <li>
                                <a href="#">Apakah virus penyebab COVID-19 dapat ditularkan melalui udara?</a>
                                <p>Tidak. Hingga saat ini penelitian menyebutkan virus penyebab COVID-19 ditularkan melalui kontak dengan tetesan kecil (droplet) dari saluran pernapasan.</p>
                            </li>
                            <li>
                                <a href="#">Berapa lama virus ini bertahan di permukaan benda?</a>
                                <p>Sampai saat ini belum diketahui dengan pasti berapa lama COVID-19 mampu bertahan di permukaan suatu benda, meski studi awal menunjukkan COVID-19 dapat bertahan hingga beberapa jam, tergantung jenis permukaan, suhu, atau kelembaban lingkungan.</p>

                                    <p> Namun, disinfektan sederhana dapat membunuh virus tersebut sehingga tidak mungkin menginfeksi orang lagi. Dan membiasakan cuci tangan dengan air dan sabun, atau hand-rub berbasis alkohol, serta hindari menyentuh mata, mulut atau hidung (segitiga wajah) lebih efektif melindungi diri Anda.</p>
                            </li>
                            <li>
                                <a href="#">Manakah yang lebih rentan terinfeksi coronavirus, apakah orang yang lebih tua, atau orang yang lebih muda?</a>
                                <p>Tidak ada batasan usia orang-orang dapat terinfeksi oleh coronavirus ini (COVID-19). Namun, orang yang lebih tua, dan orang-orang dengan kondisi medis yang sudah ada sebelumnya (seperti asma, diabetes, penyakit jantung, atau tekanan darah tinggi) tampaknya lebih rentan untuk menderita sakit parah.</p>
                            </li>
                         
                        </ul>
                    </div>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                    <img src="{{ asset('admin_remark_base/') }}/website/assets/images/faq.jpg" alt="">
                </div>
            </div>
        </div>
    </section><!--/FAQ Area-->
    

    @endsection