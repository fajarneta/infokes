@extends('layouts.website.app')
<link href="{{ asset('admin_remark_base/') }}/website/assets/custom/stylev283a7.css?v=1.9" rel="stylesheet">

@section('content')

    <style>
        /* #map {
            width: 100%;
            height: 80vh;
            position: relative;
            width: 100%;
            display: block;
            height: 100%;
            background-color: green;
        } */

        .container-maps {
            min-height: 80vh;
        }

        .map_leaflet {
            position: relative;
            width: 100%;
            display: block;
            height: 100%;
        }

        .layer {
            background-color: rgba(0, 0, 0, 0.5);
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            z-index: 1001;
        }

        .vertical-center {
            margin: 0;
            position: absolute;
            top: 43%;
            -ms-transform: translateY(-43%);
            transform: translateY(-43%);
            left: 50%;
            -ms-transform: translateX(-50%);
            transform: translateX(-50%);
        }

        .info {
            width: 150px;
            padding: 6px 8px;
            /*font: 14px/16px Arial, Helvetica, sans-serif; */
            font: 14px/16px 'Open Sans';
            background: white;
            background: rgba(255, 255, 255, 0.8);
            box-shadow: 0 0 15px rgba(0, 0, 0, 0.2);
            border-radius: 5px;
        }

        .info p {
            font-weight: 100;
            margin: 0;
        }

        .info h4 {
            font-size: 12px;
            margin: 0 0 10px;
            color: #777;
        }

        .info_wrap {
            margin: 5px 0;
        }

        .legend {
            text-align: left;
            line-height: 18px;
            color: #555;
        }

        .legend i {
            width: 18px;
            height: 18px;
            float: left;
            margin-right: 8px;
            opacity: 0.7;
        }
    </style>
    <!--breadcrumb area-->
    <section class="breadcrumb-area dark-overlay" style="background: url('assets/images/banners/1.jpg') no-repeat fixed;">
        <div class="container" >
            <div class="row">
                <div class="col-xl-8 col-lg-8 col-md-12 col-sm-12">
                    <br>
                    <div class="banner-title">
                        <h2>Data Statistik</h2>
                    </div>
                    <ul>
                        <li><a href="#">Data Statistik</a></li>
                        <li>Info Corona</li>
                    </ul>
                </div>
            </div>
        </div>
    </section><!--/breadcrumb area-->

    <section class="section pt-0 position-relative pull-top">
        <div class="container-fluid pl-lg-5 pr-lg-5 pl-md-3 pr-md-3 pl-sm-2 pr-sm-2">
           <div class="rounded-15 shadow bg-white">
              <div class="row justify-content-center">
                 <div class="col-lg-12 col-md-12 mb-12 text-center p-4">
                    <h3 class="font-hotline font-weight-bold p-3 mb-3">Statistik Kasus <span class="corona-text">COVID-19</span> Kabupaten Wonogiri</h3>
                    <p class="text-detail"><b>Update Terakhir : </b> {{getTanggalHariIni()}} | {{getWaktuHariIni()}} | <b>Sumber Data : </b> Dinas Kesehatan Kabupaten Wonogiri
                 </div>
              </div>
              <div class="row justify-content-center mr-lg-3 mr-sm-2 ml-lg-3 ml-sm-2 ml-2 mr-2">
                @foreach($kategori_kasus as $data)
                <div class="col-lg-4 col-md-12 mb-4">
                    <div class="card shadow card-extend">
                       <ul class="list-group list-group-flush">
                          <li class="list-group-item text-center list-header-red font-status"> <i class="fa fa-circle @if($data->id == getconfigvalues('KATEGORI_KASUS_PDP')[0]) fc-orange @elseif($data->id == getconfigvalues('KATEGORI_KASUS_ODP')[0])  fc-ungu @else  fc-red @endif"></i> {{$data->kategori_kasus}} </li>
                          <li class="list-group-item text-center">
                             <h3 class="font-counter @if($data->id == getconfigvalues('KATEGORI_KASUS_PDP')[0]) fc-orange @elseif($data->id == getconfigvalues('KATEGORI_KASUS_ODP')[0])  fc-ungu @else  fc-red @endif">{{getJumlahKasus($data->id)}}</h3>
                          </li>
                          <li class="list-group-item list-group-last text-center">
                             <ul class="list-unstyled list-kasus row">
                                 @foreach(getsubKategori($data->id) as $value)
                                <li class="list-item col-lg-4 col-md-4 col-sm-4 col-4 text-center list-header-default font-status">
                                   <div class="font-counter-2 @if($data->id == getconfigvalues('KATEGORI_KASUS_PDP')[0]) fc-orange @elseif($data->id == getconfigvalues('KATEGORI_KASUS_ODP')[0])  fc-ungu @else  fc-red @endif">
                                    {{getJumlahSubKasus($value->id)}} <span class="badge @if($data->id == getconfigvalues('KATEGORI_KASUS_PDP')[0])  badge-default bg-kuning text-white @elseif($data->id == getconfigvalues('KATEGORI_KASUS_ODP')[0])   badge-default bg-ungu text-white @else  badge-danger @endif font-13">{{getPresentaseSubKasusHariIni($value->id)}} % </span>&nbsp;<span class="@if($data->id == getconfigvalues('KATEGORI_KASUS_PDP')[0]) fc-orange @elseif($data->id == getconfigvalues('KATEGORI_KASUS_ODP')[0])  fc-ungu @else  fc-red @endif font-13"><i class="fa fa-angle-double-up"></i>  {{getJumlahSubKasusHariIni($value->id)}} </span> 
                                      <h6 class="font-kasus font-14 pt-1">{{$value->sub_kategori_kasus}}</h6>
                                   </div>
                                </li>
                                @endforeach
                             </ul>
                          </li>
                       </ul>
                    </div>
                 </div>
                 @endforeach
                 {{-- <div class="col-lg-4 col-md-12 mb-4">
                    <div class="card shadow card-extend">
                       <ul class="list-group list-group-flush">
                          <li class="list-group-item text-center list-header-red font-status"> <i class="fa fa-circle fc-red"></i> Positif COVID-19 </li>
                          <li class="list-group-item text-center">
                             <h3 class="font-counter fc-red">13</h3>
                          </li>
                          <li class="list-group-item list-group-last text-center">
                             <ul class="list-unstyled list-kasus row">
                                <li class="list-item col-lg-4 col-md-4 col-sm-4 col-4 text-center list-header-default font-status">
                                   <div class="font-counter-2 fc-red">
                                      7 <span class="badge badge-danger font-13">11,00 % </span>&nbsp;<span class="fc-red font-13"><i class="fa fa-angle-double-up"></i> 6</span> 
                                      <h6 class="font-kasus font-14 pt-1">Dirawat</h6>
                                   </div>
                                </li>
                                <li class="list-item col-lg-4 col-md-4 col-sm-4 col-4 text-center list-header-default font-status">
                                   <div class="font-counter-2 fc-green">
                                    41 <span class="badge badge-success font-13">0,01 %</span>&nbsp;<span class="fc-green font-13"><i class="fa fa-angle-double-up"></i> 0</span> 
                                      <h6 class="font-kasus font-14 pt-1">Sembuh</h6>
                                   </div>
                                </li>
                                <li class="list-item col-lg-4 col-md-4 col-sm-4 col-4 text-center list-header-default font-status">
                                   <div class="font-counter-2 text-black">
                                      2 <span class="d-inline d-lg-none d-md-none text-white"><br /></span> <span class="badge badge-default bg-black text-white font-13">8,42 %</span>&nbsp;<span class="fc-black font-13"><i class="fa fa-angle-double-up"></i> 1</span> 
                                      <h6 class="font-kasus font-14 pt-1">Meninggal</h6>
                                   </div>
                                </li>
                             </ul>
                          </li>
                       </ul>
                    </div>
                 </div> --}}
                 {{-- <div class="col-lg-4 col-md-12 mb-4 pl-lg-0 pr-lg-0">
                    <div class="card shadow card-extend">
                       <ul class="list-group list-group-flush">
                          <li class="list-group-item text-center list-header-oren font-status"> <i class="fa fa-circle fc-orange"></i> PDP (Pasien Dalam Pengawasan) </li>
                          <li class="list-group-item text-center">
                             <h3 class="font-counter fc-orange">30</h3>
                          </li>
                          <li class="list-group-item list-group-last text-center">
                             <ul class="list-unstyled list-kasus row">
                                <li class="list-item col-lg-4 col-md-4 col-sm-4 col-4 text-center list-header-default font-status">
                                   <div class="font-counter-2 fc-orange">
                                      10 <span class="d-inline d-lg-none d-md-none text-white"><br /></span> <span class="badge badge-default bg-kuning text-white font-13">11,04 %</span>&nbsp;<span class="fc-orange font-13"><i class="fa fa-angle-double-up"></i> 0</span> 
                                      <h6 class="font-kasus font-14 pt-1">Dirawat</h6>
                                   </div>
                                </li>
                                <li class="list-item col-lg-4 col-md-4 col-sm-4 col-4 text-center list-header-default font-status">
                                   <div class="font-counter-2 fc-orange">
                                      18 <span class="badge badge-default bg-kuning text-white font-13">75,22 %</span>&nbsp;<span class="fc-orange font-13"><i class="fa fa-angle-double-up"></i> 0</span> 
                                      <h6 class="font-kasus font-14 pt-1">Sembuh</h6>
                                   </div>
                                </li>
                                <li class="list-item col-lg-4 col-md-4 col-sm-4 col-4 text-center list-header-default font-status">
                                   <div class="font-counter-2 fc-orange">
                                      1 <span class="badge badge-default bg-kuning text-white font-13">13,75 %</span>&nbsp;<span class="fc-orange font-13"><i class="fa fa-angle-double-up"></i> 0</span> 
                                      <h6 class="font-kasus font-14 pt-1">Meninggal</h6>
                                   </div>
                                </li>
                             </ul>
                          </li>
                       </ul>
                    </div>
                 </div>
                 <div class="col-lg-4 col-md-12 mb-4">
                    <div class="card shadow card-extend">
                       <ul class="list-group list-group-flush">
                          <li class="list-group-item text-center list-header-ungu font-status"> <i class="fa fa-circle fc-ungu"></i> ODP (Orang Dalam Pemantauan) </li>
                          <li class="list-group-item text-center">
                             <h3 class="font-counter fc-ungu">45</h3>
                          </li>
                          <li class="list-group-item list-group-last text-center">
                             <ul class="list-unstyled list-kasus-odp row">
                                <li class="list-item col-lg-6 col-md-6 col-sm-6 col-6 text-center list-header-default font-status">
                                   <div class="font-counter-2 fc-ungu">
                                      30 <span class="d-inline d-lg-none d-md-none text-white"><br /></span> <span class="badge badge-default bg-ungu text-white font-13">1,38 %</span> &nbsp;<span class="fc-ungu font-13"><i class="fa fa-angle-double-up"></i> 0</span> 
                                      <h6 class="font-kasus font-13 pt-1">Dalam Pemantauan</h6>
                                   </div>
                                </li>
                                <li class="list-item col-lg-6 col-md-6 col-sm-6 col-6 text-center list-header-default font-status">
                                   <div class="font-counter-2 fc-ungu">
                                      15 <span class="d-inline d-lg-none d-md-none text-white"><br /></span> <span class="badge badge-default bg-ungu text-white font-13">98,62 %</span> &nbsp;<span class="fc-ungu font-13"><i class="fa fa-angle-double-up"></i> 0</span> 
                                      <h6 class="font-kasus font-13 pt-1">Selesai Pemantauan</h6>
                                   </div>
                                </li>
                             </ul>
                          </li>
                       </ul>
                    </div>
                 </div> --}}
              </div>
              <br>
              <br>
              <div class="row justify-content-center mr-lg-2 mr-sm-2 ml-lg-2 ml-sm-2 ml-2 mr-2">

              <div class="text-center mb-3"> 
                  <button type="button" class="btn btn-sm btn-link mb-2" id="more_covid">Tampilkan lebih banyak <i class="fa fa-chevron-down"></i></button> 
                 
                </div>
            </div>
              <div class="row justify-content-center mr-lg-2 mr-sm-2 ml-lg-2 ml-sm-2 ml-2 mr-2">
                 <div class="col-lg-12 col-md-12 col-sm-12">
                    <span class="fc-black ml-3"><b><h4>{{getJumlahKasusSemua()}} Jumlah Kasus Tercatat</h4></b></span> 
                    <br>
                    <ul class="list-unstyled row list-kasus-rs ml-2 mr-2" id="">
                        <li class="list-item col-lg-4  py-2 "> <b></b> Nama Kecamatan</b></li>
                        @foreach($kategori_kasus as $data)
                        <li class="list-item col-lg-2  py-2 @if($data->id == getconfigvalues('KATEGORI_KASUS_PDP')[0]) bg-kuning text-white @elseif($data->id == getconfigvalues('KATEGORI_KASUS_ODP')[0])   bg-ungu text-white @else  bg-danger text-white  @endif"> {{$data->kategori_kasus}}</li>
                        <li class="list-item col-lg-1 py-1 "></li>
                        @endforeach
                     </ul>
                     <hr>
                     <br>
                     @foreach(getKecamatan() as $data)
                    <ul class="list-unstyled row list-kasus-rs ml-2 mr-2" id="">
                       <li class="list-item col-lg-4  py-2 "> <b></b> {{$data->kecamatan}}</b></li>
                       @foreach($kategori_kasus as $value)
                       <li class="list-item col-lg-2  py-2 bg-gray"> <b>{{getJumlahSubKasuskecamatan($value->id,$data->id)}} Kasus </b></li>
                       <li class="list-item col-lg-1 py-1 "></li>

                       @endforeach
                    </ul>
                    @endforeach
                    <br>
                    <br>
                    <div class="text-center mb-3"> 
                    <button type="button" class="btn btn-sm btn-link mb-2" id="less_covid">Tampilkan lebih sedikit <i class="fa fa-chevron-up"></i></button> 
                </div>
                 </div>
             
              </div>
           </div>
        </div>
     </section>  <br>
     <br>
    <section><!--/Map disini-->
        <br>
        <br>
        <div class="row justify-content-center">
            <div class="col-xl-6 centered">
               <div class="section-title">
                   <h1>Peta Sebaran Kasus</h1>
                   <h4> Covid-19 Kabupaten Wonogiri </h4>
                   <p class="text-detail"><b>Update Terakhir : </b> {{getTanggalHariIni()}} | {{getWaktuHariIni()}}| <b>Sumber Data : </b> Dinas Kesehatan Kabupaten Wonogiri

               </div>
            </div>
        </div>
        <div class="container" style="max-width: 1500px;">
            <div class="row justify-content-sm-center">
             
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="container-fluid p-0 flex-fill d-flex container-maps">

                        <div class="card no-border py-5 px-3" style="width: 20%;">
                            <div class="layer list_penyakit d-none">
                                <div id="loaders_chart_heatmap" class="vertical-center">
                                    <div class="lds-spinner-white">
                                        <div></div>
                                        <div></div>
                                        <div></div>
                                        <div></div>
                                        <div></div>
                                        <div></div>
                                        <div></div>
                                        <div></div>
                                        <div></div>
                                        <div></div>
                                        <div></div>
                                        <div></div>
                                    </div>
                                </div>
                            </div>
                            <h4 id="select_penyakit_title" class="mb-5 text-center">Kategori Kasus</h4>
                            <div id="select_status_pasien" class="list-group text-center">
                                {{-- <a href="#" class="list-group-item list-group-item-action active" status_pasien="1">Orang Dalam Pemantauan (ODP)</a>
                                <a href="#" class="list-group-item list-group-item-action" status_pasien="2">Pasien Dalam Pengawasan(PDP)</a>
                                <a href="#" class="list-group-item list-group-item-action" status_pasien="3">Pasien Positif Covid-19</a> --}}
                                @foreach($kategori_kasus as $data)
                                <a href="#" class="list-group-item list-group-item-action" status_pasien="{{$data->id}}">{{$data->kategori_kasus}}</a>

                                @endforeach
                            </div>
                        </div>
                        <div class="card no-border flex-fill">
                            <div class="layer map_area d-none">
                                <div id="loaders_chart_heatmap" class="vertical-center">
                                    <div class="lds-spinner-white">
                                        <div></div>
                                        <div></div>
                                        <div></div>
                                        <div></div>
                                        <div></div>
                                        <div></div>
                                        <div></div>
                                        <div></div>
                                        <div></div>
                                        <div></div>
                                        <div></div>
                                        <div></div>
                                    </div>
                                </div>
                            </div>
                            <div class="d-flex flex-fill">
                                <div class="card-body p-0 d-flex">
                                    
                                    <div id="container_map_choropleth" class="flex-fill">
                                        <div id="map_choropleth" class="map_leaflet"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-lg-12 col-md-12 col-sm-12 mr-3 ml-3">
                   <p class="text-keterangan mb-2 font-weight-bold py-3">Catatan : </p>
                   <ul class="list-unstyled">
                      <li class="pb-3 text-detail text-justify d-flex">
                         <div class="icon-wrapper text-center"><i class="fa fa-check fc-green pr-3"></i></div>
                         <div class="text-wrapper">
                            <p> Perbesar peta untuk memperjelas titik akurat.</p>
                         </div>
                      </li>
                      <li class="pb-3 text-detail text-justify d-flex">
                         <div class="icon-wrapper text-center"><i class="fa fa-check fc-green pr-3"></i></div>
                         <div class="text-wrapper">
                            <p> Setiap titik merujuk pada Kelurahan (bukan lokasi/tempat tinggal pasien).</p>
                         </div>
                      </li>
                      <li class="pb-3 text-detail text-justify d-flex">
                         <div class="icon-wrapper text-center"><i class="fa fa-check fc-green pr-3"></i></div>
                         <div class="text-wrapper">
                            <p> Data yang ditampilkan dalam peta terus diperbaharui sesuai dengan informasi dari Dinas Kesehatan Kabupaten Wonogiri.</p>
                         </div>
                      </li>
                      <li class="pb-3 text-detail text-justify d-flex">
                         <div class="icon-wrapper text-center"><i class="fa fa-check fc-green pr-3"></i></div>
                         <div class="text-wrapper">
                            <p> Orang Dalam Pemantauan (ODP) adalah seseorang yang mengalami gejala demam (>38 C) atau riwayat demam tanpa pneumonia yang memiliki riwayat perjalanan ke wilayah yang terjangkit.</p>
                         </div>
                      </li>
                      <li class="pb-3 text-detail text-justify d-flex">
                         <div class="icon-wrapper text-center"><i class="fa fa-check fc-green pr-3"></i></div>
                         <div class="text-wrapper">
                            <p>Pasien Dalam Pemantauan (PDP) adalah pasien pnemonia ringan hingga berat yang mengalami demam (>38 C) atau riwayat demam dan memiliki riwayat kontak dengan hewan penular, riwayat kontak dengan pasien COVID-19, atau riwayat perjalanan ke negara terjangkit dalam 14 hari.</p>
                         </div>
                      </li>
                   </ul>
                </div>
             </div>
        </div>
    
    </section>
    
<br>
<br>
<br>

   <section class="section-padding dark-overlay" style="background: url('{{ asset('admin_remark_base/') }}/website/assets/images/banners/1.jpg') no-repeat fixed;">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-8 centered cl-white">
                <div class="section-title mb-20">
                    <h1>Chatbot TanggapCorona</h1><br>
                    <h2>Dapatkan Informasi Terbaru terkait Covid-19 di Kabupaten Wonogiri</h2>
                </div>
                <div class="newslatter">
                    <form action="{{url('/sendEmailNotifikasi')}}">
                        <input type="email" name="email" placeholder="Ketik Email Anda untuk registrasi" required>
                        <button type="submit"><i class="fa fa-angle-right"></i></button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
    

    @endsection
    @push('js')
    <!-- Heatmap -->
    <link rel="stylesheet" href="{{ asset('admin_remark_base/') }}/assets/css/leaflet.css" />
    <script src="{{ asset('admin_remark_base/') }}/assets/js/leaflet.js"></script>
    {{-- <script src="{{ asset('admin_remark_base/') }}/assets/js/sampel-data-wonogiri.js"></script> --}}

    <script type="text/javascript">
 
let data_wonogiri={
"type": "FeatureCollection",
"features": [
    <?php foreach($peta as $value) { ?>
    {
      "type": "Feature",
      "properties": {
    
        "DESA": "<?php echo $value->DESA; ?>",
        "value": {
            <?php foreach($kategori_kasus as $data) { ?>
                <?php echo $data->id; ?>: {
          
            jumlah: '<?php echo getJumlahKasusWilayah($data->id,$value->IDDESANO); ?>',
          },
          <?php } ?>
        },
      },
      "geometry": {
        "type": "MultiPolygon",
        "coordinates": [
          [
            [
                <?php foreach(getcoordinate($value->IDDESANO) as $row) { ?>
              [
                <?php echo $row->longitude; ?>,
                <?php echo $row->latitude; ?>,
                0
              ],
              <?php } ?>
            ]
          ]
        ]
      }
    },
    <?php } ?>
]
}
        // get color depending on population density value
        function getColor(d) {
            return d > 50 ? '#800026' :
                d > 40 ? '#BD0026' :
                d > 30 ? '#E31A1C' :
                d > 20 ? '#FC4E2A' :
                d > 15 ? '#FD8D3C' :
                d > 10 ? '#FEB24C' :
                d > 5 ? '#FED976' :
                d > 0 ? '#FED976' :
                d = 0 ? '#FFEDA0' :
                '#28a745';
        }
        
        function create_choropleth(map, data_choropleth, status_pasien, info, geojson) {

            // give basic style to the features
            function style(feature) {
                return {
                    weight: 1,
                    opacity: 1,
                    color: 'white',
                    dashArray: '0',
                    fillOpacity: 0.7,
                    fillColor: getColor(feature.properties.value[status_pasien].jumlah)
                };
            }

            function highlightFeature(e) {
                var layer = e.target;

                layer.setStyle({
                    weight: 3,
                    color: 'white',
                    dashArray: '',
                    fillOpacity: 0.6,
                });

                if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
                    layer.bringToFront();
                }

                info.update(layer.feature.properties);
            }

            //create habit style of the features

            function resetHighlight(e) {
                geojson.resetStyle(e.target);
                info.update();
            }

            function zoomToFeature(e) {
                map.fitBounds(e.target.getBounds());
            }

            function onEachFeature(feature, layer) {
                layer.on({
                    mouseover: highlightFeature,
                    mouseout: resetHighlight,
                    click: zoomToFeature,
                    click: consoleFeature,
                });
            }

            function consoleFeature(e) {
                console.log(e.target);
            }

            //insert data to map using geojson.
            geojson = L.geoJson(data_choropleth, {
                style: style,
                onEachFeature: onEachFeature,
            }).addTo(map);

            // add somtehing to bottom of the maps
            // ex : Population data &copy; <a href="http://census.gov/">US Census Bureau</a>
            map.attributionControl.addAttribution('');
            return geojson;
        }

        function create_map_base() {
            // initial map
            var map = L.map('map_choropleth', {
                scrollWheelZoom: false,
                // wheelPxPerZoomLevel:60,
            }).setView([-7.9446999,110.9641703], 11);
            var tiles = L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                maxZoom: 18,
                attribution: '&copy; Infokes Wonogiri',
            }).addTo(map);

            map.on('click', function () {
                if (map.scrollWheelZoom.enabled()) {
                    map.scrollWheelZoom.disable();
                } else {
                    map.scrollWheelZoom.enable();
                }
            });

            return map;
        }

        function add_geojson_to_map(map,status_pasien,data_geojson, geojson){
            
            // Create control that shows state info on hover
            var info = L.control();

            info.onAdd = function (map) {
                this._div = L.DomUtil.create('div', 'info');
                this.update();
                return this._div;
            };

            info.update = function (props) {
                this._div.innerHTML = '<h4>Jumlah Kasus</h4>' + (props ?
                    '<b>' + props.DESA + 
                    '</b><br /> <p>' + 
                     props.value[status_pasien].jumlah + ' Kasus' :
                    '<br><br>') + '</p>';
            };

            info.addTo(map);


            //create legend for map
            var legend = L.control({
                position: 'bottomright'
            });

            legend.onAdd = function (map) {

                // create element with the class
                var div = L.DomUtil.create('div', 'info legend'),
                    grades = [0,1,5, 10, 15, 20, 30, 40, 50],
                    labels = [],
                    from, to;

                for (var i = 0; i < grades.length; i++) {
                    from = grades[i];
                    to = grades[i + 1];

                    labels.push(
                        '<div class="info_wrap"> <i style="background:' + getColor(from + 1) + '"></i> <p>' +
                        from + (to ? ' &ndash; ' + to : '+') + '</p> </div>');
                }

                div.innerHTML = labels.join('');
                return div;
            };
            legend.addTo(map);

            
            geojsonLayer = create_choropleth(map, data_geojson, status_pasien, info, geojson);

            $('#select_penyakit_choropleth').addClass('active')
    
    
            $('a.list-group-item').click(function (e) {
                e.preventDefault();
                $('a.list-group-item.active').removeClass("active");
                $(this).addClass("active");
    
                status_pasien = $(this).attr("status_pasien");
                if (geojsonLayer) {
                    map.removeLayer(geojsonLayer);
                };
                geojsonLayer = create_choropleth(map, data_geojson, status_pasien, info, geojson);
    
            });
        }



        $(function() {

            status_pasien = 1;
            let geojson;
            let geojsonLayer;
   
            let map = create_map_base();
            map = add_geojson_to_map(map, status_pasien, data_wonogiri, geojson);
            
        });
</script>
    @endpush 