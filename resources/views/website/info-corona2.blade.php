<!DOCTYPE html> 
<html lang="en">
   <!-- Mirrored from corona.jatengprov.go.id/data by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 09 Jul 2020 23:57:09 GMT -->
   <!-- Added by HTTrack -->
   <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
   <!-- /Added by HTTrack -->
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Jateng Tanggap COVID-19</title>
      <link href="assets/images/logo.png" rel="shortcut icon">
      <link rel="stylesheet" href="assets/plugins/bootstrap/bootstrap.min.css">
      <link rel="stylesheet" href="assets/plugins/themify-icons/themify-icons.css">
      <link rel="stylesheet" href="assets/css/owl.carousel.min.css" media="screen">
      <link rel="stylesheet" href="assets/css/owl.theme.green.min.css" media="screen">
      <link rel="stylesheet" href="assets/css/fontawesome-all.css" media="screen">
      <link rel="stylesheet" href="assets/plugins/fancybox/jquery.fancybox.min.css">
      <link rel="stylesheet" href="../cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
      <link rel="stylesheet" href="assets/css/stylev283a7.css?v=1.9">
      <link href="https://fonts.googleapis.com/css?family=Karla&amp;display=swap" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Rubik&amp;display=swap" rel="stylesheet">
      <!-- JAVASCRIPTS --> <script src="assets/plugins/jquery/jquery.js"></script> <script src="assets/plugins/bootstrap/bootstrap.min.js"></script> <script src="assets/js/jquery.easing.min.js"></script> <script src="assets/js/owl.carousel.min.js"></script> <script src="assets/plugins/fancybox/jquery.fancybox.min.js"></script> <script src="assets/js/script3cc5.js?v=1.6"></script> <!-- Global site tag (gtag.js) - Google Analytics --> <script async src="https://www.googletagmanager.com/gtag/js?id=G-7F84FDTJZC"></script> <script> window.dataLayer = window.dataLayer || []; function gtag() { dataLayer.push(arguments); } gtag('js', new Date()); gtag('config', 'G-7F84FDTJZC'); </script> 
   </head>
   <body class="body-wrapper" data-spy="scroll" data-target=".privacy-nav">
      <nav id="navbar-fix" class="navbar main-nav navbar-expand-lg py-2 fixed-top navbar-toggleable-sm" data-toggle="affix">
         <div class="container-fluid container-nav">
            <a class="navbar-brand" href="index.html"> <img class="logo" src="assets/images/Putih.png" alt="logo"> </a> <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation"> <span class="icon-bar top-bar"></span> <span class="icon-bar middle-bar"></span> <span class="icon-bar bottom-bar"></span> </button> 
            <div class="collapse navbar-collapse" id="navbarNav">
               <ul class="navbar-nav ml-auto link-hover">
                  <li class="nav-item"> <a class="nav-link smoth-scroll" href="v2.html">Beranda</a> </li>
                  <li class="nav-item"> <a class="nav-link smoth-scroll" href="data.html">Data</a> </li>
                  <li class="nav-item"> <a class="nav-link smoth-scroll" href="berita.html">Berita</a> </li>
                  <li class="nav-item"> <a class="nav-link smoth-scroll" href="rumah-sakit.html">RS Rujukan</a> </li>
                  <li class="nav-item"> <a class="nav-link smoth-scroll" href="donasi.html">Donasi</a> </li>
                  <li class="nav-item"> <a class="nav-link smoth-scroll" href="tanya-jawab.html">F.A.Q</a> </li>
                  <li class="nav-item"> <a class="nav-link smoth-scroll" href="unduhan.html">Download</a> </li>
                  <li class="nav-item"> <a class="nav-link smoth-scroll" href="link-kabupaten-kota.html">Link</a> </li>
                  <li class="nav-item"> <a class="nav-link" href="screening.html">Deteksi Mandiri</a> </li>
               </ul>
            </div>
         </div>
      </nav>
      <script src="assets/js/echarts.min.js"></script> <script src="assets/js/data.js"></script> 
      <section class="section gradient-banner">
         <div class="container"></div>
      </section>
      <section class="section pt-0 position-relative pull-top">
         <div class="container-fluid pl-lg-5 pr-lg-5 pl-md-3 pr-md-3 pl-sm-2 pr-sm-2">
            <div class="rounded-15 shadow bg-white">
               <div class="row justify-content-center">
                  <div class="col-lg-12 col-md-12 mb-12 text-center p-4">
                     <h3 class="font-hotline font-weight-bold p-3 mb-3">Sebaran Kasus <span class="corona-text">COVID-19</span> Di Jawa Tengah</h3>
                     <p class="text-detail"><b>Update Terakhir : </b> Jumat, 10 Juli 2020 | 06:56 | <b>Sumber Data : </b> Dinas Kesehatan Provinsi Jawa Tengah 
                  </div>
               </div>
               <div class="row justify-content-center mr-lg-3 mr-sm-2 ml-lg-3 ml-sm-2 ml-2 mr-2">
                  <div class="col-lg-4 col-md-12 mb-4">
                     <div class="card shadow card-extend">
                        <ul class="list-group list-group-flush">
                           <li class="list-group-item text-center list-header-red font-status"> <i class="fa fa-circle fc-red"></i> Positif COVID-19 </li>
                           <li class="list-group-item text-center">
                              <h3 class="font-counter fc-red">5.545</h3>
                           </li>
                           <li class="list-group-item list-group-last text-center">
                              <ul class="list-unstyled list-kasus row">
                                 <li class="list-item col-lg-4 col-md-4 col-sm-4 col-4 text-center list-header-default font-status">
                                    <div class="font-counter-2 fc-red">
                                       2.329 <span class="badge badge-danger font-13">42,00 % </span>&nbsp;<span class="fc-red font-13"><i class="fa fa-angle-double-up"></i> 6</span> 
                                       <h6 class="font-kasus font-14 pt-1">Dirawat</h6>
                                    </div>
                                 </li>
                                 <li class="list-item col-lg-4 col-md-4 col-sm-4 col-4 text-center list-header-default font-status">
                                    <div class="font-counter-2 fc-green">
                                       2.749 <span class="badge badge-success font-13">49,58 %</span>&nbsp;<span class="fc-green font-13"><i class="fa fa-angle-double-up"></i> 0</span> 
                                       <h6 class="font-kasus font-14 pt-1">Sembuh</h6>
                                    </div>
                                 </li>
                                 <li class="list-item col-lg-4 col-md-4 col-sm-4 col-4 text-center list-header-default font-status">
                                    <div class="font-counter-2 text-black">
                                       467 <span class="d-inline d-lg-none d-md-none text-white"><br /></span> <span class="badge badge-default bg-black text-white font-13">8,42 %</span>&nbsp;<span class="fc-black font-13"><i class="fa fa-angle-double-up"></i> 1</span> 
                                       <h6 class="font-kasus font-14 pt-1">Meninggal</h6>
                                    </div>
                                 </li>
                              </ul>
                           </li>
                        </ul>
                     </div>
                  </div>
                  <div class="col-lg-4 col-md-12 mb-4 pl-lg-0 pr-lg-0">
                     <div class="card shadow card-extend">
                        <ul class="list-group list-group-flush">
                           <li class="list-group-item text-center list-header-oren font-status"> <i class="fa fa-circle fc-orange"></i> PDP (Pasien Dalam Pengawasan) </li>
                           <li class="list-group-item text-center">
                              <h3 class="font-counter fc-orange">9.668</h3>
                           </li>
                           <li class="list-group-item list-group-last text-center">
                              <ul class="list-unstyled list-kasus row">
                                 <li class="list-item col-lg-4 col-md-4 col-sm-4 col-4 text-center list-header-default font-status">
                                    <div class="font-counter-2 fc-orange">
                                       1.067 <span class="d-inline d-lg-none d-md-none text-white"><br /></span> <span class="badge badge-default bg-kuning text-white font-13">11,04 %</span>&nbsp;<span class="fc-orange font-13"><i class="fa fa-angle-double-up"></i> 0</span> 
                                       <h6 class="font-kasus font-14 pt-1">Dirawat</h6>
                                    </div>
                                 </li>
                                 <li class="list-item col-lg-4 col-md-4 col-sm-4 col-4 text-center list-header-default font-status">
                                    <div class="font-counter-2 fc-orange">
                                       7.272 <span class="badge badge-default bg-kuning text-white font-13">75,22 %</span>&nbsp;<span class="fc-orange font-13"><i class="fa fa-angle-double-up"></i> 0</span> 
                                       <h6 class="font-kasus font-14 pt-1">Sembuh</h6>
                                    </div>
                                 </li>
                                 <li class="list-item col-lg-4 col-md-4 col-sm-4 col-4 text-center list-header-default font-status">
                                    <div class="font-counter-2 fc-orange">
                                       1.329 <span class="badge badge-default bg-kuning text-white font-13">13,75 %</span>&nbsp;<span class="fc-orange font-13"><i class="fa fa-angle-double-up"></i> 0</span> 
                                       <h6 class="font-kasus font-14 pt-1">Meninggal</h6>
                                    </div>
                                 </li>
                              </ul>
                           </li>
                        </ul>
                     </div>
                  </div>
                  <div class="col-lg-4 col-md-12 mb-4">
                     <div class="card shadow card-extend">
                        <ul class="list-group list-group-flush">
                           <li class="list-group-item text-center list-header-ungu font-status"> <i class="fa fa-circle fc-ungu"></i> ODP (Orang Dalam Pemantauan) </li>
                           <li class="list-group-item text-center">
                              <h3 class="font-counter fc-ungu">48.896</h3>
                           </li>
                           <li class="list-group-item list-group-last text-center">
                              <ul class="list-unstyled list-kasus-odp row">
                                 <li class="list-item col-lg-6 col-md-6 col-sm-6 col-6 text-center list-header-default font-status">
                                    <div class="font-counter-2 fc-ungu">
                                       677 <span class="d-inline d-lg-none d-md-none text-white"><br /></span> <span class="badge badge-default bg-ungu text-white font-13">1,38 %</span> &nbsp;<span class="fc-ungu font-13"><i class="fa fa-angle-double-up"></i> 0</span> 
                                       <h6 class="font-kasus font-13 pt-1">Dalam Pemantauan</h6>
                                    </div>
                                 </li>
                                 <li class="list-item col-lg-6 col-md-6 col-sm-6 col-6 text-center list-header-default font-status">
                                    <div class="font-counter-2 fc-ungu">
                                       48.219 <span class="d-inline d-lg-none d-md-none text-white"><br /></span> <span class="badge badge-default bg-ungu text-white font-13">98,62 %</span> &nbsp;<span class="fc-ungu font-13"><i class="fa fa-angle-double-up"></i> 0</span> 
                                       <h6 class="font-kasus font-13 pt-1">Selesai Pemantauan</h6>
                                    </div>
                                 </li>
                              </ul>
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
               <div class="row justify-content-center mr-lg-2 mr-sm-2 ml-lg-2 ml-sm-2 ml-2 mr-2">
                  <div class="col-lg-12 col-md-12 col-sm-12">
                     <span class="fc-red ml-3"><b>2329 Kasus Dirawat COVID-19</b></span> 
                     <ul class="list-unstyled row list-kasus-rs ml-2 mr-2" id="list_covid">
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>341</b> Dinkes Kota Semarang</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>265</b> Dinkes Kabupaten Jepara</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>134</b> Dinkes Kabupaten Demak</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>104</b> RSUP Dr. Kariadi Semarang</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>95</b> RSUD K.R.M.T Wongsonegoro Semarang</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>82</b> RSU Mardi Rahayu Kudus</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>78</b> RSUD Loekmono Hadi Kudus</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>69</b> RS Panti Wilasa Citarum</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>63</b> RSUD Sunan Kalijaga Demak</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>58</b> Dinkes Kabupaten Semarang</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>57</b> RSUD RA Kartini Jepara</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>46</b> RS Panti Wilasa dr. Cipto</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>39</b> RS PKU Muhammadiyah Mayong</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>37</b> RSUD dr R. Soetrasno Rembang</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>34</b> RSU Sultan Agung Semarang</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>33</b> RSU Telogorejo Semarang</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>31</b> RSUD Kelet Jepara</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>29</b> Dinkes Kabupaten Kendal</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>26</b> RSUD Ki Ageng Selo Wirosari Grobogan</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>24</b> RSU Bhayangkara Semarang</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>19</b> RSUD Dr. Moewardi Solo</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>19</b> RS Roemani Muhammadiyah</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>19</b> RSU St. Elizabeth Semarang</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>19</b> RSU William Booth </b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>18</b> RS Hermina Pandanaran Kota Semarang</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>17</b> RSU Columbia Asia Semarang</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>17</b> Batealit</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>16</b> Dinkes Kabupaten Blora</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>16</b> Dinkes Kabupaten Batang</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>15</b> RSUD dr. R. Soedjati Grobogan</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>15</b> RS Nasional Diponegoro</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>15</b> RSUD dr. M Ashari Pemalang</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>15</b> RSUD Dr. H. Soewondo Kendal</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>14</b> RS Islam Sunan Kudus</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>14</b> Klinik Bhakti Padma</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>13</b> RSU Bhakti Wira Tamtama Semarang</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>13</b> RSUD Ki Ageng Getas Pendowo Gubug Grobogan</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>12</b> RSUD Batang</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>12</b> RS Pelita Anugerah</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>11</b> RSUD Banyumas</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>11</b> RS Panti Rahayu Yakkum</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>11</b> RSUD Tugurejo Semarang</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>11</b> Dinkes Kabupaten Boyolali</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>10</b> Kaliwungu</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>9</b> RSUP DR SOERADJI TIRTONEGORO KLATEN</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>9</b> RS Graha Husada Jepara</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>9</b> RS Islam Banjarnegara</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>9</b> Sidorejo Kidul</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>9</b> Karanggede</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>7</b> RSUD Kraton Pekalongan</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>7</b> RSUD Ambarawa</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>7</b> RSUD Bagas Waras Klaten</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>7</b> RSUD dr. Tjitrowardojo Purworejo</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>7</b> RS Islam Kendal</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>7</b> Dinkes Kabupaten Banyumas</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>7</b> RSU Universitas Sebelas Maret</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>7</b> RS Permata Medika Kota Semarang</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>7</b> Dinkes Kabupaten Karanganyar</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>6</b> Dinkes Kabupaten Sragen</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>6</b> RS PKU Muhammadiyah Gubug</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>5</b> RS Paru Ario Wirawan</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>5</b> RS Kasih Ibu Surakarta</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>5</b> RSUD dr. R. G. Taroenadibrata Purbalingga</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>5</b> RSUD Ungaran</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>5</b> RS Nurusyifa Kudus</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>5</b> Dinkes Kabupaten Temanggung</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>5</b> Mangunsari</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>5</b> Kalongan</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>5</b> Dersalam</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>4</b> RSUD Cilacap</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>4</b> RSUD Djoyonegoro Temanggung</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>4</b> RSUD dr. Soedirman Kab, Kebumen</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>4</b> RSU dr. Asmir Salatiga</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>4</b> RSU Islam Klaten</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>4</b> RSUD Bumiayu</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>4</b> RS dr. Oen Solo Baru</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>4</b> Dinkes Kabupaten Pekalongan</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>4</b> Nguter</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>4</b> Tengaran</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>4</b> Kembang</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>3</b> RSUD Brebes</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>3</b> RSUD Hj. Anna Lasmanah Banjarnegara</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>3</b> RSUD Pandan Arang Boyolali</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>3</b> RSUD Ajibarang</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>3</b> RSUD Karanganyar</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>3</b> RS Kumala Siwi Kudus</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>3</b> RS Asyiyah Kudus</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>3</b> RSUD Bendan Kota Pekalongan</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>3</b> Dinkes</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>3</b> Dinkes Kabupaten Sukoharjo</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>3</b> RS Harapan Sehat</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>3</b> RS Islam NU Demak</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>3</b> Tanjungrejo</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>3</b> Pagedongan</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>3</b> Bumiayu</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>2</b> RSU dr. Soedjono Magelang</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>2</b> RSUD Dr. H. RM. Seoselo Tegal</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>2</b> RSUD Tidar Kota Magelang</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>2</b> RSUD Soehadi Prijonegoro Sragen</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>2</b> RSUD Ir. Soekarno Sukoharjo</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>2</b> RSJP Prof. dr. Soerojo Magelang</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>2</b> RSUD Salatiga</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>2</b> RSU Indriati</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>2</b> RSUD dr. R. Soetijono Blora</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>2</b> RS Permata Medika</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>2</b> Dinkes Kabupaten Kebumen</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>2</b> RS PKU Muhammadiyah Cepu</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>2</b> RS Bhina Bhakti Husada Rembang</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>2</b> RS Permata Bunda</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>2</b> RSJD Dr. RM. Soedjarwadi</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>2</b> RSU PKU Muhammadiyah Banjarnegara</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>2</b> RSJD Surakarta</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>2</b> RS Siloam Kota Semarang</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>2</b> Undaan</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>2</b> Balai Kesehatan Masyarakat Wilayah Semarang</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>2</b> Bae</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>1</b> RSUD Kardinah Tegal</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>1</b> RS dr. Oen Surakarta</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>1</b> RSUD Prof. Dr. Margono Banyumas</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>1</b> RSU Siaga Medika Banyumas</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>1</b> RSUD dr. Soediran MS Wonogiri</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>1</b> RST Wijayakusuma Purwokerto</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>1</b> RSU Hermina Purwokerto</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>1</b> RSUD Muntilan Kab Magelang</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>1</b> RSU Siaga Medika</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>1</b> RSUD Budi Rahayu Magelang</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>1</b> RS PKU Muhammadiyah Sruweng</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>1</b> RSU Ananda Purwokerto</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>1</b> RSU Wiradadi Husada Banyumas</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>1</b> RS Emanuel Banjarnegara</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>1</b> RSUD R. Soeprapto Cepu</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>1</b> RSU Darul Istiqomah Kendal</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>1</b> Dinkes Kabupaten Klaten</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>1</b> Dinkes Kabupaten Magelang</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>1</b> RSK Ngesti Waluyo Parakan</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>1</b> RS Hermina Solo</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>1</b> RS Muhammadiyah Fastabiq Sehat</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>1</b> RSJD Dr. Amino Gondohutomo</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>1</b> RSI Muhammadiyah Rodliyah Achid</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>1</b> RSU Harapan Sehat</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>1</b> Ngemplak Simongan</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>1</b> Grogol</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>1</b> Sidorejo Lor</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>1</b> Baki</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>1</b> RSU PKU Muhammadiyah kutowinangun</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>1</b> RS Sejahtera Bhakti</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>1</b> Ampel</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>1</b> Jaten 1</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>1</b> Bumijawa</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>1</b> RSUD Limpung</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>1</b> Sambong</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>1</b> Jatisrono I</b></li>
                     </ul>
                     <div class="text-center mb-3"> <button type="button" class="btn btn-sm btn-link mb-2" id="more_covid">Tampilkan lebih banyak <i class="fa fa-chevron-down"></i></button> <button type="button" class="btn btn-sm fc-orange btn-link" id="less_covid">Sembunyikan beberapa <i class="fa fa-chevron-up"></i></button> </div>
                  </div>
                  <div class="col-lg-12 col-md-12 col-sm-12">
                     <span class="fc-green ml-3"><b>2749 Sembuh dan Pulang</b></span> 
                     <ul class="list-unstyled row list-kasus-rs ml-2 mr-2" id="list_sehat">
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>497</b> Dinkes Kota Semarang</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>204</b> RSUD K.R.M.T Wongsonegoro Semarang</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>139</b> Dinkes Kabupaten Temanggung</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>120</b> RSUP Dr. Kariadi Semarang</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>74</b> RSUD dr. Tjitrowardojo Purworejo</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>74</b> RSUD Merah Putih Kab Magelang</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>58</b> RSUD Setjonegoro Wonosobo</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>55</b> RSUD Djoyonegoro Temanggung</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>49</b> RSUD Dr. Moewardi Solo</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>49</b> RSU Mardi Rahayu Kudus</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>48</b> RSUD Loekmono Hadi Kudus</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>42</b> Dinkes Kabupaten Jepara</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>36</b> RSUD Sunan Kalijaga Demak</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>33</b> RS Panti Wilasa dr. Cipto</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>33</b> RSUD Tugurejo Semarang</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>32</b> Dinkes Kabupaten Demak</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>31</b> RS Kasih Ibu Surakarta</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>31</b> RSU Bhayangkara Semarang</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>31</b> RSUD dr. M Ashari Pemalang</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>31</b> RSUD Salatiga</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>30</b> RSUD dr. R. G. Taroenadibrata Purbalingga</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>29</b> Dinkes Kabupaten Boyolali</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>27</b> RSU St. Elizabeth Semarang</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>27</b> Dinkes Kabupaten Sukoharjo</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>26</b> RSUD Hj. Anna Lasmanah Banjarnegara</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>26</b> Dinkes Kabupaten Sragen</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>25</b> RSU Telogorejo Semarang</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>25</b> RSUD Majenang</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>24</b> RSUD Bagas Waras Klaten</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>24</b> RSUD dr R. Soetrasno Rembang</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>23</b> RSUD Banyumas</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>23</b> RS Paru Ario Wirawan</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>23</b> RSUD RA Kartini Jepara</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>21</b> RSUD Brebes</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>21</b> Dinkes Kabupaten Magelang</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>20</b> RSU Sultan Agung Semarang</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>20</b> RSU Columbia Asia Semarang</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>19</b> RSUD Cilacap</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>19</b> RSUD dr. Soedirman Kab, Kebumen</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>19</b> RSUD dr. R. Soedjati Grobogan</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>18</b> RSJP Prof. dr. Soerojo Magelang</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>18</b> RS QIM Batang</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>18</b> RSUD Ki Ageng Getas Pendowo Gubug Grobogan</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>17</b> RSUD Tidar Kota Magelang</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>16</b> RS Islam Sunan Kudus</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>16</b> RS Roemani Muhammadiyah</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>16</b> RSUD Muntilan Kab Magelang</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>15</b> RSUD Batang</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>15</b> RSUD Bung Karno Surakarta</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>14</b> RSUD Panti Nugroho</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>13</b> RSUD Dr. H. RM. Seoselo Tegal</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>13</b> RSUD Budi Rahayu Magelang</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>13</b> RS Islam Banjarnegara</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>13</b> Klinik Bhakti Padma</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>12</b> RSU dr. Soedjono Magelang</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>12</b> RSUD Prof. Dr. Margono Banyumas</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>12</b> RSU Siaga Medika Banyumas</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>12</b> RS PKU Muhammadiyah Wonosobo</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>12</b> RS Islam Wonosobo</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>11</b> RSU dr. Asmir Salatiga</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>10</b> RS Panti Wilasa Citarum</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>10</b> RSU Hermina Purwokerto</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>10</b> RSUD Ir. Soekarno Sukoharjo</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>10</b> RSUP DR SOERADJI TIRTONEGORO KLATEN</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>10</b> RSUD Bumiayu</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>10</b> Dinkes Kabupaten Klaten</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>9</b> RSUD dr. Soediran MS Wonogiri</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>9</b> RSUD Ajibarang</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>8</b> RSUD Kraton Pekalongan</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>8</b> RSUD RAA Soewondo Pati</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>8</b> RST Wijayakusuma Purwokerto</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>8</b> RS Nasional Diponegoro</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>8</b> RSU Bhakti Wira Tamtama Semarang</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>8</b> RSU Siaga Medika</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>8</b> RSUD Bendan Kota Pekalongan</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>8</b> Dinkes Kabupaten Kendal</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>8</b> RS Harapan Sehat</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>8</b> RSUD Ki Ageng Selo Wirosari Grobogan</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>7</b> RS dr. Oen Solo Baru</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>7</b> Dinkes Kabupaten Batang</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>6</b> RSU PKU Muh. Surakarta</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>6</b> RSUD Pandan Arang Boyolali</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>6</b> RSU Universitas Sebelas Maret</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>6</b> RS Pertamina Cilacap</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>6</b> RSU Duta Mulya</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>5</b> RSUD Kelet Jepara</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>5</b> RS Asyiyah Kudus</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>5</b> Dinkes Kabupaten Blora</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>5</b> RS Puri Asih</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>4</b> RSUD Kardinah Tegal</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>4</b> RS dr. Oen Surakarta</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>4</b> RS Nurusyifa Kudus</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>4</b> Undaan</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>3</b> RS Purbowangi</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>3</b> RSUD Karanganyar</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>3</b> RSUD Prembun</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>3</b> RSUD dr. Soeratno Gemolong</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>3</b> RS PKU Muhammadiyah Sruweng</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>3</b> RSU Indriati</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>3</b> RS PKU Muhammadiyah Gombong</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>3</b> RS Islam Kendal</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>3</b> RSUD Dr. H. Soewondo Kendal</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>3</b> RS Permata Medika</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>3</b> RSU Dadi Keluarga </b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>3</b> Dinkes Kota Pekalongan</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>3</b> RS Mitra Siaga</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>2</b> RSU Islam Harapan Anda Kota Tegal</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>2</b> RSUD Ambarawa</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>2</b> RSU St. Elisabeth Purwokerto</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>2</b> RSUD Soehadi Prijonegoro Sragen</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>2</b> RSUD Ungaran</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>2</b> RSUD dr. R. Soetijono Blora</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>2</b> RSUD R. Soeprapto Cepu</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>2</b> RS Charlie Hospital kendal</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>2</b> BBKPM Surakarta</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>2</b> RSK Ngesti Waluyo Parakan</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>2</b> Dinkes Kabupaten Kebumen</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>2</b> RS PKU Muhammadiyah Gubug</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>2</b> RSJD Dr. RM. Soedjarwadi</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>2</b> Grogol</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>2</b> Kaliwungu</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>2</b> Purwokerto Selatan</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>1</b> RSUD Kota Surakarta</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>1</b> RS Graha Husada Jepara</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>1</b> Dinkes</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>1</b> RS PKU Muhammadiyah Temanggung</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>1</b> RSUD Suradadi</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>1</b> RS Pelita Anugerah</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>1</b> RSJD Dr. Amino Gondohutomo</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>1</b> RST Slamet Riyadi Surakarta</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>1</b> RS PKU Muhammadiyah Petanahan</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>1</b> Nguter</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>1</b> Karang Anyar 1</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>1</b> Dinkes Kabupaten Cilacap</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>1</b> Baki</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>1</b> Mangunsari</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>1</b> Purwoharjo</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>1</b> Kaliwungu</b></li>
                     </ul>
                     <div class="text-center mb-3"> <button type="button" class="btn btn-sm btn-link mb-2" id="more_sehat">Tampilkan lebih banyak <i class="fa fa-chevron-down"></i></button> <button type="button" class="btn btn-sm fc-orange btn-link" id="less_sehat">Sembunyikan beberapa <i class="fa fa-chevron-up"></i></button> </div>
                  </div>
                  <div class="col-lg-12 col-md-12 col-sm-12">
                     <span class="fc-black ml-3"><b>467 Kasus Meninggal COVID-19 </b></span> 
                     <ul class="list-unstyled row list-kasus-rs ml-2 mr-2" id="list_dead">
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>76</b> RSUD K.R.M.T Wongsonegoro Semarang</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>45</b> RSUD Sunan Kalijaga Demak</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>37</b> RSUP Dr. Kariadi Semarang</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>25</b> RSUD Dr. Moewardi Solo</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>22</b> RSU Sultan Agung Semarang</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>20</b> RS Panti Wilasa Citarum</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>18</b> RS Panti Wilasa dr. Cipto</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>15</b> RS Roemani Muhammadiyah</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>15</b> RSU Telogorejo Semarang</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>15</b> RSU Mardi Rahayu Kudus</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>13</b> Dinkes Kabupaten Jepara</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>11</b> RSUD Loekmono Hadi Kudus</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>11</b> RSUD RA Kartini Jepara</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>9</b> RSUD dr R. Soetrasno Rembang</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>8</b> RS Islam Sunan Kudus</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>8</b> RSUD Tugurejo Semarang</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>8</b> RSU St. Elizabeth Semarang</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>7</b> RSU Columbia Asia Semarang</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>7</b> RS PKU Muhammadiyah Mayong</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>6</b> RSUD Tidar Kota Magelang</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>5</b> RSUD Prof. Dr. Margono Banyumas</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>5</b> RSUD dr. R. Soedjati Grobogan</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>5</b> RS Pelita Anugerah</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>4</b> RS Paru Ario Wirawan</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>4</b> RSUD Pandan Arang Boyolali</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>4</b> RSUD Ambarawa</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>3</b> RSUD Kraton Pekalongan</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>3</b> RSU Islam Harapan Anda Kota Tegal</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>3</b> RSU Bhakti Wira Tamtama Semarang</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>3</b> RSUD Ungaran</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>3</b> RS Kumala Siwi Kudus</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>3</b> RS Permata Bunda</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>3</b> RS Islam NU Demak</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>2</b> RSUD Djoyonegoro Temanggung</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>2</b> RSUD Kelet Jepara</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>2</b> RSUD Karanganyar</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>2</b> RSUD dr. M Ashari Pemalang</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>2</b> RSUP DR SOERADJI TIRTONEGORO KLATEN</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>2</b> RS Graha Husada Jepara</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>2</b> RSUD Dr. H. Soewondo Kendal</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>2</b> RSJD Dr. Amino Gondohutomo</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>2</b> RS PKU Muhammadiyah Gubug</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>1</b> RSUD Kardinah Tegal</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>1</b> RSU dr. Soedjono Magelang</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>1</b> RS dr. Oen Surakarta</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>1</b> RSUD Brebes</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>1</b> RSU Siaga Medika Banyumas</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>1</b> RSUD dr. Soedirman Kab, Kebumen</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>1</b> RSUD Batang</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>1</b> RSUD dr. Soediran MS Wonogiri</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>1</b> RS Panti Rahayu Yakkum</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>1</b> RSU St. Elisabeth Purwokerto</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>1</b> RSUD dr. R. G. Taroenadibrata Purbalingga</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>1</b> RS Nasional Diponegoro</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>1</b> RSUD Soehadi Prijonegoro Sragen</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>1</b> RSJP Prof. dr. Soerojo Magelang</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>1</b> RSUD dr. R. Soetijono Blora</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>1</b> RSU Islam Klaten</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>1</b> RSU William Booth </b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>1</b> Dinkes Kota Semarang</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>1</b> Dinkes Kabupaten Kebumen</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>1</b> RSU Universitas Sebelas Maret</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>1</b> RS Muhammadiyah Fastabiq Sehat</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>1</b> RS Mitra Siaga</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>1</b> RSJD Surakarta</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 "> <b>1</b> RS Hermina Pandanaran Kota Semarang</b></li>
                        <li class="list-item col-lg-4 col-md-6 col-sm-6 py-2 bg-gray"> <b>1</b> Mijen 2</b></li>
                     </ul>
                     <div class="text-center mb-3"> <button type="button" class="btn btn-sm btn-link mb-2" id="more_dead"> Tampilkan lebih banyak <i class="fa fa-chevron-down"></i></button> <button type="button" class="btn btn-sm fc-orange btn-link" id="less_dead">Sembunyikan beberapa <i class="fa fa-chevron-up"></i></button> </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section class="section pt-0 position-relative">
         <div class="container-fluid pl-lg-5 pr-lg-5 pl-md-3 pr-md-3 pl-sm-2 pr-sm-2">
            <div class="rounded-15 shadow bg-white">
               <div class="row justify-content-center">
                  <div class="col-lg-12 col-md-12 mb-12 text-center p-4">
                     <h3 class="font-hotline font-weight-bold p-3 mb-3">Peta Sebaran Kasus <span class="corona-text">COVID-19</span> Di Jawa Tengah</h3>
                     <p class="text-detail"><b>Update Terakhir : </b> Jumat, 10 Juli 2020 | 06:56 | <b>Sumber Data : </b> Dinas Kesehatan Provinsi Jawa Tengah 
                  </div>
               </div>
               <div class="row justify-content-center mr-lg-2 mr-sm-2 ml-lg-2 ml-sm-2">
                  <div class="col-lg-12 col-md-12 col-sm-12 mb-3 mr-3 ml-3">
                     <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item"> <a class="nav-link text-center text-detail active" id="pills-domisili2-tab" data-toggle="pill" href="#pills-domisili2" role="tab" aria-controls="pills-domisili2" aria-selected="false">Domisili</a> </li>
                        <li class="nav-item"> <a class="nav-link text-center text-detail" id="pills-rs-tab" data-toggle="pill" href="#pills-rs" role="tab" aria-controls="pills-rs" aria-selected="false">Rumah Sakit</a> </li>
                     </ul>
                     <div class="tab-content bg-white" id="pills-tabContent">
                        <div class="tab-pane fade show active" id="pills-domisili2" role="tabpanel" aria-labelledby="pills-domisili2-tab">
                           <div id="maps-kabko" class="mb-3">
                              <div class='tableauPlaceholder' id='viz1585828595593' style='position: relative'>
                                 <noscript> <a href='index.html'> <img alt=' ' src='../public.tableau.com/static/images/Pe/PersebaranCOVID-19JawaTengahPerPerson/covid-19-jateng-cases/1_rss.png' style='border: none' /> </a> </noscript>
                                 <object class='tableauViz' style='display:none;'>
                                    <param name='host_url' value='https%3A%2F%2Fpublic.tableau.com%2F' />
                                    <param name='embed_code_version' value='3' />
                                    <param name='site_root' value='' />
                                    <param name='name' value='PersebaranCOVID-19JawaTengahPerPerson&#47;covid-19-jateng-cases' />
                                    <param name='tabs' value='no' />
                                    <param name='toolbar' value='no' />
                                    <param name='static_image' value='../public.tableau.com/static/images/Pe/PersebaranCOVID-19JawaTengahPerPerson/covid-19-jateng-cases/1.png' />
                                    <param name='animate_transition' value='yes' />
                                    <param name='display_static_image' value='yes' />
                                    <param name='display_spinner' value='yes' />
                                    <param name='display_overlay' value='yes' />
                                    <param name='display_count' value='yes' />
                                 </object>
                              </div>
                              <script type='text/javascript'> var divElement = document.getElementById('viz1585828595593'); var vizElement = divElement.getElementsByTagName('object')[0]; if (divElement.offsetWidth > 800) { vizElement.style.width = '100%'; vizElement.style.height = (divElement.offsetWidth * 0.75) + 'px'; } else if (divElement.offsetWidth > 500) { vizElement.style.width = '100%'; vizElement.style.height = (divElement.offsetWidth * 0.75) + 'px'; } else { vizElement.style.width = '100%'; vizElement.style.height = '900px'; } var scriptElement = document.createElement('script'); scriptElement.src = '../public.tableau.com/javascripts/api/viz_v1.js'; vizElement.parentNode.insertBefore(scriptElement, vizElement); </script> 
                           </div>
                           <div class="row justify-content-center">
                              <div class="col-lg-12 col-md-12 col-sm-12 mr-3 ml-3">
                                 <p class="text-keterangan mb-2 font-weight-bold py-3">Catatan : </p>
                                 <ul class="list-unstyled">
                                    <li class="pb-3 text-detail text-justify d-flex">
                                       <div class="icon-wrapper text-center"><i class="fa fa-check fc-green pr-3"></i></div>
                                       <div class="text-wrapper">
                                          <p> Perbesar peta untuk memperjelas titik akurat.</p>
                                       </div>
                                    </li>
                                    <li class="pb-3 text-detail text-justify d-flex">
                                       <div class="icon-wrapper text-center"><i class="fa fa-check fc-green pr-3"></i></div>
                                       <div class="text-wrapper">
                                          <p> Setiap titik merujuk pada Kelurahan (bukan lokasi/tempat tinggal pasien).</p>
                                       </div>
                                    </li>
                                    <li class="pb-3 text-detail text-justify d-flex">
                                       <div class="icon-wrapper text-center"><i class="fa fa-check fc-green pr-3"></i></div>
                                       <div class="text-wrapper">
                                          <p> Data yang ditampilkan dalam peta terus diperbaharui sesuai dengan informasi dari Dinas Kesehatan Provinsi Jawa Tengah.</p>
                                       </div>
                                    </li>
                                    <li class="pb-3 text-detail text-justify d-flex">
                                       <div class="icon-wrapper text-center"><i class="fa fa-check fc-green pr-3"></i></div>
                                       <div class="text-wrapper">
                                          <p> Orang Dalam Pemantauan (ODP) adalah seseorang yang mengalami gejala demam (>38 C) atau riwayat demam tanpa pneumonia yang memiliki riwayat perjalanan ke wilayah yang terjangkit.</p>
                                       </div>
                                    </li>
                                    <li class="pb-3 text-detail text-justify d-flex">
                                       <div class="icon-wrapper text-center"><i class="fa fa-check fc-green pr-3"></i></div>
                                       <div class="text-wrapper">
                                          <p>Pasien Dalam Pemantauan (PDP) adalah pasien pnemonia ringan hingga berat yang mengalami demam (>38 C) atau riwayat demam dan memiliki riwayat kontak dengan hewan penular, riwayat kontak dengan pasien COVID-19, atau riwayat perjalanan ke negara terjangkit dalam 14 hari.</p>
                                       </div>
                                    </li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                        <div class="tab-pane fade text-center" id="pills-rs" role="tabpanel" aria-labelledby="pills-rs-tab">
                           <div id="maps-rs" class="mb-3"></div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section class="section pt-0 position-relative">
         <div class="container-fluid pl-lg-5 pr-lg-5 pl-md-3 pr-md-3 pl-sm-2 pr-sm-2">
            <ul class="nav nav-pills" id="chartHarian" role="tablist">
               <li><a class="btn btn-outline-danger btn-nav-danger p-lg-3 p-2 active" id="pills-harian-tab" data-toggle="pill" href="#pills-harian" role="tab" aria-controls="pills-harian" aria-selected="true">Grafik Harian</a></li>
               <li><a class="btn btn-outline-danger btn-nav-danger p-lg-3 p-2" id="pills-akumulasi-tab" data-toggle="pill" href="#pills-akumulasi" role="tab" aria-controls="pills-akumulasi" aria-selected="false">Grafik Akumulasi</a></li>
            </ul>
            <div class="tab-content mt-3">
               <div class="tab-pane fade show active" id="pills-harian" role="tabpanel" aria-labelledby="pills-harian-tab">
                  <div class="row">
                     <div class="col-lg-4 col-md-12 col-sm-12 col-12 mb-3">
                        <div class="rounded-15 shadow bg-white">
                           <div class="row">
                              <div class="col-md-12 mr-1 ml-1">
                                 <div id="chart-harian-covid" style="width:100%; height:400px;"></div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-4 col-md-12 col-sm-12 col-12 mb-3">
                        <div class="rounded-15 shadow bg-white">
                           <div class="row">
                              <div class="col-md-12 mr-1 ml-1">
                                 <div id="chart-harian-sembuh" style="width:100%; height:400px;"></div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-4 col-md-12 col-sm-12 col-12 mb-3">
                        <div class="rounded-15 shadow bg-white">
                           <div class="row">
                              <div class="col-md-12 mr-1 ml-1">
                                 <div id="chart-harian-dead" style="width:100%; height:400px;"></div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="tab-pane fade text-center" id="pills-akumulasi" role="tabpanel" aria-labelledby="pills-akumulasi-tab">
                  <div class="row">
                     <div class="col-lg-4 col-md-12 col-sm-12 col-12 mb-3">
                        <div class="rounded-15 shadow bg-white">
                           <div class="row">
                              <div class="col-md-12 mr-1 ml-1">
                                 <div id="chart-harian-covid-sum" style="width:100%; height:400px;"></div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-4 col-md-12 col-sm-12 col-12 mb-3">
                        <div class="rounded-15 shadow bg-white">
                           <div class="row">
                              <div class="col-md-12 mr-1 ml-1">
                                 <div id="chart-harian-sembuh-sum" style="width:100%; height:400px;"></div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-4 col-md-12 col-sm-12 col-12 mb-3">
                        <div class="rounded-15 shadow bg-white">
                           <div class="row">
                              <div class="col-md-12 mr-1 ml-1">
                                 <div id="chart-harian-dead-sum" style="width:100%; height:400px;"></div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section class="section pt-0 position-relative">
         <div class="container-fluid pl-lg-5 pr-lg-5 pl-md-3 pr-md-3 pl-sm-2 pr-sm-2">
            <div class="rounded-15 shadow bg-white">
               <div class="row justify-content-center">
                  <div class="col-lg-12 col-md-12 mb-12 text-center p-4">
                     <h3 class="font-hotline font-weight-bold p-3 mb-3">Tabel <span class="corona-text">COVID-19</span> Di Jawa Tengah</h3>
                     <p class="text-detail"><b>Update Terakhir : </b> Jumat, 10 Juli 2020 | 06:56 | <b>Sumber Data : </b> Dinas Kesehatan Provinsi Jawa Tengah 
                  </div>
               </div>
               <div class="row justify-content-center mr-lg-2 mr-sm-2 ml-lg-2 ml-sm-2">
                  <div class="col-lg-12 col-md-12 col-sm-12 mb-3 mr-3 ml-3">
                     <div class="table-responsive table-kabkot">
                        <table class="table table-bordered font-12" width="100%">
                           <thead>
                              <tr>
                                 <th>Kabupaten/Kota</th>
                                 <th class="fc-ungu text-right">ODP: Proses</th>
                                 <th class="fc-orange text-right">PDP: Dirawat</th>
                                 <th class="fc-green text-right">Positif: Sembuh</th>
                                 <th class="text-black text-right">Positif: Meninggal</th>
                                 <th class="fc-red text-right">Positif: Dirawat <br> <small>(dirawat + dirujuk + aps + isolasi mandiri)</small></th>
                              </tr>
                           </thead>
                           <tbody>
                              <tr>
                                 <td>KOTA SEMARANG</td>
                                 <td align="right">96</td>
                                 <td align="right">212</td>
                                 <td align="right">890</td>
                                 <td align="right">189</td>
                                 <td align="right">768</td>
                              </tr>
                              <tr>
                                 <td>DEMAK</td>
                                 <td align="right">6</td>
                                 <td align="right">116</td>
                                 <td align="right">145</td>
                                 <td align="right">94</td>
                                 <td align="right">324</td>
                              </tr>
                              <tr>
                                 <td>JEPARA</td>
                                 <td align="right">77</td>
                                 <td align="right">27</td>
                                 <td align="right">83</td>
                                 <td align="right">42</td>
                                 <td align="right">427</td>
                              </tr>
                              <tr>
                                 <td>KUDUS</td>
                                 <td align="right">75</td>
                                 <td align="right">49</td>
                                 <td align="right">93</td>
                                 <td align="right">28</td>
                                 <td align="right">143</td>
                              </tr>
                              <tr>
                                 <td>TEMANGGUNG</td>
                                 <td align="right">0</td>
                                 <td align="right">3</td>
                                 <td align="right">201</td>
                                 <td align="right">3</td>
                                 <td align="right">11</td>
                              </tr>
                              <tr>
                                 <td>GROBOGAN</td>
                                 <td align="right">61</td>
                                 <td align="right">55</td>
                                 <td align="right">56</td>
                                 <td align="right">15</td>
                                 <td align="right">88</td>
                              </tr>
                              <tr>
                                 <td>MAGELANG</td>
                                 <td align="right">0</td>
                                 <td align="right">18</td>
                                 <td align="right">138</td>
                                 <td align="right">4</td>
                                 <td align="right">5</td>
                              </tr>
                              <tr>
                                 <td>SEMARANG</td>
                                 <td align="right">17</td>
                                 <td align="right">16</td>
                                 <td align="right">29</td>
                                 <td align="right">14</td>
                                 <td align="right">88</td>
                              </tr>
                              <tr>
                                 <td>BANYUMAS</td>
                                 <td align="right">5</td>
                                 <td align="right">11</td>
                                 <td align="right">71</td>
                                 <td align="right">4</td>
                                 <td align="right">22</td>
                              </tr>
                              <tr>
                                 <td>KENDAL</td>
                                 <td align="right">14</td>
                                 <td align="right">19</td>
                                 <td align="right">30</td>
                                 <td align="right">4</td>
                                 <td align="right">62</td>
                              </tr>
                              <tr>
                                 <td>SUKOHARJO</td>
                                 <td align="right">7</td>
                                 <td align="right">20</td>
                                 <td align="right">71</td>
                                 <td align="right">7</td>
                                 <td align="right">18</td>
                              </tr>
                              <tr>
                                 <td>KOTA SALATIGA</td>
                                 <td align="right">13</td>
                                 <td align="right">1</td>
                                 <td align="right">64</td>
                                 <td align="right">0</td>
                                 <td align="right">28</td>
                              </tr>
                              <tr>
                                 <td>WONOSOBO</td>
                                 <td align="right">7</td>
                                 <td align="right">9</td>
                                 <td align="right">83</td>
                                 <td align="right">0</td>
                                 <td align="right">0</td>
                              </tr>
                              <tr>
                                 <td>PURWOREJO</td>
                                 <td align="right">0</td>
                                 <td align="right">1</td>
                                 <td align="right">76</td>
                                 <td align="right">0</td>
                                 <td align="right">7</td>
                              </tr>
                              <tr>
                                 <td>BATANG</td>
                                 <td align="right">65</td>
                                 <td align="right">17</td>
                                 <td align="right">41</td>
                                 <td align="right">2</td>
                                 <td align="right">30</td>
                              </tr>
                              <tr>
                                 <td>BOYOLALI</td>
                                 <td align="right">7</td>
                                 <td align="right">18</td>
                                 <td align="right">43</td>
                                 <td align="right">3</td>
                                 <td align="right">26</td>
                              </tr>
                              <tr>
                                 <td>REMBANG</td>
                                 <td align="right">10</td>
                                 <td align="right">22</td>
                                 <td align="right">23</td>
                                 <td align="right">7</td>
                                 <td align="right">41</td>
                              </tr>
                              <tr>
                                 <td>PURBALINGGA</td>
                                 <td align="right">6</td>
                                 <td align="right">6</td>
                                 <td align="right">57</td>
                                 <td align="right">2</td>
                                 <td align="right">8</td>
                              </tr>
                              <tr>
                                 <td>KLATEN</td>
                                 <td align="right">0</td>
                                 <td align="right">6</td>
                                 <td align="right">46</td>
                                 <td align="right">2</td>
                                 <td align="right">19</td>
                              </tr>
                              <tr>
                                 <td>CILACAP</td>
                                 <td align="right">2</td>
                                 <td align="right">5</td>
                                 <td align="right">61</td>
                                 <td align="right">1</td>
                                 <td align="right">4</td>
                              </tr>
                              <tr>
                                 <td>BLORA</td>
                                 <td align="right">11</td>
                                 <td align="right">8</td>
                                 <td align="right">23</td>
                                 <td align="right">5</td>
                                 <td align="right">36</td>
                              </tr>
                              <tr>
                                 <td>BANJARNEGARA</td>
                                 <td align="right">20</td>
                                 <td align="right">6</td>
                                 <td align="right">40</td>
                                 <td align="right">1</td>
                                 <td align="right">17</td>
                              </tr>
                              <tr>
                                 <td>PEMALANG</td>
                                 <td align="right">108</td>
                                 <td align="right">16</td>
                                 <td align="right">37</td>
                                 <td align="right">3</td>
                                 <td align="right">15</td>
                              </tr>
                              <tr>
                                 <td>SRAGEN</td>
                                 <td align="right">0</td>
                                 <td align="right">11</td>
                                 <td align="right">43</td>
                                 <td align="right">1</td>
                                 <td align="right">8</td>
                              </tr>
                              <tr>
                                 <td>KARANGANYAR</td>
                                 <td align="right">2</td>
                                 <td align="right">13</td>
                                 <td align="right">34</td>
                                 <td align="right">3</td>
                                 <td align="right">14</td>
                              </tr>
                              <tr>
                                 <td>KOTA SURAKARTA</td>
                                 <td align="right">6</td>
                                 <td align="right">13</td>
                                 <td align="right">37</td>
                                 <td align="right">4</td>
                                 <td align="right">9</td>
                              </tr>
                              <tr>
                                 <td>KEBUMEN</td>
                                 <td align="right">16</td>
                                 <td align="right">9</td>
                                 <td align="right">36</td>
                                 <td align="right">2</td>
                                 <td align="right">7</td>
                              </tr>
                              <tr>
                                 <td>BREBES</td>
                                 <td align="right">2</td>
                                 <td align="right">6</td>
                                 <td align="right">30</td>
                                 <td align="right">0</td>
                                 <td align="right">14</td>
                              </tr>
                              <tr>
                                 <td>TEGAL</td>
                                 <td align="right">3</td>
                                 <td align="right">25</td>
                                 <td align="right">24</td>
                                 <td align="right">4</td>
                                 <td align="right">8</td>
                              </tr>
                              <tr>
                                 <td>KOTA MAGELANG</td>
                                 <td align="right">8</td>
                                 <td align="right">2</td>
                                 <td align="right">27</td>
                                 <td align="right">4</td>
                                 <td align="right">3</td>
                              </tr>
                              <tr>
                                 <td>PATI</td>
                                 <td align="right">26</td>
                                 <td align="right">26</td>
                                 <td align="right">23</td>
                                 <td align="right">4</td>
                                 <td align="right">6</td>
                              </tr>
                              <tr>
                                 <td>PEKALONGAN</td>
                                 <td align="right">5</td>
                                 <td align="right">5</td>
                                 <td align="right">11</td>
                                 <td align="right">1</td>
                                 <td align="right">13</td>
                              </tr>
                              <tr>
                                 <td>WONOGIRI</td>
                                 <td align="right">0</td>
                                 <td align="right">9</td>
                                 <td align="right">15</td>
                                 <td align="right">3</td>
                                 <td align="right">3</td>
                              </tr>
                              <tr>
                                 <td>KOTA PEKALONGAN</td>
                                 <td align="right">2</td>
                                 <td align="right">0</td>
                                 <td align="right">12</td>
                                 <td align="right">1</td>
                                 <td align="right">3</td>
                              </tr>
                              <tr>
                                 <td>KOTA TEGAL</td>
                                 <td align="right">0</td>
                                 <td align="right">2</td>
                                 <td align="right">3</td>
                                 <td align="right">1</td>
                                 <td align="right">0</td>
                              </tr>
                           </tbody>
                        </table>
                     </div>
                     <div class="text-center d-flex justify-content-center">
                        <div class="chevron"></div>
                        <div class="chevron"></div>
                        <div class="chevron"></div>
                        <span class="text-scroll">Scroll untuk melihat data kabupaten lain</span> 
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section class="section pt-0 position-relative">
         <div class="container-fluid pl-lg-5 pr-lg-5 pl-md-3 pr-md-3 pl-sm-2 pr-sm-2">
            <ul class="nav nav-pills" id="chartAge" role="tablist">
               <li><a class="btn btn-outline-danger btn-nav-danger p-lg-3 p-2 active" id="pills-positif-tab" data-toggle="pill" href="#pills-positif" role="tab" aria-controls="pills-positif" aria-selected="true">Terkonfirmasi COVID-19</a></li>
               <li><a class="btn btn-outline-danger btn-nav-danger p-lg-3 p-2" id="pills-dirawat-tab" data-toggle="pill" href="#pills-dirawat" role="tab" aria-controls="pills-dirawat" aria-selected="false">Dirawat</a></li>
               <li><a class="btn btn-outline-danger btn-nav-danger p-lg-3 p-2" id="pills-sembuh-tab" data-toggle="pill" href="#pills-sembuh" role="tab" aria-controls="pills-sembuh" aria-selected="false">Sembuh</a></li>
               <li><a class="btn btn-outline-danger btn-nav-danger p-lg-3 p-2" id="pills-mati-tab" data-toggle="pill" href="#pills-mati" role="tab" aria-controls="pills-mati" aria-selected="false">Meninggal</a></li>
            </ul>
            <div class="tab-content mt-3">
               <div class="tab-pane fade show active" id="pills-positif" role="tabpanel" aria-labelledby="pills-positif-tab">
                  <div class="row">
                     <div class="col-lg-4 col-md-6 col-sm-12 col-12 mb-3">
                        <div class="rounded-15 shadow bg-white">
                           <div class="row">
                              <div class="col-md-12 mr-1 ml-1">
                                 <div id="chart-sex-covid" style="width:100%; height:400px;"></div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-8 col-md-6 col-sm-12 col-12">
                        <div class="rounded-15 shadow bg-white">
                           <div class="row">
                              <div class="col-md-12 mr-1 ml-1">
                                 <div id="chart-age-covid" style="width:100%; height:400px;"></div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="tab-pane fade" id="pills-dirawat" role="tabpanel" aria-labelledby="pills-dirawat-tab">
                  <div class="row">
                     <div class="col-lg-4 col-md-6 col-sm-12 col-12 mb-3">
                        <div class="rounded-15 shadow bg-white">
                           <div class="row">
                              <div class="col-md-12 mr-1 ml-1">
                                 <div id="chart-sex-dirawat" style="width:100%; height:400px;"></div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-8 col-md-6 col-sm-12 col-12">
                        <div class="rounded-15 shadow bg-white">
                           <div class="row">
                              <div class="col-md-12 mr-1 ml-1">
                                 <div id="chart-age-dirawat" style="width:100%; height:400px;"></div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="tab-pane fade" id="pills-sembuh" role="tabpanel" aria-labelledby="pills-sembuh-tab">
                  <div class="row">
                     <div class="col-lg-4 col-md-6 col-sm-12 col-12 mb-3">
                        <div class="rounded-15 shadow bg-white">
                           <div class="row">
                              <div class="col-md-12 mr-1 ml-1">
                                 <div id="chart-sex-sembuh" style="width:100%; height:400px;"></div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-8 col-md-6 col-sm-12 col-12">
                        <div class="rounded-15 shadow bg-white">
                           <div class="row">
                              <div class="col-md-12 mr-1 ml-1">
                                 <div id="chart-age-sembuh" style="width:100%; height:400px;"></div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="tab-pane fade" id="pills-mati" role="tabpanel" aria-labelledby="pills-mati-tab">
                  <div class="row">
                     <div class="col-lg-4 col-md-6 col-sm-12 col-12 mb-3">
                        <div class="rounded-15 shadow bg-white">
                           <div class="row">
                              <div class="col-md-12 mr-1 ml-1">
                                 <div id="chart-sex-dead" style="width:100%; height:400px;"></div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-8 col-md-6 col-sm-12 col-12">
                        <div class="rounded-15 shadow bg-white">
                           <div class="row">
                              <div class="col-md-12 mr-1 ml-1">
                                 <div id="chart-age-dead" style="width:100%; height:400px;"></div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section class="section pt-0 position-relative">
         <div class="container-fluid pl-lg-5 pr-lg-5 pl-md-3 pr-md-3 pl-sm-2 pr-sm-2">
            <ul class="nav nav-pills" id="chartGejala" role="tablist">
               <li><a class="btn btn-outline-danger btn-nav-danger p-lg-3 p-2 active" id="pills-gejala-positif-tab" data-toggle="pill" href="#pills-gejala-positif" role="tab" aria-controls="pills-gejala-positif" aria-selected="true">Terkonfirmasi COVID-19</a></li>
               <li><a class="btn btn-outline-danger btn-nav-danger p-lg-3 p-2" id="pills-gejala-dirawat-tab" data-toggle="pill" href="#pills-gejala-dirawat" role="tab" aria-controls="pills-gejala-dirawat" aria-selected="false">Dirawat</a></li>
               <li><a class="btn btn-outline-danger btn-nav-danger p-lg-3 p-2" id="pills-gejala-sembuh-tab" data-toggle="pill" href="#pills-gejala-sembuh" role="tab" aria-controls="pills-gejala-sembuh" aria-selected="false">Sembuh</a></li>
               <li><a class="btn btn-outline-danger btn-nav-danger p-lg-3 p-2" id="pills-gejala-mati-tab" data-toggle="pill" href="#pills-gejala-mati" role="tab" aria-controls="pills-gejala-mati" aria-selected="false">Meninggal</a></li>
            </ul>
            <div class="tab-content mt-3">
               <div class="tab-pane fade show active" id="pills-gejala-positif" role="tabpanel" aria-labelledby="pills-gejala-positif-tab">
                  <div class="row">
                     <div class="col-lg-6 col-md-12 col-sm-12 col-12 mb-3">
                        <div class="rounded-15 shadow bg-white">
                           <div class="rounded-15 shadow bg-white">
                              <div class="row">
                                 <div class="col-md-12 mr-1 ml-1">
                                    <div id="chart-gejala-covid" style="width:100%; height:400px;"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-6 col-md-12 col-sm-12 col-12">
                        <div class="rounded-15 shadow bg-white">
                           <div class="row">
                              <div class="col-md-12 mr-1 ml-1">
                                 <div id="chart-riwayat-covid" style="width:100%; height:400px;"></div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="tab-pane fade" id="pills-gejala-dirawat" role="tabpanel" aria-labelledby="pills-gejala-dirawat-tab">
                  <div class="row">
                     <div class="col-lg-6 col-md-12 col-sm-12 col-12 mb-3">
                        <div class="rounded-15 shadow bg-white">
                           <div class="rounded-15 shadow bg-white">
                              <div class="row">
                                 <div class="col-md-12 mr-1 ml-1">
                                    <div id="chart-gejala-dirawat" style="width:100%; height:400px;"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-6 col-md-12 col-sm-12 col-12">
                        <div class="rounded-15 shadow bg-white">
                           <div class="row">
                              <div class="col-md-12 mr-1 ml-1">
                                 <div id="chart-riwayat-dirawat" style="width:100%; height:400px;"></div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="tab-pane fade" id="pills-gejala-sembuh" role="tabpanel" aria-labelledby="pills-gejala-sembuh-tab">
                  <div class="row">
                     <div class="col-lg-6 col-md-12 col-sm-12 col-12 mb-3">
                        <div class="rounded-15 shadow bg-white">
                           <div class="rounded-15 shadow bg-white">
                              <div class="row">
                                 <div class="col-md-12 mr-1 ml-1">
                                    <div id="chart-gejala-sembuh" style="width:100%; height:400px;"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-6 col-md-12 col-sm-12 col-12">
                        <div class="rounded-15 shadow bg-white">
                           <div class="row">
                              <div class="col-md-12 mr-1 ml-1">
                                 <div id="chart-riwayat-sembuh" style="width:100%; height:400px;"></div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="tab-pane fade" id="pills-gejala-mati" role="tabpanel" aria-labelledby="pills-gejala-mati-tab">
                  <div class="row">
                     <div class="col-lg-6 col-md-12 col-sm-12 col-12 mb-3">
                        <div class="rounded-15 shadow bg-white">
                           <div class="rounded-15 shadow bg-white">
                              <div class="row">
                                 <div class="col-md-12 mr-1 ml-1">
                                    <div id="chart-gejala-dead" style="width:100%; height:400px;"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-6 col-md-12 col-sm-12 col-12">
                        <div class="rounded-15 shadow bg-white">
                           <div class="row">
                              <div class="col-md-12 mr-1 ml-1">
                                 <div id="chart-riwayat-dead" style="width:100%; height:400px;"></div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <footer>
         <div class="footer-main">
            <div class="container-fluid pl-lg-5 pr-lg-5 pl-md-3 pr-md-3 pl-sm-2 pr-sm-2">
               <div class="row">
                  <div class="col-lg-8 col-md-12 m-md-auto align-self-center">
                     <div class="block">
                        <h3 class="text-white font-weight-bold font-phone"><a href="#"><img width="36" height="36" src="assets/images/logo.png" alt="footer-logo" class="mt-2 mr-2"> Pemerintah Provinsi Jawa Tengah</h3>
                        </a> 
                        <p class="text-white">Jl. Pahlawan No.9, Mugassari, Kec. Semarang Sel.,<br />Kota Semarang, Jawa Tengah 50249</p>
                        <ul class="social-icon list-inline pt-2">
                           <li class="list-inline-item"> <a href="https://www.facebook.com/Dinas-Kesehatan-Prov-Jateng-561985847631175/"><i class="ti-facebook"></i></a> </li>
                           <li class="list-inline-item"> <a href="https://twitter.com/dinkesjateng"><i class="ti-twitter"></i></a> </li>
                           <li class="list-inline-item"> <a href="https://www.instagram.com/dinkesjateng_prov/"><i class="ti-instagram"></i></a> </li>
                        </ul>
                     </div>
                  </div>
                  <div class="col-lg-2 col-md-6 col-sm-6 col-12 m-md-auto align-self-center">
                     <div class="block-2">
                        <ul>
                           <li><a target="_blank" href="https://jatengprov.go.id/">Portal Jateng</a></li>
                           <li><a target="_blank" href="https://covid19.kemkes.go.id/">Kemenkes</a></li>
                           <li><a target="_blank" href="https://covid19.kemkes.go.id/situasi-infeksi-emerging/info-corona-virus/tanya-jawab-novel-coronavirus-2019-ncov-faq-update-4-februari-2020/#.XmzSNagzZPZ">Info Corona</a></li>
                        </ul>
                     </div>
                  </div>
                  <div class="col-lg-2 col-md-6 col-sm-6 col-12">
                     <div class="block-2">
                        <ul>
                           <li><a target="_blank" href="https://dinkesjatengprov.go.id/v2018/5-protokol-utama-penanganan-covid-19/">Protocol Penanganan COVID-19</a></li>
                           <li><a target="_blank" href="https://laporgub.jatengprov.go.id/">LaporGub</a></li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="text-center bg-black py-2"> <small class="text-secondary">© Copyright Pemerintah Provinsi Jawa Tengah. All Rights Reserved</small> </div>
      </footer>
      <a id="back-to-top" href="#" class="btn btn-danger back-to-top" role="button"><i class="fa fa-2x fa-chevron-up"></i></a> <!-- Global site tag (gtag.js) - Google Analytics --> <script async src="https://www.googletagmanager.com/gtag/js?id=UA-160611489-1"></script> <script> window.dataLayer = window.dataLayer || []; function gtag() { dataLayer.push(arguments); } gtag('js', new Date()); gtag('config', 'UA-160611489-1'); </script> 
   </body>
   <!-- Mirrored from corona.jatengprov.go.id/data by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 09 Jul 2020 23:57:35 GMT -->
</html>