<div class="modal-dialog modal-simple">

		{{ Form::model($subKategoriKasus,array('route' => array((!$subKategoriKasus->exists) ? 'sub-kategori-kasus.store':'sub-kategori-kasus.update',$subKategoriKasus->pk()),
	        'class'=>'modal-content','id'=>'sub-kategori-kasus-form','method'=>(!$subKategoriKasus->exists) ? 'POST' : 'PUT')) }}

		<div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
      </button>
      <h4 class="modal-title" id="formModalLabel">{{ ($subKategoriKasus->exists?'Edit':'Tambah').' Sub Kategori Kasus' }}</h4>
    </div>
    <div class="modal-body">
																				        {!! App\Console\Commands\Generator\Form::input('id_kategori_kasus','hidden')->model($subKategoriKasus)->showHidden() !!}
{!! App\Console\Commands\Generator\Form::autocomplete('kategori_kasus',array('value'=>$subKategoriKasus->exists?(isset($subKategoriKasus->kategori_kasus)?$subKategoriKasus->kategori_kasus->kategori_kasus:null):null))->model(null)->show() !!}
																        {!! App\Console\Commands\Generator\Form::input('sub_kategori_kasus','text')->model($subKategoriKasus)->show() !!}
																												<div class="col-md-12 float-right">
					<div class="text-right">
						<button class="btn btn-primary" id="simpan">Simpan</button>
					</div>
				</div>
		</div>

	    {{ Form::close() }}
</div>


<script type="text/javascript">
$(document).ready(function(){
	$('#sub-kategori-kasus-form').formValidation({
	  framework: "bootstrap4",
	  button: {
	    selector: "#simpan",
	    disabled: "disabled"
	  },
	  icon: null,
	  fields: {
	
},
err: {
	clazz: 'invalid-feedback'
},
control: {
	// The CSS class for valid control
	valid: 'is-valid',

	// The CSS class for invalid control
	invalid: 'is-invalid'
},
row: {
	invalid: 'has-danger'
}
});
	
					var kategori_kasusEngine = new Bloodhound({
							datumTokenizer: function(d) { return d.tokens; },
							queryTokenizer: Bloodhound.tokenizers.whitespace,
							cache: false,
							remote: {
								url: '{{ url("autocomplete/kategori_kasus") }}?q=%QUERY',
								wildcard: "%QUERY"
							}
						});

						$("#kategori_kasus").typeahead({
									hint: true,
									highlight: true,
									minLength: 1
							},
							{
									source: kategori_kasusEngine.ttAdapter(),
									name: "kategori_kasus",
									displayKey: "kategori_kasus",
									templates: {
										suggestion: function(data){
											return Handlebars.compile([
																"<div class=\"tt-dataset\">",
																		"<div>@{{kategori_kasus}}</div>",
																"</div>",
														].join(""))(data);
										},
											empty: [
													"<div>kategori_kasus tidak ditemukan</div>"
											]
									}
							}).bind("typeahead:selected", function(obj, datum, name) {
								$("#id_kategori_kasus").val(datum.id);
							}).bind("typeahead:change", function(obj, datum, name) {

							});
					

});
</script>
