<div class="modal-dialog modal-simple">

		{{ Form::model($jenisKelamin,array('route' => array((!$jenisKelamin->exists) ? 'jenis-kelamin.store':'jenis-kelamin.update',$jenisKelamin->pk()),
	        'class'=>'modal-content','id'=>'jenis-kelamin-form','method'=>(!$jenisKelamin->exists) ? 'POST' : 'PUT')) }}

		<div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
      </button>
      <h4 class="modal-title" id="formModalLabel">{{ ($jenisKelamin->exists?'Edit':'Tambah').' Jenis Kelamin' }}</h4>
    </div>
    <div class="modal-body">
																				        {!! App\Console\Commands\Generator\Form::input('jenis_kelamin','text')->model($jenisKelamin)->show() !!}
																												<div class="col-md-12 float-right">
					<div class="text-right">
						<button class="btn btn-primary" id="simpan">Simpan</button>
					</div>
				</div>
		</div>

	    {{ Form::close() }}
</div>


<script type="text/javascript">
$(document).ready(function(){
	$('#jenis-kelamin-form').formValidation({
	  framework: "bootstrap4",
	  button: {
	    selector: "#simpan",
	    disabled: "disabled"
	  },
	  icon: null,
	  fields: {
	jenis_kelamin : { validators: {
				        notEmpty: {
				          message: 'Kolom jenis_kelamin tidak boleh kosong'
							}
						}
					}
},
err: {
	clazz: 'invalid-feedback'
},
control: {
	// The CSS class for valid control
	valid: 'is-valid',

	// The CSS class for invalid control
	invalid: 'is-invalid'
},
row: {
	invalid: 'has-danger'
}
});
	

});
</script>
