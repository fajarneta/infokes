function map_penyakit_page () {

    let url_maps_1 = "http://hl-api.morbis.id/index.php/api/maps/Heatmap_bansos_all/load_data?kode_kabupaten=34.02";
    let url_maps_2 = "http://hl-api.morbis.id/index.php/api/maps/Heatmap_bansos_all/load_data?kode_kabupaten=34.71";
    let url_maps_3 = "http://hl-api.morbis.id/index.php/api/maps/Heatmap_bansos_all/load_data?kode_kabupaten=34.03";
    let url_maps_4 = "http://hl-api.morbis.id/index.php/api/maps/Heatmap_bansos_all/load_data?kode_kabupaten=34.01";
    let url_maps_5 = "http://hl-api.morbis.id/index.php/api/maps/Heatmap_bansos_all/load_data?kode_kabupaten=34.04";
    let url_maps_user_auth = "apihomeland";
    let url_maps_pass_auth = "homeland1234";

    function clgSucces (res, tableRes){
        console.log('%c Request Success', 'color: green; font-weight: bold; margin-bottom:10px;');
        console.group('%c Respoon API', 'color: orange; font-weight: bold')
        console.log(res);
        console.table(tableRes);
        console.groupEnd();
    }

    // get color depending on population density value
    function getColor(d) {
        return d > 50 ? '#800026' :
            d > 40 ? '#BD0026' :
            d > 30 ? '#E31A1C' :
            d > 20 ? '#FC4E2A' :
            d > 15 ? '#FD8D3C' :
            d > 10 ? '#FEB24C' :
            d > 5 ? '#FED976' :
            '#FFEDA0';
    }

    //indexedDB management =========================================

    //for the first time to access indexedDB, create the DB first and then insert data_choropleth to indexedDB
    function access_db_create(data_choropleth, keyName) {
        
        // console.log(data_choropleth);
        const request = indexedDB.open("db_map_bansos");
        request.onupgradeneeded = e => {
            const db = e.target.result;

            if (!db.objectStoreNames.contains('map_bansos')) { // if there's no "map_bansos" store
                // create it
                const store = db.createObjectStore('map_bansos', {
                    autoIncrement: true
                });
                store.createIndex("by_name", "name", {
                    unique: true
                });
            }
            console.log("Create Indexed DB");
        }

        request.onsuccess = e => {
            const db = e.target.result;

            const tx = db.transaction("map_bansos", "readwrite");
            const store_map_bansos = tx.objectStore("map_bansos");
            store_map_bansos.put({
                name: keyName,
                data: data_choropleth
            });
            tx.oncomplete = e => {
                console.log("berhasil memasukkan data ke indexedDB")
            }
            localStorage.setItem(keyName, true); // set status that this data has stored
            db.close();
        }

        request.onerror = e => {
            alert("Error create Indexed DB");
        }

    }

    //if the data have been stored in indexedDB, then get data_choropleth from the indexedDB
    function access_db_get(keyName, keyValue, map = null, info = null) {
        const request = indexedDB.open("db_map_bansos");
        request.onupgradeneeded = e => {
            const db = e.target.result;
            console.log("upgrade Indexed DB");
        }

        request.onsuccess = e => {
            const db = e.target.result;
            if (db.objectStoreNames.contains('map_bansos')) { // if there's "map_bansos" store
                const tx = db.transaction("map_bansos", "readonly");
                const store_map_bansos = tx.objectStore("map_bansos");
                const index = store_map_bansos.index("by_name");
                const request_store = index.get(keyName)
                request_store.onsuccess = e => {
                    localStorage.setItem(keyName, true);

                    const matching = e.target.result;
                    if (matching !== undefined) {
                        console.log("berhasil mendapatkan data map_bansos dari indexedDB");
                        value = matching.data; //data from indexedDB

                        //create choropleth using data from indexedDB
                        geojsonLayer = create_choropleth_on_map(map, value, info, keyValue);
                        $('#clear-geojsonLayer').click(function () {
                          map.removeLayer(geojsonLayer);
                        });

                    } else {
                        // console.log(keyName);
                        // localStorage.removeItem(keyName);
                        console.log("data map_bansos tidak ditemukan di dalam indexedDB");
                    }
                }

                tx.oncomplete = e => {
                    console.log("transaction indexedDB has been completed")
                }

            } else {

                //if there's no map_bansos store. call API to get the data
                localStorage.removeItem(keyName, true);

                let deleteRequest = indexedDB.deleteDatabase("db_map_bansos");
                deleteRequest.onerror = e => {
                    console.log("gagal menghapus database");
                }
                console.log("memuat ulang halaman untuk memanggil API");

                // load_10_penyakit();
            }
            db.close();
        }

        request.onerror = e => {
            alert("Error get data from Indexed DB");
        }
    }
    // end indexedDB management =========================================


    function create_map_base(div_id) {
        // initial map
        var map = L.map(div_id, {
            scrollWheelZoom: false,
        }).setView([-7.7956,110.3695], 10);
        var tiles = L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            maxZoom: 18,
            attribution: '&copy; <a href="http://openstreetmap.org/copyright">OpenStreetMap</a> contributors | Choropleth Map',
        }).addTo(map);

        map.on('click', function () {
            if (map.scrollWheelZoom.enabled()) {
                map.scrollWheelZoom.disable();
            } else {
                map.scrollWheelZoom.enable();
            }
        });
        
        return map;
    }

    function create_map_area(map,keyValue){

        if(!map){
            alert('map kosong');
        }

        // Create control that shows state info on hover
        var info = L.control();

        info.onAdd = function (map) {
            this._div = L.DomUtil.create('div', 'info');
            this.update();
            return this._div;
        };

        info.update = function (props) {
            this._div.innerHTML = '<p class="title_info text-14">Jumlah Penduduk</p>' + (props ?
                '<b>' + props.desa + '</b><br /> <p>' + (props[keyValue] ? props[keyValue] : 0) + ' Orang' :
                '<br><br>') + '</p>';
        };

        info.addTo(map);


        //create legend for map
        var legend = L.control({
            position: 'bottomright'
        });

        legend.onAdd = function (map) {

            // create element with the class
            var div = L.DomUtil.create('div', 'info legend'),
                grades = [0, 5, 10, 15, 20, 30, 40, 50],
                labels = [],
                from, to;
            labels.push(
                `<p class="title_legend">Rentang Jumlah Penduduk</p>`
            );
            for (var i = 0; i < grades.length; i++) {
                from = grades[i];
                to = grades[i + 1];

                labels.push(
                    `<div class="info_wrap"> 
                        <i style="background:` + getColor(from + 1) + `"></i> 
                        <p>` + from + (to ? ' &ndash; ' + to : '+') + `</p>
                    </div>`
                );
            }

            div.innerHTML = labels.join('');
            return div;
        };
        legend.addTo(map);

        return { info, legend };
    }

    function load_geojson_by_type(url, map, info, keyValue,index_db_num) {

        // let dataGeojsonPenyakit;
        
        return new Promise((resolve, reject)=> {
            console.log(`ambil choropleth_bansos_${index_db_num}`)
            if (localStorage.getItem(`choropleth_bansos_${index_db_num}`)) {
                console.log(`choropleth_bansos_${index_db_num} adaaa`)
                access_db_get(`choropleth_bansos_${index_db_num}`, keyValue, map, info);
                // create_map_area(map, keyValue);
                resolve();
            } else {
                console.log(url);
                $.ajax({
                    url: url,
                    headers: {
                    "Authorization": "Basic " + btoa(url_maps_user_auth + ":" + url_maps_pass_auth)
                    },
                    type: "GET",
                    success: function (response) {
                        // clgSucces(response, response.data.features);
                        
                        access_db_create(response.data, `choropleth_bansos_${index_db_num}`);
                        
                        geojsonLayer = create_choropleth_on_map(map, response.data, info, keyValue);
                        $('#clear-geojsonLayer').click(function () {
                            map.removeLayer(geojsonLayer);
                        });

                    },
                    error: function (xhr, ajaxOptions, throwError) {
                        $('.layer').addClass("d-none");
                        alert("gagal memuat peta: " + throwError);
                        reject(throwError);
                    },
                    complete: (e) => {
                        resolve();
                    }
                });
            }
        })
        
    }

    function create_choropleth_on_map(map, data_choropleth,info, keyValue) {
        // give basic style to the features
        function style(feature) {
            return {
                weight: 1,
                opacity: 1,
                color: 'white',
                dashArray: '0',
                fillOpacity: 0.5,
                fillColor: getColor(feature.properties[keyValue])
            };
        }

        function highlightFeature(e) {
            var layer = e.target;

            layer.setStyle({
                weight: 3,
                color: 'white',
                dashArray: '',
                fillOpacity: 0.7,
            });

            if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
                layer.bringToFront();
            }

            info.update(layer.feature.properties);
        }

        //create habit style of the features

        function resetHighlight(e) {
            geojson.resetStyle(e.target);
            info.update();
        }

        function zoomToFeature(e) {
            map.fitBounds(e.target.getBounds());
        }

        function onEachFeature(feature, layer) {
            layer.on({
                mouseover: highlightFeature,
                mouseout: resetHighlight,
                click: zoomToFeature,
                click: consoleFeature,
            });
        }

        function consoleFeature(e) {
            console.log(e.target);
        }

        //insert data to map using geojson.
        geojson = L.geoJson(data_choropleth, {
            style: style,
            onEachFeature: onEachFeature
        }).addTo(map);

        // add somtehing to bottom of the maps
        // ex : Population data &copy; <a href="http://census.gov/">US Census Bureau</a>
        map.attributionControl.addAttribution('');
        return geojson;
    }

    $(function() {

        //Settings Variable
        let map_div_id = 'geopartial_map'
        let keyValue = 'jml_penduduk';
        
        let map_base = create_map_base(map_div_id);
        let { info, legend } = create_map_area(map_base,keyValue);
        
        const call_all_api = async () => {
            try {
                geojson_1 = load_geojson_by_type(url_maps_1, map_base, info, keyValue, "1");
                geojson_2 = load_geojson_by_type(url_maps_2, map_base, info, keyValue, "2");
                geojson_3 = load_geojson_by_type(url_maps_3, map_base, info, keyValue, "3");
                geojson_4 = load_geojson_by_type(url_maps_4, map_base, info, keyValue, "4");
                geojson_5 = load_geojson_by_type(url_maps_5, map_base, info, keyValue, "5");
    
                // all_api_called = await Promise.all([geojson_1]);
                all_api_called = await Promise.all([geojson_1, geojson_2, geojson_3, geojson_4, geojson_5]);
                return all_api_called;
            } catch (err) {
                console.log(err);
            }
        }
        call_all_api()
            .then(res => {
                $('.layer').addClass("d-none");
            })
            .catch(err => console.log(err));
    });


}