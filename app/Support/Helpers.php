<?php

function message($isSuccess,$successMessage="Data has been saved",$failedMessage="Failed to save data")
{
    if($isSuccess){
        Session::flash('message',$successMessage);
    } else {
        Session::flash('message',$failedMessage);
    }

    Session::flash('messageType',$isSuccess ? 'success' : 'error');
}

function validatorMessageStr($errors){
	$err="<ul>";
    foreach ($errors->all() as $error):
        $err.="<li>$error</li>";
    endforeach;
    $err.="</ul>";
    return $err;
}

function rupiahTanpaKoma($nominal,$withRp=true){
    $rupiah = number_format($nominal, 0, ",", ".");
    if($withRp)
        $rupiah = "Rp " . $rupiah ;
    return $rupiah;
}

function nominalKoma($nominal,$withRp=true){
    $pecah = explode('.',$nominal);
    if (empty($pecah[1])) {
        $rupiah = number_format($nominal, 0, ",", ".");
    }else {
        $rupiah = number_format($nominal, 2, ",", ".");
    }
if($withRp)
    $rupiah = "Rp " . $rupiah ;
return $rupiah;
}

function getsubKategori($id){
  $data = \App\Models\SubKategoriKasus::where('id_kategori_kasus',$id)->get();
  return $data;
}

function getJumlahKasusWilayah($id_kategori,$id_kelurahan){
    $data = \App\Models\RekamKasus::select(\DB::raw('IFNULL(count(rekam_kasus.id),0) as jumlah'))
    ->join('penduduk','penduduk.id','=','rekam_kasus.id_penduduk')
    ->join('kelurahan','kelurahan.id','=','penduduk.id_kelurahan_ktp')
    ->join('sub_kategori_kasus','sub_kategori_kasus.id','=','rekam_kasus.id_sub_kategori_kasus')
    ->where('sub_kategori_kasus.id_kategori_kasus',$id_kategori)
    ->where('kelurahan.id',$id_kelurahan)
    ->first();
    return $data->jumlah;
  }
function getJumlahKasus($id){
    $data = \App\Models\RekamKasus::select(\DB::raw('IFNULL(count(rekam_kasus.id),0) as jumlah'))
    ->join('sub_kategori_kasus','sub_kategori_kasus.id','=','rekam_kasus.id_sub_kategori_kasus')
    ->where('sub_kategori_kasus.id_kategori_kasus',$id)
    ->first();
    return $data->jumlah;
  }

  function getJumlahKasusSemua(){
    $data = \App\Models\RekamKasus::select(\DB::raw('IFNULL(count(rekam_kasus.id),0) as jumlah'))
    ->join('sub_kategori_kasus','sub_kategori_kasus.id','=','rekam_kasus.id_sub_kategori_kasus')
 
    ->first();
    return $data->jumlah;
  }

  function getJumlahSubKasus($id){
    $data = \App\Models\RekamKasus::select(\DB::raw('IFNULL(count(rekam_kasus.id),0) as jumlah'))
    ->join('sub_kategori_kasus','sub_kategori_kasus.id','=','rekam_kasus.id_sub_kategori_kasus')
    ->where('sub_kategori_kasus.id',$id)
    ->first();
    return $data->jumlah;
  }
  function getJumlahSubKasuskecamatan($id_kategori,$id_kecamatan){
    $data = \App\Models\RekamKasus::select(\DB::raw('IFNULL(count(rekam_kasus.id),0) as jumlah'))
    ->join('sub_kategori_kasus','sub_kategori_kasus.id','=','rekam_kasus.id_sub_kategori_kasus')
    ->join('penduduk','penduduk.id','=','rekam_kasus.id_penduduk')
    ->join('kelurahan','kelurahan.id','=','penduduk.id_kelurahan_ktp')
    ->where('sub_kategori_kasus.id_kategori_kasus',$id_kategori)
    ->where('kelurahan.id_kecamatan',$id_kecamatan)
    ->first();
    return $data->jumlah;
  }
  
  function getJumlahSubKasusHariIni($id){
    $data = \App\Models\RekamKasus::select(\DB::raw('IFNULL(count(rekam_kasus.id),0) as jumlah'))
    ->join('sub_kategori_kasus','sub_kategori_kasus.id','=','rekam_kasus.id_sub_kategori_kasus')
    ->where('sub_kategori_kasus.id',$id)
    ->where('tanggal',date('Y-m-d'))
    ->first();
    return $data->jumlah;
  }
  
  function getcoordinate($id){
    $datamap = \App\Models\Map::where('id_kelurahan',$id)->get();
  return $datamap;
}

  function getTanggalHariIni(){
      $last = \App\Models\RekamKasus::orderby('created_at','desc')->first();
    $tgl=date_indo(\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $last->created_at )->format('Y-m-d'));
    return $tgl;
  }

  function getWaktuHariIni(){
    $last = \App\Models\RekamKasus::orderby('id','desc')->first();
  $waktu=\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $last->created_at )->format('H:i');
  return $waktu;
}

  function getPresentaseSubKasusHariIni($id){
    $datahariini = \App\Models\RekamKasus::select(\DB::raw('IFNULL(count(rekam_kasus.id),0) as jumlah'))
    ->join('sub_kategori_kasus','sub_kategori_kasus.id','=','rekam_kasus.id_sub_kategori_kasus')
    ->where('sub_kategori_kasus.id',$id)
    ->where('tanggal',date('Y-m-d'))
    ->first()->jumlah;

    $datasemua = \App\Models\RekamKasus::select(\DB::raw('IFNULL(count(rekam_kasus.id),0) as jumlah'))
    ->join('sub_kategori_kasus','sub_kategori_kasus.id','=','rekam_kasus.id_sub_kategori_kasus')
    ->where('sub_kategori_kasus.id',$id)
    ->first()->jumlah;

    $data = $datahariini>0?(($datahariini / $datasemua)* 100):0;
    return $data;
  }

  function getKecamatan(){
    $data = \App\Models\Kecamatan::orderby('kecamatan','asc')->get();
  return $data;
}

function currencyToNumber($a){
    $b=str_replace(".", "", $a);
    return str_replace(",",".",$b);
}

function date_indo($tgl)
{
    $ubah = gmdate($tgl, time()+60*60*8);
    $pecah = explode("-",$ubah);
    $tanggal = $pecah[2];
    $bulan = bulan($pecah[1]);
    $tahun = $pecah[0];
    return $tanggal.' '.$bulan.' '.$tahun;
}



function bulan($bln)
{
    switch ($bln)
    {
        case 1:
        return "Januari";
        break;
        case 2:
        return "Februari";
        break;
        case 3:
        return "Maret";
        break;
        case 4:
        return "April";
        break;
        case 5:
        return "Mei";
        break;
        case 6:
        return "Juni";
        break;
        case 7:
        return "Juli";
        break;
        case 8:
        return "Agustus";
        break;
        case 9:
        return "September";
        break;
        case 10:
        return "Oktober";
        break;
        case 11:
        return "November";
        break;
        case 12:
        return "Desember";
        break;
    }
}

function date_test()
{
    return date('Y-m-d');
}

function hour_test()
{
    // return date('09:00:s');
    // return date('17:00:s');
    return date('H:i:s');
}

function limitTextChars($content = false, $limit = false, $stripTags = false, $ellipsis = false) 
{
    if ($content && $limit) {
        $content  = ($stripTags ? strip_tags($content) : $content);
        $ellipsis = ($ellipsis ? "..." : $ellipsis);
        $content  = mb_strimwidth($content, 0, $limit, $ellipsis);
    }
    return $content;
}

function limitTextWords($content = false, $limit = false, $stripTags = false, $ellipsis = false) 
{
    if ($content && $limit) {
        $content = ($stripTags ? strip_tags($content) : $content);
        $content = explode(' ', $content, $limit+1);
        array_pop($content);
        if ($ellipsis) {
            array_push($content, '...');
        }
        $content = implode(' ', $content);
    }
    return $content;
}

function time_elapsed_string($datetime, $full = false) {
    $now = new DateTime;
    $ago = new DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'tahun',
        'm' => 'bulan',
        'w' => 'minggu',
        'd' => 'hari',
        'h' => 'jam',
        'i' => 'menit',
        's' => 'detik',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? '' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full) $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' yang lalu' : 'baru saja';
}

function getConfigValues($configName){
    return \App\Models\ConfigId::getValues($configName);
}

function time_diff_string($from, $to, $full = false) {
    $from = new DateTime($from);
    $to = new DateTime($to);
    $diff = $to->diff($from);

    // $diff->w = floor($diff->d / 7);
    // $diff->d -= $diff->w * 7;

    // $string = array(
    //     'y' => 'year',
    //     'm' => 'month',
    //     'w' => 'week',
    //     'd' => 'day',
    //     'h' => 'hour',
    //     'i' => 'minute',
    //     's' => 'second',
    // );
    // foreach ($string as $k => &$v) {
    //     dd($diff->$k);
    //     // if ($diff->$k) {
    //     //     $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
    //     // } 
    //     // else {
    //     //     unset($string[$k]);
    //     // }
    // }
    $data['tahun']=$diff->y;
    $data['bulan']=$diff->m;
    $data['hari']=($diff->d-1);

    return $data;
}