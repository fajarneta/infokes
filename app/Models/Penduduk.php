<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
// use App\Traits\RecordSignature;

class Penduduk extends Model {
    // use RecordSignature;
    
    public $guarded = ["id","created_at","updated_at"];
    protected $table="penduduk";
    public $timestamps=true;
    protected $primaryKey = "id";
    public $incrementing = true;
    public static function findRequested()
    {
        $query = Penduduk::query();

        // search results based on user input
        \Request::input('id') and $query->where('id',\Request::input('id'));
        \Request::input('nama_lengkap') and $query->where('nama_lengkap','like','%'.\Request::input('nama_lengkap').'%');
        \Request::input('nik') and $query->where('nik','like','%'.\Request::input('nik').'%');
        \Request::input('alamat_ktp') and $query->where('alamat_ktp','like','%'.\Request::input('alamat_ktp').'%');
        \Request::input('id_kelurahan_ktp') and $query->where('id_kelurahan_ktp',\Request::input('id_kelurahan_ktp'));
        \Request::input('alamat_domisili') and $query->where('alamat_domisili','like','%'.\Request::input('alamat_domisili').'%');
        \Request::input('id_kelurahan_domisili') and $query->where('id_kelurahan_domisili',\Request::input('id_kelurahan_domisili'));
        \Request::input('tempat_lahir') and $query->where('tempat_lahir','like','%'.\Request::input('tempat_lahir').'%');
        \Request::input('tanggal_lahir') and $query->where('tanggal_lahir',\Request::input('tanggal_lahir'));
        \Request::input('usia') and $query->where('usia',\Request::input('usia'));
        \Request::input('no_hp') and $query->where('no_hp','like','%'.\Request::input('no_hp').'%');
        \Request::input('foto') and $query->where('foto','like','%'.\Request::input('foto').'%');
        \Request::input('id_golongan_darah') and $query->where('id_golongan_darah',\Request::input('id_golongan_darah'));
        \Request::input('id_jenis_kelamin') and $query->where('id_jenis_kelamin',\Request::input('id_jenis_kelamin'));
        \Request::input('id_agama') and $query->where('id_agama',\Request::input('id_agama'));
        \Request::input('id_status_pernikahan') and $query->where('id_status_pernikahan',\Request::input('id_status_pernikahan'));
        \Request::input('id_profesi') and $query->where('id_profesi',\Request::input('id_profesi'));
        \Request::input('created_at') and $query->where('created_at',\Request::input('created_at'));
        \Request::input('updated_at') and $query->where('updated_at',\Request::input('updated_at'));
        
        // sort results
        \Request::input("sort") and $query->orderBy(\Request::input("sort"),\Request::input("sortType","asc"));

        // paginate results
        return $query->paginate(15);
    }

    public static function validationRules( $attributes = null )
    {
        $rules = [
            'nama_lengkap' => 'string|max:100',
            'nik' => 'string|max:20',
            'alamat_ktp' => 'string|max:100',
            'id_kelurahan_ktp' => 'integer',
            'alamat_domisili' => 'string|max:100',
            'id_kelurahan_domisili' => 'integer',
            'tempat_lahir' => 'string|max:100',
            'tanggal_lahir' => 'date',
            'usia' => 'integer',
            'no_hp' => 'string|max:15',
            'foto' => 'string|max:100',
            'id_golongan_darah' => 'integer',
            'id_jenis_kelamin' => 'integer',
            'id_agama' => 'integer',
            'id_status_pernikahan' => 'integer',
            'id_profesi' => 'integer',
        ];

        // no list is provided
        if(!$attributes)
            return $rules;

        // a single attribute is provided
        if(!is_array($attributes))
            return [ $attributes => $rules[$attributes] ];

        // a list of attributes is provided
        $newRules = [];
        foreach ( $attributes as $attr )
            $newRules[$attr] = $rules[$attr];
        return $newRules;
    }

    public function pk(){
      return $this->{$this->primaryKey};
    }

    
    public function kelurahan_ktp()
    {
        return $this->belongsTo('App\Models\Kelurahan','id_kelurahan_ktp');
    }

    public function setKelurahan_ktpAttribute($kelurahan_ktp) {
      unset($this->attributes['kelurahan_ktp']);
    }

    
    
    public function kelurahan_domisili()
    {
        return $this->belongsTo('App\Models\Kelurahan','id_kelurahan_domisili');
    }

    public function setKelurahan_domisiliAttribute($kelurahan_domisili) {
      unset($this->attributes['kelurahan_domisili']);
    }

    
    
    public function golongan_darah()
    {
        return $this->belongsTo('App\Models\Golongan_darah','id_golongan_darah');
    }

    public function setGolongan_darahAttribute($golongan_darah) {
      unset($this->attributes['golongan_darah']);
    }

    
    
    public function jenis_kelamin()
    {
        return $this->belongsTo('App\Models\Jenis_kelamin','id_jenis_kelamin');
    }

    public function setJenis_kelaminAttribute($jenis_kelamin) {
      unset($this->attributes['jenis_kelamin']);
    }

    
    
    public function agama()
    {
        return $this->belongsTo('App\Models\Agama','id_agama');
    }

    public function setAgamaAttribute($agama) {
      unset($this->attributes['agama']);
    }

    
    
    public function status_pernikahan()
    {
        return $this->belongsTo('App\Models\Status_pernikahan','id_status_pernikahan');
    }

    public function setStatus_pernikahanAttribute($status_pernikahan) {
      unset($this->attributes['status_pernikahan']);
    }

    
    
    public function profesi()
    {
        return $this->belongsTo('App\Models\profesi','id_profesi');
    }

    public function setprofesiAttribute($profesi) {
      unset($this->attributes['profesi']);
    }

    
    }
