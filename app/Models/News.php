<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\RecordSignature;

class News extends Model {
    use RecordSignature;
    
    public $guarded = ["id","created_at","updated_at"];
    protected $table="news";
    public $timestamps=true;
    protected $primaryKey = "id";
    public $incrementing = true;
    public static function findRequested()
    {
        $query = News::query();

        // search results based on user input
        \Request::input('id') and $query->where('id',\Request::input('id'));
        \Request::input('judul') and $query->where('judul','like','%'.\Request::input('judul').'%');
        \Request::input('isi') and $query->where('isi','like','%'.\Request::input('isi').'%');
        \Request::input('gambar') and $query->where('gambar','like','%'.\Request::input('gambar').'%');
        \Request::input('flag_aktif') and $query->where('flag_aktif','like','%'.\Request::input('flag_aktif').'%');
        \Request::input('id_kategori_news') and $query->where('id_kategori_news',\Request::input('id_kategori_news'));
        \Request::input('user_input') and $query->where('user_input',\Request::input('user_input'));
        \Request::input('user_update') and $query->where('user_update',\Request::input('user_update'));
        \Request::input('created_at') and $query->where('created_at',\Request::input('created_at'));
        \Request::input('updated_at') and $query->where('updated_at',\Request::input('updated_at'));
        
        // sort results
        \Request::input("sort") and $query->orderBy(\Request::input("sort"),\Request::input("sortType","asc"));

        // paginate results
        return $query->paginate(15);
    }

    public static function validationRules( $attributes = null )
    {
        $rules = [
            'judul' => 'string|max:100',
            'isi' => 'string',
            'gambar' => 'string|max:100',
            'flag_aktif' => 'string|max:1',
            'id_kategori_news' => 'integer',
            'user_input' => 'integer',
            'user_update' => 'integer',
        ];

        // no list is provided
        if(!$attributes)
            return $rules;

        // a single attribute is provided
        if(!is_array($attributes))
            return [ $attributes => $rules[$attributes] ];

        // a list of attributes is provided
        $newRules = [];
        foreach ( $attributes as $attr )
            $newRules[$attr] = $rules[$attr];
        return $newRules;
    }

    public function pk(){
      return $this->{$this->primaryKey};
    }

    
    public function kategori_news()
    {
        return $this->belongsTo('App\Models\Kategori_news','id_kategori_news');
    }

    public function setKategori_newsAttribute($kategori_news) {
      unset($this->attributes['kategori_news']);
    }

    
    }
