<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\RecordSignature;

class SubKategoriKasus extends Model {
    // use RecordSignature;
    
    public $guarded = ["id","created_at","updated_at"];
    protected $table="sub_kategori_kasus";
    public $timestamps=true;
    protected $primaryKey = "id";
    public $incrementing = true;
    public static function findRequested()
    {
        $query = SubKategoriKasus::query();

        // search results based on user input
        \Request::input('id') and $query->where('id',\Request::input('id'));
        \Request::input('id_kategori_kasus') and $query->where('id_kategori_kasus',\Request::input('id_kategori_kasus'));
        \Request::input('sub_kategori_kasus') and $query->where('sub_kategori_kasus','like','%'.\Request::input('sub_kategori_kasus').'%');
        \Request::input('created_at') and $query->where('created_at',\Request::input('created_at'));
        \Request::input('updated_at') and $query->where('updated_at',\Request::input('updated_at'));
        
        // sort results
        \Request::input("sort") and $query->orderBy(\Request::input("sort"),\Request::input("sortType","asc"));

        // paginate results
        return $query->paginate(15);
    }

    public static function validationRules( $attributes = null )
    {
        $rules = [
            'id_kategori_kasus' => 'integer',
            'sub_kategori_kasus' => 'string|max:100',
        ];

        // no list is provided
        if(!$attributes)
            return $rules;

        // a single attribute is provided
        if(!is_array($attributes))
            return [ $attributes => $rules[$attributes] ];

        // a list of attributes is provided
        $newRules = [];
        foreach ( $attributes as $attr )
            $newRules[$attr] = $rules[$attr];
        return $newRules;
    }

    public function pk(){
      return $this->{$this->primaryKey};
    }

    
    public function kategori_kasus()
    {
        return $this->belongsTo('App\Models\KategoriKasus','id_kategori_kasus');
    }

    public function setKategori_kasusAttribute($kategori_kasus) {
      unset($this->attributes['kategori_kasus']);
    }

    
    }
