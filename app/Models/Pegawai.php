<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\RecordSignature;

class Pegawai extends Model {
    // use RecordSignature;
    
    public $guarded = ["id","created_at","updated_at"];
    protected $table="pegawai";
    public $timestamps=true;
    protected $primaryKey = "id";
    public $incrementing = true;
    public static function findRequested()
    {
        $query = Pegawai::query();

        // search results based on user input
        \Request::input('id') and $query->where('id',\Request::input('id'));
        \Request::input('nip') and $query->where('nip','like','%'.\Request::input('nip').'%');
        \Request::input('id_penduduk') and $query->where('id_penduduk',\Request::input('id_penduduk'));
        \Request::input('id_jabatan') and $query->where('id_jabatan',\Request::input('id_jabatan'));
        \Request::input('id_unit_kerja') and $query->where('id_unit_kerja',\Request::input('id_unit_kerja'));
        \Request::input('id_status_pegawai') and $query->where('id_status_pegawai',\Request::input('id_status_pegawai'));
        \Request::input('flag_aktif') and $query->where('flag_aktif','like','%'.\Request::input('flag_aktif').'%');
        \Request::input('tgl_bergabung') and $query->where('tgl_bergabung',\Request::input('tgl_bergabung'));
        \Request::input('created_at') and $query->where('created_at',\Request::input('created_at'));
        \Request::input('updated_at') and $query->where('updated_at',\Request::input('updated_at'));
        
        // sort results
        \Request::input("sort") and $query->orderBy(\Request::input("sort"),\Request::input("sortType","asc"));

        // paginate results
        return $query->paginate(15);
    }

    public static function validationRules( $attributes = null )
    {
        $rules = [
            'nip' => 'string|max:100',
            'id_penduduk' => 'integer',
            'id_jabatan' => 'integer',
            'id_unit_kerja' => 'integer',
            'id_status_pegawai' => 'integer',
            'flag_aktif' => 'string|max:1',
            'tgl_bergabung' => 'date',
        ];

        // no list is provided
        if(!$attributes)
            return $rules;

        // a single attribute is provided
        if(!is_array($attributes))
            return [ $attributes => $rules[$attributes] ];

        // a list of attributes is provided
        $newRules = [];
        foreach ( $attributes as $attr )
            $newRules[$attr] = $rules[$attr];
        return $newRules;
    }

    public function pk(){
      return $this->{$this->primaryKey};
    }

    
    public function penduduk()
    {
        return $this->belongsTo('App\Models\Penduduk','id_penduduk');
    }

    public function setPendudukAttribute($penduduk) {
      unset($this->attributes['penduduk']);
    }

    
    
    public function jabatan()
    {
        return $this->belongsTo('App\Models\Jabatan','id_jabatan');
    }

    public function setJabatanAttribute($jabatan) {
      unset($this->attributes['jabatan']);
    }

    
    
    public function unit_kerja()
    {
        return $this->belongsTo('App\Models\UnitKerja','id_unit_kerja');
    }

    public function setUnit_kerjaAttribute($unit_kerja) {
      unset($this->attributes['unit_kerja']);
    }

    
    
    public function status_pegawai()
    {
        return $this->belongsTo('App\Models\StatusPegawai','id_status_pegawai');
    }

    public function setStatus_pegawaiAttribute($status_pegawai) {
      unset($this->attributes['status_pegawai']);
    }

    
    }
