<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\RecordSignature;

class RekamKasus extends Model {
    use RecordSignature;
    
    public $guarded = ["id","created_at","updated_at"];
    protected $table="rekam_kasus";
    public $timestamps=true;
    protected $primaryKey = "id";
    public $incrementing = true;
    public static function findRequested()
    {
        $query = RekamKasus::query();

        // search results based on user input
        \Request::input('id') and $query->where('id',\Request::input('id'));
        \Request::input('id_penduduk') and $query->where('id_penduduk',\Request::input('id_penduduk'));
        \Request::input('id_kategori_kasus') and $query->where('id_kategori_kasus',\Request::input('id_kategori_kasus'));
        \Request::input('tanggal') and $query->where('tanggal',\Request::input('tanggal'));
        \Request::input('riwayat_perjalanan') and $query->where('riwayat_perjalanan','like','%'.\Request::input('riwayat_perjalanan').'%');
        \Request::input('diagnosa') and $query->where('diagnosa','like','%'.\Request::input('diagnosa').'%');
        \Request::input('hasil') and $query->where('hasil','like','%'.\Request::input('hasil').'%');
        \Request::input('keterangan') and $query->where('keterangan','like','%'.\Request::input('keterangan').'%');
        \Request::input('user_input') and $query->where('user_input',\Request::input('user_input'));
        \Request::input('user_update') and $query->where('user_update',\Request::input('user_update'));
        \Request::input('created_at') and $query->where('created_at',\Request::input('created_at'));
        \Request::input('updated_at') and $query->where('updated_at',\Request::input('updated_at'));
        
        // sort results
        \Request::input("sort") and $query->orderBy(\Request::input("sort"),\Request::input("sortType","asc"));

        // paginate results
        return $query->paginate(15);
    }

    public static function validationRules( $attributes = null )
    {
        $rules = [
            'id_penduduk' => 'integer',
            'id_kategori_kasus' => 'integer',
            'tanggal' => 'date',
            'riwayat_perjalanan' => 'string',
            'diagnosa' => 'string',
            'hasil' => 'string',
            'keterangan' => 'string',
            'user_input' => 'integer',
            'user_update' => 'integer',
        ];

        // no list is provided
        if(!$attributes)
            return $rules;

        // a single attribute is provided
        if(!is_array($attributes))
            return [ $attributes => $rules[$attributes] ];

        // a list of attributes is provided
        $newRules = [];
        foreach ( $attributes as $attr )
            $newRules[$attr] = $rules[$attr];
        return $newRules;
    }

    public function pk(){
      return $this->{$this->primaryKey};
    }

    
    public function penduduk()
    {
        return $this->belongsTo('App\Models\Penduduk','id_penduduk');
    }

    public function setPendudukAttribute($penduduk) {
      unset($this->attributes['penduduk']);
    }

    
    
    public function sub_kategori_kasus()
    {
        return $this->belongsTo('App\Models\SubKategoriKasus','id_sub_kategori_kasus');
    }

    public function setSub_Kategori_kasusAttribute($sub_kategori_kasus) {
      unset($this->attributes['sub_kategori_kasus']);
    }

    
    }
