<?php
namespace App\Http\Controllers;

use App\Models\RekamKasus;
use App\Models\Penduduk;
use App\Models\Provinsi;
use App\Models\Kabupaten;
use App\Models\Kecamatan;
use App\Models\Kelurahan;
use App\Models\User;
use App\Models\Agama;
use App\Models\GolonganDarah;
use App\Models\Profesi;
use App\Models\JenisKelamin;
use App\Models\StatusPernikahan;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Datatables;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
date_default_timezone_set("Asia/Jakarta");
class RekamKasusController extends Controller
{
    public $viewDir = "rekam_kasus";
    public $breadcrumbs = array(
         'permissions'=>array('title'=>'Rekam-kasus','link'=>"#",'active'=>false,'display'=>true),
       );

       public function __construct()
       {
           $this->middleware('permission:read-rekam-kasus');
       }

       public function index()
       {
           return $this->view( "index");
       }

       public function tambahData()
       {
           return $this->view( "form-rekam-kasus");
       }

       public function editData($kode)
       {
           $rekamKasus=RekamKasus::find($kode);
           $data['rekamKasus']=$rekamKasus;
           return $this->view( "form-rekam-kasus",$data);
       }
       /**
        * Show the form for creating a new resource.
        *
        * @return  \Illuminate\Http\Response
        */
       public function create()
       {
           return $this->view("form",['rekamKasus' => new RekamKasus]);
       }

       public function simpanData(Request $request)
      {
        // dd($request->all());
        $validation = Validator::make($request->all(), [
          'nama' => 'required',
          'nik' => 'required|unique:penduduk',
     
          'tempat_lahir' => 'required',
          'tanggal_lahir' => 'required',
          'agama' => 'required',
          'status_pernikahan' => 'required',
          'jenis_kelamin' => 'required',
          'golongan_darah' => 'required',
          'pekerjaan' => 'required',
          'alamat_ktp' => 'required',
          'kelurahan_ktp' => 'required',
          'alamat_domisili' => 'required',
          'kelurahan_domisili' => 'required',
          'kategori_kasus' => 'required',
          'sub_kategori_kasus' => 'required',
          'tanggal' => 'required',
          'diagnosa' => 'required',
        ]);
    
        if (!$validation->passes()){
          $status=array(
            'status' => false,
            'msg' => $validation->errors()->all()
          );
        }
    
        $all_data=$request->all();
        DB::beginTransaction();
        try {
          $data=array(
            'nama_lengkap'=>$all_data['nama'],
            'nik'=>$all_data['nik'],
        
            'tempat_lahir'=>$all_data['tempat_lahir'],
            'tanggal_lahir'=>date('Y-m-d',strtotime($all_data['tanggal_lahir'])),
            'usia'=>$all_data['usia'],
            'id_agama'=>$all_data['agama'],
            'id_jenis_kelamin'=>$all_data['jenis_kelamin'],
            'id_golongan_darah'=>$all_data['golongan_darah'],
            'id_status_pernikahan'=>$all_data['status_pernikahan'],
            'id_profesi'=>$all_data['pekerjaan'],
            'alamat_ktp'=>$all_data['alamat_ktp'],
            'alamat_domisili'=>$all_data['alamat_domisili'],
            'id_kelurahan_ktp'=>$all_data['kelurahan_ktp'],
            'id_kelurahan_domisili'=>$all_data['kelurahan_domisili']
          );
        
          $datapenduduk=Penduduk::where('nik',$all_data['nik'])->first();
    
            if(!isset($datapenduduk)){
              $insert_to_penduduk=Penduduk::create($data);
              $id_penduduk=$insert_to_penduduk->id;
            }else{
              $insert_to_penduduk=$datapenduduk->update($data);
              $id_penduduk=$datapenduduk->id;
            }

            $dataKasus=array(
                'id_sub_kategori_kasus'=>$all_data['sub_kategori_kasus'],
                'tanggal'=>$all_data['tanggal'],
                'riwayat_perjalanan'=>$all_data['riwayat_perjalanan'],
                'diagnosa'=>$all_data['diagnosa'],
                'hasil'=>$all_data['hasil'],
                'keterangan'=>$all_data['keterangan'],
                'tanggal'=>date('Y-m-d'),
                'id_penduduk'=> $id_penduduk,
            
              );
              if($all_data['id_rekam_kasus']){
                $RekamKasus=RekamKasus::find($all_data['id_rekam_kasus']);
                $insert_to_kasus=$RekamKasus->update($dataKasus);
              }else{
                $insert_to_kasus=RekamKasus::create($dataKasus);
              }
          
    
          if($insert_to_kasus==true)
          {
              $status=array(
                'status' => true,
                'msg' => 'Data Berhasil Dimasukkan'
              );
          }
          else
          {
             $status=array(
              'status' => false,
              'msg' => 'Data Gagal Dimasukkan'
            );
         }
    
        } catch (Exception $e) {
          // echo 'Message' .$e->getMessage();
         $status=array(
          'status' => false,
          'msg' => $e->getMessage()
        );
         DB::rollback();
       }
       DB::commit();
    
       return \Response::json($status);
     }

       /**
        * Store a newly created resource in storage.
        *
        * @param    \Illuminate\Http\Request  $request
        * @return  \Illuminate\Http\Response
        */
       public function store( Request $request )
       {
           $this->validate($request, RekamKasus::validationRules());

           $act=RekamKasus::create($request->all());
           message($act,'Data Rekam Kasus berhasil ditambahkan','Data Rekam Kasus gagal ditambahkan');
           return redirect('rekam-kasus');
       }

       /**
        * Display the specified resource.
        *
        * @return  \Illuminate\Http\Response
        */
       public function show(Request $request, $kode)
       {
           $rekamKasus=RekamKasus::find($kode);
           return $this->view("show",['rekamKasus' => $rekamKasus]);
       }

       /**
        * Show the form for editing the specified resource.
        *
        * @return  \Illuminate\Http\Response
        */
       public function edit(Request $request, $kode)
       {
           $rekamKasus=RekamKasus::find($kode);
           return $this->view( "form", ['rekamKasus' => $rekamKasus] );
       }

       /**
        * Update the specified resource in storage.
        *
        * @param    \Illuminate\Http\Request  $request
        * @return  \Illuminate\Http\Response
        */
       public function update(Request $request, $kode)
       {
           $rekamKasus=RekamKasus::find($kode);
           if( $request->isXmlHttpRequest() )
           {
               $data = [$request->name  => $request->value];
               $validator = \Validator::make( $data, RekamKasus::validationRules( $request->name ) );
               if($validator->fails())
                   return response($validator->errors()->first( $request->name),403);
               $rekamKasus->update($data);
               return "Record updated";
           }
           $this->validate($request, RekamKasus::validationRules());

           $act=$rekamKasus->update($request->all());
           message($act,'Data Rekam Kasus berhasil diupdate','Data Rekam Kasus gagal diupdate');

           return redirect('/rekam-kasus');
       }

       /**
        * Remove the specified resource from storage.
        *
        * @return  \Illuminate\Http\Response
        */
       public function destroy(Request $request, $kode)
       {
           $rekamKasus=RekamKasus::find($kode);
           $act=false;
           try {
               $act=$rekamKasus->forceDelete();
           } catch (\Exception $e) {
               $rekamKasus=RekamKasus::find($rekamKasus->pk());
               $act=$rekamKasus->delete();
           }
       }

       protected function view($view, $data = [])
       {
           return view($this->viewDir.".".$view, $data);
       }
       public function loadData()
       {
           $GLOBALS['nomor']=\Request::input('start',1)+1;
           $dataList = RekamKasus::select('*');
           if (request()->get('status') == 'trash') {
               $dataList->onlyTrashed();
           }
           return Datatables::of($dataList)
               ->addColumn('nomor',function($kategori){
                   return $GLOBALS['nomor']++;
               })
               ->addColumn('nama_lengkap',function($data){
                if(isset($data->penduduk->nama_lengkap)){
                  return $data->penduduk->nama_lengkap;
                }else{
                  return '-';
                }
              })
              ->addColumn('nik',function($data){
                if(isset($data->penduduk->nik)){
                  return $data->penduduk->nik;
                }else{
                  return '-';
                }
              })
              ->addColumn('alamat_domisili',function($data){
                if(isset($data->penduduk->alamat_domisili)){
                  return $data->penduduk->alamat_domisili;
                }else{
                  return '-';
                }
              })
              ->addColumn('alamat_ktp',function($data){
                if(isset($data->penduduk->alamat_ktp)){
                  return $data->penduduk->alamat_ktp;
                }else{
                  return '-';
                }
              })
              ->addColumn('kategori_kasus',function($data){
                if(isset($data->sub_kategori_kasus->kategori_kasus->kategori_kasus)){
                  return $data->sub_kategori_kasus->kategori_kasus->kategori_kasus;
                }else{
                  return '-';
                }
              })
              ->addColumn('usia',function($data){
                if(isset($data->penduduk->usia)){
                  return $data->penduduk->usia.' Tahun';
                }else{
                  return '-';
                }
              })
              ->addColumn('tanggal',function($data){
                if(isset($data->tanggal)){
                  return \Carbon\Carbon::createfromformat('Y-m-d H:i:s',$data->tanggal)->format('d-m-Y');
                }else{
                  return '-';
                }
              })
               ->addColumn('action', function ($data) {
                
                   $delete=url("rekam-kasus/".$data->pk());
                 $content = '';
                 $content .= "<center><a href='".url("edit-rekam-kasus")."/".$data->pk()."'><span  class='btn btn-sm btn-warning'><span style='color:white;'><i class='icon md-edit' ></i>Edit</a></span>";
                 $content .= " <span style='color:white;'><a onclick='hapus(\"$delete\")' class='btn btn-sm btn-danger'><i class='icon md-delete'></i>Hapus</a></span></center>";


                   return $content;
               })
               ->make(true);
       }
         }
