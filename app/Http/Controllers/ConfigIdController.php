<?php
namespace App\Http\Controllers;

use App\Models\ConfigId;
use App\Models\Perusahaan;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Datatables;

class ConfigIdController extends Controller
{
    public $viewDir = "config_id";
    public $breadcrumbs = array(
         'permissions'=>array('title'=>'Config-ids','link'=>"#",'active'=>false,'display'=>true),
       );

       public function __construct()
       {
           // $this->middleware('permission:read-config-ids');
       }

       public function index()
       {
           return $this->view( "index");
       }

       public function settings()
       {
        // dd(getConfigValues('Wonogiri'));
          $perusahaan=Perusahaan::orderby('id','desc')->first();
          return $this->view( "settings",compact('perusahaan'));
       }

       /**
        * Show the form for creating a new resource.
        *
        * @return  \Illuminate\Http\Response
        */
       public function create()
       {
           return $this->view("form",['configId' => new ConfigId]);
       }

       /**
        * Store a newly created resource in storage.
        *
        * @param    \Illuminate\Http\Request  $request
        * @return  \Illuminate\Http\Response
        */
       public function store( Request $request )
       {
        $all_data=$request->all();

        if(isset($all_data['mode']) && $all_data['mode']=='config')
        {
          $data=array(
            'config_name'=>$all_data['config_name'],
            'table_source'=>$all_data['table_source'],
            'config_value'=>$all_data['config_value'],
            'description'=>$all_data['description'],
          );
          // dd($data);
           $act=ConfigId::create($data);
        }
        else
        {
          $cek_perusahaan_exists=Perusahaan::orderby('id','desc')->first();

          if(isset($cek_perusahaan_exists) && !empty($cek_perusahaan_exists))
          {
            $logo=null;
            if(isset($all_data['file_logo']))
            {
              $logo=$all_data['file_logo'];
            }

            if($request->hasFile('logo'))
            {
              $extension = $request->file('logo')->getClientOriginalExtension();
              $dir = 'images/logo_perusahaan/';
              $logo = uniqid() . '_' . time() . '.' . $extension;
              $request->file('logo')->move($dir, $logo);
            }


            $data=array(
              'nama_perusahaan'=>$all_data['nama_perusahaan'],
              'alamat'=>$all_data['alamat'],
              'website'=>$all_data['website1'],
              'email'=>$all_data['email'].''.$all_data['at'].''.$all_data['email1'],
              'logo'=>$logo,
            );

            $act=$cek_perusahaan_exists->update($data);
          }
          else
          {
            $logo=null;
            if($request->hasFile('logo'))
            {
              $extension = $request->file('logo')->getClientOriginalExtension();
              $dir = 'images/logo_perusahaan/';
              $logo = uniqid() . '_' . time() . '.' . $extension;
              $request->file('logo')->move($dir, $logo);
            }

            $data=array(
              'nama_perusahaan'=>$all_data['nama_perusahaan'],
              'alamat'=>$all_data['alamat'],
              'website'=>$all_data['website1'],
              'email'=>$all_data['email'].''.$all_data['at'].''.$all_data['email1'],
              'logo'=>$logo,
            );

            $act=Perusahaan::create($data);
          }
          
        }
           // $this->validate($request, ConfigId::validationRules());

          
           message($act,'Data berhasil ditambahkan','Data gagal ditambahkan');
           return redirect('/pengaturan');
       }

       public function deleteLogo()
       {
        $delete_foto=Perusahaan::orderby('id','desc')->first();
        $delete=$delete_foto->update(['logo'=>null]);

        if($delete==true)
        {
          $data=array(
            'status'=>true,
            'msg'=>'Logo Berhasil Dihapus'
          );
        }
        else
        {
          $data=array(
            'status'=>false,
            'errors'=>'Logo Gagal Dihapus',
          );
        }
        return \Response::json($data);
      }

       /**
        * Display the specified resource.
        *
        * @return  \Illuminate\Http\Response
        */
       public function show(Request $request, $kode)
       {
           $configId=ConfigId::find($kode);
           return $this->view("show",['configId' => $configId]);
       }

       /**
        * Show the form for editing the specified resource.
        *
        * @return  \Illuminate\Http\Response
        */
       public function edit(Request $request, $kode)
       {
           $configId=ConfigId::find($kode);
           return $this->view( "form", ['configId' => $configId] );
       }

       /**
        * Update the specified resource in storage.
        *
        * @param    \Illuminate\Http\Request  $request
        * @return  \Illuminate\Http\Response
        */
       public function update(Request $request, $kode)
       {
           $configId=ConfigId::find($kode);
           if( $request->isXmlHttpRequest() )
           {
               $data = [$request->name  => $request->value];
               $validator = \Validator::make( $data, ConfigId::validationRules( $request->name ) );
               if($validator->fails())
                   return response($validator->errors()->first( $request->name),403);
               $configId->update($data);
               return "Record updated";
           }
           // $this->validate($request, ConfigId::validationRules());

           $all_data=$request->all();
           $data=array(
            'config_name'=>$all_data['config_name'],
            'table_source'=>$all_data['table_source'],
            'config_value'=>$all_data['config_value'],
            'description'=>$all_data['description'],
          );

           $act=$configId->update($data);
           message($act,'Data Config Ids berhasil diupdate','Data Config Ids gagal diupdate');

           return redirect('/pengaturan');
       }

       /**
        * Remove the specified resource from storage.
        *
        * @return  \Illuminate\Http\Response
        */
       public function destroy(Request $request, $kode)
       {
           $configId=ConfigId::find($kode);
           $act=false;
           try {
               $act=$configId->forceDelete();
           } catch (\Exception $e) {
               $configId=ConfigId::find($configId->pk());
               $act=$configId->delete();
           }
       }

       protected function view($view, $data = [])
       {
           return view($this->viewDir.".".$view, $data);
       }
       public function loadData()
       {
           $GLOBALS['nomor']=\Request::input('start',1)+1;
           $dataList = ConfigId::select('*');
           if (request()->get('status') == 'trash') {
               $dataList->onlyTrashed();
           }
           return Datatables::of($dataList)
               ->addColumn('nomor',function($kategori){
                   return $GLOBALS['nomor']++;
               })
               ->addColumn('action', function ($data) {
                   $edit=url("config-id/".$data->pk())."/edit";
                   $delete=url("config-id/".$data->pk());
                 $content = '';
                  $content .= "<a onclick='show_modal(\"$edit\")' class='btn btn-sm btn-icon btn-pure btn-default on-default edit-row ' data-toggle='tooltip' data-original-title='Edit'><i class='icon md-edit' aria-hidden='true'></i></a>";
                  $content .= " <a onclick='hapus(\"$delete\")' class='btn btn-sm btn-icon btn-pure btn-default on-default remove-row' data-toggle='tooltip' data-original-title='Remove'><i class='icon md-delete' aria-hidden='true'></i></a>";

                   return $content;
               })
               ->make(true);
       }
         }
