<?php
namespace App\Http\Controllers;

use App\Models\SubKategoriKasus;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Datatables;

class SubKategoriKasusController extends Controller
{
    public $viewDir = "sub_kategori_kasus";
    public $breadcrumbs = array(
         'permissions'=>array('title'=>'Sub-kategori-kasus','link'=>"#",'active'=>false,'display'=>true),
       );

       public function __construct()
       {
           $this->middleware('permission:read-sub-kategori-kasus');
       }

       public function index()
       {
           return $this->view( "index");
       }

       /**
        * Show the form for creating a new resource.
        *
        * @return  \Illuminate\Http\Response
        */
       public function create()
       {
           return $this->view("form",['subKategoriKasus' => new SubKategoriKasus]);
       }

       /**
        * Store a newly created resource in storage.
        *
        * @param    \Illuminate\Http\Request  $request
        * @return  \Illuminate\Http\Response
        */
       public function store( Request $request )
       {
           $this->validate($request, SubKategoriKasus::validationRules());
            $data = [
                'id_kategori_kasus'=>$request->input('id_kategori_kasus'),
                'sub_kategori_kasus'=>$request->input('sub_kategori_kasus'),
            ];
           $act=SubKategoriKasus::create($data);
           message($act,'Data Sub Kategori Kasus berhasil ditambahkan','Data Sub Kategori Kasus gagal ditambahkan');
           return redirect('sub-kategori-kasus');
       }

       /**
        * Display the specified resource.
        *
        * @return  \Illuminate\Http\Response
        */
       public function show(Request $request, $kode)
       {
           $subKategoriKasus=SubKategoriKasus::find($kode);
           return $this->view("show",['subKategoriKasus' => $subKategoriKasus]);
       }

       /**
        * Show the form for editing the specified resource.
        *
        * @return  \Illuminate\Http\Response
        */
       public function edit(Request $request, $kode)
       {
           $subKategoriKasus=SubKategoriKasus::find($kode);
           return $this->view( "form", ['subKategoriKasus' => $subKategoriKasus] );
       }

       /**
        * Update the specified resource in storage.
        *
        * @param    \Illuminate\Http\Request  $request
        * @return  \Illuminate\Http\Response
        */
       public function update(Request $request, $kode)
       {
           $subKategoriKasus=SubKategoriKasus::find($kode);
           if( $request->isXmlHttpRequest() )
           {
               $data = [$request->name  => $request->value];
               $validator = \Validator::make( $data, SubKategoriKasus::validationRules( $request->name ) );
               if($validator->fails())
                   return response($validator->errors()->first( $request->name),403);
                   $dataupdate[] = [
                    'id_kategori_kasus'=>$request->input('id_kategori_kasus'),
                    'sub_kategori_kasus'=>$request->input('sub_kategori_kasus'),
                ];
               $subKategoriKasus->update($dataupdate);
               return "Record updated";
           }
           $this->validate($request, SubKategoriKasus::validationRules());
           $dataupdate[] = [
            'id_kategori_kasus'=>$request->input('id_kategori_kasus'),
            'sub_kategori_kasus'=>$request->input('sub_kategori_kasus'),
        ];
    
           $act=$subKategoriKasus->update($dataupdate);
           message($act,'Data Sub Kategori Kasus berhasil diupdate','Data Sub Kategori Kasus gagal diupdate');

           return redirect('/sub-kategori-kasus');
       }

       /**
        * Remove the specified resource from storage.
        *
        * @return  \Illuminate\Http\Response
        */
       public function destroy(Request $request, $kode)
       {
           $subKategoriKasus=SubKategoriKasus::find($kode);
           $act=false;
           try {
               $act=$subKategoriKasus->forceDelete();
           } catch (\Exception $e) {
               $subKategoriKasus=SubKategoriKasus::find($subKategoriKasus->pk());
               $act=$subKategoriKasus->delete();
           }
       }

       protected function view($view, $data = [])
       {
           return view($this->viewDir.".".$view, $data);
       }
       public function loadData()
       {
           $GLOBALS['nomor']=\Request::input('start',1)+1;
           $dataList = SubKategoriKasus::select('*');
           if (request()->get('status') == 'trash') {
               $dataList->onlyTrashed();
           }
        
           return Datatables::of($dataList)
               ->addColumn('nomor',function($kategori){
                   return $GLOBALS['nomor']++;
               })
               ->addColumn('kategori_kasus',function($data){
                if(isset($data->kategori_kasus->kategori_kasus)){
                  return $data->kategori_kasus->kategori_kasus;
                }else{
                  return '-';
                }
              })
               ->addColumn('action', function ($data) {
                   $edit=url("sub-kategori-kasus/".$data->pk())."/edit";
                   $delete=url("sub-kategori-kasus/".$data->pk());
                 $content = '';
                  $content .= "<a onclick='show_modal(\"$edit\")' class='btn btn-sm btn-icon btn-pure btn-default on-default edit-row ' data-toggle='tooltip' data-original-title='Edit'><i class='icon md-edit' aria-hidden='true'></i></a>";
                  $content .= " <a onclick='hapus(\"$delete\")' class='btn btn-sm btn-icon btn-pure btn-default on-default remove-row' data-toggle='tooltip' data-original-title='Remove'><i class='icon md-delete' aria-hidden='true'></i></a>";

                   return $content;
               })
               ->make(true);
       }
         }
