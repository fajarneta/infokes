<?php
namespace App\Http\Controllers;

use App\Models\Jabatan;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Datatables;

class JabatanController extends Controller
{
    public $viewDir = "jabatan";
    public $breadcrumbs = array(
         'permissions'=>array('title'=>'Jabatan','link'=>"#",'active'=>false,'display'=>true),
       );

       public function __construct()
       {
           $this->middleware('permission:read-jabatan');
       }

       public function index()
       {
           return $this->view( "index");
       }

       /**
        * Show the form for creating a new resource.
        *
        * @return  \Illuminate\Http\Response
        */
       public function create()
       {
           return $this->view("form",['jabatan' => new Jabatan]);
       }

       /**
        * Store a newly created resource in storage.
        *
        * @param    \Illuminate\Http\Request  $request
        * @return  \Illuminate\Http\Response
        */
       public function store( Request $request )
       {
           $this->validate($request, Jabatan::validationRules());

           $act=Jabatan::create($request->all());
           message($act,'Data Jabatan berhasil ditambahkan','Data Jabatan gagal ditambahkan');
           return redirect('jabatan');
       }

       /**
        * Display the specified resource.
        *
        * @return  \Illuminate\Http\Response
        */
       public function show(Request $request, $kode)
       {
           $jabatan=Jabatan::find($kode);
           return $this->view("show",['jabatan' => $jabatan]);
       }

       /**
        * Show the form for editing the specified resource.
        *
        * @return  \Illuminate\Http\Response
        */
       public function edit(Request $request, $kode)
       {
           $jabatan=Jabatan::find($kode);
           return $this->view( "form", ['jabatan' => $jabatan] );
       }

       /**
        * Update the specified resource in storage.
        *
        * @param    \Illuminate\Http\Request  $request
        * @return  \Illuminate\Http\Response
        */
       public function update(Request $request, $kode)
       {
           $jabatan=Jabatan::find($kode);
           if( $request->isXmlHttpRequest() )
           {
               $data = [$request->name  => $request->value];
               $validator = \Validator::make( $data, Jabatan::validationRules( $request->name ) );
               if($validator->fails())
                   return response($validator->errors()->first( $request->name),403);
               $jabatan->update($data);
               return "Record updated";
           }
           $this->validate($request, Jabatan::validationRules());

           $act=$jabatan->update($request->all());
           message($act,'Data Jabatan berhasil diupdate','Data Jabatan gagal diupdate');

           return redirect('/jabatan');
       }

       /**
        * Remove the specified resource from storage.
        *
        * @return  \Illuminate\Http\Response
        */
       public function destroy(Request $request, $kode)
       {
           $jabatan=Jabatan::find($kode);
           $act=false;
           try {
               $act=$jabatan->forceDelete();
           } catch (\Exception $e) {
               $jabatan=Jabatan::find($jabatan->pk());
               $act=$jabatan->delete();
           }
       }

       protected function view($view, $data = [])
       {
           return view($this->viewDir.".".$view, $data);
       }
       public function loadData()
       {
           $GLOBALS['nomor']=\Request::input('start',1)+1;
           $dataList = Jabatan::select('*');
           if (request()->get('status') == 'trash') {
               $dataList->onlyTrashed();
           }
           return Datatables::of($dataList)
               ->addColumn('nomor',function($kategori){
                   return $GLOBALS['nomor']++;
               })
               ->addColumn('action', function ($data) {
                   $edit=url("jabatan/".$data->pk())."/edit";
                   $delete=url("jabatan/".$data->pk());
                 $content = '';
                  $content .= "<a onclick='show_modal(\"$edit\")' class='btn btn-sm btn-icon btn-pure btn-default on-default edit-row ' data-toggle='tooltip' data-original-title='Edit'><i class='icon md-edit' aria-hidden='true'></i></a>";
                  $content .= " <a onclick='hapus(\"$delete\")' class='btn btn-sm btn-icon btn-pure btn-default on-default remove-row' data-toggle='tooltip' data-original-title='Remove'><i class='icon md-delete' aria-hidden='true'></i></a>";

                   return $content;
               })
               ->make(true);
       }
         }
