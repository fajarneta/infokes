<?php
namespace App\Http\Controllers;

use App\Models\Map;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Datatables;

class MapController extends Controller
{
    public $viewDir = "map";
    public $breadcrumbs = array(
         'permissions'=>array('title'=>'Map','link'=>"#",'active'=>false,'display'=>true),
       );

       public function __construct()
       {
           $this->middleware('permission:read-map');
       }

       public function index()
       {
           return $this->view( "index");
       }

       /**
        * Show the form for creating a new resource.
        *
        * @return  \Illuminate\Http\Response
        */
       public function create()
       {
           return $this->view("form",['map' => new Map]);
       }

       /**
        * Store a newly created resource in storage.
        *
        * @param    \Illuminate\Http\Request  $request
        * @return  \Illuminate\Http\Response
        */
       public function store( Request $request )
       {
           $this->validate($request, Map::validationRules());

           $act=Map::create($request->all());
           message($act,'Data Map berhasil ditambahkan','Data Map gagal ditambahkan');
           return redirect('map');
       }

       /**
        * Display the specified resource.
        *
        * @return  \Illuminate\Http\Response
        */
       public function show(Request $request, $kode)
       {
           $map=Map::find($kode);
           return $this->view("show",['map' => $map]);
       }

       /**
        * Show the form for editing the specified resource.
        *
        * @return  \Illuminate\Http\Response
        */
       public function edit(Request $request, $kode)
       {
           $map=Map::find($kode);
           return $this->view( "form", ['map' => $map] );
       }

       /**
        * Update the specified resource in storage.
        *
        * @param    \Illuminate\Http\Request  $request
        * @return  \Illuminate\Http\Response
        */
       public function update(Request $request, $kode)
       {
           $map=Map::find($kode);
           if( $request->isXmlHttpRequest() )
           {
               $data = [$request->name  => $request->value];
               $validator = \Validator::make( $data, Map::validationRules( $request->name ) );
               if($validator->fails())
                   return response($validator->errors()->first( $request->name),403);
               $map->update($data);
               return "Record updated";
           }
           $this->validate($request, Map::validationRules());

           $act=$map->update($request->all());
           message($act,'Data Map berhasil diupdate','Data Map gagal diupdate');

           return redirect('/map');
       }

       /**
        * Remove the specified resource from storage.
        *
        * @return  \Illuminate\Http\Response
        */
       public function destroy(Request $request, $kode)
       {
           $map=Map::find($kode);
           $act=false;
           try {
               $act=$map->forceDelete();
           } catch (\Exception $e) {
               $map=Map::find($map->pk());
               $act=$map->delete();
           }
       }

       protected function view($view, $data = [])
       {
           return view($this->viewDir.".".$view, $data);
       }
       public function loadData()
       {
           $GLOBALS['nomor']=\Request::input('start',1)+1;
           $dataList = Map::select('*');
           if (request()->get('status') == 'trash') {
               $dataList->onlyTrashed();
           }
           return Datatables::of($dataList)
               ->addColumn('nomor',function($kategori){
                   return $GLOBALS['nomor']++;
               })
               ->addColumn('kelurahan',function($data){
                if(isset($data->kelurahan->kelurahan)){
                    return $data->kelurahan->kelurahan;
                  }else{
                    return '-';
                  }
            })
               ->addColumn('action', function ($data) {
                   $edit=url("map/".$data->pk())."/edit";
                   $delete=url("map/".$data->pk());
                 $content = '';
                  $content .= "<a onclick='show_modal(\"$edit\")' class='btn btn-sm btn-icon btn-pure btn-default on-default edit-row ' data-toggle='tooltip' data-original-title='Edit'><i class='icon md-edit' aria-hidden='true'></i></a>";
                  $content .= " <a onclick='hapus(\"$delete\")' class='btn btn-sm btn-icon btn-pure btn-default on-default remove-row' data-toggle='tooltip' data-original-title='Remove'><i class='icon md-delete' aria-hidden='true'></i></a>";

                   return $content;
               })
               ->make(true);
       }
         }
