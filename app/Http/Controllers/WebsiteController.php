<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class WebsiteController extends Controller
{
    public $viewDir = "website";
    //

    public function index()
    {
      return $this->view( "index");
    }

    public function petaPersebaran()
    {
      return $this->view( "peta-persebaran");
    }
    public function infoCorona()
    {
  
      $kasus = \App\Models\RekamKasus::select('*')->get();
      $petakelurahan = \App\Models\Map::select(\DB::raw('kelurahan.kelurahan as DESA,map.id_kelurahan as IDDESANO, IFNULL(rekam.jumlah,0) as jumlah'))
      ->join('kelurahan','kelurahan.id','=','map.id_kelurahan')
      ->leftjoin(
        \DB::raw('
          (select IFNULL(count(rekam_kasus.id),0) as jumlah , penduduk.id_kelurahan_ktp from rekam_kasus join penduduk on penduduk.id = rekam_kasus.id_penduduk group by penduduk.id_kelurahan_ktp) as rekam
        ')
      ,'rekam.id_kelurahan_ktp','=','kelurahan.id')
      ->groupby('kelurahan.kelurahan')
      ->groupby('map.id_kelurahan')
      ->groupby('rekam.jumlah')
      ->get();
      $coordinates = [];
      $kategorivalues = [];
      $kategorivalues= [
        'ODP','PDP'
      ];
      foreach ($petakelurahan as $keyz => $value_nilai) {
        
        $kelurahan = $value_nilai['DESA'];
        $datamap = \App\Models\Map::where('id_kelurahan',$value_nilai['IDDESANO'])->limit(2)->get();
        foreach ($datamap as $key => $row) {
          
          $coordinates[] = [$row['longitude'],$row['latitude']];
        }
        
        
        $features[] = (object)[
          "type" => "Feature",
          "properties" => [
           "DESA"=> $value_nilai['DESA'],
           "value" => [
           1 =>[
             'jumlah' => 10
           ],
           2 =>[
            'jumlah' => 10
          ]
           ]
          ],
          "geometry" => (object)[
            "type" => "MultiPolygon",
            "coordinates" => [[$coordinates]]
            ]
          ];
        }
        
        //set the format json here
        $FeatureCollection = (object)[
          "type" => "FeatureCollection",
          "features" => $features
        ];
        // dd($FeatureCollection);
      $kategori_kasus = \App\Models\KategoriKasus::all();
      $data['test']=100;
      $data['kategori_kasus']=$kategori_kasus;
      $data['peta']=$petakelurahan;
      $data['datamap']=$datamap;
      return $this->view( "info-corona",$data);
    }


    protected function view($view, $data = [])
    {
      return view($this->viewDir.".".$view, $data);
    }
}
