<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Penduduk;
use App\Models\Provinsi;
use App\Models\Kabupaten;
use App\Models\Kecamatan;
use App\Models\Kelurahan;

use App\Http\Requests;
use App\Models\User;
use App\Models\RoleUser;
use App\Models\GolonganDarah;
use App\Models\Jabatan;
use App\Models\UnitKerja;
use App\Models\KategoriKasus;
use App\Models\RekamKasus;

use App\Http\Controllers\Controller;
use Datatables;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Datetime;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
date_default_timezone_set("Asia/Jakarta");
use Response;
use App\Traits\ActivityTraits;
class LaporanController extends Controller
{
    public $viewDir = "laporan";
    public $breadcrumbs = array(
         'permissions'=>array('title'=>'Laporan','link'=>"#",'active'=>false,'display'=>true),
       );
       public function __construct()
       {
        //    $this->middleware('permission:read-laporan');
       }

       public function index()
       {
           return $this->view( "index");
       }
       public function indexLaporanRekamKasus()
       {
           $data['kategori_kasus']=KategoriKasus::all();
           return $this->view( "index",$data);
       }
       public function cetakLaporanRekamKasus($date_from,$date_to,$kategori_kasus)
       {
        $datarekam =RekamKasus::select(\DB::raw('penduduk.nama_lengkap as nama_penduduk,penduduk.alamat_ktp,penduduk.nik,rekam_kasus.tanggal'))
        ->join('sub_kategori_kasus','sub_kategori_kasus.id','=','rekam_kasus.id_sub_kategori_kasus')
        ->join('kategori_kasus','kategori_kasus.id','=','sub_kategori_kasus.id_kategori_kasus')
        ->join('penduduk','penduduk.id','=','rekam_kasus.id_penduduk')
        ->where(function($q) use ($date_from,$date_to,$kategori_kasus){
            if(!empty($date_from)){
                $q->where('tanggal', '>=', $date_from);
            }
            if(!empty($date_to)){
                $q->where('tanggal', '<=', $date_to);
            }
            if(!empty($kategori_kasus)){
                $q->where('kategori_kasus.kategori_kasus', 'like', '%'.$kategori_kasus.'%');
            }
       
          })
          ->orderby('rekam_kasus.id','asc')
          ->get();
          $data['rekamkasus']=$datarekam;
          $data['status']=$kategori_kasus;
          return $this->view( "cetak-laporan",$data);
       }

       protected function view($view, $data = [])
       {
           return view($this->viewDir.".".$view, $data);
       }

       public function loadDataLaporanRekamKasus()
       {
           $GLOBALS['nomor']=\Request::input('start',1)+1;
           $date_from_awal = \Request::input('date_from');
           $date_to_awal = \Request::input('date_to');
           $kategori_kasus = \Request::input('kategori_kasus');
           if(!empty($date_from_awal)){
         
            $date_from = Carbon::createFromFormat('d-m-Y', $date_from_awal);  
            $date_from = $date_from->format('Y-m-d');
        }
        if(!empty($date_to_awal)){
          
            $date_to = Carbon::createFromFormat('d-m-Y', $date_to_awal);
            $date_to = $date_to->format('Y-m-d');
        }
           $dataList = RekamKasus::select(\DB::raw('IFNULL(count(rekam_kasus.id),0) as jumlah'))
           ->join('sub_kategori_kasus','sub_kategori_kasus.id','=','rekam_kasus.id_sub_kategori_kasus')
           ->join('kategori_kasus','kategori_kasus.id','=','sub_kategori_kasus.id_kategori_kasus')
           ->where(function($q) use ($date_from,$date_to,$kategori_kasus){
            if(!empty($date_from)){
                $q->where('tanggal', '>=', $date_from);
            }
            if(!empty($date_to)){
                $q->where('tanggal', '<=', $date_to);
            }
            if(!empty($kategori_kasus)){
                $q->where('kategori_kasus.kategori_kasus', 'like', '%'.$kategori_kasus.'%');
            }
       
          })
          ->groupby('kategori_kasus.id');
           if (request()->get('status') == 'trash') {
               $dataList->onlyTrashed();
           }
           return Datatables::of($dataList)
               ->addColumn('nomor',function($kategori){
                   return $GLOBALS['nomor']++;
               })
               ->addColumn('kategori_kasus',function($data) use( $kategori_kasus){
        
                return  $kategori_kasus;
           
            })
            ->addColumn('jumlah',function($data) {
        
                return  $data->jumlah.' Kasus';
           
            })
              ->addColumn('tanggal',function($data) use( $date_from_awal,$date_to_awal){
        
                  return  $date_from_awal.' - '.$date_to_awal;
             
              })
               ->addColumn('action', function ($data) use( $date_from,$date_to,$kategori_kasus) {
                
                   $delete=url("rekam-kasus/".$data->pk());
                 $content = '';
                 $content .= "<center><a href='".url('laporan-rekam-kasus/cetak/'.$date_from.'/'.$date_to.'/'.$kategori_kasus.'')."'><span  class='btn btn-lg btn-info'><span style='color:white;'><i class='icon md-print' ></i>Cetak Laporan</a></span> </center>";



                   return $content;
               })
               ->make(true);
       }
}
