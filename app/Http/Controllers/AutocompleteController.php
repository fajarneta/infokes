<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Response;
use App\Models;

class AutocompleteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function search($method,Request $r)
    {
        return $this->$method($r);
    }

    //autocomplete goes here
  	 private function permission($r){
        $q=$r->input("q");
        $query=Models\Permission::select(\DB::raw("permissions.*"))
        ->where("name","like","%$q%")
        ->limit(20);
        $results=$query->get();
        return \Response::json($results->toArray());
      }
	     private function parent($r){
          $q=$r->input("q");
          $query=Models\Menu::select(\DB::raw("menus.*"))
          ->where("name","like","%$q%")
          ->limit(20);
          $results=$query->get();
          return \Response::json($results->toArray());
        }
    private function role($r){
      $q=strtoupper($r->input("q"));
      $query=\App\Role::select(\DB::raw("*"))
      ->where("name","like","%$q%")
      ->limit(20);
      $results=$query->get();
      return \Response::json($results->toArray());
    }
    private function provinsi($r)
    {
        $q=strtoupper($r->input("q"));
        $query= Models\Provinsi::select(\DB::raw("*"))
      ->where("provinsi", "like", "%$q%")
      ->limit(20);
        $results=$query->get();
        return \Response::json($results->toArray());
    }

    private function kabupaten($r)
    {
        $q=strtoupper($r->input("q"));
        $id=$r->input("id");
        if (!empty($id)) {
            $query= Models\Kabupaten::select(\DB::raw("*"))
        ->where("kabupaten", "like", "%$q%")
        ->where("id_provinsi", $id)
        ->limit(20);
            $results=$query->get();
        } else {
            $query= Models\Kabupaten::select(\DB::raw("*"))
        ->where("kabupaten", "like", "%$q%")
       
        ->limit(20);
            $results=$query->get();
        }
     
        return \Response::json($results->toArray());
    }

    private function kecamatan($r)
    {
        $q=strtoupper($r->input("q"));
        $id=$r->input("id");
        // dd($id);
        if (!empty($id)) {
            $query= Models\Kecamatan::select(\DB::raw("*"))
        ->where("kecamatan", "like", "%$q%")
        ->where("id_kabupaten", $id)
        ->limit(20);
            $results=$query->get();
        } else {
            $query= Models\Kecamatan::select(\DB::raw("*"))
        ->where("kecamatan", "like", "%$q%")
      
        ->limit(20);
            $results=$query->get();
        }
     
        return \Response::json($results->toArray());
    }

    private function kelurahan($r)
    {
        $q=strtoupper($r->input("q"));
        $id=$r->input("id");
        $query= Models\Kelurahan::select(\DB::raw("*"))
      ->where("kelurahan", "like", "%$q%")
      ->where("id_kecamatan", $id)
      ->limit(20);
        $results=$query->get();
        return \Response::json($results->toArray());
    }

  
    private function agama($r)
    {
        $q=strtoupper($r->input("q"));
        $query= Models\Agama::select(\DB::raw("*"))
      ->where("agama", "like", "%$q%")
      ->limit(20);
        $results=$query->get();
        return \Response::json($results->toArray());
    }

    private function kategori_kasus($r)
    {
        $q=strtoupper($r->input("q"));
        $query= Models\KategoriKasus::select(\DB::raw("*"))
      ->where("kategori_kasus", "like", "%$q%")
      ->limit(20);
        $results=$query->get();
        return \Response::json($results->toArray());
    }




}
