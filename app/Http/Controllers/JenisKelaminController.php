<?php
namespace App\Http\Controllers;

use App\Models\JenisKelamin;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Datatables;

class JenisKelaminController extends Controller
{
    public $viewDir = "jenis_kelamin";
    public $breadcrumbs = array(
         'permissions'=>array('title'=>'Jenis-kelamin','link'=>"#",'active'=>false,'display'=>true),
       );

       public function __construct()
       {
           $this->middleware('permission:read-jenis-kelamin');
       }

       public function index()
       {
           return $this->view( "index");
       }

       /**
        * Show the form for creating a new resource.
        *
        * @return  \Illuminate\Http\Response
        */
       public function create()
       {
           return $this->view("form",['jenisKelamin' => new JenisKelamin]);
       }

       /**
        * Store a newly created resource in storage.
        *
        * @param    \Illuminate\Http\Request  $request
        * @return  \Illuminate\Http\Response
        */
       public function store( Request $request )
       {
           $this->validate($request, JenisKelamin::validationRules());

           $act=JenisKelamin::create($request->all());
           message($act,'Data Jenis Kelamin berhasil ditambahkan','Data Jenis Kelamin gagal ditambahkan');
           return redirect('jenis-kelamin');
       }

       /**
        * Display the specified resource.
        *
        * @return  \Illuminate\Http\Response
        */
       public function show(Request $request, $kode)
       {
           $jenisKelamin=JenisKelamin::find($kode);
           return $this->view("show",['jenisKelamin' => $jenisKelamin]);
       }

       /**
        * Show the form for editing the specified resource.
        *
        * @return  \Illuminate\Http\Response
        */
       public function edit(Request $request, $kode)
       {
           $jenisKelamin=JenisKelamin::find($kode);
           return $this->view( "form", ['jenisKelamin' => $jenisKelamin] );
       }

       /**
        * Update the specified resource in storage.
        *
        * @param    \Illuminate\Http\Request  $request
        * @return  \Illuminate\Http\Response
        */
       public function update(Request $request, $kode)
       {
           $jenisKelamin=JenisKelamin::find($kode);
           if( $request->isXmlHttpRequest() )
           {
               $data = [$request->name  => $request->value];
               $validator = \Validator::make( $data, JenisKelamin::validationRules( $request->name ) );
               if($validator->fails())
                   return response($validator->errors()->first( $request->name),403);
               $jenisKelamin->update($data);
               return "Record updated";
           }
           $this->validate($request, JenisKelamin::validationRules());

           $act=$jenisKelamin->update($request->all());
           message($act,'Data Jenis Kelamin berhasil diupdate','Data Jenis Kelamin gagal diupdate');

           return redirect('/jenis-kelamin');
       }

       /**
        * Remove the specified resource from storage.
        *
        * @return  \Illuminate\Http\Response
        */
       public function destroy(Request $request, $kode)
       {
           $jenisKelamin=JenisKelamin::find($kode);
           $act=false;
           try {
               $act=$jenisKelamin->forceDelete();
           } catch (\Exception $e) {
               $jenisKelamin=JenisKelamin::find($jenisKelamin->pk());
               $act=$jenisKelamin->delete();
           }
       }

       protected function view($view, $data = [])
       {
           return view($this->viewDir.".".$view, $data);
       }
       public function loadData()
       {
           $GLOBALS['nomor']=\Request::input('start',1)+1;
           $dataList = JenisKelamin::select('*');
           if (request()->get('status') == 'trash') {
               $dataList->onlyTrashed();
           }
           return Datatables::of($dataList)
               ->addColumn('nomor',function($kategori){
                   return $GLOBALS['nomor']++;
               })
               ->addColumn('action', function ($data) {
                   $edit=url("jenis-kelamin/".$data->pk())."/edit";
                   $delete=url("jenis-kelamin/".$data->pk());
                 $content = '';
                  $content .= "<a onclick='show_modal(\"$edit\")' class='btn btn-sm btn-icon btn-pure btn-default on-default edit-row ' data-toggle='tooltip' data-original-title='Edit'><i class='icon md-edit' aria-hidden='true'></i></a>";
                  $content .= " <a onclick='hapus(\"$delete\")' class='btn btn-sm btn-icon btn-pure btn-default on-default remove-row' data-toggle='tooltip' data-original-title='Remove'><i class='icon md-delete' aria-hidden='true'></i></a>";

                   return $content;
               })
               ->make(true);
       }
         }
