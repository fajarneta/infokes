<?php
namespace App\Http\Controllers;

use App\Models\KategoriNews;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Datatables;

class KategoriNewsController extends Controller
{
    public $viewDir = "kategori_news";
    public $breadcrumbs = array(
         'permissions'=>array('title'=>'Kategori-news','link'=>"#",'active'=>false,'display'=>true),
       );

       public function __construct()
       {
           $this->middleware('permission:read-kategori-news');
       }

       public function index()
       {
           return $this->view( "index");
       }

       /**
        * Show the form for creating a new resource.
        *
        * @return  \Illuminate\Http\Response
        */
       public function create()
       {
           return $this->view("form",['kategoriNews' => new KategoriNews]);
       }

       /**
        * Store a newly created resource in storage.
        *
        * @param    \Illuminate\Http\Request  $request
        * @return  \Illuminate\Http\Response
        */
       public function store( Request $request )
       {
           $this->validate($request, KategoriNews::validationRules());

           $act=KategoriNews::create($request->all());
           message($act,'Data Kategori News berhasil ditambahkan','Data Kategori News gagal ditambahkan');
           return redirect('kategori-news');
       }

       /**
        * Display the specified resource.
        *
        * @return  \Illuminate\Http\Response
        */
       public function show(Request $request, $kode)
       {
           $kategoriNews=KategoriNews::find($kode);
           return $this->view("show",['kategoriNews' => $kategoriNews]);
       }

       /**
        * Show the form for editing the specified resource.
        *
        * @return  \Illuminate\Http\Response
        */
       public function edit(Request $request, $kode)
       {
           $kategoriNews=KategoriNews::find($kode);
           return $this->view( "form", ['kategoriNews' => $kategoriNews] );
       }

       /**
        * Update the specified resource in storage.
        *
        * @param    \Illuminate\Http\Request  $request
        * @return  \Illuminate\Http\Response
        */
       public function update(Request $request, $kode)
       {
           $kategoriNews=KategoriNews::find($kode);
           if( $request->isXmlHttpRequest() )
           {
               $data = [$request->name  => $request->value];
               $validator = \Validator::make( $data, KategoriNews::validationRules( $request->name ) );
               if($validator->fails())
                   return response($validator->errors()->first( $request->name),403);
               $kategoriNews->update($data);
               return "Record updated";
           }
           $this->validate($request, KategoriNews::validationRules());

           $act=$kategoriNews->update($request->all());
           message($act,'Data Kategori News berhasil diupdate','Data Kategori News gagal diupdate');

           return redirect('/kategori-news');
       }

       /**
        * Remove the specified resource from storage.
        *
        * @return  \Illuminate\Http\Response
        */
       public function destroy(Request $request, $kode)
       {
           $kategoriNews=KategoriNews::find($kode);
           $act=false;
           try {
               $act=$kategoriNews->forceDelete();
           } catch (\Exception $e) {
               $kategoriNews=KategoriNews::find($kategoriNews->pk());
               $act=$kategoriNews->delete();
           }
       }

       protected function view($view, $data = [])
       {
           return view($this->viewDir.".".$view, $data);
       }
       public function loadData()
       {
           $GLOBALS['nomor']=\Request::input('start',1)+1;
           $dataList = KategoriNews::select('*');
           if (request()->get('status') == 'trash') {
               $dataList->onlyTrashed();
           }
           return Datatables::of($dataList)
               ->addColumn('nomor',function($kategori){
                   return $GLOBALS['nomor']++;
               })
               ->addColumn('action', function ($data) {
                   $edit=url("kategori-news/".$data->pk())."/edit";
                   $delete=url("kategori-news/".$data->pk());
                 $content = '';
                  $content .= "<a onclick='show_modal(\"$edit\")' class='btn btn-sm btn-icon btn-pure btn-default on-default edit-row ' data-toggle='tooltip' data-original-title='Edit'><i class='icon md-edit' aria-hidden='true'></i></a>";
                  $content .= " <a onclick='hapus(\"$delete\")' class='btn btn-sm btn-icon btn-pure btn-default on-default remove-row' data-toggle='tooltip' data-original-title='Remove'><i class='icon md-delete' aria-hidden='true'></i></a>";

                   return $content;
               })
               ->make(true);
       }
         }
