<?php
namespace App\Http\Controllers;

use App\Models\Agama;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Datatables;
use Carbon\Carbon;

class AgamaController extends Controller
{
    public $viewDir = "agama";
    public $breadcrumbs = array(
         'permissions'=>array('title'=>'Agama','link'=>"#",'active'=>false,'display'=>true),
       );

       public function __construct()
       {
           $this->middleware('auth');
           $this->middleware('permission:read-agama');
       }

       public function index()
       {
           return $this->view( "index");
       }

       /**
        * Show the form for creating a new resource.
        *
        * @return  \Illuminate\Http\Response
        */
       public function create()
       {
           return $this->view("form",['agama' => new Agama]);
       }

       /**
        * Store a newly created resource in storage.
        *
        * @param    \Illuminate\Http\Request  $request
        * @return  \Illuminate\Http\Response
        */
       public function store( Request $request )
       {
           $this->validate($request, Agama::validationRules());

           $act=Agama::create($request->all());
           message($act,'Data Agama berhasil ditambahkan','Data Agama gagal ditambahkan');
           return redirect('agama');
       }

       /**
        * Display the specified resource.
        *
        * @return  \Illuminate\Http\Response
        */
       public function show(Request $request, $kode)
       {
           $agama=Agama::find($kode);
           return $this->view("show",['agama' => $agama]);
       }

       /**
        * Show the form for editing the specified resource.
        *
        * @return  \Illuminate\Http\Response
        */
       public function edit(Request $request, $kode)
       {
           $agama=Agama::find($kode);
           return $this->view( "form", ['agama' => $agama] );
       }

       /**
        * Update the specified resource in storage.
        *
        * @param    \Illuminate\Http\Request  $request
        * @return  \Illuminate\Http\Response
        */
       public function update(Request $request, $kode)
       {
           $agama=Agama::find($kode);
           if( $request->isXmlHttpRequest() )
           {
               $data = [$request->name  => $request->value];
               $validator = \Validator::make( $data, Agama::validationRules( $request->name ) );
               if($validator->fails())
                   return response($validator->errors()->first( $request->name),403);
               $agama->update($data);
               return "Record updated";
           }
           $this->validate($request, Agama::validationRules());

           $act=$agama->update($request->all());
           message($act,'Data Agama berhasil diupdate','Data Agama gagal diupdate');

           return redirect('/agama');
       }

       /**
        * Remove the specified resource from storage.
        *
        * @return  \Illuminate\Http\Response
        */
       public function destroy(Request $request, $kode)
       {
           $agama=Agama::find($kode);
           $act=false;
           try {
               $act=$agama->forceDelete();
           } catch (\Exception $e) {
               $agama=Agama::find($agama->pk());
               $act=$agama->delete();
           }
       }

       protected function view($view, $data = [])
       {
           return view($this->viewDir.".".$view, $data);
       }
       public function loadData()
       {
           $GLOBALS['nomor']=\Request::input('start',1)+1;
           $dataList = Agama::select('*');
           if (request()->get('status') == 'trash') {
               $dataList->onlyTrashed();
           }
           return Datatables::of($dataList)
               ->addColumn('nomor',function($kategori){
                   return $GLOBALS['nomor']++;
               })
                  ->addColumn('created_at',function($data){
          if(isset($data->created_at)){
            $tanggal=Carbon::createFromFormat('Y-m-d H:i:s', $data->created_at);
        // dd($tanggal->format('d-m-Y'));
            return $tanggal->format('d-m-Y');
                //   return $data->tanggal_lahir;
          }else{
            return null;
          }
        })
               ->addColumn('action', function ($data) {
                   $edit=url("agama/".$data->pk())."/edit";
                   $delete=url("agama/".$data->pk());
                 $content = '';
                  $content .= "<a onclick='show_modal(\"$edit\")' class='btn btn-sm btn-icon btn-pure btn-default on-default edit-row ' data-toggle='tooltip' data-original-title='Edit'><i class='icon md-edit' aria-hidden='true'></i></a>";
                  $content .= " <a onclick='hapus(\"$delete\")' class='btn btn-sm btn-icon btn-pure btn-default on-default remove-row' data-toggle='tooltip' data-original-title='Remove'><i class='icon md-delete' aria-hidden='true'></i></a>";

                   return $content;
               })
               ->make(true);
       }
      

         }
