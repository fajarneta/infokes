<?php
namespace App\Http\Controllers;

use App\Models\GolonganDarah;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Datatables;

class GolonganDarahController extends Controller
{
    public $viewDir = "golongan_darah";
    public $breadcrumbs = array(
         'permissions'=>array('title'=>'Golongan-darah','link'=>"#",'active'=>false,'display'=>true),
       );

       public function __construct()
       {
           $this->middleware('permission:read-golongan-darah');
       }

       public function index()
       {
           return $this->view( "index");
       }

       /**
        * Show the form for creating a new resource.
        *
        * @return  \Illuminate\Http\Response
        */
       public function create()
       {
           return $this->view("form",['golonganDarah' => new GolonganDarah]);
       }

       /**
        * Store a newly created resource in storage.
        *
        * @param    \Illuminate\Http\Request  $request
        * @return  \Illuminate\Http\Response
        */
       public function store( Request $request )
       {
           $this->validate($request, GolonganDarah::validationRules());

           $act=GolonganDarah::create($request->all());
           message($act,'Data Golongan Darah berhasil ditambahkan','Data Golongan Darah gagal ditambahkan');
           return redirect('golongan-darah');
       }

       /**
        * Display the specified resource.
        *
        * @return  \Illuminate\Http\Response
        */
       public function show(Request $request, $kode)
       {
           $golonganDarah=GolonganDarah::find($kode);
           return $this->view("show",['golonganDarah' => $golonganDarah]);
       }

       /**
        * Show the form for editing the specified resource.
        *
        * @return  \Illuminate\Http\Response
        */
       public function edit(Request $request, $kode)
       {
           $golonganDarah=GolonganDarah::find($kode);
           return $this->view( "form", ['golonganDarah' => $golonganDarah] );
       }

       /**
        * Update the specified resource in storage.
        *
        * @param    \Illuminate\Http\Request  $request
        * @return  \Illuminate\Http\Response
        */
       public function update(Request $request, $kode)
       {
           $golonganDarah=GolonganDarah::find($kode);
           if( $request->isXmlHttpRequest() )
           {
               $data = [$request->name  => $request->value];
               $validator = \Validator::make( $data, GolonganDarah::validationRules( $request->name ) );
               if($validator->fails())
                   return response($validator->errors()->first( $request->name),403);
               $golonganDarah->update($data);
               return "Record updated";
           }
           $this->validate($request, GolonganDarah::validationRules());

           $act=$golonganDarah->update($request->all());
           message($act,'Data Golongan Darah berhasil diupdate','Data Golongan Darah gagal diupdate');

           return redirect('/golongan-darah');
       }

       /**
        * Remove the specified resource from storage.
        *
        * @return  \Illuminate\Http\Response
        */
       public function destroy(Request $request, $kode)
       {
           $golonganDarah=GolonganDarah::find($kode);
           $act=false;
           try {
               $act=$golonganDarah->forceDelete();
           } catch (\Exception $e) {
               $golonganDarah=GolonganDarah::find($golonganDarah->pk());
               $act=$golonganDarah->delete();
           }
       }

       protected function view($view, $data = [])
       {
           return view($this->viewDir.".".$view, $data);
       }
       public function loadData()
       {
           $GLOBALS['nomor']=\Request::input('start',1)+1;
           $dataList = GolonganDarah::select('*');
           if (request()->get('status') == 'trash') {
               $dataList->onlyTrashed();
           }
           return Datatables::of($dataList)
               ->addColumn('nomor',function($kategori){
                   return $GLOBALS['nomor']++;
               })
               ->addColumn('action', function ($data) {
                   $edit=url("golongan-darah/".$data->pk())."/edit";
                   $delete=url("golongan-darah/".$data->pk());
                 $content = '';
                  $content .= "<a onclick='show_modal(\"$edit\")' class='btn btn-sm btn-icon btn-pure btn-default on-default edit-row ' data-toggle='tooltip' data-original-title='Edit'><i class='icon md-edit' aria-hidden='true'></i></a>";
                  $content .= " <a onclick='hapus(\"$delete\")' class='btn btn-sm btn-icon btn-pure btn-default on-default remove-row' data-toggle='tooltip' data-original-title='Remove'><i class='icon md-delete' aria-hidden='true'></i></a>";

                   return $content;
               })
               ->make(true);
       }
         }
