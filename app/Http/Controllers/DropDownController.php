<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Response;
use App\Models;
use DB;

class DropDownController extends Controller
{
    function index ($method,Request $r){
      return $this->$method($r);
    }

    private function provinsi(){
      $results= Models\Provinsi::get();
      return \Response::json($results->toArray());
    }
    private function kabupaten($r){
      $id=$r->input("id");
      if (!empty($id)) {
        $results= Models\Kabupaten::where("id_provinsi", $id)->get();
      }else {
        $results= Models\Kabupaten::get();
      }
      // return \Response::json($results->toArray());
      $data=array('query'=>$results);
      echo json_encode($data);
    }
    private function kecamatan($r){
      $id=$r->input("id");
      if (!empty($id)) {
        $results= Models\Kecamatan::where("id_kabupaten", $id)->get();
      }else {
        $results= Models\Kecamatan::get();
      }
      // return \Response::json($results->toArray());
      $data=array('query'=>$results);
      echo json_encode($data);
    }
    private function kelurahan($r){
      $id=$r->input("id");
      if (!empty($id)) {
        $results= Models\Kelurahan::where("id_kecamatan", $id)->get();
      }else {
        $results= Models\Kelurahan::get();
      }
      // return \Response::json($results->toArray());
      $data=array('query'=>$results);
      echo json_encode($data);
    }
    private function agama(){
      $results= Models\Agama::get();
      return \Response::json($results->toArray());
    }
    private function golonganDarah(){
        $results= Models\GolonganDarah::get();
        return \Response::json($results->toArray());
    }
    private function statusPernikahan(){
        $results= Models\StatusPernikahan::get();
        return \Response::json($results->toArray());
    }
    private function pekerjaan(){
        $results= Models\Profesi::get();
        return \Response::json($results->toArray());
    }
    private function golonganMasyarakat(){
        $results= Models\GolonganMasyarakat::get();
        return \Response::json($results->toArray());
    }
    private function paketBantuan(){
        $results= Models\PaketBantuan::get();
        return \Response::json($results->toArray());
    }
    private function jenisBantuan(){
        $results= Models\JenisBantuan::get();
        return \Response::json($results->toArray());
    }
    private function jenisKelamin(){
        $results= Models\JenisKelamin::get();
        return \Response::json($results->toArray());
    }

    private function kategoriKasus(){
      $results= Models\KategoriKasus::get();
      return \Response::json($results->toArray());
  }
  private function SubKategoriKasus($r){
    $id=$r->input("id");
    if (!empty($id)) {
      $results= Models\SubKategoriKasus::where("id_kategori_kasus", $id)->get();
    }else {
      $results= Models\SubKategoriKasus::get();
    }
    // return \Response::json($results->toArray());
    $data=array('query'=>$results);
    echo json_encode($data);
  }
}
