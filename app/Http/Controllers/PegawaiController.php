<?php
namespace App\Http\Controllers;

use App\Models\Pegawai;
use Illuminate\Http\Request;

use App\Models\Penduduk;
use App\Models\Provinsi;
use App\Models\Kabupaten;
use App\Models\Kecamatan;
use App\Models\Kelurahan;

use App\Http\Requests;
use App\Models\User;
use App\Models\RoleUser;
use App\Models\GolonganDarah;
use App\Models\Jabatan;
use App\Models\UnitKerja;

use App\Http\Controllers\Controller;
use Datatables;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Datetime;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
date_default_timezone_set("Asia/Jakarta");
use Response;
use App\Traits\ActivityTraits;

class PegawaiController extends Controller
{
    public $viewDir = "pegawai";
    public $breadcrumbs = array(
         'permissions'=>array('title'=>'Pegawai','link'=>"#",'active'=>false,'display'=>true),
       );

       public function __construct()
       {
           $this->middleware('permission:read-pegawai');
       }

       public function index()
       {
           return $this->view( "index");
       }

       /**
        * Show the form for creating a new resource.
        *
        * @return  \Illuminate\Http\Response
        */
       public function create()
       {
           return $this->view("form",['pegawai' => new Pegawai]);
       }
       public function lihatPegawai($kode)
       {
        $pegawai=Pegawai::find($kode);  
      
        return $this->view("form-detail",compact('pegawai'));
       }

       public function editPegawai($kode)
       {
        $pegawai=Pegawai::join('penduduk','penduduk.id','=','pegawai.id_penduduk')
        ->leftjoin('jabatan','jabatan.id','=','pegawai.id_jabatan')
        ->leftjoin('status_pegawai','status_pegawai.id','=','pegawai.id_status_pegawai')
        ->leftjoin('status_pernikahan','status_pernikahan.id','=','penduduk.id_status_pernikahan')
        ->leftjoin('unit_kerja','unit_kerja.id','=','pegawai.id_unit_kerja')
        ->leftjoin('kelurahan','kelurahan.id','=','penduduk.id_kelurahan_ktp')
        ->leftjoin('kecamatan','kecamatan.id','=','kelurahan.id_kecamatan')
        ->leftjoin('kabupaten','kabupaten.id','=','kecamatan.id_kabupaten')
        ->leftjoin('provinsi','provinsi.id','=','kabupaten.id_provinsi')

        ->find($kode);
        $user=User::where('id_penduduk',$pegawai->id_penduduk)->first();
      
        $provinsi=\DB::table('provinsi')
        ->join('kabupaten','kabupaten.id_provinsi','=','provinsi.id')
        ->join('kecamatan','kecamatan.id_kabupaten','=','kabupaten.id')
        ->join('kelurahan','kelurahan.id_kecamatan','=','kecamatan.id')

        ->where('kelurahan.id',$pegawai->id_kelurahan_ktp)->first();

        $provinsi1=\DB::table('provinsi')
        ->join('kabupaten','kabupaten.id_provinsi','=','provinsi.id')
        ->join('kecamatan','kecamatan.id_kabupaten','=','kabupaten.id')
        ->join('kelurahan','kelurahan.id_kecamatan','=','kecamatan.id')
    
        ->where('kelurahan.id',$pegawai->id_kelurahan_domisili)->first();
   
        $golongan_darah=GolonganDarah::get();    
        return $this->view("form-create",compact('pegawai','provinsi','provinsi1','id_pegawai','user', 'periodegaji','roles','golongan_darah'));
       }
       public function tambahPegawai()
       {
        $pegawai=new Pegawai();  
        $golongan_darah=GolonganDarah::get();    
        return $this->view("form-create",compact('pegawai','golongan_darah'));
       }

       /**
        * Store a newly created resource in storage.
        *
        * @param    \Illuminate\Http\Request  $request
        * @return  \Illuminate\Http\Response
        */
       public function store( Request $request )
       {
         request()->validate([
          'email' => 'required|string|email|unique:users',
          'username' => 'required|string|unique:users',
          'nama_lengkap' => 'required|string',
          'nip' => 'required',
        
      ], [
          'required' => 'Isian :attribute wajib diisi',
          'string' => 'Isian :attribute harus berupa teks',
          'email' => 'Isian :attribute harus merupakan format email yang benar',
          'unique' => 'Isian :attribute sudah digunakan, silakan ganti isian anda',
  
      ]);
      
      $all_data=$request->all();
      DB::beginTransaction();
      try {
        $data_penduduk=array(
          'nama_lengkap'=>$all_data['nama_lengkap'],
          'nik'=>$all_data['nik'],
          'tempat_lahir'=>$all_data['tempat_lahir'],
          'tanggal_lahir'=>date('Y-m-d',strtotime($all_data['tanggal_lahir'])),
          'id_agama'=>$all_data['id_agama'],
          'id_jenis_kelamin'=>$all_data['id_jenis_kelamin'],
          'id_golongan_darah'=>$all_data['id_golongan_darah'],
          'id_status_pernikahan'=>$all_data['id_status_pernikahan'],
          'alamat_ktp'=>$all_data['alamat_ktp'],
          'alamat_domisili'=>$all_data['alamat_domisili'],
          'id_kelurahan_ktp'=>$all_data['id_kelurahan_ktp'],
          'id_kelurahan_domisili'=>$all_data['id_kelurahan_domisili'],
          'no_hp'=>$all_data['no_hp'],
        );
        $data_pegawai=array(
          'nip'=>$all_data['nip'],
          'id_status_pegawai'=>$all_data['status_pegawai'],
          'id_jabatan'=>$all_data['jabatan'],
          'id_unit_kerja'=>$all_data['unit_kerja'],
          'flag_aktif'=>$all_data['flag_aktif'],
          'tgl_bergabung'=>date('Y-m-d',strtotime($all_data['tgl_bergabung'])),
        
        );
        $data_user=array(
          'name'=>$all_data['nama_lengkap'],
          'username'=>$all_data['username'],
          'email'=>$all_data['email'],
          'password'=>bcrypt('password'),
          'verified'=>true,
        
        );
        
        if($all_data['id_pegawai_check']){
          $pegawai = Pegawai::find($all_data['id_pegawai_check']);
          $id_penduduk = $pegawai->id_penduduk;
          $penduduk = Penduduk::find($id_penduduk);
          $update_penduduk=$penduduk->update($data_penduduk);
          $update_pegawai=$pegawai->update($data_pegawai);
          $data_user=array(
            'name'=>$all_data['nama_lengkap'],
            'username'=>$all_data['username'],
            'email'=>$all_data['email'],
          
          
          );
          $update_user=$users->update($data_user);
        }else{

          $penduduk = Penduduk::create($data_penduduk);
          $pegawai = Pegawai::create($data_pegawai);
          $update_pegawai=$pegawai->update(
            [
              'id_penduduk'=>$penduduk->id,
            ]
            );
          $id_penduduk = $penduduk->id;
          $data_user=array(
            'name'=>$all_data['nama_lengkap'],
            'username'=>$all_data['username'],
            'email'=>$all_data['email'],
            'password'=>bcrypt('password'),
            'verified'=>true,
          
          );
          $user=User::create($data_user);
          $roleUser = RoleUser::firstOrCreate([
            'role_id'=>getconfigvalues('ROLE_PEGAWAI')[0],
            'user_id'=>$user->id,
            'user_type'=>'App\User'
          ]);
        }
       

          } catch (Exception $e) {
            $act=false;
       
           DB::rollback();
         }
         $act=true;
         DB::commit();
         message($act,'Data Pegawai berhasil ditambahkan','Data Pegawai gagal ditambahkan');
         return redirect('pegawai');
       }

       /**
        * Display the specified resource.
        *
        * @return  \Illuminate\Http\Response
        */
       public function show(Request $request, $kode)
       {
           $pegawai=Pegawai::find($kode);
           return $this->view("show",['pegawai' => $pegawai]);
       }

       /**
        * Show the form for editing the specified resource.
        *
        * @return  \Illuminate\Http\Response
        */
       public function edit(Request $request, $kode)
       {
           $pegawai=Pegawai::find($kode);
           return $this->view( "form", ['pegawai' => $pegawai] );
       }

       /**
        * Update the specified resource in storage.
        *
        * @param    \Illuminate\Http\Request  $request
        * @return  \Illuminate\Http\Response
        */
       public function update(Request $request, $kode)
       {
           $pegawai=Pegawai::find($kode);
           if( $request->isXmlHttpRequest() )
           {
               $data = [$request->name  => $request->value];
               $validator = \Validator::make( $data, Pegawai::validationRules( $request->name ) );
               if($validator->fails())
                   return response($validator->errors()->first( $request->name),403);
               $pegawai->update($data);
               return "Record updated";
           }
           $this->validate($request, Pegawai::validationRules());

           $act=$pegawai->update($request->all());
           message($act,'Data Pegawai berhasil diupdate','Data Pegawai gagal diupdate');

           return redirect('/pegawai');
       }

       /**
        * Remove the specified resource from storage.
        *
        * @return  \Illuminate\Http\Response
        */
       public function destroy(Request $request, $kode)
       {
           $pegawai=Pegawai::find($kode);
           $act=false;
           try {
               $act=$pegawai->forceDelete();
           } catch (\Exception $e) {
               $pegawai=Pegawai::find($pegawai->pk());
               $act=$pegawai->delete();
           }
       }

       protected function view($view, $data = [])
       {
           return view($this->viewDir.".".$view, $data);
       }
       public function loadData()
       {
           $GLOBALS['nomor']=\Request::input('start',1)+1;
           $dataList = Pegawai::select('*');
           if (request()->get('status') == 'trash') {
               $dataList->onlyTrashed();
           }
           return Datatables::of($dataList)
               ->addColumn('nomor',function($kategori){
                   return $GLOBALS['nomor']++;
               })
               ->addColumn('nama_lengkap',function($data){
                if(isset($data->penduduk->nama_lengkap)){
                  return $data->penduduk->nama_lengkap;
                }else{
                  return '-';
                }
              })
              ->addColumn('nik',function($data){
                if(isset($data->penduduk->nik)){
                  return $data->penduduk->nik;
                }else{
                  return '-';
                }
              })
              ->addColumn('nip',function($data){
                if(isset($data->nip)){
                  return $data->nip;
                }else{
                  return '-';
                }
              })
              ->addColumn('jabatan',function($data){
                if(isset($data->jabatan->jabatan)){
                  return $data->jabatan->jabatan;
                }else{
                  return '-';
                }
              })
              ->addColumn('unit_kerja',function($data){
                if(isset($data->unit_kerja->unit_kerja)){
                  return $data->unit_kerja->unit_kerja;
                }else{
                  return '-';
                }
              })
              ->addColumn('status_pegawai',function($data){
                if(isset($data->status_pegawai->status_pegawai)){
                  return $data->status_pegawai->status_pegawai;
                }else{
                  return '-';
                }
              })
               ->addColumn('action', function ($data) {
                $id_pegawai=$data->pk();
                $edit=url("edit-pegawai/".$data->pk())."";
           $delete=url("pegawai/".$data->pk());
           $profil=url("lihat-pegawai/".$data->pk())."";
           $content = '';
           $content.='<a class="btn btn-sm btn-icon btn-pure btn-default on-default edit-row" title="Edit" href='.$edit.'><i class="icon md-edit" aria-hidden="true"></i></a>';
      

           $content .= " <a class='btn btn-sm btn-icon btn-pure btn-default on-default remove-row' title='Lihat Profil' data-toggle='tooltip' data-original-title='' href='".$profil."'><i class='icon md-eye' aria-hidden='true'></i></a>";

           if($data->flag_aktif=='N')
           {
            $content .= " <a class='btn btn-sm btn-icon btn-pure btn-default on-default remove-row' title='Aktifkan Pegawai' data-toggle='tooltip' data-original-title='' onclick='activate(\"$id_pegawai\",\"$data->flag_aktif\")'><i class='icon md-check' aria-hidden='true'></i></a>";
           }
           else
           {
            $content .= " <a class='btn btn-sm btn-icon btn-pure btn-default on-default remove-row' title='Nonaktifkan Pegawai' data-toggle='tooltip' data-original-title='' onclick='activate(\"$id_pegawai\",\"$data->flag_aktif\")'><i class='icon md-close' aria-hidden='true'></i></a>";
           }
        
           return $content;
               })
               ->make(true);
       }
         }
