<?php
namespace App\Http\Controllers;

use App\Models\Perusahaan;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Datatables;

class PerusahaanController extends Controller
{
    public $viewDir = "perusahaan";
    public $breadcrumbs = array(
         'permissions'=>array('title'=>'Perusahaan','link'=>"#",'active'=>false,'display'=>true),
       );

       public function __construct()
       {
           $this->middleware('permission:read-perusahaan');
       }

       public function index()
       {
           return $this->view( "index");
       }

       /**
        * Show the form for creating a new resource.
        *
        * @return  \Illuminate\Http\Response
        */
       public function create()
       {
           return $this->view("form",['perusahaan' => new Perusahaan]);
       }

       /**
        * Store a newly created resource in storage.
        *
        * @param    \Illuminate\Http\Request  $request
        * @return  \Illuminate\Http\Response
        */
       public function store( Request $request )
       {
           $this->validate($request, Perusahaan::validationRules());

           $act=Perusahaan::create($request->all());
           message($act,'Data Perusahaan berhasil ditambahkan','Data Perusahaan gagal ditambahkan');
           return redirect('perusahaan');
       }

       /**
        * Display the specified resource.
        *
        * @return  \Illuminate\Http\Response
        */
       public function show(Request $request, $kode)
       {
           $perusahaan=Perusahaan::find($kode);
           return $this->view("show",['perusahaan' => $perusahaan]);
       }

       /**
        * Show the form for editing the specified resource.
        *
        * @return  \Illuminate\Http\Response
        */
       public function edit(Request $request, $kode)
       {
           $perusahaan=Perusahaan::find($kode);
           return $this->view( "form", ['perusahaan' => $perusahaan] );
       }

       /**
        * Update the specified resource in storage.
        *
        * @param    \Illuminate\Http\Request  $request
        * @return  \Illuminate\Http\Response
        */
       public function update(Request $request, $kode)
       {
           $perusahaan=Perusahaan::find($kode);
           if( $request->isXmlHttpRequest() )
           {
               $data = [$request->name  => $request->value];
               $validator = \Validator::make( $data, Perusahaan::validationRules( $request->name ) );
               if($validator->fails())
                   return response($validator->errors()->first( $request->name),403);
               $perusahaan->update($data);
               return "Record updated";
           }
           $this->validate($request, Perusahaan::validationRules());

           $act=$perusahaan->update($request->all());
           message($act,'Data Perusahaan berhasil diupdate','Data Perusahaan gagal diupdate');

           return redirect('/perusahaan');
       }

       /**
        * Remove the specified resource from storage.
        *
        * @return  \Illuminate\Http\Response
        */
       public function destroy(Request $request, $kode)
       {
           $perusahaan=Perusahaan::find($kode);
           $act=false;
           try {
               $act=$perusahaan->forceDelete();
           } catch (\Exception $e) {
               $perusahaan=Perusahaan::find($perusahaan->pk());
               $act=$perusahaan->delete();
           }
       }

       protected function view($view, $data = [])
       {
           return view($this->viewDir.".".$view, $data);
       }
       public function loadData()
       {
           $GLOBALS['nomor']=\Request::input('start',1)+1;
           $dataList = Perusahaan::select('*');
           if (request()->get('status') == 'trash') {
               $dataList->onlyTrashed();
           }
           return Datatables::of($dataList)
               ->addColumn('nomor',function($kategori){
                   return $GLOBALS['nomor']++;
               })
               ->addColumn('action', function ($data) {
                   $edit=url("perusahaan/".$data->pk())."/edit";
                   $delete=url("perusahaan/".$data->pk());
                 $content = '';
                  $content .= "<a onclick='show_modal(\"$edit\")' class='btn btn-sm btn-icon btn-pure btn-default on-default edit-row ' data-toggle='tooltip' data-original-title='Edit'><i class='icon md-edit' aria-hidden='true'></i></a>";
                  $content .= " <a onclick='hapus(\"$delete\")' class='btn btn-sm btn-icon btn-pure btn-default on-default remove-row' data-toggle='tooltip' data-original-title='Remove'><i class='icon md-delete' aria-hidden='true'></i></a>";

                   return $content;
               })
               ->make(true);
       }
         }
