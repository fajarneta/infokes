<?php
namespace App\Http\Controllers;

use App\Models\Penduduk;
use App\Models\Provinsi;
use App\Models\Kabupaten;
use App\Models\Kecamatan;
use App\Models\Kelurahan;
use App\Models\User;
use App\Models\Agama;
use App\Models\GolonganDarah;
use App\Models\Profesi;
use App\Models\JenisKelamin;
use App\Models\StatusPernikahan;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Datatables;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
date_default_timezone_set("Asia/Jakarta");

class PendudukController extends Controller
{
  public $viewDir = "Penduduk";
  public $breadcrumbs = array(
   'permissions'=>array('title'=>'Penduduk','link'=>"#",'active'=>false,'display'=>true),
 );

  public function __construct()
  {
   $this->middleware('permission:read-penduduk');
 }

 public function index()
 {
   return $this->view( "index");
 }

 public function loadProfile()
 {
  $foto=Penduduk::find(Auth::user()->id_penduduk);
  if(isset($foto->foto) && $foto->foto!==null)
  {
    $foto_user=$foto->foto;
  }
  else
  {
    $foto_user=null;
  }
  $penduduk=Penduduk::find(Auth::user()->id_penduduk);
  return $this->view( "edit-profile",compact('foto_user','penduduk'));
}

       /**
        * Show the form for creating a new resource.
        *
        * @return  \Illuminate\Http\Response
        */
       public function create()
       {
         return $this->view("form",['penduduk' => new Penduduk]);
       }

       /**
        * Store a newly created resource in storage.
        *
        * @param    \Illuminate\Http\Request  $request
        * @return  \Illuminate\Http\Response
        */
       public function store( Request $request )
       {
         $this->validate($request, Penduduk::validationRules());

         $act=Penduduk::create($request->all());
         message($act,'Data Penduduk berhasil ditambahkan','Data Penduduk gagal ditambahkan');
         return redirect('penduduk');
       }

       /**
        * Display the specified resource.
        *
        * @return  \Illuminate\Http\Response
        */
       public function show(Request $request, $kode)
       {
         $penduduk=Penduduk::find($kode);
         return $this->view("show",['penduduk' => $penduduk]);
       }

       /**
        * Show the form for editing the specified resource.
        *
        * @return  \Illuminate\Http\Response
        */
       public function edit(Request $request, $kode)
       {
        $penduduk=Penduduk::find($kode);
        return $this->view( "form-penduduk", ['penduduk' => $penduduk] );
       }

       public function createPenduduk()
       {
      
        return $this->view( "form-penduduk");
      }

      public function simpanData(Request $request)
      {
        // dd($request->all());
        $validation = Validator::make($request->all(), [
          'nama' => 'required',
          'nik' => 'required',
     
          'tempat_lahir' => 'required',
          'tanggal_lahir' => 'required',
          'agama' => 'required',
          'status_pernikahan' => 'required',
          'jenis_kelamin' => 'required',
          'golongan_darah' => 'required',
          'pekerjaan' => 'required',
          'alamat_ktp' => 'required',
          'kelurahan_ktp' => 'required',
          'alamat_domisili' => 'required',
          'kelurahan_domisili' => 'required',
          // 'no_hp' => 'required',
        ]);
    
        if (!$validation->passes()){
          $status=array(
            'status' => false,
            'msg' => $validation->errors()->all()
          );
        }
    
        $all_data=$request->all();
        DB::beginTransaction();
        try {
          $data=array(
            'nama_lengkap'=>$all_data['nama'],
            'nik'=>$all_data['nik'],
        
            'tempat_lahir'=>$all_data['tempat_lahir'],
            'tanggal_lahir'=>date('Y-m-d',strtotime($all_data['tanggal_lahir'])),
            'id_agama'=>$all_data['agama'],
            'id_jenis_kelamin'=>$all_data['jenis_kelamin'],
            'id_golongan_darah'=>$all_data['golongan_darah'],
            'id_status_pernikahan'=>$all_data['status_pernikahan'],
            'id_profesi'=>$all_data['pekerjaan'],
            'alamat_ktp'=>$all_data['alamat_ktp'],
            'alamat_domisili'=>$all_data['alamat_domisili'],
            'id_kelurahan_ktp'=>$all_data['kelurahan_ktp'],
            'id_kelurahan_domisili'=>$all_data['kelurahan_domisili']
            // 'no_hp'=>$all_data['no_hp'],
          );
          $datapenduduk=Penduduk::where('nik','like','%'.$all_data['nik'].'%')->first();
    
            if(!isset($datapenduduk)){
              $insert_to_penduduk=Penduduk::create($data);
              $id_penduduk=$insert_to_penduduk->id;
            }else{
              $insert_to_penduduk=$datapenduduk->update($data);
              $id_penduduk=$datapenduduk->id;
            }
    
    
          if($insert_to_penduduk==true)
          {
              $status=array(
                'status' => true,
                'msg' => 'Data Berhasil Dimasukkan'
              );
          }
          else
          {
             $status=array(
              'status' => false,
              'msg' => 'Data Gagal Dimasukkan'
            );
         }
    
        } catch (Exception $e) {
          // echo 'Message' .$e->getMessage();
         $status=array(
          'status' => false,
          'msg' => $e->getMessage()
        );
         DB::rollback();
       }
       DB::commit();
    
       return \Response::json($status);
     }
    
       /**
        * Update the specified resource in storage.
        *
        * @param    \Illuminate\Http\Request  $request
        * @return  \Illuminate\Http\Response
        */
       public function update(Request $request, $kode)
       {
         $penduduk=Penduduk::find($kode);
         if( $request->isXmlHttpRequest() )
         {
           $data = [$request->name  => $request->value];
           $validator = \Validator::make( $data, Penduduk::validationRules( $request->name ) );
           if($validator->fails())
             return response($validator->errors()->first( $request->name),403);
           $penduduk->update($data);
           return "Record updated";
         }
         $this->validate($request, Penduduk::validationRules());

         $act=$penduduk->update($request->all());
         message($act,'Data Penduduk berhasil diupdate','Data Penduduk gagal diupdate');

         return redirect('/penduduk');
       }

       /**
        * Remove the specified resource from storage.
        *
        * @return  \Illuminate\Http\Response
        */
       public function destroy(Request $request, $kode)
       {
         $penduduk=Penduduk::find($kode);
         $act=false;
         try {
           $act=$penduduk->forceDelete();
         } catch (\Exception $e) {
           $penduduk=Penduduk::find($penduduk->pk());
           $act=$penduduk->delete();
         }
       }

       protected function view($view, $data = [])
       {
         return view($this->viewDir.".".$view, $data);
       }
       public function loadData()
       {
         $GLOBALS['nomor']=\Request::input('start',1)+1;
         $dataList = Penduduk::select('*');
         if (request()->get('status') == 'trash') {
           $dataList->onlyTrashed();
         }
         return Datatables::of($dataList)
         ->addColumn('nomor',function($kategori){
           return $GLOBALS['nomor']++;
         })
         ->addColumn('action', function ($data) {
          $edit=url("penduduk/".$data->pk())."/edit";
          $delete=url("penduduk/".$data->pk());
          $content = '';
          $content .= "<a href='".$edit."' class='btn btn-sm btn-icon btn-pure btn-default on-default edit-row ' data-original-title='Edit'><i class='icon md-edit' aria-hidden='true'></i></a>";
          $content .= " <a onclick='hapus(\"$delete\")' class='btn btn-sm btn-icon btn-pure btn-default on-default remove-row' data-toggle='tooltip' data-original-title='Remove'><i class='icon md-delete' aria-hidden='true'></i></a>";

          return $content;
        })
         ->make(true);
       }

       public function loadAgama()
       {
        $agama=Agama::select('*')->get();
        echo json_encode($agama);
      }

      public function loadGolonganDarah()
       {
        $golongan_darah=GolonganDarah::select('*')->get();
        echo json_encode($golongan_darah);
      }

      public function loadStatus()
      {
        $status=StatusPernikahan::select('*')->whereIn('id',[1,2])->get();
        echo json_encode($status);
      }

      public function loadPekerjaan()
      {
        $pekerjaan=Profesi::select('*')->get();
        echo json_encode($pekerjaan);
      }

       public function loadJenisKelamin()
      {
        $jk=JenisKelamin::select('*')->get();
        echo json_encode($jk);
      }

      public function loadProvinsi()
      {
        $provinsi=Provinsi::select('*')->get();
        echo json_encode($provinsi);
      }

      public function getKabupaten()
      {
        $id = \Request::input('id',null);
        $query=Kabupaten::where('id_provinsi',$id)->get();
        $data=array('query'=>$query);

        echo json_encode($data);
      }

      public function getKecamatan()
      {
        $id = \Request::input('id',null);
        $query=Kecamatan::where('id_kabupaten',$id)->get();
        $data=array('query'=>$query);

        echo json_encode($data);
      }
      public function getKelurahan()
      {
        $id = \Request::input('id',null);
        $query=Kelurahan::where('id_kecamatan',$id)->get();
        $data=array('query'=>$query);

        echo json_encode($data);
      }

          public function uploadFoto(Request $request)
       {

        if ($request->isMethod('get'))
          return $this->view( "edit-profile");
        else {
          $validation = Validator::make($request->all(), [
            'select_file' => 'required|image|mimes:jpeg,png,jpg|max:2048'
          ]);
          if (!$validation->passes())
           $data=array(
            'fail' => true,
            'errors' => $validation->errors()->all()
          );
         $extension = $request->file('select_file')->getClientOriginalExtension();
         $dir = 'images/profil/';
         $filename = uniqid() . '_' . time() . '.' . $extension;
         $request->file('select_file')->move($dir, $filename);
          // return $filename;
         $user_check=User::find(Auth::user()->id);

         $foto_exists=Penduduk::find(Auth::user()->id_penduduk);

         $insert_data=array(
          'foto'=>$filename,
        );

         if(isset($user_check) && $user_check->id_penduduk!==null)
         {
          if(isset($foto_exists) && $foto_exists->foto!==null)
          {
            if(file_exists( public_path().'/images/profil/'.Auth::user()->penduduk->foto ))
            {
              $path =  public_path().'/images/profil/'.$foto_exists->foto;
              $new_dir=public_path().'/images/recycle_bin/'.$foto_exists->foto;
              $move = File::move($path, $new_dir);             
            }         
         }
         

          $insert_to_pelamar=Penduduk::where('id',$user_check->id_penduduk)->update($insert_data);
        }
        else
        {
          $insert_to_pelamar=Penduduk::create($insert_data);
          $data1=array(
            'id_penduduk'=>$insert_to_pelamar->id
          );
          $user_id=$user_check->update($data1);
        }

        if($insert_to_pelamar==true)
        {
         $data=array(
          'fail'=>false,
          'filename'=>$filename,
          'msg'=>'Upload Completed',
        );
       }
       else
       {
        $data=array(
          'fail'=>true,
          'errors'=>'Foto Profil Gagal Ditambahkan',
        );
      }

      return \Response::json($data);
    }
  }

  public function deleteFoto()
  {
    // dd('aaaa');
    $delete_foto=Penduduk::find(Auth::user()->id_penduduk);
    $path =  public_path().'/images/profil/'.$delete_foto->foto;
    $new_dir=public_path().'/images/recycle_bin/'.$delete_foto->foto;
    $move = File::move($path, $new_dir);

    $delete=$delete_foto->update(['foto'=>null]);

    if($delete==true)
    {
      $data=array(
        'status'=>true,
        'msg'=>'Foto Berhasil Dihapus'
      );
    }
    else
    {
      $data=array(
      'status'=>false,
      'errors'=>'Foto Gagal Dihapus',
    );
    }
     return \Response::json($data);
  }

  public function simpan(Request $request)
  {
    // dd($request->all());
    $user_check=User::find(Auth::user()->id);
    $validation = Validator::make($request->all(), [
      'nama' => 'required',
      'nik' => 'required',
      'tempat_lahir' => 'required',
      'tanggal_lahir' => 'required',
      'agama' => 'required',
      'status_pernikahan' => 'required',
      'jenis_kelamin' => 'required',
      'golongan_darah' => 'required',
      'pekerjaan' => 'required',
      'alamat_ktp' => 'required',
      'kelurahan_ktp' => 'required',
      'alamat_domisili' => 'required',
      'kelurahan_domisili' => 'required',
      'no_hp' => 'required',
    ]);

    if (!$validation->passes()){
      $status=array(
        'status' => false,
        'msg' => $validation->errors()->all()
      );
    }

    $all_data=$request->all();
    DB::beginTransaction();
    try {
      $data=array(
        'nama_lengkap'=>$all_data['nama'],
        'nik'=>$all_data['nik'],
        'tempat_lahir'=>$all_data['tempat_lahir'],
        'tanggal_lahir'=>date('Y-m-d',strtotime($all_data['tanggal_lahir'])),
        'id_agama'=>$all_data['agama'],
        'id_jenis_kelamin'=>$all_data['jenis_kelamin'],
        'id_golongan_darah'=>$all_data['golongan_darah'],
        'id_status_pernikahan'=>$all_data['status_pernikahan'],
        'id_profesi'=>$all_data['pekerjaan'],
        'alamat_ktp'=>$all_data['alamat_ktp'],
        'alamat_domisili'=>$all_data['alamat_domisili'],
        'id_kelurahan_ktp'=>$all_data['kelurahan_ktp'],
        'id_kelurahan_domisili'=>$all_data['kelurahan_domisili'],
        'no_hp'=>$all_data['no_hp'],
      );

      if(isset($user_check) && $user_check->id_penduduk!==null)
      {
        if($user_check->id_penduduk==null)
        {
          $get_id=Penduduk::where('nama_lengkap','like','%'.$user_check->name.'%')->first();
          $data1=array(
            'id_penduduk'=>$get_id->id,
          );
          $user_check->update($data1);
        }
        $insert_to_penduduk=Penduduk::where('id',$user_check->id_penduduk)->update($data);
        $id_penduduk=$user_check->id_penduduk;  
      }
      else
      {
        $insert_to_penduduk=Penduduk::create($data);
        $id_penduduk=$insert_to_penduduk->id; 
        $data1=array(
          'id_penduduk'=>$id_penduduk
        );
        $user_id=$user_check->update($data1);
      }

      if($insert_to_penduduk==true)
      {
          $status=array(
            'status' => true,
            'msg' => 'Data Berhasil Dimasukkan'
          );
      }
      else
      {
         $status=array(
          'status' => false,
          'msg' => 'Data Gagal Dimasukkan'
        );
     }

    } catch (Exception $e) {
      // echo 'Message' .$e->getMessage();
     $status=array(
      'status' => false,
      'msg' => $e->getMessage()
    );
     DB::rollback();
   }
   DB::commit();

   return \Response::json($status);  
 }

     }
